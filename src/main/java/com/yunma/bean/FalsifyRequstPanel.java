package com.yunma.bean;

import com.common.bean.RequestInfoBean;

import javax.swing.*;

/**
 * Created by 黄威 on 8/19/16.<br >
 */
public class FalsifyRequstPanel extends JPanel {
    private RequestInfoBean requestInfoBean;

    public RequestInfoBean getRequestInfoBean() {
        return requestInfoBean;
    }

    public void setRequestInfoBean(RequestInfoBean requestInfoBean) {
        this.requestInfoBean = requestInfoBean;
    }
}

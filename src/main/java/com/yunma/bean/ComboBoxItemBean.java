package com.yunma.bean;

/**
 * Created by 黄威 on 03/11/2016.<br >
 */
public class ComboBoxItemBean {
    private String displayName;
    private Integer index;

    public ComboBoxItemBean() {
    }

    public ComboBoxItemBean(String displayName, Integer index) {
        this.displayName = displayName;
        this.index = index;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return displayName;
    }
}

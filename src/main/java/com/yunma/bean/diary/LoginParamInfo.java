package com.yunma.bean.diary;

import java.io.Serializable;

/**
 * Created by whuanghkl on 17/5/12.
 */
public class LoginParamInfo implements Serializable {
    private String username;
    private String password;
    private String requestCookie;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRequestCookie() {
        return requestCookie;
    }

    public void setRequestCookie(String requestCookie) {
        this.requestCookie = requestCookie;
    }
}

package com.yunma.bean;

import java.io.Serializable;

/***
 * json转化 Java Bean
 * @author whuanghkl
 *
 */
public class Json2JavaBean implements Serializable {
    private String json;
    private String className;
    private String packagePath;
    private String rootPath;

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getPackagePath() {
        return packagePath;
    }

    public void setPackagePath(String packagePath) {
        this.packagePath = packagePath;
    }

    public String getRootPath() {
        return rootPath;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }
}

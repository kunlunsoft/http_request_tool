package com.yunma.bean;

/***
 * 订单的相关信息<br>:
 * orderNo,orgId
 *
 * @author huangweii
 *         2016年3月8日
 */
public class OrderSimpleInfo {
    /***
     * 订单号
     */
    private String orderNo;
    /***
     * 企业id
     */
    private String orgId;
    /***
     * 企业名称
     */
    private String orgFullName;
    private String username;
    private String password;
    private String code;
    private String uuid;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgFullName() {
        return orgFullName;
    }

    public void setOrgFullName(String orgFullName) {
        this.orgFullName = orgFullName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }


}

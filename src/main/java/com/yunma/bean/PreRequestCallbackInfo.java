package com.yunma.bean;

import com.yunma.callback.RequestCallBack;

public class PreRequestCallbackInfo {
    /***
     * 是否应该被回调
     */
    private boolean isCallback = false;
    private RequestCallBack requestCallBack;

    public boolean isCallback() {
        return isCallback;
    }

    /**
     * 是否应该被回调
     *
     * @param isCallback
     */
    public void setCallback(boolean isCallback) {
        this.isCallback = isCallback;
    }

    public RequestCallBack getRequestCallBack() {
        return requestCallBack;
    }

    public void setRequestCallBack(RequestCallBack requestCallBack) {
        this.requestCallBack = requestCallBack;
    }

}

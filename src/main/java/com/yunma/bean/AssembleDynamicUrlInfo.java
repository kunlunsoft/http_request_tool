package com.yunma.bean;

import java.io.Serializable;

/**
 * Created by whuanghkl on 17/5/26.
 */
public class AssembleDynamicUrlInfo implements Serializable {
    private String urlTemplate;
    private String placeholderName;
    private String placeholderName2;
    private String columnInResponse;
    private String columnInResponse2;
    private String api;
    private String api2;

    public String getUrlTemplate() {
        return urlTemplate;
    }

    public void setUrlTemplate(String urlTemplate) {
        this.urlTemplate = urlTemplate;
    }

    public String getPlaceholderName() {
        return placeholderName;
    }

    public void setPlaceholderName(String placeholderName) {
        this.placeholderName = placeholderName;
    }

    public String getPlaceholderName2() {
        return placeholderName2;
    }

    public void setPlaceholderName2(String placeholderName2) {
        this.placeholderName2 = placeholderName2;
    }

    public String getColumnInResponse() {
        return columnInResponse;
    }

    public void setColumnInResponse(String columnInResponse) {
        this.columnInResponse = columnInResponse;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }

    public String getColumnInResponse2() {
        return columnInResponse2;
    }

    public void setColumnInResponse2(String columnInResponse2) {
        this.columnInResponse2 = columnInResponse2;
    }

    public String getApi2() {
        return api2;
    }

    public void setApi2(String api2) {
        this.api2 = api2;
    }
}

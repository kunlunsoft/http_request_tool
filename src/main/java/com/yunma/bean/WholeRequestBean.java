package com.yunma.bean;

import com.common.bean.ModifyResponseBodyBean;
import com.common.bean.MvnInstallConfig;
import com.common.bean.QRCodeInfoBean;
import com.common.bean.RequestInfoBean;
import com.yunma.bean.diary.LoginParamInfo;

import java.io.Serializable;
import java.util.List;

public class WholeRequestBean implements Serializable {
    private static final long serialVersionUID = -5844608509377866368L;
    private List<RequestInfoBean> requestInfoBeans;
    /***
     * HTTP默认请求值
     */
    private RequestDefaultBean requestDefaultBean;
    private GlobalInfo globalInfo;
    private UnbindMobileBean unbindMobileBean;
    private ModifyResponseBodyBean modifyResponseBodyBean;
    /***
     * "maven安装本地jar"的配置
     */
    private MvnInstallConfig mvnInstallConfig;
    private QRCodeInfoBean qrCodeInfoBean;
    private Json2JavaBean json2JavaBean;
    private LoginParamInfo loginParamInfo;
    private AssembleDynamicUrlInfo assembleDynamicUrlInfo;

    public List<RequestInfoBean> getRequestInfoBeans() {
        return requestInfoBeans;
    }

    public void setRequestInfoBeans(List<RequestInfoBean> requestInfoBeans) {
        this.requestInfoBeans = requestInfoBeans;
    }

    public RequestDefaultBean getRequestDefaultBean() {
        return requestDefaultBean;
    }

    public void setRequestDefaultBean(RequestDefaultBean requestDefaultBean) {
        this.requestDefaultBean = requestDefaultBean;
    }

    public GlobalInfo getGlobalInfo() {
        return globalInfo;
    }

    public void setGlobalInfo(GlobalInfo globalInfo) {
        this.globalInfo = globalInfo;
    }

    public UnbindMobileBean getUnbindMobileBean() {
        return unbindMobileBean;
    }

    public void setUnbindMobileBean(UnbindMobileBean unbindMobileBean) {
        this.unbindMobileBean = unbindMobileBean;
    }

    public QRCodeInfoBean getQrCodeInfoBean() {
        return qrCodeInfoBean;
    }

    public void setQrCodeInfoBean(QRCodeInfoBean qrCodeInfoBean) {
        this.qrCodeInfoBean = qrCodeInfoBean;
    }

    public Json2JavaBean getJson2JavaBean() {
        return json2JavaBean;
    }

    public void setJson2JavaBean(Json2JavaBean json2JavaBean) {
        this.json2JavaBean = json2JavaBean;
    }

    /**
     * @return the mvnInstallConfig
     */
    public MvnInstallConfig getMvnInstallConfig() {
        return mvnInstallConfig;
    }

    /**
     * @param mvnInstallConfig the mvnInstallConfig to set
     */
    public void setMvnInstallConfig(MvnInstallConfig mvnInstallConfig) {
        this.mvnInstallConfig = mvnInstallConfig;
    }

    public ModifyResponseBodyBean getModifyResponseBodyBean() {
        return modifyResponseBodyBean;
    }

    public void setModifyResponseBodyBean(ModifyResponseBodyBean modifyResponseBodyBean) {
        this.modifyResponseBodyBean = modifyResponseBodyBean;
    }

    public LoginParamInfo getLoginParamInfo() {
        return loginParamInfo;
    }

    public void setLoginParamInfo(LoginParamInfo loginParamInfo) {
        this.loginParamInfo = loginParamInfo;
    }

    public AssembleDynamicUrlInfo getAssembleDynamicUrlInfo() {
        return assembleDynamicUrlInfo;
    }

    public void setAssembleDynamicUrlInfo(AssembleDynamicUrlInfo assembleDynamicUrlInfo) {
        this.assembleDynamicUrlInfo = assembleDynamicUrlInfo;
    }
}

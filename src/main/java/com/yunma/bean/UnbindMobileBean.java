package com.yunma.bean;

import java.io.Serializable;

public class UnbindMobileBean implements Serializable {
    private String mobile;
    private String ciaDomain;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCiaDomain() {
        return ciaDomain;
    }

    public void setCiaDomain(String ciaDomain) {
        this.ciaDomain = ciaDomain;
    }
}

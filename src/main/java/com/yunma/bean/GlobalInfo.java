package com.yunma.bean;

import com.common.bean.GlobalConfig;

import java.io.Serializable;

/***
 * 全局的保留现场的信息<br>
 * 比如选中的是哪个请求panel
 * @author huangweii
 * 2015年9月10日
 */
public class GlobalInfo implements Serializable {
    private static final long serialVersionUID = -840406360252757428L;
    /***
     * 当前的请求panel
     */
    private int currentTabbedIndex;
    /***
     * 上面的TextArea
     */
    private String responseText;
    /***
     * 备忘录
     */
    private String memorandum1;
    /***
     * 备忘录
     */
    private String memorandum2;
    /***
     * 下面的TextArea
     */
    private String statusText;
    /***
     * 是否保存到配置文件
     */
    private boolean willSave = true;
    private boolean hasShowWish = false;
    private GlobalConfig globalConfig;

    public int getCurrentTabbedIndex() {
        return currentTabbedIndex;
    }

    public void setCurrentTabbedIndex(int currentTabbedIndex) {
        this.currentTabbedIndex = currentTabbedIndex;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public String getMemorandum1() {
        return memorandum1;
    }

    public void setMemorandum1(String memorandum1) {
        this.memorandum1 = memorandum1;
    }

    public String getMemorandum2() {
        return memorandum2;
    }

    public void setMemorandum2(String memorandum2) {
        this.memorandum2 = memorandum2;
    }

    public boolean isHasShowWish() {
        return hasShowWish;
    }

    public void setHasShowWish(boolean hasShowWish) {
        this.hasShowWish = hasShowWish;
    }

    public boolean isWillSave() {
        return willSave;
    }

    public void setWillSave(boolean willSave) {
        this.willSave = willSave;
    }

    public GlobalConfig getGlobalConfig() {
        return globalConfig;
    }

    public void setGlobalConfig(GlobalConfig globalConfig) {
        this.globalConfig = globalConfig;
    }
}

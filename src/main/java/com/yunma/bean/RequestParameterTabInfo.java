package com.yunma.bean;

import java.io.Serializable;

/***
 *
 * @author Administrator
 * @date 2015年11月2日
 */
public class RequestParameterTabInfo implements Serializable {
    /***
     * 上一次Tab索引
     */
    private int lastTabIndex;
    private int currentTabIndex;

    public int getLastTabIndex() {
        return lastTabIndex;
    }

    public void setLastTabIndex(int lastTabIndex) {
        this.lastTabIndex = lastTabIndex;
    }

    public int getCurrentTabIndex() {
        return currentTabIndex;
    }

    public void setCurrentTabIndex(int currentTabIndex) {
        this.currentTabIndex = currentTabIndex;
    }

}

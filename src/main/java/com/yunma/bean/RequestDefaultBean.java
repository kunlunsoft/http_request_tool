package com.yunma.bean;

import java.io.Serializable;
import java.util.TreeMap;

/***
 * HTTP 请求默认值
 *
 * @author huangweii
 *         2015年6月28日
 */
public class RequestDefaultBean implements Serializable {
    private static final long serialVersionUID = -4368716311207852567L;
    /***
     * 服务器的ip
     */
    private String serverIp;
    private String port;
    /***
     * 项目名称,例如"tv_mobile"
     */
    private String contextPath;
    private String charset;
    /***
     * 默认的请求要素
     */
    private TreeMap<String, Object> requestParameters;
    /***
     * POST:2<br>GET:0
     */
    private int requestMethod;
    /***
     * 默认cookie,<br >优先级比单个请求的cookie高,所以会覆盖掉单个请求中设置的cookie
     */
    private String defaultCookie;

    public String getServerIp() {
        return serverIp;
    }

    public void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getContextPath() {
        return contextPath;
    }

    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public TreeMap<String, Object> getRequestParameters() {
        return requestParameters;
    }

    public void setRequestParameters(TreeMap<String, Object> requestParameters) {
        this.requestParameters = requestParameters;
    }

    public int getRequestMethod() {
        return requestMethod;
    }

    public void setRequestMethod(int requestMethod) {
        this.requestMethod = requestMethod;
    }

    /***
     * 默认cookie,<br >优先级比单个请求的cookie高,所以会覆盖掉单个请求中设置的cookie
     * @return
     */
    public String getDefaultCookie() {
        return defaultCookie;
    }

    /***
     * 默认cookie,<br >优先级比单个请求的cookie高,所以会覆盖掉单个请求中设置的cookie
     * @param defaultCookie
     */
    public void setDefaultCookie(String defaultCookie) {
        this.defaultCookie = defaultCookie;
    }
}

package com.yunma.callback;

import com.yunma.autotest.RequestPanel;
import net.sf.json.JSONException;

import java.io.UnsupportedEncodingException;

public abstract class RequestCallBack {
    private RequestPanel afterRequestPanel;

    public abstract void callback(String resultJson) throws JSONException, UnsupportedEncodingException, org.json.JSONException;

    public RequestPanel getAfterRequestPanel() {
        return afterRequestPanel;
    }

    public void setAfterRequestPanel(RequestPanel afterRequestPanel) {
        this.afterRequestPanel = afterRequestPanel;
    }

}

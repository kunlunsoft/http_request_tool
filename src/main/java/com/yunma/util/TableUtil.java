package com.yunma.util;

import com.common.util.SystemHWUtil;
import com.io.hw.file.util.FileUtils;
import com.string.widget.util.RegexUtil;
import com.string.widget.util.ValueWidget;
import com.swing.dialog.CustomDefaultDialog;
import com.swing.dialog.toast.ToastMessage;
import com.swing.table.TableUtil3;
import com.yunma.autotest.RequestPanel;
import com.yunma.component.ParameterPanel;
import org.apache.commons.collections.map.ListOrderedMap;

import javax.swing.*;
import javax.swing.event.MouseInputListener;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class TableUtil extends TableUtil3 {
    public static final String context_format = "<Context path=\"/\" docBase=\"%s\" debug=\"0\" 	reloadable=\"true\" crossContext=\"true\" />";

    public static void deleteContextNode(File confFile) {
        RegexUtil.sed(confFile, "<Context.*$", "", "GBK");
    }

    public static void addContextNode(File confFile, String docBase) {
        String result = String.format(context_format, docBase/*"E:\\work\\project\\chanjet_web_store\\src\\main\\webapp"*/);
        RegexUtil.sed(confFile, "(</Host>)", result.replace("\\", "\\\\") + SystemHWUtil.CRLF + "$1", "GBK");
    }

    public static File getConfFile(String tomcatPath) {
        String server_xml = "server.xml";
        if (tomcatPath.endsWith("conf") || tomcatPath.endsWith("conf\\")) {
            return new File(tomcatPath, server_xml);
        } else {
            return new File(tomcatPath, "conf" + File.separator + server_xml);
        }
    }

    public static File getTomcatBin(String tomcatPath) {
        if (tomcatPath.endsWith("conf") || tomcatPath.endsWith("conf\\")) {
            return new File(tomcatPath.replace("conf", "bin"));
        } else {
            return new File(tomcatPath, "bin");
        }
    }

    public static String showCurrentWebapp(File confFile) {
        String input;
        try {
            input = FileUtils.getFullContent2(confFile, "GBK");
            String context_format2 = "<Context path=\"/\" ";
            boolean isFind = RegexUtil.isFind(input, context_format2, Pattern.DOTALL);
            String result = null;
            System.out.println("isFind:" + isFind);
            if (isFind) {
                String context_format = ".*<Context path=\"/\" docBase=\"%s\" debug=\"0\" 	reloadable=\"true\" crossContext=\"true\" />.*";
                String regex22 = String.format(context_format, "([^\\s]*)");
                result = RegexUtil.replaceAll2(input, regex22, "$1", Pattern.DOTALL);
            }
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void showHelpDialog() {
        String crlKey = null;
        if (SystemHWUtil.isMacOSX) {//苹果系统
            crlKey = "Command";
        } else {
            crlKey = "Ctrl";
        }
        CustomDefaultDialog customDefaultDialog = new CustomDefaultDialog(
                "<span style=\"color:red\" >接口调试工具</span><br><div> " +
                        "<span style=\"font-weight: bolder;color: blue\" >Alt+Shift+X</span>&nbsp; :运行</div>\n" +
                        "    <div><span style=\"font-weight: bolder;color: blue\" >Shift+Tab</span>&nbsp;:在请求panel之间切换</div>\n" +
                        "    <div><span style=\"font-weight: bolder;color: blue\" >" + crlKey + "+Q</span>&nbsp;:code换token</div>\n" +
                        "    <div><span style=\"font-weight: bolder;color: blue\" >" + crlKey + "+T</span>&nbsp;:tomcat 配置文件</div>\n" +
                        "    <div><span style=\"font-weight: bolder;color: blue\" >Command+,(逗号)</span>&nbsp;:配置面板</div>\n" +
                        "    <div><span style=\"font-weight: bolder;color: blue\" >" + crlKey + "+Alt+M</span>&nbsp;:最大化请求结果</div>" +
                        "    <div><span style=\"font-weight: bolder;color: blue\" >" + crlKey + "+S</span>&nbsp;:立刻保存配置文件</div>" +
                        "    <div><span style=\"font-weight: bolder;color: blue\" >" + crlKey + "+E</span>&nbsp;:最近访问的请求面板</div>" +
                        "    <div><span style=\"font-weight: bolder;color: blue\" >" + crlKey + "(control)+O</span>&nbsp;:订单信息快捷入口</div>" +
                        "    <div><span style=\"font-weight: bolder;color: blue\" >" + crlKey + "(command)+F</span>&nbsp;:文本框搜索</div>" +
                        "    <div><span style=\"font-weight: bolder;color: blue\" >" + crlKey + "(command)+L</span>&nbsp;:文本框最大化</div>" +
                        "    <div><span style=\"font-weight: bolder;color: blue\" >" + crlKey + "(command)+W</span>&nbsp;:关闭\"窗口最大化\"对话框</div>" +
                        "    <div><span style=\"font-weight: bolder;color: blue\" >" + crlKey + "+D</span>&nbsp;:文本框清除内容</div>" +
                        "    <div><span style=\"font-weight: bolder;color: blue\" >" + crlKey + "+Shift+E</span>&nbsp;:使文本框可编辑</div>" +
                        "    <div><span style=\"font-weight: bolder;color: blue\" >Ctrl(control)+H</span>&nbsp;:显示帮助</div>" +
                        "    <div><span style=\"font-weight: bolder;color: blue\" >Alt+M</span>&nbsp;:最大化表格</div>" +
                        "    <div><span style=\"font-weight: bolder;color: blue\" >Alt+D</span>&nbsp;:检查DNS</div>" +
                        "<div><span style=\"font-weight: bolder;color: blue\" >" + crlKey + "+G</span>&nbsp;:文本框截图</div>" +
                        "<div><span style=\"font-weight: bolder;color: blue\" >文本框中按下鼠标中键</span>&nbsp;:删除全部并黏贴</div>" +
                        "<div><span style=\"font-weight: bolder;color: blue\" >文本框双击" + crlKey + "</span>&nbsp;:删除全部并黏贴</div>" +
                        "<div><span style=\"font-weight: bolder;color: blue\" >文本框双击 escape  </span>&nbsp;:只读</div>" +
                        "<div><span style=\"font-weight: bolder;color: blue\" >文本框单击 escape  </span>&nbsp;:循环搜索</div>" +
                        "<div><span style=\"font-weight: bolder;color: blue\" >文本框中鼠标右键双击</span>&nbsp;:复制全部</div>" +
                        "<div><span style=\"font-weight: bolder;color: blue\" >\"请求参数\"表格的行上鼠标右键双击</span>&nbsp;:增加表格行的高度</div>" +
                        "    <div><span style=\"font-weight: bolder;color: blue\" >command+P</span>&nbsp;:启动插件</div>" +
                        "    <div><span style=\"font-weight: bolder;color: blue\" >右边的Alt/option</span>&nbsp;:运行</div>" +
                        "<div>java -jar 第一个参数为 <span style=\"font-weight: bolder;color: blue\" >\"ui\"</span>&nbsp;,表示启用原生样式</div>" +
                        "<div>搜索请求时,可以以 <span style=\"font-weight: bolder;color: blue\" >两个空格</span>分割&nbsp;空格前匹配 servlet path, 空格后匹配请求名称或别名</div>" +
                        "<div><span style=\"font-weight: bolder;color: blue\" >文本框按下" + crlKey + "+Enter</span>&nbsp;:发送请求</div>", "帮助-接口调试工具", false, 760, null);
        customDefaultDialog.setVisible(true);

    }

    public static ListOrderedMap getParameterOrderedMap(java.util.List<ParameterPanel> parameterPanels) {
        return getParameterOrderedMap(parameterPanels, true);
    }

    public static ListOrderedMap getParameterOrderedMap(java.util.List<ParameterPanel> parameterPanels, boolean isToast) {
        int size = parameterPanels.size();
        ListOrderedMap parameters = new ListOrderedMap();
        for (int i = 0; i < size; i++) {
            ParameterPanel panel_3222 = (ParameterPanel) parameterPanels.get(i);
            if (!ValueWidget.isNullOrEmpty(panel_3222.getKey())) {
                parameters.put(panel_3222.getKey(), panel_3222.getVal());
            }

        }
        System.out.println(parameters);
        if (isToast && parameters.size() == 0) {
            ToastMessage.toast("您没有输入参数", 2000, Color.red);
            return null;
        }
        return parameters;
    }

    public static void initParameterPanel(Map parameters, JPanel panel_1, java.util.List<ParameterPanel> parameterPanels) {
        initParameterPanel(parameters, panel_1, parameterPanels, null);
    }

    public static void initParameterPanel(String key, String val, JPanel panel_1, java.util.List<ParameterPanel> parameterPanels,
                                          MouseInputListener mouseInputListener) {
        addHeaderOneParam(panel_1, parameterPanels, mouseInputListener, key, val);
        panel_1.updateUI();
    }

    public static void initParameterPanel(Map parameters, JPanel panel_1, java.util.List<ParameterPanel> parameterPanels,
                                          MouseInputListener mouseInputListener) {
        if (!ValueWidget.isNullOrEmpty(parameters)) {
            //参数不为空,则先清空
            clearUpparameterPanels(panel_1, parameterPanels);
            for (Iterator iterator = parameters.keySet().iterator(); iterator.hasNext(); ) {
                String key = (String) iterator.next();
                String val = (String) parameters.get(key);
                addHeaderOneParam(panel_1, parameterPanels, mouseInputListener, key, val);
            }
            panel_1.updateUI();
        }
    }

    public static void addHeaderOneParam(JPanel panel_1, List<ParameterPanel> parameterPanels, MouseInputListener mouseInputListener, String key, String val) {
        ParameterPanel panel_3222 = new ParameterPanel();
        panel_3222.layout2(key, val);
        if (null != mouseInputListener) {
            panel_3222.addMouseListener(mouseInputListener);
        }
        addOptionalParameterPanel(panel_3222, panel_1, parameterPanels);
    }

    public static void clearUpparameterPanels(JPanel panel_1, java.util.List<ParameterPanel> parameterPanels) {
        panel_1.removeAll();
        parameterPanels.clear();
    }

    public static void addOptionalParameterPanel(ParameterPanel panel_3222, JPanel panel_1, java.util.List<ParameterPanel> parameterPanels) {
        panel_1.add(panel_3222);
        parameterPanels.add(panel_3222);
    }

    public static void cleanUpParameterpanel(java.util.List<ParameterPanel> parameterPanels) {
        if (!ValueWidget.isNullOrEmpty(parameterPanels)) {
            int size = parameterPanels.size();
            for (int i = 0; i < size; i++) {
                ParameterPanel parameterPanel = (ParameterPanel) parameterPanels.get(i);
                parameterPanel.cleanUp();
            }
        }
    }

    public static String getPluginClassSimpleName(RequestPanel requestPanel) {
        return requestPanel.getOriginalRequestInfoBean().getPluginClassSimpleName();
    }

}

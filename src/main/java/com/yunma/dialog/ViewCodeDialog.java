package com.yunma.dialog;

import com.swing.dialog.DialogUtil;
import com.swing.dialog.GenericDialog;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rsyntaxtextarea.SyntaxScheme;
import org.fife.ui.rsyntaxtextarea.code.CodeRSyntaxTextArea;
import org.fife.ui.rtextarea.RTextScrollPane;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ViewCodeDialog extends GenericDialog {

    private JPanel contentPane;
    private RSyntaxTextArea codeTextArea;

    /**
     * Create the frame.
     */
    public ViewCodeDialog(String code) {
        setModal(true);
//		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		setBounds(100, 100, 450, 300);
        setLoc(850, 500);
        setTitle("查看代码");
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        RTextScrollPane scrollPane = new RTextScrollPane();
        scrollPane.setFoldIndicatorEnabled(true);
        contentPane.add(scrollPane, BorderLayout.CENTER);

        codeTextArea = new CodeRSyntaxTextArea();
        codeTextArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);

        SyntaxScheme scheme = codeTextArea.getSyntaxScheme();

        scheme.getStyle(13).foreground = Color.BLUE;
        scheme.getStyle(10).foreground = new Color(164, 0, 0);
        scheme.getStyle(11).foreground = new Color(164, 0, 0);
        scheme.getStyle(9).foreground = Color.RED;
        scheme.getStyle(23).foreground = Color.BLACK;
        codeTextArea.revalidate();
//        codeTextArea.addMouseListener(new TextAreaMouseListener(null));

        scrollPane.setViewportView(codeTextArea);
        codeTextArea.setColumns(10);
        if (null != code) {
            codeTextArea.setText(code);
        }

        JPanel panel = new JPanel();
        contentPane.add(panel, BorderLayout.SOUTH);

        JButton btnClose = new JButton("close");
        btnClose.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ViewCodeDialog.this.dispose();
            }
        });
        panel.add(btnClose);
        DialogUtil.escape2CloseDialog(this);
    }

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    ViewCodeDialog frame = new ViewCodeDialog(null);
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void setCode(String code) {
        this.codeTextArea.setText(code);
        this.codeTextArea.revalidate();
    }
}

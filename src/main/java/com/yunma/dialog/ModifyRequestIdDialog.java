package com.yunma.dialog;

import com.string.widget.util.ValueWidget;
import com.swing.component.AssistPopupTextField;
import com.swing.dialog.DialogUtil;
import com.swing.dialog.GenericDialog;
import com.swing.dialog.toast.ToastMessage;
import com.yunma.autotest.AutoTestPanel;
import com.yunma.autotest.RequestPanel;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ModifyRequestIdDialog extends GenericDialog {

    private static final long serialVersionUID = -1526566447572101560L;
    private AssistPopupTextField oldRequestIdTF;
    private AssistPopupTextField newRequestIdTF;

    /**
     * Launch the application.
     */
	/*public static void main(String[] args) {
		try {
			ModifyRequestIdDialog dialog = new ModifyRequestIdDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

    /**
     * Create the dialog.
     */
    public ModifyRequestIdDialog(final AutoTestPanel autoTestPanel) {
        RequestPanel currentRequestPanel = autoTestPanel.getCurrentRequestPanel();
        setLoc(450, 300);
        setTitle("修改请求ID");
        setModal(true);
        getContentPane().setLayout(new BorderLayout());
        JPanel contentPanelTF = new JPanel();
        contentPanelTF.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanelTF, BorderLayout.CENTER);
        GridBagLayout gbl_contentPanelTF = new GridBagLayout();
        gbl_contentPanelTF.columnWidths = new int[]{0, 0, 0, 0};
        gbl_contentPanelTF.rowHeights = new int[]{0, 0, 0};
        gbl_contentPanelTF.columnWeights = new double[]{0.0, 0.0, 1.0,
                Double.MIN_VALUE};
        gbl_contentPanelTF.rowWeights = new double[]{0.0, 0.0,
                Double.MIN_VALUE};
        contentPanelTF.setLayout(gbl_contentPanelTF);
        JLabel lblid = new JLabel("原请求ID");
        GridBagConstraints gbc_lblid = new GridBagConstraints();
        gbc_lblid.insets = new Insets(0, 0, 5, 5);
        gbc_lblid.gridx = 0;
        gbc_lblid.gridy = 0;
        contentPanelTF.add(lblid, gbc_lblid);
        oldRequestIdTF = new AssistPopupTextField(currentRequestPanel.getRequestId());
        oldRequestIdTF.setEditable(false);
        GridBagConstraints gbc_oldRequestId = new GridBagConstraints();
        gbc_oldRequestId.insets = new Insets(0, 0, 5, 0);
        gbc_oldRequestId.fill = GridBagConstraints.HORIZONTAL;
        gbc_oldRequestId.gridx = 2;
        gbc_oldRequestId.gridy = 0;
        contentPanelTF.add(oldRequestIdTF, gbc_oldRequestId);
        oldRequestIdTF.setColumns(10);
        JLabel label = new JLabel("修改为:");
        GridBagConstraints gbc_label = new GridBagConstraints();
        gbc_label.insets = new Insets(0, 0, 0, 5);
        gbc_label.gridx = 0;
        gbc_label.gridy = 1;
        contentPanelTF.add(label, gbc_label);
        newRequestIdTF = new AssistPopupTextField();
        newRequestIdTF.setColumns(10);
        GridBagConstraints gbc_newRequestIdTF = new GridBagConstraints();
        gbc_newRequestIdTF.fill = GridBagConstraints.HORIZONTAL;
        gbc_oldRequestId.fill = GridBagConstraints.HORIZONTAL;
        gbc_newRequestIdTF.gridx = 2;
        gbc_newRequestIdTF.gridy = 1;
        contentPanelTF.add(newRequestIdTF, gbc_newRequestIdTF);
        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
        getContentPane().add(buttonPane, BorderLayout.SOUTH);
        JButton okButton = new JButton("OK");
        okButton.setActionCommand("OK");
        buttonPane.add(okButton);
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String oldRequestId2 = oldRequestIdTF.getText();
                if (ValueWidget.isNullOrEmpty(oldRequestId2)) {
                    oldRequestIdTF.requestFocus();
                    ToastMessage.toast("原请求ID不能为空", 3000, Color.red);
                    return;
                }
                String newRequestId = newRequestIdTF.getText();
                if (ValueWidget.isNullOrEmpty(newRequestId)) {
                    newRequestIdTF.requestFocus();
                    ToastMessage.toast("请输入新请求ID", 3000, Color.red);
                    return;
                }
                autoTestPanel.modifyRequestIdAction(newRequestId);
                ModifyRequestIdDialog.this.dispose();
            }
        });

        getRootPane().setDefaultButton(okButton);
        JButton cancelButton = new JButton("关闭");
        cancelButton.setActionCommand("关闭");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ModifyRequestIdDialog.this.dispose();
            }
        });
        buttonPane.add(cancelButton);
        DialogUtil.escape2CloseDialog(this);
    }

    public AssistPopupTextField getNewRequestIdTF() {
        return newRequestIdTF;
    }

    public void setNewRequestIdTF(AssistPopupTextField newRequestIdTF) {
        this.newRequestIdTF = newRequestIdTF;
    }

}

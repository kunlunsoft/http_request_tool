package com.yunma.dialog;

import com.common.bean.GlobalConfig;
import com.string.widget.util.ValueWidget;
import com.swing.component.AssistPopupTextField;
import com.swing.dialog.DialogUtil;
import com.swing.dialog.GenericDialog;
import com.yunma.autotest.AutoTestPanel;
import org.jb2011.lnf.beautyeye.ch3_button.BEButtonUI;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/***
 * see /Users/whuanghkl/work/mygit/io0007_new/src/main/java/com/common/bean/ParameterIncludeBean.java
 */
public class ConfigDialog extends GenericDialog {

    private JPanel contentPane;
    private AutoTestPanel autoTestPanel;
    private JCheckBox checkBoxAutoUrlEncoding;
    private JCheckBox autoGenerateCodeUnitTestcheckBox;
    /***
     * 是否立即上传文件<br />
     * see com/yunma/dialog/UploadPicDialog.java的dragResponse方法
     */
    private JCheckBox autoUploadImmediatelyCheckBox;
    /***
     * 创建请求时,是否自动选择"应用http默认值"
     */
    private JCheckBox autoApplyDefaultHTTPWhenNewRequestCheckBox;
    /***
     * 是否生成更基础的发送http请求代码
     */
    private JCheckBox moreBasicHttpSendCodeCheckBox;
    /**
     * 智能模式
     */
    private JCheckBox intelligenceModeCheckBox;
    /**
     * 是否支持文件夹上传
     */
    private JCheckBox uploadSupportFolder;
    /***
     * 添加请求panel时免确认
     */
    private JCheckBox addRequestPanelNoConfirm;
    /***
     * 删除请求panel时免确认
     */
    private JCheckBox DELETERequestPanelNoConfirm;
    /***
     * 请求应用商店线上接口 时,是否弹框确认
     */
    private JCheckBox requestOnlineStoreNoConfirm;
    /***
     * 聚焦searchTextField 时,是否选择全部(全选)
     */
    private JCheckBox selectAllOnFocusSearchTF;
    private AssistPopupTextField saveCodeDefaultFolderText;
    /***
     * http 的超时时间
     */
    private AssistPopupTextField connectTimeoutText;
    /**
     * http 的超时时间
     */
    private AssistPopupTextField readTimeoutText;
    /***
     * 切换server ip 到 127.0.0.1 时,自动修改端口号为下
     */
    private AssistPopupTextField localhostSwitchPortText;
    /***
     * 接口 路径
     */
    private AssistPopupTextField servletPathTf;
    /***
     * 重复发送请求的次数
     */
    private AssistPopupTextField retryTimesTf;

    /**
     * Create the frame.
     */
    public ConfigDialog(final AutoTestPanel autoTestPanel) {
        this.autoTestPanel = autoTestPanel;
//		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setModal(true);
//		setBounds(100, 100, 450, 300);
        setLoc(450, 580);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        JSplitPane splitPane = new JSplitPane();
        splitPane.setDividerLocation(380);
        splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
        contentPane.add(splitPane, BorderLayout.CENTER);

        JPanel upPanel = new JPanel();
        splitPane.setLeftComponent(upPanel);
        upPanel.setLayout(new GridLayout(17, 1, 0, 0));

        checkBoxAutoUrlEncoding = new JCheckBox("请求参数自动转码");
        upPanel.add(checkBoxAutoUrlEncoding);

        autoGenerateCodeUnitTestcheckBox = new JCheckBox("生成单元测试声明(用于自动生成代码)");
        upPanel.add(autoGenerateCodeUnitTestcheckBox);

        autoUploadImmediatelyCheckBox = new JCheckBox("立即上传文件");
        upPanel.add(autoUploadImmediatelyCheckBox);


        autoApplyDefaultHTTPWhenNewRequestCheckBox = new JCheckBox("创建请求时,是否自动选择\"应用http默认值\"");
        upPanel.add(autoApplyDefaultHTTPWhenNewRequestCheckBox);

        moreBasicHttpSendCodeCheckBox = new JCheckBox("是否显示生成更基础的发送http请求代码");
        upPanel.add(moreBasicHttpSendCodeCheckBox);

        intelligenceModeCheckBox = new JCheckBox("智能模式");
        intelligenceModeCheckBox.setToolTipText("如果是智能模式,判断如果没有请求体,则修改请求方式为GET,并且cookie输入框为空,则自动填充cookie");
        upPanel.add(intelligenceModeCheckBox);

        uploadSupportFolder = new JCheckBox("上传文件支持目录");
        upPanel.add(uploadSupportFolder);


        //添加请求panel时免确认
        addRequestPanelNoConfirm = new JCheckBox("添加请求panel时免确认(不会弹框)");
        upPanel.add(addRequestPanelNoConfirm);

        DELETERequestPanelNoConfirm = new JCheckBox("删除请求panel时免确认(不会弹框)");
        upPanel.add(DELETERequestPanelNoConfirm);

//请求应用商店线上接口panel时免确认
        requestOnlineStoreNoConfirm = new JCheckBox("请求应用商店线上接口 时,是否免弹框确认");
        upPanel.add(requestOnlineStoreNoConfirm);

        selectAllOnFocusSearchTF = new JCheckBox("聚焦searchTextField 时,是否选择全部(全选)");
        upPanel.add(selectAllOnFocusSearchTF);

        saveCodeDefaultFolderText = new AssistPopupTextField();
        String placeHOlder = "保存自动生成的代码 默认的保存目录";
        saveCodeDefaultFolderText.placeHolder(placeHOlder);
        saveCodeDefaultFolderText.setToolTipText(placeHOlder);
        upPanel.add(saveCodeDefaultFolderText);


        connectTimeoutText = new AssistPopupTextField();
        String placeHOlder2 = "http connectTimeout";
        connectTimeoutText.placeHolder(placeHOlder2);
        connectTimeoutText.setToolTipText(placeHOlder2);
        upPanel.add(connectTimeoutText);

        readTimeoutText = new AssistPopupTextField();
        String placeHOlder3 = "http readTimeout";
        readTimeoutText.placeHolder(placeHOlder3);
        readTimeoutText.setToolTipText(placeHOlder3);
        upPanel.add(readTimeoutText);

        localhostSwitchPortText = new AssistPopupTextField();
        String placeHOlder4 = "切换到本地ip,要修改端口号为";
        localhostSwitchPortText.placeHolder(placeHOlder4);
        localhostSwitchPortText.setToolTipText(placeHOlder4);
        upPanel.add(localhostSwitchPortText);

        JPanel sendRetryTimesPanel = new JPanel();
        FlowLayout flowLayout = (FlowLayout) sendRetryTimesPanel.getLayout();
        flowLayout.setAlignment(FlowLayout.LEFT);
        flowLayout.setVgap(0);
        servletPathTf = new AssistPopupTextField();
        servletPathTf.setColumns(26);
        String placeHolder6 = "匹配的请求地址(支持多个)";
        servletPathTf.setToolTipText(placeHolder6);
        servletPathTf.placeHolder(placeHolder6);
        sendRetryTimesPanel.add(servletPathTf);

        retryTimesTf = new AssistPopupTextField();
        String placeHolder7 = "重复发送请求的次数(0无效,建议3次)";
        retryTimesTf.setToolTipText(placeHolder7);
        retryTimesTf.placeHolder(placeHolder7);
        sendRetryTimesPanel.add(retryTimesTf);
        retryTimesTf.setColumns(4);
        upPanel.add(sendRetryTimesPanel);

        JPanel downloadPanel_1 = new JPanel();
        splitPane.setRightComponent(downloadPanel_1);
        GridBagLayout gbl_downloadPanel_1 = new GridBagLayout();
        gbl_downloadPanel_1.columnWidths = new int[]{0};
        gbl_downloadPanel_1.rowHeights = new int[]{0};
        gbl_downloadPanel_1.columnWeights = new double[]{Double.MIN_VALUE};
        gbl_downloadPanel_1.rowWeights = new double[]{Double.MIN_VALUE};
        downloadPanel_1.setLayout(gbl_downloadPanel_1);

        JPanel panel = new JPanel();
        contentPane.add(panel, BorderLayout.SOUTH);

        JButton btnSave = new JButton("save");
        btnSave.setForeground(Color.white);
        btnSave.setUI(new BEButtonUI().setNormalColor(BEButtonUI.NormalColor.green));
        btnSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GlobalConfig globalConfig = autoTestPanel.getGlobalConfig();
                if (null == globalConfig) {
                    globalConfig = new GlobalConfig();
                    autoTestPanel.setGlobalConfig(globalConfig);
                }
                globalConfig.setAutoUrlEncoding(checkBoxAutoUrlEncoding.isSelected());
                globalConfig.setGenerateRequestCodeWithUnitTest(autoGenerateCodeUnitTestcheckBox.isSelected());
                globalConfig.setAutoUploadImmediately(autoUploadImmediatelyCheckBox.isSelected());
                globalConfig.setAutoApplyDefaultHTTPWhenNewRequest(autoApplyDefaultHTTPWhenNewRequestCheckBox.isSelected());
                //是否生成更基础的发送http请求代码
                globalConfig.setMoreBasicHttpSendCode(moreBasicHttpSendCodeCheckBox.isSelected());
                globalConfig.setSaveCodeDefaultFolder(saveCodeDefaultFolderText.getText2());
                globalConfig.setIntelligenceMode(intelligenceModeCheckBox.isSelected());
                globalConfig.setUploadSupportFolder(uploadSupportFolder.isSelected());
                globalConfig.setNoConfirmOnAddRequestPane(addRequestPanelNoConfirm.isSelected());
                globalConfig.setNoConfirmOnDeleteRequestPane(DELETERequestPanelNoConfirm.isSelected());
                globalConfig.setAlertConfirmWhenRequestOnline(!requestOnlineStoreNoConfirm.isSelected());
                globalConfig.setSelectAllOnFocusSearchTF(selectAllOnFocusSearchTF.isSelected());
                if (!ValueWidget.isNullOrEmpty(connectTimeoutText.getText2())) {
                    globalConfig.setConnectTimeout(Integer.parseInt(connectTimeoutText.getText2()));
                }
                if (!ValueWidget.isNullOrEmpty(readTimeoutText.getText2())) {
                    globalConfig.setReadTimeout(Integer.parseInt(readTimeoutText.getText2()));
                }
                //切换server ip 到 127.0.0.1 时,自动修改端口号为下
                if (!ValueWidget.isNullOrEmpty(localhostSwitchPortText.getText2())) {
                    globalConfig.setLocalhostSwitchPort(Integer.parseInt(localhostSwitchPortText.getText2()));
                }
                globalConfig.setSendRetryServletPath(servletPathTf.getText());
                if (!ValueWidget.isNullOrEmpty(retryTimesTf.getText())) {
                    globalConfig.setSendRetryTimes(Integer.parseInt(retryTimesTf.getText()));
                }
                System.out.println(globalConfig.isAutoUrlEncoding());
                ConfigDialog.this.dispose();
            }

        });
        panel.add(btnSave);

        JButton closeBtn = new JButton("cancel");
        closeBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ConfigDialog.this.dispose();
            }
        });
        DialogUtil.escape2CloseDialog(this);

        panel.add(closeBtn);
        readConfig();
    }

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    ConfigDialog frame = new ConfigDialog(null);
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void readConfig() {
        GlobalConfig globalConfig = autoTestPanel.getGlobalConfig();
        if (null != globalConfig) {
            checkBoxAutoUrlEncoding.setSelected(globalConfig.isAutoUrlEncoding());
            autoGenerateCodeUnitTestcheckBox.setSelected(globalConfig.isGenerateRequestCodeWithUnitTest());
            autoUploadImmediatelyCheckBox.setSelected(globalConfig.isAutoUploadImmediately());
            autoApplyDefaultHTTPWhenNewRequestCheckBox.setSelected(globalConfig.isAutoApplyDefaultHTTPWhenNewRequest());
            //是否生成更基础的发送http请求代码
            moreBasicHttpSendCodeCheckBox.setSelected(globalConfig.isMoreBasicHttpSendCode());
            intelligenceModeCheckBox.setSelected(globalConfig.isIntelligenceMode());
            uploadSupportFolder.setSelected(globalConfig.isUploadSupportFolder());
            saveCodeDefaultFolderText.setText(globalConfig.getSaveCodeDefaultFolder());
            addRequestPanelNoConfirm.setSelected(globalConfig.isNoConfirmOnAddRequestPane());
            DELETERequestPanelNoConfirm.setSelected(globalConfig.isNoConfirmOnDeleteRequestPane());
            requestOnlineStoreNoConfirm.setSelected(!globalConfig.isAlertConfirmWhenRequestOnline());
            selectAllOnFocusSearchTF.setSelected(globalConfig.isSelectAllOnFocusSearchTF());
            if (null != globalConfig.getConnectTimeout()) {
                connectTimeoutText.setText(globalConfig.getConnectTimeout().toString(), true);
            }
            if (null != globalConfig.getReadTimeout()) {
                readTimeoutText.setText(globalConfig.getReadTimeout().toString(), true);
            }
            //切换server ip 到 127.0.0.1 时,自动修改端口号为下
            if (null != globalConfig.getLocalhostSwitchPort()) {
                localhostSwitchPortText.setText(globalConfig.getLocalhostSwitchPort().toString(), true);
            }
            servletPathTf.setText(globalConfig.getSendRetryServletPath());
            if (null != globalConfig.getSendRetryTimes()) {
                retryTimesTf.setText(String.valueOf(globalConfig.getSendRetryTimes()));
            }
        }
    }

    @Override
    public void layout3(Container contentPane) {
        super.layout3(contentPane);
        setShortCuts(this);
        setShortCuts(this.getContentPane());
    }
}

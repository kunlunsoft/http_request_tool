package com.yunma.dialog;

import com.string.widget.util.ValueWidget;
import com.swing.component.AssistPopupTextArea;
import com.swing.dialog.DialogUtil;
import com.yunma.autotest.AutoTestPanel;
import com.yunma.bean.AssembleDynamicUrlInfo;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AssembleUrlDialog extends JDialog {

    private final JPanel contentPanel = new JPanel();
    private JTextField placeholderTextField;
    private JTextField columnNameInResponsetextField;
    private JTextField placeholder2textField;
    private JTextField column2textField;
    private JComboBox api2comboBox;
    private JComboBox apiComboBox;
    private AutoTestPanel autoTestPanel;
    private AssistPopupTextArea urlTemplateArea;

    /**
     * Launch the application.
     */
   /* public static void main(String[] args) {
        try {
            AssembleUrlDialog dialog = new AssembleUrlDialog();
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dialog.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
*/

    /**
     * Create the dialog.
     */
    public AssembleUrlDialog(AutoTestPanel autoTestPanel) {
        this.autoTestPanel = autoTestPanel;
        java.util.List<String> requestIdList2 = this.autoTestPanel.getRequestIdList();
        int length = requestIdList2.size();
        AssembleDynamicUrlInfo assembleDynamicUrlInfo = this.autoTestPanel.getAssembleDynamicUrlInfo();
        setModal(true);
        setBounds(100, 100, 650, 400);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        GridBagLayout gbl_contentPanel = new GridBagLayout();
        gbl_contentPanel.columnWidths = new int[]{0, 120, 0, 0, 0};
        gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0, 0};
        gbl_contentPanel.columnWeights = new double[]{0.0, 0.0, 1.0, 1.0, Double.MIN_VALUE};
        gbl_contentPanel.rowWeights = new double[]{1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        contentPanel.setLayout(gbl_contentPanel);
        {
            JScrollPane scrollPane = new JScrollPane();
            GridBagConstraints gbc_scrollPane = new GridBagConstraints();
            gbc_scrollPane.gridwidth = 4;
            gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
            gbc_scrollPane.fill = GridBagConstraints.BOTH;
            gbc_scrollPane.gridx = 0;
            gbc_scrollPane.gridy = 0;
            contentPanel.add(scrollPane, gbc_scrollPane);
            {
                urlTemplateArea = new AssistPopupTextArea();
                urlTemplateArea.setText(assembleDynamicUrlInfo.getUrlTemplate());
                scrollPane.setViewportView(urlTemplateArea);
            }
        }
        {
            JLabel label = new JLabel("占位符");
            GridBagConstraints gbc_label = new GridBagConstraints();
            gbc_label.anchor = GridBagConstraints.EAST;
            gbc_label.insets = new Insets(0, 0, 5, 5);
            gbc_label.gridx = 0;
            gbc_label.gridy = 1;
            contentPanel.add(label, gbc_label);
        }
        {
            placeholderTextField = new JTextField();
            placeholderTextField.setText(assembleDynamicUrlInfo.getPlaceholderName());
            GridBagConstraints gbc_placeholderTextField = new GridBagConstraints();
            gbc_placeholderTextField.insets = new Insets(0, 0, 5, 5);
            gbc_placeholderTextField.fill = GridBagConstraints.HORIZONTAL;
            gbc_placeholderTextField.gridx = 1;
            gbc_placeholderTextField.gridy = 1;
            contentPanel.add(placeholderTextField, gbc_placeholderTextField);
            placeholderTextField.setColumns(10);
        }
        {
            apiComboBox = new JComboBox();
            addItem2ComboBox(requestIdList2, length, apiComboBox);
            apiComboBox.setSelectedItem(assembleDynamicUrlInfo.getApi());
            GridBagConstraints gbc_apiComboBox = new GridBagConstraints();
            gbc_apiComboBox.insets = new Insets(0, 0, 5, 5);
            gbc_apiComboBox.fill = GridBagConstraints.HORIZONTAL;
            gbc_apiComboBox.gridx = 2;
            gbc_apiComboBox.gridy = 1;
            contentPanel.add(apiComboBox, gbc_apiComboBox);
        }
        {
            columnNameInResponsetextField = new JTextField();
            columnNameInResponsetextField.setText(assembleDynamicUrlInfo.getColumnInResponse());
            GridBagConstraints gbc_columnNameInResponsetextField = new GridBagConstraints();
            gbc_columnNameInResponsetextField.insets = new Insets(0, 0, 5, 0);
            gbc_columnNameInResponsetextField.fill = GridBagConstraints.HORIZONTAL;
            gbc_columnNameInResponsetextField.gridx = 3;
            gbc_columnNameInResponsetextField.gridy = 1;
            contentPanel.add(columnNameInResponsetextField, gbc_columnNameInResponsetextField);
            columnNameInResponsetextField.setColumns(10);
        }
        {
            JLabel label = new JLabel("占位符");
            GridBagConstraints gbc_label = new GridBagConstraints();
            gbc_label.anchor = GridBagConstraints.EAST;
            gbc_label.insets = new Insets(0, 0, 5, 5);
            gbc_label.gridx = 0;
            gbc_label.gridy = 2;
            contentPanel.add(label, gbc_label);
        }
        {
            placeholder2textField = new JTextField();
            GridBagConstraints gbc_placeholder2textField = new GridBagConstraints();
            gbc_placeholder2textField.insets = new Insets(0, 0, 5, 5);
            gbc_placeholder2textField.fill = GridBagConstraints.HORIZONTAL;
            gbc_placeholder2textField.gridx = 1;
            gbc_placeholder2textField.gridy = 2;
            contentPanel.add(placeholder2textField, gbc_placeholder2textField);
            placeholder2textField.setColumns(10);
        }
        {
            api2comboBox = new JComboBox();
            addItem2ComboBox(requestIdList2, length, api2comboBox);
            GridBagConstraints gbc_api2comboBox = new GridBagConstraints();
            gbc_api2comboBox.insets = new Insets(0, 0, 5, 5);
            gbc_api2comboBox.fill = GridBagConstraints.HORIZONTAL;
            gbc_api2comboBox.gridx = 2;
            gbc_api2comboBox.gridy = 2;
            contentPanel.add(api2comboBox, gbc_api2comboBox);
        }
        {
            column2textField = new JTextField();
            GridBagConstraints gbc_column2textField = new GridBagConstraints();
            gbc_column2textField.insets = new Insets(0, 0, 5, 0);
            gbc_column2textField.fill = GridBagConstraints.HORIZONTAL;
            gbc_column2textField.gridx = 3;
            gbc_column2textField.gridy = 2;
            contentPanel.add(column2textField, gbc_column2textField);
            column2textField.setColumns(10);
        }
        {
            JLabel label = new JLabel("占位符");
            GridBagConstraints gbc_label = new GridBagConstraints();
            gbc_label.insets = new Insets(0, 0, 0, 5);
            gbc_label.gridx = 0;
            gbc_label.gridy = 3;
            contentPanel.add(label, gbc_label);
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton okButton = new JButton("OK");
                okButton.setActionCommand("OK");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
                okButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String placehorder1 = placeholderTextField.getText();
                        String api1 = apiComboBox.getSelectedItem().toString();
                        String urlTemplate = urlTemplateArea.getText();
                        String columnInResponse = columnNameInResponsetextField.getText();
                        getAssembleDynamicUrlInfo().setApi(api1);
                        getAssembleDynamicUrlInfo().setColumnInResponse(columnInResponse);
                        getAssembleDynamicUrlInfo().setPlaceholderName(placehorder1);
                        getAssembleDynamicUrlInfo().setUrlTemplate(urlTemplate);
                        AssembleUrlDialog.this.dispose();
                    }
                });
            }
            {
                JButton cancelButton = new JButton("Cancel");
                cancelButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        AssembleUrlDialog.this.dispose();
                    }
                });
                cancelButton.setActionCommand("Cancel");
                buttonPane.add(cancelButton);
            }
        }
        DialogUtil.escape2CloseDialog(this);
    }

    public void addItem2ComboBox(java.util.List<String> requestIdList2, int length, JComboBox apiComboBox) {
        for (int i = 0; i < length; i++) {
            String requestId22 = requestIdList2.get(i);
            if (null != requestId22) {
                requestId22 = requestId22.trim();
            }
            if (ValueWidget.isNullOrEmpty(requestId22)) {
                continue;
            }
            apiComboBox.addItem(requestId22);
        }
    }

    public AssembleDynamicUrlInfo getAssembleDynamicUrlInfo() {
        return this.autoTestPanel.getAssembleDynamicUrlInfo();
    }
}

package com.yunma.dialog;

import com.cmd.dos.hw.util.CMDUtil;
import com.common.util.SystemHWUtil;
import com.string.widget.util.ValueWidget;
import com.swing.component.AssistPopupTextArea;
import com.swing.component.AssistPopupTextField;
import com.swing.component.ComponentUtil;
import com.swing.dialog.DialogUtil;
import com.swing.dialog.GenericDialog;
import com.yunma.util.TableUtil;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class TomcatConfDialog extends GenericDialog {

    public static final String configFilePath = System.getProperty("user.home") + File.separator + ".tomcat_conf.properties";
    private static final long serialVersionUID = -7861782074936501013L;
    private final JPanel contentPanel = new JPanel();
    private AssistPopupTextField docBaseTextField;
    private AssistPopupTextField tomcatPathTextField;
    private AssistPopupTextArea resultTextArea;
    private JButton replaceButton;
    /**
     * Launch the application.
     */
	/*public static void main(String[] args) {
		try {
			TomcatConfDialog dialog = new TomcatConfDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

    /**
     * Create the dialog.
     */
    public TomcatConfDialog() {
        setModal(true);
        setLoc(650, 600);
        setTitle("修改tomcat 配置文件");
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        GridBagLayout gbl_contentPanel = new GridBagLayout();
        gbl_contentPanel.columnWidths = new int[]{0, 0, 0};
        gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
        gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
        contentPanel.setLayout(gbl_contentPanel);
        {
            JLabel lblTomcat = new JLabel("tomcat 路径");
            GridBagConstraints gbc_lblTomcat = new GridBagConstraints();
            gbc_lblTomcat.anchor = GridBagConstraints.WEST;
            gbc_lblTomcat.insets = new Insets(0, 0, 5, 5);
            gbc_lblTomcat.gridx = 0;
            gbc_lblTomcat.gridy = 0;
            contentPanel.add(lblTomcat, gbc_lblTomcat);
        }
        {
            tomcatPathTextField = new AssistPopupTextField();
            tomcatPathTextField.setText("E:\\apache-tomcat-7.0.53-windows-x64_3\\apache-tomcat-7.0.53\\conf");
            GridBagConstraints gbc_tomcatPathTextField = new GridBagConstraints();
            gbc_tomcatPathTextField.insets = new Insets(0, 0, 5, 0);
            gbc_tomcatPathTextField.fill = GridBagConstraints.HORIZONTAL;
            gbc_tomcatPathTextField.gridx = 1;
            gbc_tomcatPathTextField.gridy = 0;
            contentPanel.add(tomcatPathTextField, gbc_tomcatPathTextField);
            tomcatPathTextField.setColumns(10);
        }
        {
            JComboBox tomcatPathComboBox = new JComboBox();
            GridBagConstraints gbc_tomcatPathComboBox = new GridBagConstraints();
            gbc_tomcatPathComboBox.gridwidth = 2;
            gbc_tomcatPathComboBox.insets = new Insets(0, 0, 5, 0);
            gbc_tomcatPathComboBox.fill = GridBagConstraints.HORIZONTAL;
            gbc_tomcatPathComboBox.gridx = 0;
            gbc_tomcatPathComboBox.gridy = 1;
            contentPanel.add(tomcatPathComboBox, gbc_tomcatPathComboBox);
        }
        {
            JPanel panel = new JPanel();
            GridBagConstraints gbc_panel = new GridBagConstraints();
            gbc_panel.gridwidth = 2;
            gbc_panel.insets = new Insets(0, 0, 5, 5);
            gbc_panel.fill = GridBagConstraints.BOTH;
            gbc_panel.gridx = 0;
            gbc_panel.gridy = 2;
            contentPanel.add(panel, gbc_panel);
            {
                JButton startButton = new JButton("启动");
                startButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {//启动tomcat
                        if (!DialogUtil.verifyTFAndExist(tomcatPathTextField, "tomcat路径")) {
                            return;
                        }
//						final String cmdFolder=null;//H:\NA52QXHQ\study\项目\apache-tomcat-7.0.53-windows-x64_3\apache-tomcat-7.0.53\bin
                        final String cmdFolder = TableUtil.getTomcatBin(tomcatPathTextField.getText()).getAbsolutePath() /*replaceAll("webapps\\\\[\\w]+\\\\WEB-INF.*$", "bin")*/;
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                CMDUtil.startTomcatAction(cmdFolder);
                            }
                        }).start();
                    }
                });
                panel.add(startButton);
            }
            {
                JButton stopBtn = new JButton("停止");
                stopBtn.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {//启动tomcat
//				final String cmdFolder=null;//H:\NA52QXHQ\study\项目\apache-tomcat-7.0.53-windows-x64_3\apache-tomcat-7.0.53\bin
                        if (!DialogUtil.verifyTFAndExist(tomcatPathTextField, "tomcat路径")) {
                            return;
                        }
                        final String cmdFolder = TableUtil.getTomcatBin(tomcatPathTextField.getText()).getAbsolutePath();
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                CMDUtil.shutdownTomcatAction(cmdFolder);
                            }
                        }).start();
                    }
                });
                panel.add(stopBtn);
            }
        }
        {
            JLabel lblWebapp = new JLabel("webapp路径");
            GridBagConstraints gbc_lblWebapp = new GridBagConstraints();
            gbc_lblWebapp.insets = new Insets(0, 0, 5, 5);
            gbc_lblWebapp.anchor = GridBagConstraints.WEST;
            gbc_lblWebapp.gridx = 0;
            gbc_lblWebapp.gridy = 3;
            contentPanel.add(lblWebapp, gbc_lblWebapp);
        }
        {
            docBaseTextField = new AssistPopupTextField("E:\\work\\project\\chanjet_web_store\\src\\main\\webapp");
            GridBagConstraints gbc_docBaseTextField = new GridBagConstraints();
            gbc_docBaseTextField.insets = new Insets(0, 0, 5, 0);
            gbc_docBaseTextField.fill = GridBagConstraints.HORIZONTAL;
            gbc_docBaseTextField.gridx = 1;
            gbc_docBaseTextField.gridy = 3;
            contentPanel.add(docBaseTextField, gbc_docBaseTextField);
            docBaseTextField.setColumns(10);
        }
        {
            JComboBox comboBox = new JComboBox();
            GridBagConstraints gbc_comboBox = new GridBagConstraints();
            gbc_comboBox.gridwidth = 2;
            gbc_comboBox.insets = new Insets(0, 0, 5, 0);
            gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
            gbc_comboBox.gridx = 0;
            gbc_comboBox.gridy = 4;
            contentPanel.add(comboBox, gbc_comboBox);
        }
        {
            JPanel panel = new JPanel();
            GridBagConstraints gbc_panel = new GridBagConstraints();
            gbc_panel.insets = new Insets(0, 0, 5, 0);
            gbc_panel.gridwidth = 2;
            gbc_panel.fill = GridBagConstraints.BOTH;
            gbc_panel.gridx = 0;
            gbc_panel.gridy = 5;
            contentPanel.add(panel, gbc_panel);
            {
                JButton button = new JButton("当前路径");
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (!DialogUtil.verifyTFAndExist(tomcatPathTextField, "tomcat路径")) {
                            return;
                        }
                        File confFile = TableUtil.getConfFile(tomcatPathTextField.getText());
                        String currentWebappPath = TableUtil.showCurrentWebapp(confFile);
                        if (ValueWidget.isNullOrEmpty(currentWebappPath)) {
                            currentWebappPath = "该节点已删除";
                        }
                        ComponentUtil.appendResult(resultTextArea, currentWebappPath, true, false);
                    }
                });
                panel.add(button);
            }
            {
                JButton button = new JButton("删除");
                button.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        if (!DialogUtil.verifyTFAndExist(tomcatPathTextField, "tomcat路径")) {
                            return;
                        }
                        File confFile = TableUtil.getConfFile(tomcatPathTextField.getText());
                        TableUtil.deleteContextNode(confFile);
                    }
                });
                panel.add(button);
            }
            {
                replaceButton = new JButton("替换");
                replaceButton.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (!DialogUtil.verifyTFAndExist(tomcatPathTextField, "tomcat路径")) {
                            return;
                        }
                        if (!DialogUtil.verifyTFAndExist(docBaseTextField, "docBase")) {
                            return;
                        }
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                replaceButton.setEnabled(false);
                                File confFile = TableUtil.getConfFile(tomcatPathTextField.getText());
                                TableUtil.deleteContextNode(confFile);
                                TableUtil.addContextNode(confFile, docBaseTextField.getText());
                                replaceButton.setEnabled(true);
                                ComponentUtil.appendResult(resultTextArea, "替换成功", true, false);
                            }
                        }).start();
                    }
                });
                panel.add(replaceButton);
            }
            JButton cleanUpBtn = new JButton("清空");
            cleanUpBtn.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    resultTextArea.setText(SystemHWUtil.EMPTY);
                }
            });
            panel.add(cleanUpBtn);
        }
        {
            JScrollPane scrollPane = new JScrollPane();
            GridBagConstraints gbc_scrollPane = new GridBagConstraints();
            gbc_scrollPane.gridwidth = 2;
            gbc_scrollPane.fill = GridBagConstraints.BOTH;
            gbc_scrollPane.gridx = 0;
            gbc_scrollPane.gridy = 6;
            contentPanel.add(scrollPane, gbc_scrollPane);
            resultTextArea = new AssistPopupTextArea();
            resultTextArea.setLineWrap(true);
            resultTextArea.setWrapStyleWord(true);
            scrollPane.setViewportView(resultTextArea);
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton cancelButton = new JButton("Cancel");
                cancelButton.setActionCommand("Cancel");
                buttonPane.add(cancelButton);
                cancelButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        TomcatConfDialog.this.dispose();
                    }
                });
            }
        }
        DialogUtil.escape2CloseDialog(this);
    }
    /***
     * 保存到配置文件中
     */
    /*public void saveConfig(){
//        File configFile=new File(configFilePath);
        if(!configFile.exists()){
            try {
                SystemHWUtil.createEmptyFile(configFile);
            } catch (IOException e) {
                e.printStackTrace();
                GUIUtil23.errorDialog(e);
            }
        }
        CMDUtil.show(configFilePath);//因为隐藏文件是只读的
//        FileUtils.writeToFile(configFilePath, content);
        if(ValueWidget.isNullOrEmpty(props)){
        	props= new Properties();
        }
        //分享的url
        String url2=urlTextField.getText();
        if(!ValueWidget.isNullOrEmpty(url2)){
        	props.setProperty(PROP_KEY_SHARE_URL, url2);
        }

        try {
			OutputStream out=new FileOutputStream(configFile);
			props.store(out, TimeHWUtil.formatDateTimeZH(null));
			out.close();//及时关闭资源
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        CMDUtil.hide(configFilePath);
    }*/

}

package com.yunma.dialog;

import com.common.bean.QRCodeInfoBean;
import com.swing.component.QRCodePanel;
import com.swing.dialog.DialogUtil;
import com.swing.dialog.GenericDialog;
import com.yunma.autotest.AutoTestPanel;

import javax.swing.*;

public class QRCodeDialog extends GenericDialog {
    private QRCodeInfoBean qrCodeInfoBean;
    /**
     * Launch the application.
     */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					QRCodeDialog frame = new QRCodeDialog();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

    /**
     * Create the frame.<br >
     */
    public QRCodeDialog(AutoTestPanel autoTestPanel, QRCodeInfoBean qrCodeInfoBean) {
        this.qrCodeInfoBean = qrCodeInfoBean;
        setTitle("生成二维码");
        setModal(true);
        fullScreen();
        JPanel contentPane = new QRCodePanel(autoTestPanel, this.qrCodeInfoBean);
        setContentPane(contentPane);
        DialogUtil.escape2CloseDialog(this);
    }

    public QRCodeInfoBean getQrCodeInfoBean() {
        return qrCodeInfoBean;
    }

    public void setQrCodeInfoBean(QRCodeInfoBean qrCodeInfoBean) {
        this.qrCodeInfoBean = qrCodeInfoBean;
    }
}

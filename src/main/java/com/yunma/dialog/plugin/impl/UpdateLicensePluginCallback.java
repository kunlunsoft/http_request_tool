package com.yunma.dialog.plugin.impl;

import com.common.util.SystemHWUtil;
import com.common.util.WindowUtil;
import com.io.hw.json.HWJacksonUtils;
import com.io.hw.json.JSONHWUtil;
import com.swing.dialog.toast.ToastMessage;
import com.time.util.TimeHWUtil;
import com.yunma.autotest.RequestPanel;
import com.yunma.dialog.plugin.callback.PluginCallback;
import com.yunma.dialog.plugin.dto.LicenseItemDto;
import com.yunma.dialog.plugin.dto.LicenseUpdateParam;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.Map;

/**
 * 用于生成修改鉴权接口 的参数<br />
 * 关键参数:orgId,userId<br />
 * 错误码:<br />
 * 20106 : 没有传递 userId
 * 10001: sourceId的值必须加上双引号
 */
public class UpdateLicensePluginCallback implements PluginCallback {
    private JTextArea previewTa;

    /***
     *
     * @param licenseItemDto
     * @param buyMore : 延迟使用期限
     * @return
     */
    public static LicenseUpdateParam buildLicenseUpdateParam(LicenseItemDto licenseItemDto, boolean buyMore) {
        LicenseUpdateParam licenseUpdateParam = new LicenseUpdateParam();
//        BeanUtils.copyProperties(licenseItemDto,licenseUpdateParam);
        try {
            org.apache.commons.beanutils.BeanUtils.copyProperties(licenseUpdateParam, licenseItemDto);
            if (buyMore) {
                String endDateStr = licenseItemDto.getEndDate();
                Date endDate = TimeHWUtil.getDate4Str(endDateStr);
                licenseUpdateParam.setStartDate(TimeHWUtil.formatDate(TimeHWUtil.getDateAfter(endDate, 1)));
                licenseUpdateParam.setEndDate(TimeHWUtil.formatDate(TimeHWUtil.getDateAfterByYear(endDate, 1)));
            } else {
                licenseUpdateParam.setLicense(0L);
                licenseUpdateParam.setCustomAmount(0);
                //authItems 是覆盖
                licenseUpdateParam.setStartDate(TimeHWUtil.getCurrentDateTime().split(" ")[0]);
            }

            licenseUpdateParam.setSourceType(2);//来源类型, 1: 订单系统, 2: 运营管理平台
            licenseUpdateParam.setSourceId(licenseItemDto.getSubscribeId());

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        if (null == licenseUpdateParam.getOrgId()) {
            licenseUpdateParam.setOrgId("orgId未指定");
        }
        return licenseUpdateParam;
    }

    @Override
    public boolean callbackAction(String input, RequestPanel requestPanel) {
        System.out.println("input :" + input);
        input = input.replace("\"authItems\": \"\"", "\"authItems\": {}")
                .replace("\"authItems\":\"\"", "\"authItems\": {}");
        if (!input.contains("}")) {
            ToastMessage.toast("请输入鉴权", 2000, Color.red);
            return false;
        }
        LicenseItemDto licenseItemDto = (LicenseItemDto) HWJacksonUtils.deSerialize(input, LicenseItemDto.class);

        LicenseUpdateParam licenseUpdateParam = buildLicenseUpdateParam(licenseItemDto, false);
        LicenseUpdateParam licenseUpdateParamBuyMore = buildLicenseUpdateParam(licenseItemDto, true);
        Map<String, Float> authItems = licenseUpdateParam.getAuthItems();
//        System.out.println(" :" +authItems);
        if (null != authItems) {
            final String itemAccounting = "accountant";//accountant  这个是会计数
            if (!authItems.containsKey(itemAccounting)) {//会计数
                authItems.put(itemAccounting, 0F);
            }
            final String version = "version";
            if (!authItems.containsKey(version)) {//版本号
                authItems.put(version, 0F);
            }
        }
        String param = HWJacksonUtils.getJsonP(licenseUpdateParam)/*.replace(".0", "")*/;
        String paramBuyMore = HWJacksonUtils.getJsonP(licenseUpdateParamBuyMore)/*.replace(".0", "")*/;
        System.out.println(" :" + param);
        WindowUtil.setSysClipboardText(param);
        ToastMessage.toast("已复制到剪切板", 2000);
        requestPanel.selectRequestPanel("createOrUpdateLicense");
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("// 更新已有鉴权(增量)").append(SystemHWUtil.CRLF)
                .append(JSONHWUtil.formatJson(param) + SystemHWUtil.CRLF + SystemHWUtil.CRLF).append(SystemHWUtil.CRLF)
                .append("// 延长使用期限").append(SystemHWUtil.CRLF)
                .append(JSONHWUtil.formatJson(paramBuyMore));
        setText2(stringBuffer.toString());
//        JSONHWUtil.formatJson(this.previewTa, false, null, false);
        return false;
    }

    /***
     * 也可以不覆写,<br />
     * 不管怎么样,Dialog一定会调用该方法
     * @param ta
     */
    @Override
    public void setPreviewTa(JTextArea ta) {
        this.previewTa = ta;
        this.previewTa.setText("只有点击[OK]按钮才会真正干活");
    }

    /***
     * 第一个文本域是否只读
     * @return
     */
    @Override
    public boolean readOnly() {
        return true;
    }

    @Override
    public String getTitle() {
        return "修改鉴权";
    }

    private void setText2(String txt) {
        if (null != this.previewTa) {
            this.previewTa.setText(txt);
        }
    }
}

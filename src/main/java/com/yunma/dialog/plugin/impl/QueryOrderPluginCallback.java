package com.yunma.dialog.plugin.impl;

import com.common.util.SystemHWUtil;
import com.io.hw.json.HWJacksonUtils;
import com.string.widget.util.RegexUtil;
import com.string.widget.util.ValueWidget;
import com.yunma.autotest.RequestPanel;
import com.yunma.dialog.plugin.callback.PluginCallback;
import com.yunma.dialog.plugin.dto.order.ResultList;
import com.yunma.dialog.plugin.dto.order.SearchOrderDto;

import javax.swing.*;
import java.util.List;

public class QueryOrderPluginCallback implements PluginCallback {
    private JTextArea previewTa;

    @Override
    public boolean callbackAction(String input, RequestPanel requestPanel) {
        String serverIp = requestPanel.getServerIp();
        String prefix = "";
        if (serverIp.startsWith("inte-")) {
            prefix = "inte-";
        }
        //获取企业 id
        String orgId = RegexUtil.getDigitAnyWhere(input);
        // 查询订单orgId,无需token
        com.common.bean.RequestSendChain requestInfoBeanOrderKIG = new com.common.bean.RequestSendChain();
        requestInfoBeanOrderKIG.setServerIp(prefix + "bsvc.chanapp.chanjet.com");
        requestInfoBeanOrderKIG.setSsl(false);
        requestInfoBeanOrderKIG.setActionPath("/api/v1/bss/searchOrders");
        requestInfoBeanOrderKIG.setCustomRequestContentType("");
        requestInfoBeanOrderKIG.setRequestMethod(com.common.dict.Constant2.REQUEST_METHOD_POST);
        // requestInfoBeanOrderKIG.setDependentRequest(requestInfoBeanLogin);
        requestInfoBeanOrderKIG.setCurrRequestParameterName("");
        requestInfoBeanOrderKIG.setPreRequestParameterName("");

        java.util.TreeMap parameterMapqVmA = new java.util.TreeMap();//请求参数
        parameterMapqVmA.put("appKey", "8669cfcf-93b9-4911-b855-4d6bc87d75df");
        parameterMapqVmA.put("appSecret", "sdxpke");
        parameterMapqVmA.put("clientId", "8669cfcf-93b9-4911-b855-4d6bc87d75df");
        parameterMapqVmA.put("client_secret", "sdxpke");

        net.sf.json.JSONObject jsonObjectOlF = new net.sf.json.JSONObject();
        jsonObjectOlF.put("queryParams", "{orgId=" + orgId + "}");
        jsonObjectOlF.put("start", "0");
        jsonObjectOlF.put("rows", "20");
        parameterMapqVmA.put("param", jsonObjectOlF.toString());
        requestInfoBeanOrderKIG.setRequestParameters(parameterMapqVmA);
        requestInfoBeanOrderKIG.updateRequestBody();

//                    org.apache.commons.collections.map.ListOrderedMap header=new org.apache.commons.collections.map.ListOrderedMap();
//                    requestInfoBeanOrderKIG.setHeaderMap( header);

        com.common.bean.ResponseResult responseResultOrderNZt = requestInfoBeanOrderKIG.request(); //new RequestPanel.ResponseResult(requestInfoBeanLogin).invoke();
        String responseOrderIDm = responseResultOrderNZt.getResponseJsonResult();
        System.out.println("responseText:" + responseOrderIDm);
        System.out.println(responseOrderIDm);
        SearchOrderDto searchOrderDto = (SearchOrderDto) HWJacksonUtils.deSerialize(responseOrderIDm, SearchOrderDto.class);
        StringBuffer stringBuffer = new StringBuffer();
        if (searchOrderDto.getResult()) {
            List<ResultList> resultLists = searchOrderDto.getValue().getResultList();
            if (ValueWidget.isNullOrEmpty(resultLists)) {
                stringBuffer.append("// 无订单").append(SystemHWUtil.CRLF).append(responseOrderIDm);
            } else {
                stringBuffer.append("userId: " + resultLists.get(0).getUid());
            }
        } else {
            stringBuffer.append(responseOrderIDm);
        }
        setText2(stringBuffer.toString());
        //    System.out.println(com.io.hw.json.JSONHWUtil.jsonFormatter( responseOrderIDm));
        return false;
    }

    @Override
    public void setPreviewTa(JTextArea ta) {
        this.previewTa = ta;
    }

    @Override
    public boolean readOnly() {
        return false;
    }

    @Override
    public String getTitle() {
        return "通过企业id查询userId";
    }

    private void setText2(String txt) {
        if (null != this.previewTa) {
            this.previewTa.setText(txt);
        }
    }
}

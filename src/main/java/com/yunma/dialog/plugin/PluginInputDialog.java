package com.yunma.dialog.plugin;

import com.swing.component.AssistPopupTextArea;
import com.swing.dialog.DialogUtil;
import com.yunma.autotest.RequestPanel;
import com.yunma.dialog.plugin.callback.PluginCallback;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.code.CodeRSyntaxTextArea;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PluginInputDialog extends JDialog {

    private final JPanel contentPanel = new JPanel();
    private PluginCallback pluginCallback;
    private AssistPopupTextArea inputTxtArea;
    private RSyntaxTextArea previewTa;
    /**
     * Launch the application.
     */
    /*public static void main(String[] args) {
		try {
			PluginInputDialog dialog = new PluginInputDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

    /**
     * Create the dialog.
     */
    public PluginInputDialog(final String input, RequestPanel requestPanel) {
        this.pluginCallback = requestPanel.getPluginCallback();

        setTitle("插件--" + this.pluginCallback.getTitle());
        setModal(true);

        setBounds(100, 100, 900, 400);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(new GridLayout(1, 2, 0, 0));
        JScrollPane scrollPane = new JScrollPane();
        contentPanel.add(scrollPane);
        {
            inputTxtArea = new AssistPopupTextArea();
            inputTxtArea.setText(input);
            inputTxtArea.setEditable(!this.pluginCallback.readOnly());
            scrollPane.setViewportView(inputTxtArea);
        }

        previewTa = new CodeRSyntaxTextArea();
        JScrollPane scrollPane1 = new JScrollPane(previewTa);
        Border borderResultTAQuf = BorderFactory.createEtchedBorder(Color.white,
                new Color(148, 145, 140));
        TitledBorder resultTitleQuf = new TitledBorder(borderResultTAQuf, "预览");
        scrollPane1.setBorder(resultTitleQuf);
        contentPanel.add(scrollPane1);

        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
        getContentPane().add(buttonPane, BorderLayout.SOUTH);
        {
            JButton okButton = new JButton("OK");
            okButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    pluginAction(inputTxtArea.getText(), requestPanel);
                }
            });
            okButton.setActionCommand("OK");
            buttonPane.add(okButton);
            getRootPane().setDefaultButton(okButton);
        }
        {
            JButton cancelButton = new JButton("Cancel");
            cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    dispose();
                }
            });
            cancelButton.setActionCommand("Cancel");
            buttonPane.add(cancelButton);
        }
        this.pluginCallback.setPreviewTa(this.previewTa);
        new Thread(new Runnable() {
            @Override
            public void run() {
                pluginAction(inputTxtArea.getText(), requestPanel);
            }
        }).start();
        DialogUtil.escape2CloseDialog(this);
    }

    private void pluginAction(String input, RequestPanel requestPanel) {
        //返回true,将关闭对话框
        if (this.pluginCallback.callbackAction(input, requestPanel)) {
            dispose();
        }
    }

}

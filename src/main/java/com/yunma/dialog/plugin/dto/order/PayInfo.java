/* * Copyright 2014-2017 Chanjet Information Technology Company Limited. */
package com.yunma.dialog.plugin.dto.order;

/**
 * @author 黄威  <br>
 * 2017-11-24 10:01:09
 */
public class PayInfo implements java.io.Serializable {
    private String address;
    private String bank;
    private Long payAmount;
    private String payInfoList;
    private String payNo;
    private String payTime;
    private Long payType;
    private String payer;
    private String payerAccount;
    private String phone;
    private String receiver;
    private String receiverAccount;
    private String remark;
    private String zipcode;

    /**
     * @return address
     */
    public String getAddress() {
        return this.address;
    }

    /**
     * @param address address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return bank
     */
    public String getBank() {
        return this.bank;
    }

    /**
     * @param bank bank
     */
    public void setBank(String bank) {
        this.bank = bank;
    }

    /**
     * @return payAmount
     */
    public Long getPayAmount() {
        return this.payAmount;
    }

    /**
     * @param payAmount payAmount
     */
    public void setPayAmount(Long payAmount) {
        this.payAmount = payAmount;
    }

    /**
     * @return payInfoList
     */
    public String getPayInfoList() {
        return this.payInfoList;
    }

    /**
     * @param payInfoList payInfoList
     */
    public void setPayInfoList(String payInfoList) {
        this.payInfoList = payInfoList;
    }

    /**
     * @return payNo
     */
    public String getPayNo() {
        return this.payNo;
    }

    /**
     * @param payNo payNo
     */
    public void setPayNo(String payNo) {
        this.payNo = payNo;
    }

    /**
     * @return payTime
     */
    public String getPayTime() {
        return this.payTime;
    }

    /**
     * @param payTime payTime
     */
    public void setPayTime(String payTime) {
        this.payTime = payTime;
    }

    /**
     * @return payType
     */
    public Long getPayType() {
        return this.payType;
    }

    /**
     * @param payType payType
     */
    public void setPayType(Long payType) {
        this.payType = payType;
    }

    /**
     * @return payer
     */
    public String getPayer() {
        return this.payer;
    }

    /**
     * @param payer payer
     */
    public void setPayer(String payer) {
        this.payer = payer;
    }

    /**
     * @return payerAccount
     */
    public String getPayerAccount() {
        return this.payerAccount;
    }

    /**
     * @param payerAccount payerAccount
     */
    public void setPayerAccount(String payerAccount) {
        this.payerAccount = payerAccount;
    }

    /**
     * @return phone
     */
    public String getPhone() {
        return this.phone;
    }

    /**
     * @param phone phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return receiver
     */
    public String getReceiver() {
        return this.receiver;
    }

    /**
     * @param receiver receiver
     */
    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    /**
     * @return receiverAccount
     */
    public String getReceiverAccount() {
        return this.receiverAccount;
    }

    /**
     * @param receiverAccount receiverAccount
     */
    public void setReceiverAccount(String receiverAccount) {
        this.receiverAccount = receiverAccount;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return this.remark;
    }

    /**
     * @param remark remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return zipcode
     */
    public String getZipcode() {
        return this.zipcode;
    }

    /**
     * @param zipcode zipcode
     */
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }
}
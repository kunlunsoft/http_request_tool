/* * Copyright 2014-2017 Chanjet Information Technology Company Limited. */
package com.yunma.dialog.plugin.dto.order;

/**
 * @author 黄威  <br>
 * 2017-11-24 10:07:09
 */
public class SearchOrderDto implements java.io.Serializable {
    private Error error;
    private Boolean result;
    private Value value;

    /**
     * @return error
     */
    public Error getError() {
        return this.error;
    }

    /**
     * @param error error
     */
    public void setError(Error error) {
        this.error = error;
    }

    /**
     * @return result
     */
    public Boolean getResult() {
        return this.result;
    }

    /**
     * @param result result
     */
    public void setResult(Boolean result) {
        this.result = result;
    }

    /**
     * @return value
     */
    public Value getValue() {
        return this.value;
    }

    /**
     * @param value value
     */
    public void setValue(Value value) {
        this.value = value;
    }
}
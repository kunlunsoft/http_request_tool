/* * Copyright 2014-2017 Chanjet Information Technology Company Limited. */
package com.yunma.dialog.plugin.dto.order;

/**
 * @author 黄威  <br>
 * 2017-11-24 10:01:09
 */
public class PaySharingRatio implements java.io.Serializable {
    private Long agentRatio;
    private String creator;
    private Long isvRatio;
    private String memo;

    /**
     * @return agentRatio
     */
    public Long getAgentRatio() {
        return this.agentRatio;
    }

    /**
     * @param agentRatio agentRatio
     */
    public void setAgentRatio(Long agentRatio) {
        this.agentRatio = agentRatio;
    }

    /**
     * @return creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator creator
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return isvRatio
     */
    public Long getIsvRatio() {
        return this.isvRatio;
    }

    /**
     * @param isvRatio isvRatio
     */
    public void setIsvRatio(Long isvRatio) {
        this.isvRatio = isvRatio;
    }

    /**
     * @return memo
     */
    public String getMemo() {
        return this.memo;
    }

    /**
     * @param memo memo
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }
}
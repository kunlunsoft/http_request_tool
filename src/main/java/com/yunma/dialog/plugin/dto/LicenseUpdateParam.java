/* * Copyright 2014-2017 Chanjet Information Technology Company Limited. */
package com.yunma.dialog.plugin.dto;

import com.time.util.TimeHWUtil;

import java.util.Map;

/**
 * @author 黄威  <br>
 * 2017-10-18 15:11:07
 */
public class LicenseUpdateParam implements java.io.Serializable {
    private String orgId;
    private Long appId;
    private Long userId;
    private Integer sourceType;
    private String startDate;
    private String endDate;
    private String sourceId;
    private Long license;
    private Map<String, Float> authItems;
    private String description;
    /***
     * 账套
     */
    private Integer customAmount;

    /**
     * @return orgId
     */
    public String getOrgId() {
        return this.orgId;
    }

    /**
     * @param orgId orgId
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    /**
     * @return appId
     */
    public Long getAppId() {
        return this.appId;
    }

    /**
     * @param appId appId
     */
    public void setAppId(Long appId) {
        this.appId = appId;
    }

    /**
     * @return userId
     */
    public Long getUserId() {
        return this.userId;
    }

    /**
     * @param userId userId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return sourceType
     */
    public Integer getSourceType() {
        return this.sourceType;
    }

    /**
     * @param sourceType sourceType
     */
    public void setSourceType(Integer sourceType) {
        this.sourceType = sourceType;
    }

    /**
     * @return startDate
     */
    public String getStartDate() {
        return this.startDate;
    }

    /**
     * @param startDate startDate
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * @return endDate
     */
    public String getEndDate() {
        return this.endDate;
    }

    /**
     * @param endDate endDate
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    /**
     * @return sourceId
     */
    public String getSourceId() {
        return this.sourceId;
    }

    /**
     * @param sourceId sourceId
     */
    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    /**
     * @return license
     */
    public Long getLicense() {
        return this.license;
    }

    /**
     * @param license license
     */
    public void setLicense(Long license) {
        this.license = license;
    }

    /**
     * @return authItems
     */
    public Map<String, Float> getAuthItems() {
        return this.authItems;
    }

    /**
     * @param authItems authItems
     */
    public void setAuthItems(Map<String, Float> authItems) {
        this.authItems = authItems;
    }

    public String getDescription() {
        if (null == description) {
            return "通过接口直接修改鉴权,authItems将覆盖,其他是增量更新,time:" + TimeHWUtil.getCurrentFormattedTime();
        }
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCustomAmount() {
        return customAmount;
    }

    public void setCustomAmount(Integer customAmount) {
        this.customAmount = customAmount;
    }
}
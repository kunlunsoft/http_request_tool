/* * Copyright 2014-2017 Chanjet Information Technology Company Limited. */
package com.yunma.dialog.plugin.dto.order;

/**
 * @author 黄威  <br>
 * 2017-11-24 10:01:09
 */
public class Error implements java.io.Serializable {
    private Long code;
    private String hint;
    private String msg;

    /**
     * @return code
     */
    public Long getCode() {
        return this.code;
    }

    /**
     * @param code code
     */
    public void setCode(Long code) {
        this.code = code;
    }

    /**
     * @return hint
     */
    public String getHint() {
        return this.hint;
    }

    /**
     * @param hint hint
     */
    public void setHint(String hint) {
        this.hint = hint;
    }

    /**
     * @return msg
     */
    public String getMsg() {
        return this.msg;
    }

    /**
     * @param msg msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }
}
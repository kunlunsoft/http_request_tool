/* * Copyright 2014-2017 Chanjet Information Technology Company Limited. */
package com.yunma.dialog.plugin.dto.order;

/**
 * @author 黄威  <br>
 * 2017-11-24 10:01:09
 */
public class Item implements java.io.Serializable {
    private String agentCode;
    private String agentLinkPhone;
    private String agentMobile;
    private String agentName;
    private String agentOrigin;
    private String amountInfo;
    private String cardNo;
    private String costCheckingStatus;
    private String couponSharingRatios;
    private Double discountAmount;
    private String displayInfo;
    private String displayName;
    private String endTime;
    private Long id;
    private Long itemType;
    private String memo;
    private Long orderId;
    private Long orderItemId;
    private String orgFullName;
    private Long orgId;
    private Double payPrice;
    private PaySharingRatio paySharingRatio;
    private Long policyId;
    private Double price;
    private Long priceId;
    private String priceRemark;
    private Long productBuyType;
    private Long productId;
    private String productName;
    private String profitSharingInfo;
    private String psiStatus;
    private String refereeName;
    private String startTime;

    /**
     * @return agentCode
     */
    public String getAgentCode() {
        return this.agentCode;
    }

    /**
     * @param agentCode agentCode
     */
    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    /**
     * @return agentLinkPhone
     */
    public String getAgentLinkPhone() {
        return this.agentLinkPhone;
    }

    /**
     * @param agentLinkPhone agentLinkPhone
     */
    public void setAgentLinkPhone(String agentLinkPhone) {
        this.agentLinkPhone = agentLinkPhone;
    }

    /**
     * @return agentMobile
     */
    public String getAgentMobile() {
        return this.agentMobile;
    }

    /**
     * @param agentMobile agentMobile
     */
    public void setAgentMobile(String agentMobile) {
        this.agentMobile = agentMobile;
    }

    /**
     * @return agentName
     */
    public String getAgentName() {
        return this.agentName;
    }

    /**
     * @param agentName agentName
     */
    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    /**
     * @return agentOrigin
     */
    public String getAgentOrigin() {
        return this.agentOrigin;
    }

    /**
     * @param agentOrigin agentOrigin
     */
    public void setAgentOrigin(String agentOrigin) {
        this.agentOrigin = agentOrigin;
    }

    /**
     * @return amountInfo
     */
    public String getAmountInfo() {
        return this.amountInfo;
    }

    /**
     * @param amountInfo amountInfo
     */
    public void setAmountInfo(String amountInfo) {
        this.amountInfo = amountInfo;
    }

    /**
     * @return cardNo
     */
    public String getCardNo() {
        return this.cardNo;
    }

    /**
     * @param cardNo cardNo
     */
    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    /**
     * @return costCheckingStatus
     */
    public String getCostCheckingStatus() {
        return this.costCheckingStatus;
    }

    /**
     * @param costCheckingStatus costCheckingStatus
     */
    public void setCostCheckingStatus(String costCheckingStatus) {
        this.costCheckingStatus = costCheckingStatus;
    }

    /**
     * @return couponSharingRatios
     */
    public String getCouponSharingRatios() {
        return this.couponSharingRatios;
    }

    /**
     * @param couponSharingRatios couponSharingRatios
     */
    public void setCouponSharingRatios(String couponSharingRatios) {
        this.couponSharingRatios = couponSharingRatios;
    }

    /**
     * @return discountAmount
     */
    public Double getDiscountAmount() {
        return this.discountAmount;
    }

    /**
     * @param discountAmount discountAmount
     */
    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }

    /**
     * @return displayInfo
     */
    public String getDisplayInfo() {
        return this.displayInfo;
    }

    /**
     * @param displayInfo displayInfo
     */
    public void setDisplayInfo(String displayInfo) {
        this.displayInfo = displayInfo;
    }

    /**
     * @return displayName
     */
    public String getDisplayName() {
        return this.displayName;
    }

    /**
     * @param displayName displayName
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @return endTime
     */
    public String getEndTime() {
        return this.endTime;
    }

    /**
     * @param endTime endTime
     */
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /**
     * @return id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @param id id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return itemType
     */
    public Long getItemType() {
        return this.itemType;
    }

    /**
     * @param itemType itemType
     */
    public void setItemType(Long itemType) {
        this.itemType = itemType;
    }

    /**
     * @return memo
     */
    public String getMemo() {
        return this.memo;
    }

    /**
     * @param memo memo
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * @return orderId
     */
    public Long getOrderId() {
        return this.orderId;
    }

    /**
     * @param orderId orderId
     */
    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    /**
     * @return orderItemId
     */
    public Long getOrderItemId() {
        return this.orderItemId;
    }

    /**
     * @param orderItemId orderItemId
     */
    public void setOrderItemId(Long orderItemId) {
        this.orderItemId = orderItemId;
    }

    /**
     * @return orgFullName
     */
    public String getOrgFullName() {
        return this.orgFullName;
    }

    /**
     * @param orgFullName orgFullName
     */
    public void setOrgFullName(String orgFullName) {
        this.orgFullName = orgFullName;
    }

    /**
     * @return orgId
     */
    public Long getOrgId() {
        return this.orgId;
    }

    /**
     * @param orgId orgId
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * @return payPrice
     */
    public Double getPayPrice() {
        return this.payPrice;
    }

    /**
     * @param payPrice payPrice
     */
    public void setPayPrice(Double payPrice) {
        this.payPrice = payPrice;
    }

    /**
     * @return paySharingRatio
     */
    public PaySharingRatio getPaySharingRatio() {
        return this.paySharingRatio;
    }

    /**
     * @param paySharingRatio paySharingRatio
     */
    public void setPaySharingRatio(PaySharingRatio paySharingRatio) {
        this.paySharingRatio = paySharingRatio;
    }

    /**
     * @return policyId
     */
    public Long getPolicyId() {
        return this.policyId;
    }

    /**
     * @param policyId policyId
     */
    public void setPolicyId(Long policyId) {
        this.policyId = policyId;
    }

    /**
     * @return price
     */
    public Double getPrice() {
        return this.price;
    }

    /**
     * @param price price
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     * @return priceId
     */
    public Long getPriceId() {
        return this.priceId;
    }

    /**
     * @param priceId priceId
     */
    public void setPriceId(Long priceId) {
        this.priceId = priceId;
    }

    /**
     * @return priceRemark
     */
    public String getPriceRemark() {
        return this.priceRemark;
    }

    /**
     * @param priceRemark priceRemark
     */
    public void setPriceRemark(String priceRemark) {
        this.priceRemark = priceRemark;
    }

    /**
     * @return productBuyType
     */
    public Long getProductBuyType() {
        return this.productBuyType;
    }

    /**
     * @param productBuyType productBuyType
     */
    public void setProductBuyType(Long productBuyType) {
        this.productBuyType = productBuyType;
    }

    /**
     * @return productId
     */
    public Long getProductId() {
        return this.productId;
    }

    /**
     * @param productId productId
     */
    public void setProductId(Long productId) {
        this.productId = productId;
    }

    /**
     * @return productName
     */
    public String getProductName() {
        return this.productName;
    }

    /**
     * @param productName productName
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return profitSharingInfo
     */
    public String getProfitSharingInfo() {
        return this.profitSharingInfo;
    }

    /**
     * @param profitSharingInfo profitSharingInfo
     */
    public void setProfitSharingInfo(String profitSharingInfo) {
        this.profitSharingInfo = profitSharingInfo;
    }

    /**
     * @return psiStatus
     */
    public String getPsiStatus() {
        return this.psiStatus;
    }

    /**
     * @param psiStatus psiStatus
     */
    public void setPsiStatus(String psiStatus) {
        this.psiStatus = psiStatus;
    }

    /**
     * @return refereeName
     */
    public String getRefereeName() {
        return this.refereeName;
    }

    /**
     * @param refereeName refereeName
     */
    public void setRefereeName(String refereeName) {
        this.refereeName = refereeName;
    }

    /**
     * @return startTime
     */
    public String getStartTime() {
        return this.startTime;
    }

    /**
     * @param startTime startTime
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
}
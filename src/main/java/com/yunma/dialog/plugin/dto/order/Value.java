/* * Copyright 2014-2017 Chanjet Information Technology Company Limited. */
package com.yunma.dialog.plugin.dto.order;

import java.util.List;

/**
 * @author 黄威  <br>
 * 2017-11-24 10:07:09
 */

public class Value implements java.io.Serializable {
    private List<ResultList> resultList;
    private Long totalCount;
    private String totalSum;

    /**
     * @return resultList
     */
    public List<ResultList> getResultList() {
        return this.resultList;
    }

    /**
     * @param resultList resultList
     */
    public void setResultList(List<ResultList> resultList) {
        this.resultList = resultList;
    }

    /**
     * @return totalCount
     */
    public Long getTotalCount() {
        return this.totalCount;
    }

    /**
     * @param totalCount totalCount
     */
    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * @return totalSum
     */
    public String getTotalSum() {
        return this.totalSum;
    }

    /**
     * @param totalSum totalSum
     */
    public void setTotalSum(String totalSum) {
        this.totalSum = totalSum;
    }
}
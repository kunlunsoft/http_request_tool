/* * Copyright 2014-2017 Chanjet Information Technology Company Limited. */
package com.yunma.dialog.plugin.dto;

import java.util.Map;

/**
 * @author 黄威  <br>
 * 2017-10-18 15:04:54
 */
public class LicenseItemDto implements java.io.Serializable {
    private String amount;
    private Map<String, Float> authItems;
    private String endDate;
    private String appName;
    private String displayName;
    private String subscribeId;
    private String customAmount;
    private String type;
    private String license;
    private String extraLicense;
    private String appId;
    private String startDate;
    private String status;

    /**
     * @return amount
     */
    public String getAmount() {
        return this.amount;
    }

    /**
     * @param amount amount
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * @return authItems
     */
    public Map<String, Float> getAuthItems() {
        return this.authItems;
    }

    /**
     * @param authItems authItems
     */
    public void setAuthItems(Map<String, Float> authItems) {
        this.authItems = authItems;
    }

    /**
     * @return endDate
     */
    public String getEndDate() {
        return this.endDate;
    }

    /**
     * @param endDate endDate
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    /**
     * @return appName
     */
    public String getAppName() {
        return this.appName;
    }

    /**
     * @param appName appName
     */
    public void setAppName(String appName) {
        this.appName = appName;
    }

    /**
     * @return displayName
     */
    public String getDisplayName() {
        return this.displayName;
    }

    /**
     * @param displayName displayName
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @return subscribeId
     */
    public String getSubscribeId() {
        return this.subscribeId;
    }

    /**
     * @param subscribeId subscribeId
     */
    public void setSubscribeId(String subscribeId) {
        this.subscribeId = subscribeId;
    }

    /**
     * @return customAmount
     */
    public String getCustomAmount() {
        return this.customAmount;
    }

    /**
     * @param customAmount customAmount
     */
    public void setCustomAmount(String customAmount) {
        this.customAmount = customAmount;
    }

    /**
     * @return type
     */
    public String getType() {
        return this.type;
    }

    /**
     * @param type type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return license
     */
    public String getLicense() {
        return this.license;
    }

    /**
     * @param license license
     */
    public void setLicense(String license) {
        this.license = license;
    }

    /**
     * @return extraLicense
     */
    public String getExtraLicense() {
        return this.extraLicense;
    }

    /**
     * @param extraLicense extraLicense
     */
    public void setExtraLicense(String extraLicense) {
        this.extraLicense = extraLicense;
    }

    /**
     * @return appId
     */
    public String getAppId() {
        return this.appId;
    }

    /**
     * @param appId appId
     */
    public void setAppId(String appId) {
        this.appId = appId;
    }

    /**
     * @return startDate
     */
    public String getStartDate() {
        return this.startDate;
    }

    /**
     * @param startDate startDate
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * @return status
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * @param status status
     */
    public void setStatus(String status) {
        this.status = status;
    }
}
/* * Copyright 2014-2017 Chanjet Information Technology Company Limited. */
package com.yunma.dialog.plugin.dto.order;

import java.util.List;

/**
 * @author 黄威  <br>
 * 2017-11-24 10:07:09
 */
public class ResultList implements java.io.Serializable {
    private String accountCheckingDate;
    private String accountCheckingStatus;
    private String agentCode;
    private String agentName;
    private String agentOrigin;
    private Long businessType;
    private String channel;
    private String contractTime;
    private String couponInfo;
    private String createTime;
    private String description;
    private Double discountAmount;
    private Long dispatchStatus;
    private String dispatchTime;
    private String dispatchUid;
    private String dispatchUname;

    private String extraInfo;
    private String finishTime;
    private Long id;

    private List<Item> items;
    private String modifyTime;
    private Boolean needInvoice;
    private String optRemark;
    private Long orderCategory;
    private String orderNo;
    private String orderOrigin;
    private Long orderStatus;
    private Double orderTotal;
    private String orderTraceListInfo;
    private Long orderType;
    private String payEndTime;
    private PayInfo payInfo;
    private Long payMode;
    private String payStartTime;
    private Long payStatus;
    private Long payTimes;
    private Double payTotal;
    private Long payType;
    private Long payerId;
    private String payerName;
    private Double realPayTotal;
    private String referMobile;
    private String referUserId;
    private String referUserName;
    private String refereeName;
    private String salesType;
    private Long uid;
    private String uname;

    /**
     * @return accountCheckingDate
     */
    public String getAccountCheckingDate() {
        return this.accountCheckingDate;
    }

    /**
     * @param accountCheckingDate accountCheckingDate
     */
    public void setAccountCheckingDate(String accountCheckingDate) {
        this.accountCheckingDate = accountCheckingDate;
    }

    /**
     * @return accountCheckingStatus
     */
    public String getAccountCheckingStatus() {
        return this.accountCheckingStatus;
    }

    /**
     * @param accountCheckingStatus accountCheckingStatus
     */
    public void setAccountCheckingStatus(String accountCheckingStatus) {
        this.accountCheckingStatus = accountCheckingStatus;
    }

    /**
     * @return agentCode
     */
    public String getAgentCode() {
        return this.agentCode;
    }

    /**
     * @param agentCode agentCode
     */
    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    /**
     * @return agentName
     */
    public String getAgentName() {
        return this.agentName;
    }

    /**
     * @param agentName agentName
     */
    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    /**
     * @return agentOrigin
     */
    public String getAgentOrigin() {
        return this.agentOrigin;
    }

    /**
     * @param agentOrigin agentOrigin
     */
    public void setAgentOrigin(String agentOrigin) {
        this.agentOrigin = agentOrigin;
    }

    /**
     * @return businessType
     */
    public Long getBusinessType() {
        return this.businessType;
    }

    /**
     * @param businessType businessType
     */
    public void setBusinessType(Long businessType) {
        this.businessType = businessType;
    }

    /**
     * @return channel
     */
    public String getChannel() {
        return this.channel;
    }

    /**
     * @param channel channel
     */
    public void setChannel(String channel) {
        this.channel = channel;
    }

    /**
     * @return contractTime
     */
    public String getContractTime() {
        return this.contractTime;
    }

    /**
     * @param contractTime contractTime
     */
    public void setContractTime(String contractTime) {
        this.contractTime = contractTime;
    }

    /**
     * @return couponInfo
     */
    public String getCouponInfo() {
        return this.couponInfo;
    }

    /**
     * @param couponInfo couponInfo
     */
    public void setCouponInfo(String couponInfo) {
        this.couponInfo = couponInfo;
    }

    /**
     * @return createTime
     */
    public String getCreateTime() {
        return this.createTime;
    }

    /**
     * @param createTime createTime
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * @param description description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return discountAmount
     */
    public Double getDiscountAmount() {
        return this.discountAmount;
    }

    /**
     * @param discountAmount discountAmount
     */
    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }

    /**
     * @return dispatchStatus
     */
    public Long getDispatchStatus() {
        return this.dispatchStatus;
    }

    /**
     * @param dispatchStatus dispatchStatus
     */
    public void setDispatchStatus(Long dispatchStatus) {
        this.dispatchStatus = dispatchStatus;
    }

    /**
     * @return dispatchTime
     */
    public String getDispatchTime() {
        return this.dispatchTime;
    }

    /**
     * @param dispatchTime dispatchTime
     */
    public void setDispatchTime(String dispatchTime) {
        this.dispatchTime = dispatchTime;
    }

    /**
     * @return dispatchUid
     */
    public String getDispatchUid() {
        return this.dispatchUid;
    }

    /**
     * @param dispatchUid dispatchUid
     */
    public void setDispatchUid(String dispatchUid) {
        this.dispatchUid = dispatchUid;
    }

    /**
     * @return dispatchUname
     */
    public String getDispatchUname() {
        return this.dispatchUname;
    }

    /**
     * @param dispatchUname dispatchUname
     */
    public void setDispatchUname(String dispatchUname) {
        this.dispatchUname = dispatchUname;
    }


    /**
     * @return extraInfo
     */
    public String getExtraInfo() {
        return this.extraInfo;
    }

    /**
     * @param extraInfo extraInfo
     */
    public void setExtraInfo(String extraInfo) {
        this.extraInfo = extraInfo;
    }

    /**
     * @return finishTime
     */
    public String getFinishTime() {
        return this.finishTime;
    }

    /**
     * @param finishTime finishTime
     */
    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    /**
     * @return id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @param id id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return items
     */
    public List<Item> getItems() {
        return this.items;
    }

    /**
     * @param items items
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }

    /**
     * @return modifyTime
     */
    public String getModifyTime() {
        return this.modifyTime;
    }

    /**
     * @param modifyTime modifyTime
     */
    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    /**
     * @return needInvoice
     */
    public Boolean getNeedInvoice() {
        return this.needInvoice;
    }

    /**
     * @param needInvoice needInvoice
     */
    public void setNeedInvoice(Boolean needInvoice) {
        this.needInvoice = needInvoice;
    }

    /**
     * @return optRemark
     */
    public String getOptRemark() {
        return this.optRemark;
    }

    /**
     * @param optRemark optRemark
     */
    public void setOptRemark(String optRemark) {
        this.optRemark = optRemark;
    }

    /**
     * @return orderCategory
     */
    public Long getOrderCategory() {
        return this.orderCategory;
    }

    /**
     * @param orderCategory orderCategory
     */
    public void setOrderCategory(Long orderCategory) {
        this.orderCategory = orderCategory;
    }

    /**
     * @return orderNo
     */
    public String getOrderNo() {
        return this.orderNo;
    }

    /**
     * @param orderNo orderNo
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * @return orderOrigin
     */
    public String getOrderOrigin() {
        return this.orderOrigin;
    }

    /**
     * @param orderOrigin orderOrigin
     */
    public void setOrderOrigin(String orderOrigin) {
        this.orderOrigin = orderOrigin;
    }

    /**
     * @return orderStatus
     */
    public Long getOrderStatus() {
        return this.orderStatus;
    }

    /**
     * @param orderStatus orderStatus
     */
    public void setOrderStatus(Long orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     * @return orderTotal
     */
    public Double getOrderTotal() {
        return this.orderTotal;
    }

    /**
     * @param orderTotal orderTotal
     */
    public void setOrderTotal(Double orderTotal) {
        this.orderTotal = orderTotal;
    }

    /**
     * @return orderTraceListInfo
     */
    public String getOrderTraceListInfo() {
        return this.orderTraceListInfo;
    }

    /**
     * @param orderTraceListInfo orderTraceListInfo
     */
    public void setOrderTraceListInfo(String orderTraceListInfo) {
        this.orderTraceListInfo = orderTraceListInfo;
    }

    /**
     * @return orderType
     */
    public Long getOrderType() {
        return this.orderType;
    }

    /**
     * @param orderType orderType
     */
    public void setOrderType(Long orderType) {
        this.orderType = orderType;
    }

    /**
     * @return payEndTime
     */
    public String getPayEndTime() {
        return this.payEndTime;
    }

    /**
     * @param payEndTime payEndTime
     */
    public void setPayEndTime(String payEndTime) {
        this.payEndTime = payEndTime;
    }

    /**
     * @return payInfo
     */
    public PayInfo getPayInfo() {
        return this.payInfo;
    }

    /**
     * @param payInfo payInfo
     */
    public void setPayInfo(PayInfo payInfo) {
        this.payInfo = payInfo;
    }

    /**
     * @return payMode
     */
    public Long getPayMode() {
        return this.payMode;
    }

    /**
     * @param payMode payMode
     */
    public void setPayMode(Long payMode) {
        this.payMode = payMode;
    }

    /**
     * @return payStartTime
     */
    public String getPayStartTime() {
        return this.payStartTime;
    }

    /**
     * @param payStartTime payStartTime
     */
    public void setPayStartTime(String payStartTime) {
        this.payStartTime = payStartTime;
    }

    /**
     * @return payStatus
     */
    public Long getPayStatus() {
        return this.payStatus;
    }

    /**
     * @param payStatus payStatus
     */
    public void setPayStatus(Long payStatus) {
        this.payStatus = payStatus;
    }

    /**
     * @return payTimes
     */
    public Long getPayTimes() {
        return this.payTimes;
    }

    /**
     * @param payTimes payTimes
     */
    public void setPayTimes(Long payTimes) {
        this.payTimes = payTimes;
    }

    /**
     * @return payTotal
     */
    public Double getPayTotal() {
        return this.payTotal;
    }

    /**
     * @param payTotal payTotal
     */
    public void setPayTotal(Double payTotal) {
        this.payTotal = payTotal;
    }

    /**
     * @return payType
     */
    public Long getPayType() {
        return this.payType;
    }

    /**
     * @param payType payType
     */
    public void setPayType(Long payType) {
        this.payType = payType;
    }

    /**
     * @return payerId
     */
    public Long getPayerId() {
        return this.payerId;
    }

    /**
     * @param payerId payerId
     */
    public void setPayerId(Long payerId) {
        this.payerId = payerId;
    }

    /**
     * @return payerName
     */
    public String getPayerName() {
        return this.payerName;
    }

    /**
     * @param payerName payerName
     */
    public void setPayerName(String payerName) {
        this.payerName = payerName;
    }

    /**
     * @return realPayTotal
     */
    public Double getRealPayTotal() {
        return this.realPayTotal;
    }

    /**
     * @param realPayTotal realPayTotal
     */
    public void setRealPayTotal(Double realPayTotal) {
        this.realPayTotal = realPayTotal;
    }


    /**
     * @return referMobile
     */
    public String getReferMobile() {
        return this.referMobile;
    }

    /**
     * @param referMobile referMobile
     */
    public void setReferMobile(String referMobile) {
        this.referMobile = referMobile;
    }

    /**
     * @return referUserId
     */
    public String getReferUserId() {
        return this.referUserId;
    }

    /**
     * @param referUserId referUserId
     */
    public void setReferUserId(String referUserId) {
        this.referUserId = referUserId;
    }

    /**
     * @return referUserName
     */
    public String getReferUserName() {
        return this.referUserName;
    }

    /**
     * @param referUserName referUserName
     */
    public void setReferUserName(String referUserName) {
        this.referUserName = referUserName;
    }

    /**
     * @return refereeName
     */
    public String getRefereeName() {
        return this.refereeName;
    }

    /**
     * @param refereeName refereeName
     */
    public void setRefereeName(String refereeName) {
        this.refereeName = refereeName;
    }

    /**
     * @return salesType
     */
    public String getSalesType() {
        return this.salesType;
    }

    /**
     * @param salesType salesType
     */
    public void setSalesType(String salesType) {
        this.salesType = salesType;
    }

    /**
     * @return uid
     */
    public Long getUid() {
        return this.uid;
    }

    /**
     * @param uid uid
     */
    public void setUid(Long uid) {
        this.uid = uid;
    }

    /**
     * @return uname
     */
    public String getUname() {
        return this.uname;
    }

    /**
     * @param uname uname
     */
    public void setUname(String uname) {
        this.uname = uname;
    }
}
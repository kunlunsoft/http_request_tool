package com.yunma.dialog.plugin.callback;

import com.yunma.autotest.RequestPanel;

import javax.swing.*;

public interface PluginCallback {
    /***
     * 返回true,将关闭对话框
     * @param input
     * @param requestPanel
     * @return
     */
    public boolean callbackAction(String input, RequestPanel requestPanel);

    /***
     * 设置预览的文本域,<br />
     * 如果插件中不使用,则可以不必调用该方法
     * @param ta
     */
    public void setPreviewTa(JTextArea ta);

    /***
     * 第一个文本域是否只读
     * @return
     */
    public boolean readOnly();

    String getTitle();
}

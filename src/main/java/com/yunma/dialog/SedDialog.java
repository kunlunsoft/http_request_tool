package com.yunma.dialog;

import com.string.widget.util.RegexUtil;
import com.swing.component.AssistPopupTextArea;
import com.swing.component.AssistPopupTextField;
import com.swing.component.ComponentUtil;
import com.swing.component.OpenDirTextField;
import com.swing.dialog.DialogUtil;
import com.swing.dialog.GenericDialog;
import com.swing.dialog.toast.ToastMessage;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class SedDialog extends GenericDialog {

    private final AssistPopupTextField pathTextField;
    private final AssistPopupTextField replaceMentTextField;
    private final JTextArea regexTextArea;
    /***
     * 文件编码
     */
    private final JComboBox<String> encodingComboBox;
    private AssistPopupTextArea resultTextArea;

    /**
     * Launch the application.
     */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SedDialog frame = new SedDialog();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

    /**
     * Create the frame.
     */
    public SedDialog() {
        setModal(true);
//		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("使用sed修改文件内容");
        setLoc(550, 500);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        GridBagLayout gbl_contentPane = new GridBagLayout();
        gbl_contentPane.columnWidths = new int[]{0, 0, 0};
        gbl_contentPane.rowHeights = new int[]{0, 76, 61, 0, 0, 0, 93, 0};
        gbl_contentPane.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        gbl_contentPane.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
        contentPane.setLayout(gbl_contentPane);

        JLabel label = new JLabel("文件路径");
        GridBagConstraints gbc_label = new GridBagConstraints();
        gbc_label.insets = new Insets(0, 0, 5, 5);
        gbc_label.anchor = GridBagConstraints.EAST;
        gbc_label.gridx = 0;
        gbc_label.gridy = 0;
        contentPane.add(label, gbc_label);

        pathTextField = new OpenDirTextField("E:\\tmp\\1030\\a.txt");
        drag(label, pathTextField);
        drag(pathTextField);
        GridBagConstraints gbc_pathTextField = new GridBagConstraints();
        gbc_pathTextField.insets = new Insets(0, 0, 5, 0);
        gbc_pathTextField.fill = GridBagConstraints.HORIZONTAL;
        gbc_pathTextField.gridx = 1;
        gbc_pathTextField.gridy = 0;
        contentPane.add(pathTextField, gbc_pathTextField);
        pathTextField.setColumns(10);

        JScrollPane scrollPane = new JScrollPane();
        Border border2323date22 = BorderFactory.createEtchedBorder(Color.blue,
                new Color(255, 0, 0));
        TitledBorder openFileTitle2 = new TitledBorder(border2323date22, "正则表达式");
        scrollPane.setBorder(openFileTitle2);
        GridBagConstraints gbc_scrollPane = new GridBagConstraints();
        gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
        gbc_scrollPane.gridwidth = 2;
        gbc_scrollPane.fill = GridBagConstraints.BOTH;
        gbc_scrollPane.gridx = 0;
        gbc_scrollPane.gridy = 1;
        contentPane.add(scrollPane, gbc_scrollPane);

        regexTextArea = new AssistPopupTextArea("(__isLogin=[\\s]*)Flase2");
        scrollPane.setViewportView(regexTextArea);

        JScrollPane scrollPane_2 = new JScrollPane();
        GridBagConstraints gbc_scrollPane_2 = new GridBagConstraints();
        gbc_scrollPane_2.gridwidth = 2;
        gbc_scrollPane_2.insets = new Insets(0, 0, 5, 0);
        gbc_scrollPane_2.fill = GridBagConstraints.BOTH;
        gbc_scrollPane_2.gridx = 0;
        gbc_scrollPane_2.gridy = 2;
        contentPane.add(scrollPane_2, gbc_scrollPane_2);
        String noteDesc = "[\\s]表示空格,包括Tab,换行等.注意:只有一个斜杠哦";
        JLabel noteLabel = new JLabel(noteDesc);
        scrollPane_2.setViewportView(noteLabel);

        JLabel label_1 = new JLabel("替换为");
        GridBagConstraints gbc_label_1 = new GridBagConstraints();
        gbc_label_1.anchor = GridBagConstraints.WEST;
        gbc_label_1.insets = new Insets(0, 0, 5, 5);
        gbc_label_1.gridx = 0;
        gbc_label_1.gridy = 3;
        contentPane.add(label_1, gbc_label_1);

        replaceMentTextField = new AssistPopupTextField("$1true");
        GridBagConstraints gbc_replaceMentTextField = new GridBagConstraints();
        gbc_replaceMentTextField.insets = new Insets(0, 0, 5, 0);
        gbc_replaceMentTextField.fill = GridBagConstraints.HORIZONTAL;
        gbc_replaceMentTextField.gridx = 1;
        gbc_replaceMentTextField.gridy = 3;
        contentPane.add(replaceMentTextField, gbc_replaceMentTextField);
        replaceMentTextField.setColumns(10);

        JLabel label_2 = new JLabel("文件编码");
        GridBagConstraints gbc_label_2 = new GridBagConstraints();
        gbc_label_2.anchor = GridBagConstraints.EAST;
        gbc_label_2.insets = new Insets(0, 0, 5, 5);
        gbc_label_2.gridx = 0;
        gbc_label_2.gridy = 4;
        contentPane.add(label_2, gbc_label_2);

        encodingComboBox = ComponentUtil.getEncodingComboBox();
        GridBagConstraints gbc_comboBox = new GridBagConstraints();
        gbc_comboBox.insets = new Insets(0, 0, 5, 0);
        gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
        gbc_comboBox.gridx = 1;
        gbc_comboBox.gridy = 4;
        contentPane.add(encodingComboBox, gbc_comboBox);

        JPanel panel = new JPanel();
        GridBagConstraints gbc_panel = new GridBagConstraints();
        gbc_panel.insets = new Insets(0, 0, 5, 0);
        gbc_panel.gridwidth = 2;
        gbc_panel.fill = GridBagConstraints.BOTH;
        gbc_panel.gridx = 0;
        gbc_panel.gridy = 5;
        contentPane.add(panel, gbc_panel);

        JButton sedButton = new JButton("替换");
        panel.add(sedButton);
        sedButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!DialogUtil.verifyTFAndExist(pathTextField, "文件")) {
                    return;
                }
                if (!DialogUtil.verifyTFEmpty(regexTextArea, "正则表达式")) {
                    return;
                }
                if (!DialogUtil.verifyTFEmpty(replaceMentTextField, "替换内容")) {
                    return;
                }
                String pathStr = pathTextField.getText();
                File file = new File(pathStr);
                String regex = regexTextArea.getText();
                String replacement = replaceMentTextField.getText();
                RegexUtil.sed(file, regex, replacement, (String) encodingComboBox.getSelectedItem());
//				ComponentUtil.appendResult(resultTextArea, regex+" 替换为 "+replacement, true, false);
                resultTextArea.setText(regex + " 替换为 " + replacement);
                ToastMessage.toast("操作完成!", 1000);
            }
        });

        JScrollPane scrollPane_1 = new JScrollPane();
        Border border2323date = BorderFactory.createEtchedBorder(Color.red,
                new Color(255, 0, 0));
        TitledBorder openFileTitle = new TitledBorder(border2323date, "结果");
        scrollPane_1.setBorder(openFileTitle);

        GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
        gbc_scrollPane_1.gridwidth = 2;
        gbc_scrollPane_1.insets = new Insets(0, 0, 0, 0);
        gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
        gbc_scrollPane_1.gridx = 0;
        gbc_scrollPane_1.gridy = 6;
        contentPane.add(scrollPane_1, gbc_scrollPane_1);

        resultTextArea = new AssistPopupTextArea();
        resultTextArea.setLineWrap(true);
        resultTextArea.setWrapStyleWord(true);
        scrollPane_1.setViewportView(resultTextArea);
    }

}

package com.yunma.dialog.callback;

import com.yunma.autotest.RequestPanel;

public interface PluginCallback {
    /***
     * 返回true,将关闭对话框
     * @param input
     * @param requestPanel
     * @return
     */
    public boolean callbackAction(String input, RequestPanel requestPanel);
}

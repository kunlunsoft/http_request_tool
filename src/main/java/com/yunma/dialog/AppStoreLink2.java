package com.yunma.dialog;

import com.common.bean.RequestSendChain;
import com.common.bean.ResponseResult;
import com.common.dict.Constant2;
import com.common.util.SystemHWUtil;
import com.swing.component.AssistPopupTextArea;
import com.swing.component.AssistPopupTextField;
import com.swing.component.AssistPopupTextPane;
import com.swing.component.ComponentUtil;
import com.swing.dialog.GenericDialog;
import com.yunma.autotest.AutoTestPanel;
import com.yunma.bean.OrderSimpleInfo;
import net.sf.json.JSONException;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.TreeMap;

public class AppStoreLink2 extends GenericDialog {

    /***
     * 支付成功的页面
     */
    public static final String PAY_SUCCESS_PAGE = "http://store.chanjet.com/order/paySuccess?orderId=%s&orgId=%s";
    /***
     * 模拟支付页面
     */
    public static final String moni_pay_page = "https://test_platform.chanjet.com/alpayinfo?order_id=%s";
    public static final String pay_method_page = "https://store.chanjet.com/order/payOrder?orgId=%s&orderId=%s";
    private JPanel contentPane;
    private AssistPopupTextField orderNoTextField;
    private AssistPopupTextField orgIdTextField;
    private AssistPopupTextField textField;
    private AssistPopupTextField usernameTextField;
    private AssistPopupTextArea resultTextArea;
    private JTextPane referenceTA;
    private AutoTestPanel autoTestPanel;

    /**
     * Create the frame.
     */
    public AppStoreLink2(AutoTestPanel autoTestPanel) {
        this.autoTestPanel = autoTestPanel;
//		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		setBounds(100, 100, 450, 300);
        setTitle("订单信息");
        setModal(true);
        setLoc(1050, 800);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        GridBagLayout gbl_contentPane = new GridBagLayout();
        gbl_contentPane.columnWidths = new int[]{0, 0, 0};
        gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
        gbl_contentPane.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
        contentPane.setLayout(gbl_contentPane);

        JLabel label = new JLabel("订单号");
        GridBagConstraints gbc_label = new GridBagConstraints();
        gbc_label.insets = new Insets(0, 0, 5, 5);
        gbc_label.anchor = GridBagConstraints.EAST;
        gbc_label.gridx = 0;
        gbc_label.gridy = 0;
        contentPane.add(label, gbc_label);

        orderNoTextField = new AssistPopupTextField("INTE2016031000009");
        GridBagConstraints gbc_orderNoTextField = new GridBagConstraints();
        gbc_orderNoTextField.insets = new Insets(0, 0, 5, 0);
        gbc_orderNoTextField.fill = GridBagConstraints.HORIZONTAL;
        gbc_orderNoTextField.gridx = 1;
        gbc_orderNoTextField.gridy = 0;
        contentPane.add(orderNoTextField, gbc_orderNoTextField);
        orderNoTextField.setColumns(10);

        JLabel lblid = new JLabel("企业id");
        GridBagConstraints gbc_lblid = new GridBagConstraints();
        gbc_lblid.anchor = GridBagConstraints.EAST;
        gbc_lblid.insets = new Insets(0, 0, 5, 5);
        gbc_lblid.gridx = 0;
        gbc_lblid.gridy = 1;
        contentPane.add(lblid, gbc_lblid);

        orgIdTextField = new AssistPopupTextField();
        orgIdTextField.placeHolder("企业id,例如\"90001000846\"");
        GridBagConstraints gbc_orgIdTextField = new GridBagConstraints();
        gbc_orgIdTextField.insets = new Insets(0, 0, 5, 0);
        gbc_orgIdTextField.fill = GridBagConstraints.HORIZONTAL;
        gbc_orgIdTextField.gridx = 1;
        gbc_orgIdTextField.gridy = 1;
        contentPane.add(orgIdTextField, gbc_orgIdTextField);
        orgIdTextField.setColumns(10);

        JLabel label_2 = new JLabel("用户名和密码");
        GridBagConstraints gbc_label_2 = new GridBagConstraints();
        gbc_label_2.anchor = GridBagConstraints.EAST;
        gbc_label_2.insets = new Insets(0, 0, 5, 5);
        gbc_label_2.gridx = 0;
        gbc_label_2.gridy = 2;
        contentPane.add(label_2, gbc_label_2);

        usernameTextField = new AssistPopupTextField("17000031234 111111");
        usernameTextField.placeHolder("格式:13312340098    111111");
        GridBagConstraints gbc_usernameTextField = new GridBagConstraints();
        gbc_usernameTextField.insets = new Insets(0, 0, 5, 0);
        gbc_usernameTextField.fill = GridBagConstraints.HORIZONTAL;
        gbc_usernameTextField.gridx = 1;
        gbc_usernameTextField.gridy = 2;
        contentPane.add(usernameTextField, gbc_usernameTextField);
        usernameTextField.setColumns(10);

		/*JLabel label_1 = new JLabel("预留");
        GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.anchor = GridBagConstraints.EAST;
		gbc_label_1.insets = new Insets(0, 0, 5, 5);
		gbc_label_1.gridx = 0;
		gbc_label_1.gridy = 3;
		contentPane.add(label_1, gbc_label_1);

		textField = new AssistPopupTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 0);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 3;
		contentPane.add(textField, gbc_textField);
		textField.setColumns(10);*/


        JPanel panel = new JPanel();
        GridBagConstraints gbc_panel = new GridBagConstraints();
        gbc_panel.insets = new Insets(0, 0, 5, 0);
        gbc_panel.gridwidth = 2;
        gbc_panel.fill = GridBagConstraints.BOTH;
        gbc_panel.gridx = 0;
        gbc_panel.gridy = 4;
        contentPane.add(panel, gbc_panel);

        JButton button = new JButton("生成链接");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {//生成应用商店常用链接
                OrderSimpleInfo orderSimpleInfo = getOrderSimpleInfo();
                String paySuccessPage = String.format(PAY_SUCCESS_PAGE, orderSimpleInfo.getOrderNo(), orderSimpleInfo.getOrgId());
                String moniPayPage = String.format(moni_pay_page, orderSimpleInfo.getOrderNo().replaceAll("^[a-zA-Z]+", SystemHWUtil.EMPTY));
                String payMethodPage = String.format(pay_method_page, orderSimpleInfo.getOrgId(), orderSimpleInfo.getOrderNo());
//                System.out.println("paySuccessPage:" + paySuccessPage);
//                System.out.println("moniPayPage:" + moniPayPage);
                ComponentUtil.appendResult(resultTextArea, "支付成功页面:" + SystemHWUtil.CRLF + paySuccessPage, false, false);
                ComponentUtil.appendResult(resultTextArea, "模拟支付:" + SystemHWUtil.CRLF + moniPayPage, false, false);
                ComponentUtil.appendResult(resultTextArea, "支付方式选择:" + SystemHWUtil.CRLF + payMethodPage, true, false);
            }
        });
        panel.add(button);

        JButton button_1 = new JButton("订单详情(O)");
        button_1.setMnemonic('O');
        button_1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OrderSimpleInfo orderSimpleInfo = getOrderSimpleInfo();
                RequestSendChain requestInfoBean = new RequestSendChain();
                requestInfoBean.setServerIp("cia.chanapp.chanjet.com");
                requestInfoBean.setActionPath("/internal_api/authorizeByJsonp");
                requestInfoBean.setRequestMethod(Constant2.REQUEST_METHOD_GET);
                TreeMap treeMap = new TreeMap();
                treeMap.put("callback", "callback");
                treeMap.put("client_id", "chanjetapp");
                requestInfoBean.setRequestParameters(treeMap);
                requestInfoBean.updateRequestBody();
//                RequestPanel.ResponseResult responseResult = new RequestPanel.ResponseResult(requestInfoBean).invoke();

//                String resultJson = responseResult.getResponseJsonResult();
//                System.out.println("resultJson:" + resultJson);
//                Map<String, String> responseMap;
                try {
//                    responseMap = responseResult.getResponseJsonMap();
//                    String auth_code = responseMap.get("auth_code");

                    RequestSendChain requestInfoBeanLogin = new RequestSendChain();
                    requestInfoBeanLogin.setServerIp("cia.chanapp.chanjet.com");
                    requestInfoBeanLogin.setActionPath("/internal_api/v1/webLoginAppAndAuthorizeApp");
                    requestInfoBeanLogin.setRequestMethod(Constant2.REQUEST_METHOD_POST);
                    requestInfoBeanLogin.setCurrRequestParameterName("auth_code");
                    requestInfoBeanLogin.setPreRequestParameterName("auth_code");
                    requestInfoBeanLogin.setDependentRequest(requestInfoBean);
                    treeMap = new TreeMap();
//                    treeMap.put("", auth_code);
                    treeMap.put("auth_username", orderSimpleInfo.getUsername());
                    treeMap.put("needOrgLists", "0");
                    treeMap.put("originCode", "chanjet");
                    treeMap.put("password", SystemHWUtil.getMD5(orderSimpleInfo.getPassword(), SystemHWUtil.CHARSET_UTF));

                    treeMap.put("{DES}/jVtMAHuT1k=", "{DES}CqiosOGcCy0JcOaF4cnShvOWSuPYoItXnF8lzO59tqLIrigGBh7nvQ==");
                    treeMap.put("{DES}gfUUxw79kM0pxktdDXuaIQ==", "{DES}d0rnGztsKpA=");
                    treeMap.put("{DES}ahg1TAT803W5c3Qxiv/pjg==", "{DES}p667i0QSzw8=");
                    treeMap.put("{DES}/R75tDrXsqroO6DI4SvW7w==", "{DES}KfmoGyUTEZCqeQk9Qfpduw==");
                    requestInfoBeanLogin.setRequestParameters(treeMap);
                    requestInfoBeanLogin.updateRequestBody();
                    //记录日志
                    AppStoreLink2.this.autoTestPanel.appendStr2LogFile("url:" + requestInfoBeanLogin.getActionPath(), true);
                    AppStoreLink2.this.autoTestPanel.appendStr2LogFile("parameter:" + requestInfoBeanLogin.getRequestBodyData(), true);
                    AppStoreLink2.this.autoTestPanel.appendStr2LogFile("username:" + orderSimpleInfo.getUsername() + " , password:" + orderSimpleInfo.getPassword(), true);
//                    RequestPanel.ResponseResult responseResultLogin =requestInfoBeanLogin.request(); //new RequestPanel.ResponseResult(requestInfoBeanLogin).invoke();
//                    String responseLogin = responseResultLogin.getResponseJsonResult();
//                    System.out.println("responseLogin:" + responseLogin);
//                    ComponentUtil.appendResult(resultTextArea, JSONHWUtil.jsonFormatter(responseLogin), true, false);
//                    Map<String, String> responseMapLogin = responseResultLogin.getResponseJsonMap();
//                    String accessToken = responseMapLogin.get("access_token");
//                    System.out.println("accessToken:" + accessToken);


                    RequestSendChain requestInfoBeanOrder = new RequestSendChain();
                    requestInfoBeanOrder.setServerIp("bsvc.chanapp.chanjet.com");
                    requestInfoBeanOrder.setActionPath("/api/v1/appstore/queryOrders");
                    requestInfoBeanOrder.setRequestMethod(Constant2.REQUEST_METHOD_POST);
                    requestInfoBeanOrder.setDependentRequest(requestInfoBeanLogin);
                    requestInfoBeanOrder.setCurrRequestParameterName("access_token");
                    requestInfoBeanOrder.setPreRequestParameterName("access_token");
                    treeMap = new TreeMap();
                    treeMap.put("params", "[\"" + orderSimpleInfo.getOrderNo() + "\"]");
                    treeMap.put("queryType", "orderNo");

                    treeMap.put("{DES}/jVtMAHuT1k=", "{DES}CqiosOGcCy0JcOaF4cnShvOWSuPYoItXnF8lzO59tqLIrigGBh7nvQ==");
                    treeMap.put("{DES}gfUUxw79kM0pxktdDXuaIQ==", "{DES}d0rnGztsKpA=");
                    treeMap.put("{DES}ahg1TAT803W5c3Qxiv/pjg==", "{DES}p667i0QSzw8=");
                    treeMap.put("{DES}/R75tDrXsqroO6DI4SvW7w==", "{DES}KfmoGyUTEZCqeQk9Qfpduw==");
//                    treeMap.put("access_token", accessToken);
                    requestInfoBeanOrder.setRequestParameters(treeMap);
                    requestInfoBeanOrder.updateRequestBody();
                    ResponseResult responseResultOrder = requestInfoBeanOrder.request(); //new RequestPanel.ResponseResult(requestInfoBeanLogin).invoke();
                    String responseOrder = responseResultOrder.getResponseJsonResult();
                    System.out.println("responseOrder:" + responseOrder);
                    ComponentUtil.appendResult(resultTextArea, responseOrder, true, false);

                    //如果通过orderNo查不到,就根据orgId查询


                } catch (JSONException e1) {
                    e1.printStackTrace();
                }

            }
        });
        panel.add(button_1);

        JButton cleanUpBtn = new JButton("清空(D)");
        cleanUpBtn.setMnemonic('D');
        cleanUpBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                resultTextArea.setText(SystemHWUtil.EMPTY);
            }
        });
        panel.add(cleanUpBtn);

        JSplitPane splitPane = new JSplitPane();

        JScrollPane scrollPane = new JScrollPane();
        GridBagConstraints gbc_scrollPane = new GridBagConstraints();
        gbc_scrollPane.gridwidth = 2;
        gbc_scrollPane.fill = GridBagConstraints.BOTH;
        gbc_scrollPane.gridx = 0;
        gbc_scrollPane.gridy = 5;
        contentPane.add(splitPane, gbc_scrollPane);
        resultTextArea = new AssistPopupTextArea();
        resultTextArea.setLineWrap(true);
        resultTextArea.setWrapStyleWord(true);
        scrollPane.setViewportView(resultTextArea);
        splitPane.setLeftComponent(scrollPane);
        splitPane.setDividerLocation(600);

        String tips = "<div ><p>订单状态</p>" +
                "<p>自付、担保支付</p>" +
                "<table border=\"1\" style=\"\" >" +
                "        <thead>" +
                "        <tr>" +
                "            <th>字段</th>" +
                "            <th>提交</th>" +
                "            <th>支付</th>" +
                "            <th>服务开始</th>" +
                "            <th>服务中</th>" +
                "            <th>完成</th>" +
                "        </tr>" +
                "        </thead>" +
                "        <tbody>" +
                "        <tr>" +
                "            <td>orderStatus</td>" +
                "            <td>1, 1</td>" +
                "            <td>2, 2</td>" +
                "            <td>–, 3</td>" +
                "            <td>–, 4</td>" +
                "            <td>–, 5</td>" +
                "        </tr>" +
                "        <tr>" +
                "            <td>payStatus</td>" +
                "            <td>0, 0</td>" +
                "            <td>1, 10</td>" +
                "            <td>–, 10</td>" +
                "            <td>–, 1</td>" +
                "            <td>–, 1</td>" +
                "        </tr>" +
                "        </tbody>" +
                "    </table>" +
                "</div>";
        referenceTA = new AssistPopupTextPane();
        referenceTA.setContentType("text/html; charset=UTF-8");
        referenceTA.setText(tips);
        referenceTA.setEditable(false);
        splitPane.setRightComponent(new JScrollPane(referenceTA));


    }


    /**
     * Launch the application.
     */
    /*public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    AppStoreLink frame = new AppStoreLink();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }*/
    public OrderSimpleInfo getOrderSimpleInfo() {
        OrderSimpleInfo orderSimpleInfo = new OrderSimpleInfo();
        orderSimpleInfo.setOrderNo(orderNoTextField.getText2());
        orderSimpleInfo.setOrgId(orgIdTextField.getText2());
        String usernamePass = usernameTextField.getText2();
        String[] usernameAndPass = usernamePass.split("[\\s,-]");
        orderSimpleInfo.setUsername(usernameAndPass[0]);
        if (usernameAndPass.length > 1) {
            orderSimpleInfo.setPassword(usernameAndPass[1]);
        } else {
            orderSimpleInfo.setPassword("111111");//默认密码是111111
        }

        return orderSimpleInfo;
    }

    public AssistPopupTextField getUsernameTextField() {
        return usernameTextField;
    }

    public void setUsernameTextField(AssistPopupTextField usernameTextField) {
        this.usernameTextField = usernameTextField;
    }

    public AssistPopupTextField getOrgIdTextField() {
        return orgIdTextField;
    }

    public void setOrgIdTextField(AssistPopupTextField orgIdTextField) {
        this.orgIdTextField = orgIdTextField;
    }

    public AssistPopupTextField getOrderNoTextField() {
        return orderNoTextField;
    }

    public void setOrderNoTextField(AssistPopupTextField orderNoTextField) {
        this.orderNoTextField = orderNoTextField;
    }
}

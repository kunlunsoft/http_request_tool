package com.yunma.dialog;

import com.common.bean.RequestInfoBean;
import com.string.widget.util.ValueWidget;
import com.swing.dialog.GenericDialog;
import com.swing.dialog.toast.ToastMessage;
import com.yunma.autotest.AutoTestPanel;
import com.yunma.component.RequestInfoCheckBox;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by 黄威 on 16/10/2016.<br >
 */
public class SaveSelectDialog extends GenericDialog {
    private java.util.List<RequestInfoCheckBox> checkBoxes = new ArrayList<RequestInfoCheckBox>();
    private AutoTestPanel swingApp = null;

    public SaveSelectDialog(Frame owner, boolean modal) {
        super(owner, modal);
    }

    public SaveSelectDialog() {
        super();
    }

    public SaveSelectDialog(AutoTestPanel swingApp) {
        super();
        this.swingApp = swingApp;
    }

    @Override
    public void layout3(Container contentPane) {
        super.layout3(contentPane);
        final java.util.List<RequestInfoBean> requestInfoBeans = swingApp.getRequestInfoBeens();
        JPanel checkboxPanel = new JPanel();
        int size = requestInfoBeans.size();
        for (int i = 0; i < size; i++) {
            RequestInfoBean requestInfoBean = requestInfoBeans.get(i);
            String checkboxLabel = requestInfoBean.getCurrentRequestName();
            if (ValueWidget.isNullOrEmpty(checkboxLabel)) {
                checkboxLabel = requestInfoBean.getActionPath();
            }
            checkboxLabel = (i + 1) + "__" + checkboxLabel;
            RequestInfoCheckBox checkBox = new RequestInfoCheckBox(checkboxLabel, requestInfoBean);
            checkboxPanel.add(checkBox);
            checkBoxes.add(checkBox);
        }
        checkboxPanel.setAutoscrolls(true);
        checkboxPanel.setLayout(new BoxLayout(checkboxPanel, BoxLayout.PAGE_AXIS));
        JScrollPane scrollPane = new JScrollPane(checkboxPanel);
        contentPane.add(scrollPane);
        int height = 23 * size + 72;
        if (height > 600) {
            height = 600;
        }
        setLoc(420, height);

        JPanel bottomPane = new JPanel();
        JButton selectAllBtn = new JButton("全选");
        selectAllBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int size = checkBoxes.size();
                for (int i = 0; i < size; i++) {
                    JCheckBox checkBox = checkBoxes.get(i);
                    checkBox.setSelected(true);
                }
            }
        });
        bottomPane.add(selectAllBtn);
        JButton deSelectAllBtn = new JButton("取消全选");
        deSelectAllBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int size = checkBoxes.size();
                for (int i = 0; i < size; i++) {
                    JCheckBox checkBox = checkBoxes.get(i);
                    checkBox.setSelected(false);
                }
            }
        });
        bottomPane.add(deSelectAllBtn);

        JButton saveBtn = new JButton("保存");
        saveBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int size = checkBoxes.size();
                java.util.List<RequestInfoBean> requestInfoBeans = new ArrayList<RequestInfoBean>();
                for (int i = 0; i < size; i++) {
                    RequestInfoCheckBox requestInfoCheckBox = checkBoxes.get(i);
                    if (requestInfoCheckBox.isSelected()) {
//                        System.out.println(requestInfoCheckBox.getText());
                        requestInfoBeans.add(requestInfoCheckBox.getRequestInfoBean());
                    }
                }
                if (requestInfoBeans.isEmpty()) {
                    ToastMessage.toast("请选择", 2000, Color.RED);
                } else {
                    swingApp.saveConfig(requestInfoBeans);
                    swingApp.closeAllWindow();
                }
            }
        });
        bottomPane.add(saveBtn);
        contentPane.add(bottomPane, BorderLayout.SOUTH);
    }
}

package com.yunma.dialog;

import com.common.util.WindowUtil;
import com.string.widget.util.ValueWidget;
import com.swing.component.AssistPopupTextArea;
import com.swing.component.AssistPopupTextField;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginInfoDialog extends JDialog {

    private final JPanel contentPanel = new JPanel();
    private AssistPopupTextField serverTextField;
    private AssistPopupTextField cookieTextField_1;
    private AssistPopupTextArea resultTextArea;

    /**
     * Create the dialog.
     */
    public LoginInfoDialog() {
        setBounds(100, 100, 650, 400);
        setTitle("登录信息");
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        GridBagLayout gbl_contentPanel = new GridBagLayout();
        gbl_contentPanel.columnWidths = new int[]{0, 0, 0};
        gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0};
        gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
        contentPanel.setLayout(gbl_contentPanel);
        JLabel label = new JLabel("接口");
        GridBagConstraints gbc_label = new GridBagConstraints();
        gbc_label.insets = new Insets(0, 0, 5, 5);
        gbc_label.anchor = GridBagConstraints.WEST;
        gbc_label.gridx = 0;
        gbc_label.gridy = 0;
        contentPanel.add(label, gbc_label);

        serverTextField = new AssistPopupTextField();
        serverTextField.setEditable(false);
        GridBagConstraints gbc_textField = new GridBagConstraints();
        gbc_textField.insets = new Insets(0, 0, 5, 0);
        gbc_textField.fill = GridBagConstraints.HORIZONTAL;
        gbc_textField.gridx = 1;
        gbc_textField.gridy = 0;
        contentPanel.add(serverTextField, gbc_textField);
        serverTextField.setColumns(10);


        JLabel lblCookie = new JLabel("cookie");
        GridBagConstraints gbc_lblCookie = new GridBagConstraints();
        gbc_lblCookie.anchor = GridBagConstraints.WEST;
        gbc_lblCookie.insets = new Insets(0, 0, 5, 5);
        gbc_lblCookie.gridx = 0;
        gbc_lblCookie.gridy = 1;
        contentPanel.add(lblCookie, gbc_lblCookie);

        cookieTextField_1 = new AssistPopupTextField();
        cookieTextField_1.placeHolder("conventionk");
        GridBagConstraints gbc_textField_1 = new GridBagConstraints();
        gbc_textField_1.insets = new Insets(0, 0, 5, 0);
        gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
        gbc_textField_1.gridx = 1;
        gbc_textField_1.gridy = 1;
        contentPanel.add(cookieTextField_1, gbc_textField_1);
        cookieTextField_1.setColumns(10);


        cookieTextField_1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loginInfoAction();
            }
        });

        {
            JScrollPane scrollPane = new JScrollPane();
            GridBagConstraints gbc_scrollPane = new GridBagConstraints();
            gbc_scrollPane.gridwidth = 2;
            gbc_scrollPane.insets = new Insets(0, 0, 0, 5);
            gbc_scrollPane.fill = GridBagConstraints.BOTH;
            gbc_scrollPane.gridx = 0;
            gbc_scrollPane.gridy = 2;
            contentPanel.add(scrollPane, gbc_scrollPane);
            resultTextArea = new AssistPopupTextArea();
            scrollPane.setViewportView(resultTextArea);
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
			/*{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}*/
            {
                JButton cancelButton = new JButton("Cancel");
                cancelButton.setActionCommand("Cancel");
                cancelButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        LoginInfoDialog.this.dispose();
                    }
                });
                buttonPane.add(cancelButton);
            }
        }
        init();
    }

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        try {
            LoginInfoDialog dialog = new LoginInfoDialog();
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dialog.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String fetchLoginInfo(String serverIp, String port, String cookie) {
        com.common.bean.RequestSendChain requestInfoBeanOrderNzW = new com.common.bean.RequestSendChain();
        requestInfoBeanOrderNzW.setServerIp(serverIp);
        requestInfoBeanOrderNzW.setSsl(false);
        requestInfoBeanOrderNzW.setPort(port);
        requestInfoBeanOrderNzW.setActionPath("/user/info/simple");
        requestInfoBeanOrderNzW.setRequestCookie("");
        requestInfoBeanOrderNzW.setCustomRequestContentType("");
        requestInfoBeanOrderNzW.setRequestMethod(com.common.dict.Constant2.REQUEST_METHOD_GET);
        // requestInfoBeanOrderNzW.setDependentRequest(requestInfoBeanLogin);
        requestInfoBeanOrderNzW.setCurrRequestParameterName("");
        requestInfoBeanOrderNzW.setPreRequestParameterName("");

        java.util.TreeMap parameterMapqzye = new java.util.TreeMap();//请求参数
        parameterMapqzye.put("cookie_conventionk", cookie);
        parameterMapqzye.put("clientId", "ersfsdf*&%23467fsdf");
        requestInfoBeanOrderNzW.setRequestParameters(parameterMapqzye);
        requestInfoBeanOrderNzW.updateRequestBody();

//                    org.apache.commons.collections.map.ListOrderedMap header=new org.apache.commons.collections.map.ListOrderedMap();
//                    requestInfoBeanOrderNzW.setHeaderMap( header);

        com.common.bean.ResponseResult responseResultOrderpbv = requestInfoBeanOrderNzW.request(); //new RequestPanel.ResponseResult(requestInfoBeanLogin).invoke();
        String responseOrderoZW = responseResultOrderpbv.getResponseJsonResult();
        System.out.println("responseText:" + responseOrderoZW);
        System.out.println(responseOrderoZW);
        return responseOrderoZW;
    }

    public void loginInfoAction() {
        String serverIp = "house.yhskyc.com";
        String port = "80";
        if (!ValueWidget.isNullOrEmpty(serverTextField.getText2())) {
            String[] strings = serverTextField.getText2().split("[: ]", 2);
            serverIp = strings[0];
            if (strings.length > 1) {
                port = strings[1];
            }
        }
        String cookie = cookieTextField_1.getText();
        String responseOrderoZW = fetchLoginInfo(serverIp, port, cookie);
        resultTextArea.setText(com.io.hw.json.JSONHWUtil.jsonFormatter(responseOrderoZW));
    }

    private void init() {
        String clip = WindowUtil.getSysClipboardText();

        if (!ValueWidget.isNullOrEmpty(clip)) {
            int length = clip.length();
            if (length > 30 && length < 50) {
                cookieTextField_1.setText(clip, true);
                loginInfoAction();
            }
        }
    }

    public void launch() {
        setVisible(true);
        cookieTextField_1.requestFocus();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                cookieTextField_1.requestFocus();
            }
        }).start();
    }

}

package com.yunma.dialog;

import com.common.bean.RequestInRangeInfo;
import com.string.widget.util.ValueWidget;
import com.swing.component.AssistPopupTextArea;
import com.swing.component.AssistPopupTextField;
import com.swing.dialog.DialogUtil;
import com.swing.dialog.GenericDialog;
import com.yunma.autotest.RequestPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RequestInRangeDialog extends GenericDialog {
    private AssistPopupTextField parameterNameTextField;
    private AssistPopupTextField savePathTextField;
    private JTabbedPane tabbedPane;
    private AssistPopupTextArea rangeTextArea;
    private JCheckBox activeCheckBox;
    /***
     * 是否格式化json
     */
    private JCheckBox formatJsonCheckBox;
    /**
     * Launch the application.
     */
    /*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RequestInRangeDialog dialog = new RequestInRangeDialog();
					dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					dialog.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

    /**
     * Create the dialog.
     */
    public RequestInRangeDialog(final JTabbedPane tabbedPane) {
        this.tabbedPane = tabbedPane;
        final RequestPanel requestPanel = (RequestPanel) tabbedPane.getSelectedComponent();
        final RequestInRangeInfo requestInRangeInfo = requestPanel.getRequestInRangeInfo();
        String title = requestPanel.getRequestName();
        if (ValueWidget.isNullOrEmpty(title)) {
            title = requestPanel.getActionName();
        }
        setTitle("取值范围-" + title);
        setModal(true);
        setLoc(650, 300);
        JPanel panel = new JPanel();
        getContentPane().add(panel, BorderLayout.CENTER);
        GridBagLayout gbl_panel = new GridBagLayout();
        gbl_panel.columnWidths = new int[]{10, 0, 111, 100, 0};
        gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0};
        gbl_panel.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
        gbl_panel.rowWeights = new double[]{0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
        panel.setLayout(gbl_panel);

        JLabel label = new JLabel("参数名");
        GridBagConstraints gbc_label = new GridBagConstraints();
        gbc_label.insets = new Insets(0, 0, 5, 5);
        gbc_label.anchor = GridBagConstraints.WEST;
        gbc_label.gridx = 1;
        gbc_label.gridy = 0;
        panel.add(label, gbc_label);

        parameterNameTextField = new AssistPopupTextField();
        parameterNameTextField.setText(requestInRangeInfo.getParameterName());
        GridBagConstraints gbc_parameterNameTextField = new GridBagConstraints();
        gbc_parameterNameTextField.insets = new Insets(0, 0, 5, 5);
        gbc_parameterNameTextField.fill = GridBagConstraints.HORIZONTAL;
        gbc_parameterNameTextField.gridx = 2;
        gbc_parameterNameTextField.gridy = 0;
        panel.add(parameterNameTextField, gbc_parameterNameTextField);
        parameterNameTextField.setColumns(10);

        JComboBox parameterNameComboBox = new JComboBox();
        GridBagConstraints gbc_parameterNameComboBox = new GridBagConstraints();
        gbc_parameterNameComboBox.insets = new Insets(0, 0, 5, 0);
        gbc_parameterNameComboBox.fill = GridBagConstraints.HORIZONTAL;
        gbc_parameterNameComboBox.gridx = 3;
        gbc_parameterNameComboBox.gridy = 0;
        panel.add(parameterNameComboBox, gbc_parameterNameComboBox);

        JLabel label_1 = new JLabel("取值范围");
        GridBagConstraints gbc_label_1 = new GridBagConstraints();
        gbc_label_1.anchor = GridBagConstraints.WEST;
        gbc_label_1.insets = new Insets(0, 0, 5, 5);
        gbc_label_1.gridx = 1;
        gbc_label_1.gridy = 1;
        panel.add(label_1, gbc_label_1);

        JComboBox typecomboBox = new JComboBox();
        typecomboBox.addItem("手动设定");
        typecomboBox.addItem("从接口获取");
        GridBagConstraints gbc_typecomboBox = new GridBagConstraints();
        gbc_typecomboBox.insets = new Insets(0, 0, 5, 5);
        gbc_typecomboBox.fill = GridBagConstraints.HORIZONTAL;
        gbc_typecomboBox.gridx = 2;
        gbc_typecomboBox.gridy = 1;
        panel.add(typecomboBox, gbc_typecomboBox);

        rangeTextArea = new AssistPopupTextArea();
        rangeTextArea.setText(requestInRangeInfo.getRange());
        JScrollPane scrollPane = new JScrollPane(rangeTextArea);
        GridBagConstraints gbc_scrollPane = new GridBagConstraints();
        gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
        gbc_scrollPane.gridwidth = 3;
        gbc_scrollPane.fill = GridBagConstraints.BOTH;
        gbc_scrollPane.gridx = 1;
        gbc_scrollPane.gridy = 2;
        panel.add(scrollPane, gbc_scrollPane);

        JLabel lblResponse = new JLabel("response保存地址");
        GridBagConstraints gbc_lblResponse = new GridBagConstraints();
        gbc_lblResponse.anchor = GridBagConstraints.EAST;
        gbc_lblResponse.insets = new Insets(0, 0, 0, 5);
        gbc_lblResponse.gridx = 1;
        gbc_lblResponse.gridy = 3;
        panel.add(lblResponse, gbc_lblResponse);

        savePathTextField = new AssistPopupTextField();
        savePathTextField.setText(requestInRangeInfo.getResponseSavePath());
        GridBagConstraints gbc_savePathTextField = new GridBagConstraints();
        gbc_savePathTextField.insets = new Insets(0, 0, 0, 5);
        gbc_savePathTextField.fill = GridBagConstraints.HORIZONTAL;
        gbc_savePathTextField.gridx = 2;
        gbc_savePathTextField.gridy = 3;
        panel.add(savePathTextField, gbc_savePathTextField);
        savePathTextField.setColumns(10);

        JButton btnBrowser = DialogUtil.getBrowserBtn("browser", savePathTextField, JFileChooser.FILES_ONLY, tabbedPane.getRootPane());//new JButton();
        GridBagConstraints gbc_btnBrowser = new GridBagConstraints();
        gbc_btnBrowser.gridx = 3;
        gbc_btnBrowser.gridy = 3;
        panel.add(btnBrowser, gbc_btnBrowser);

        JPanel panel_1 = new JPanel();
        getContentPane().add(panel_1, BorderLayout.SOUTH);

        formatJsonCheckBox = new JCheckBox("格式化json");
        if (requestInRangeInfo.isFormatJson()) {
            formatJsonCheckBox.setSelected(true);
        }
        panel_1.add(formatJsonCheckBox);

        activeCheckBox = new JCheckBox("启用");
        if (requestInRangeInfo.isActivate()) {
            activeCheckBox.setSelected(true);
        }
        panel_1.add(activeCheckBox);

        JButton btnSave = new JButton("save");
        btnSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                requestInRangeInfo.setActivate(activeCheckBox.isSelected())
                        .setResponseSavePath(savePathTextField.getText())
                        .setRange(rangeTextArea.getText2())
                        .setFormatJson(formatJsonCheckBox.isSelected())
                        .setParameterName(parameterNameTextField.getText());
                RequestInRangeDialog.this.dispose();
            }
        });
        panel_1.add(btnSave);
        JButton cancelbtn = new JButton("cancel");
        cancelbtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                RequestInRangeDialog.this.dispose();
            }
        });
        panel_1.add(cancelbtn);
    }

}

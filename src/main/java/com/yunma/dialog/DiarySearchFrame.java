package com.yunma.dialog;

import com.swing.dialog.DialogUtil;
import com.swing.dialog.GenericDialog;
import com.yunma.bean.diary.LoginParamInfo;
import com.yunma.listen.DiaryMenuListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class DiarySearchFrame extends GenericDialog {

    private JPanel contentPane;
    private JTextField txtKeyword;
    private JTable table;
    private LoginParamInfo loginParamInfo;


    /**
     * Create the frame.
     */
    public DiarySearchFrame(LoginParamInfo loginParamInfo) {
        this.loginParamInfo = loginParamInfo;
        setModal(true);
//        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		setBounds(100, 100, 450, 300);
        setTitle("记录日志");
        setLoc(1000, 600);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        contentPane.add(tabbedPane, BorderLayout.CENTER);

        JPanel panel_1 = new JPanel();
        tabbedPane.addTab("编辑日志", null, new DiaryEditPanel(getCookieSet()), null);
        tabbedPane.addTab("日志列表", null, panel_1, null);
        panel_1.setLayout(new BorderLayout(0, 0));

        JPanel panel = new JPanel();
        panel_1.add(panel, BorderLayout.NORTH);

        txtKeyword = new JTextField();
        txtKeyword.setText("keyword");
        panel.add(txtKeyword);
        txtKeyword.setColumns(10);

        JButton btnSearch = new JButton("search");
        panel.add(btnSearch);

        JScrollPane scrollPane = new JScrollPane();
        panel_1.add(scrollPane, BorderLayout.CENTER);

        table = new JTable();
        scrollPane.setViewportView(table);
        setMenu();
        DialogUtil.escape2CloseDialog(this);
    }

    /**
     * 设置菜单
     */
    private void setMenu() {
        JMenuBar menuBar = new JMenuBar();
        DiaryMenuListener diaryMenuListener = new DiaryMenuListener(this);

        JMenuItem loginM = new JMenuItem("login");
        loginM.addActionListener(diaryMenuListener);
        menuBar.add(loginM);

        JMenuItem uploadFileM = new JMenuItem("上传图片");
        uploadFileM.addActionListener(diaryMenuListener);
        menuBar.add(uploadFileM);

        setJMenuBar(menuBar);
    }

    public String getCookieSet() {
        if (null != loginParamInfo) {
            return loginParamInfo.getRequestCookie();
        }
        return "JSESSIONID=BD99F44EEA246AE2AA594CC7C7633AFD; conventionk=50E6F4EFEE6B489B495A29F0C3E9AAD5";
    }

    public LoginParamInfo getLoginParamInfo() {
        return loginParamInfo;
    }

    public void setLoginParamInfo(LoginParamInfo loginParamInfo) {
        this.loginParamInfo = loginParamInfo;
    }
}

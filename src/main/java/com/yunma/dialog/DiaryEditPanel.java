package com.yunma.dialog;

import com.io.hw.json.HWJacksonUtils;
import com.swing.dialog.GenericPanel;
import com.swing.dialog.toast.ToastMessage;
import com.yunma.bean.diary.Diary;
import com.yunma.util.DiaryUtil;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.code.CodeRSyntaxTextArea;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DiaryEditPanel extends GenericPanel {
    private Diary diary;
    private RSyntaxTextArea diaryTextArea;
    /***
     * 更新日志
     */
    private JButton submitBtn;
    /***
     * 获取最新的日志
     */
    private JButton editBtn;
    private JPanel panel_1;
    private JLabel label;
    private JLabel createTimeLabel;
    private String requestCookie;

    /**
     * Create the panel.
     */
    public DiaryEditPanel(String requestCookie) {
        this.requestCookie = requestCookie;
        setLayout(new BorderLayout(0, 0));

        JPanel panel = new JPanel();
        add(panel, BorderLayout.SOUTH);

        editBtn = new JButton("最新日志");
        editBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
//                String cookie = "conventionk=50E6F4EFEE6B489B495A29F0C3E9AAD5";
                editBtn.setEnabled(false);
                String responseOrderNFL = fetchDiary(DiaryEditPanel.this.requestCookie, DiaryEditPanel.this.diary.getId());
                editBtn.setEnabled(true);
                ToastMessage.toast("拉取完成", 2000);
                diary = (Diary) HWJacksonUtils.deSerialize(responseOrderNFL, Diary.class);
                diaryTextArea.setText(diary.getContent());
            }
        });
        panel.add(editBtn);

        submitBtn = new JButton("submit");
        submitBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
//                String cookie = "conventionk=50E6F4EFEE6B489B495A29F0C3E9AAD5";
                submitBtn.setEnabled(false);
                String responseOrderNFL = updateDiary(DiaryEditPanel.this.requestCookie, DiaryEditPanel.this.diaryTextArea.getText(), DiaryEditPanel.this.diary.getId());
                submitBtn.setEnabled(true);
                ToastMessage.toast("提交成功", 2000);
                System.out.println("responseText:" + responseOrderNFL);
                System.out.println(com.io.hw.json.JSONHWUtil.jsonFormatter(responseOrderNFL));
            }
        });
        panel.add(submitBtn);

        JScrollPane scrollPane = new JScrollPane();
        add(scrollPane, BorderLayout.CENTER);

        diaryTextArea = new CodeRSyntaxTextArea();
        scrollPane.setViewportView(diaryTextArea);

        panel_1 = new JPanel();
        FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
        flowLayout.setAlignment(FlowLayout.LEFT);
        add(panel_1, BorderLayout.NORTH);

        label = new JLabel("日期:");
        panel_1.add(label);

        createTimeLabel = new JLabel();
        panel_1.add(createTimeLabel);
        //初始化
        this.init();
    }

    private String updateDiary(String cookie, String diary, Integer id) {
        // 修改日记
        com.common.bean.RequestSendChain requestInfoBeanOrderdzQ = new com.common.bean.RequestSendChain();
        requestInfoBeanOrderdzQ.setServerIp(DiaryUtil.SERVER_IP);
        requestInfoBeanOrderdzQ.setSsl(false);
        requestInfoBeanOrderdzQ.setPort("8084");
        requestInfoBeanOrderdzQ.setActionPath("/convention2/diary/" + id + "/json/update");
        requestInfoBeanOrderdzQ.setRequestCookie(cookie);
        requestInfoBeanOrderdzQ.setRequestMethod(com.common.dict.Constant2.REQUEST_METHOD_POST);
        // requestInfoBeanOrderdzQ.setDependentRequest(requestInfoBeanLogin);
        requestInfoBeanOrderdzQ.setCurrRequestParameterName("");
        requestInfoBeanOrderdzQ.setPreRequestParameterName("");

        java.util.TreeMap parameterMapUQfb = new java.util.TreeMap();//请求参数
        parameterMapUQfb.put("content", diary);
        requestInfoBeanOrderdzQ.setRequestParameters(parameterMapUQfb);
        requestInfoBeanOrderdzQ.updateRequestBody();

        com.common.bean.ResponseResult responseResultOrderrej = requestInfoBeanOrderdzQ.request(); //new RequestPanel.ResponseResult(requestInfoBeanLogin).invoke();
        return responseResultOrderrej.getResponseJsonResult();
    }

    private void init() {
        /*if(null==diary){
	        return;
        }*/
//        String cookie = "JSESSIONID=BD99F44EEA246AE2AA594CC7C7633AFD; conventionk=50E6F4EFEE6B489B495A29F0C3E9AAD5";
        new Thread(new Runnable() {
            @Override
            public void run() {
                String responseOrderYCv = fetchCurrentDiary(DiaryEditPanel.this.requestCookie);
                System.out.println("responseText:" + responseOrderYCv);
                System.out.println(com.io.hw.json.JSONHWUtil.jsonFormatter(responseOrderYCv));
                DiaryEditPanel.this.diary = (Diary) HWJacksonUtils.deSerialize(responseOrderYCv, Diary.class);
                DiaryEditPanel.this.diaryTextArea.setText(diary.getContent());
                DiaryEditPanel.this.createTimeLabel.setText(DiaryEditPanel.this.diary.getCreateTime());
            }
        }).start();
    }

    private String fetchCurrentDiary(String cookie) {
        com.common.bean.RequestSendChain requestInfoBeanOrderOUw = new com.common.bean.RequestSendChain();
        requestInfoBeanOrderOUw.setServerIp(DiaryUtil.SERVER_IP);
        requestInfoBeanOrderOUw.setSsl(false);
        requestInfoBeanOrderOUw.setPort("8084");
        requestInfoBeanOrderOUw.setActionPath("/convention2/diary/getCurrent/json");
        requestInfoBeanOrderOUw.setRequestCookie(cookie);
        requestInfoBeanOrderOUw.setRequestMethod(com.common.dict.Constant2.REQUEST_METHOD_GET);
        // requestInfoBeanOrderOUw.setDependentRequest(requestInfoBeanLogin);
        requestInfoBeanOrderOUw.setCurrRequestParameterName("");
        requestInfoBeanOrderOUw.setPreRequestParameterName("");

        requestInfoBeanOrderOUw.updateRequestBody();

        com.common.bean.ResponseResult responseResultOrderAWl = requestInfoBeanOrderOUw.request(); //new RequestPanel.ResponseResult(requestInfoBeanLogin).invoke();
        return responseResultOrderAWl.getResponseJsonResult();
    }

    private String fetchDiary(String cookie, Integer diaryId) {
        // 查询日记
        com.common.bean.RequestSendChain requestInfoBeanOrderQOC = new com.common.bean.RequestSendChain();
        requestInfoBeanOrderQOC.setServerIp(DiaryUtil.SERVER_IP);
        requestInfoBeanOrderQOC.setSsl(false);
        requestInfoBeanOrderQOC.setPort("8084");
        requestInfoBeanOrderQOC.setActionPath("/convention2/diary/" + diaryId + "/json");
        requestInfoBeanOrderQOC.setRequestCookie(cookie);
        requestInfoBeanOrderQOC.setRequestMethod(com.common.dict.Constant2.REQUEST_METHOD_GET);
        // requestInfoBeanOrderQOC.setDependentRequest(requestInfoBeanLogin);
        requestInfoBeanOrderQOC.setCurrRequestParameterName("");
        requestInfoBeanOrderQOC.setPreRequestParameterName("");

        java.util.TreeMap parameterMapYfvm = new java.util.TreeMap();//请求参数
        parameterMapYfvm.put("appKey", "8669cfcf-93b9-4911-b855-4d6bc87d75df");
        parameterMapYfvm.put("appSecret", "sdxpke");
        parameterMapYfvm.put("client_secret", "sdxpke");
        requestInfoBeanOrderQOC.setRequestParameters(parameterMapYfvm);
        requestInfoBeanOrderQOC.updateRequestBody();

        com.common.bean.ResponseResult responseResultOrderxot = requestInfoBeanOrderQOC.request(); //new RequestPanel.ResponseResult(requestInfoBeanLogin).invoke();
        String responseOrderiFv = responseResultOrderxot.getResponseJsonResult();
        System.out.println("responseText:" + responseOrderiFv);
        System.out.println(com.io.hw.json.JSONHWUtil.jsonFormatter(responseOrderiFv));
        return responseOrderiFv;
    }

    public Diary getDiary() {
        return diary;
    }

    public void setDiary(Diary diary) {
        this.diary = diary;
    }

}

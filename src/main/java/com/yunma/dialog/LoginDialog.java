package com.yunma.dialog;

import com.common.bean.BaseResponseDto;
import com.common.util.SystemHWUtil;
import com.io.hw.json.HWJacksonUtils;
import com.swing.component.AssistPopupTextField;
import com.swing.dialog.GenericDialog;
import com.swing.dialog.toast.ToastMessage;
import com.yunma.bean.diary.LoginParamInfo;
import com.yunma.util.DiaryUtil;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.UnsupportedEncodingException;

public class LoginDialog extends GenericDialog {

    private final JPanel contentPanel = new JPanel();
    private AssistPopupTextField usernameTxtField;
    private JPasswordField passwordTextField;
    //    private String requestCookie;
//    private LoginInfo loginInfo;
    private JButton okButton;
    /**
     * Launch the application.
     */
    /*public static void main(String[] args) {
        try {
            LoginDialog dialog = new LoginDialog();
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dialog.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    /**
     * Create the dialog.
     */
    public LoginDialog(final String requestCookie, final LoginParamInfo loginParamInfo) {
//        this.loginInfo=loginInfo;
        setModal(true);
//        this.requestCookie = requestCookie;
//		setBounds(100, 100, 450, 300);
        setLoc(450, 160);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        GridBagLayout gbl_contentPanel = new GridBagLayout();
        gbl_contentPanel.columnWidths = new int[]{0, 0, 0};
        gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0};
        gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
        contentPanel.setLayout(gbl_contentPanel);
        JLabel lblUsername = new JLabel("username");
        GridBagConstraints gbc_lblUsername = new GridBagConstraints();
        gbc_lblUsername.insets = new Insets(0, 0, 5, 5);
        gbc_lblUsername.anchor = GridBagConstraints.WEST;
        gbc_lblUsername.gridx = 0;
        gbc_lblUsername.gridy = 0;
        contentPanel.add(lblUsername, gbc_lblUsername);
        usernameTxtField = new AssistPopupTextField();
        GridBagConstraints gbc_usernameeTxtField = new GridBagConstraints();
        gbc_usernameeTxtField.insets = new Insets(0, 0, 5, 0);
        gbc_usernameeTxtField.fill = GridBagConstraints.HORIZONTAL;
        gbc_usernameeTxtField.gridx = 1;
        gbc_usernameeTxtField.gridy = 0;
        contentPanel.add(usernameTxtField, gbc_usernameeTxtField);
        usernameTxtField.setColumns(10);
        JLabel lblPassword = new JLabel("password");
        GridBagConstraints gbc_lblPassword = new GridBagConstraints();
        gbc_lblPassword.anchor = GridBagConstraints.WEST;
        gbc_lblPassword.insets = new Insets(0, 0, 5, 5);
        gbc_lblPassword.gridx = 0;
        gbc_lblPassword.gridy = 1;
        contentPanel.add(lblPassword, gbc_lblPassword);
        passwordTextField = new JPasswordField();
        GridBagConstraints gbc_passwordTextField = new GridBagConstraints();
        gbc_passwordTextField.insets = new Insets(0, 0, 5, 0);
        gbc_passwordTextField.fill = GridBagConstraints.HORIZONTAL;
        gbc_passwordTextField.gridx = 1;
        gbc_passwordTextField.gridy = 1;
        contentPanel.add(passwordTextField, gbc_passwordTextField);
        passwordTextField.setColumns(10);
        JPanel panel = new JPanel();
        GridBagConstraints gbc_panel = new GridBagConstraints();
        gbc_panel.gridwidth = 2;
        gbc_panel.insets = new Insets(0, 0, 0, 5);
        gbc_panel.fill = GridBagConstraints.BOTH;
        gbc_panel.gridx = 0;
        gbc_panel.gridy = 2;
        contentPanel.add(panel, gbc_panel);
        JCheckBox rememberchMeeckBox = new JCheckBox("记住密码");
        panel.add(rememberchMeeckBox);
        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
        getContentPane().add(buttonPane, BorderLayout.SOUTH);
        {
            okButton = new JButton("OK");
            okButton.setActionCommand("OK");
            okButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    String username = usernameTxtField.getText2();
                    String password = new String(passwordTextField.getPassword());
                    okButton.setEnabled(false);
                    // 登录
                    com.common.bean.RequestSendChain requestInfoBeanOrderFVw = new com.common.bean.RequestSendChain();
                    requestInfoBeanOrderFVw.setServerIp(DiaryUtil.SERVER_IP);
                    requestInfoBeanOrderFVw.setSsl(false);
                    requestInfoBeanOrderFVw.setPort("8084");
                    requestInfoBeanOrderFVw.setActionPath("/convention2/user/json/login");
                    requestInfoBeanOrderFVw.setRequestCookie("");
                    requestInfoBeanOrderFVw.setCustomRequestContentType("");
                    requestInfoBeanOrderFVw.setRequestMethod(com.common.dict.Constant2.REQUEST_METHOD_GET);
                    // requestInfoBeanOrderFVw.setDependentRequest(requestInfoBeanLogin);
                    requestInfoBeanOrderFVw.setCurrRequestParameterName("");
                    requestInfoBeanOrderFVw.setPreRequestParameterName("");

                    requestInfoBeanOrderFVw.setRequestCookie(requestCookie);
                    java.util.TreeMap parameterMapNlrG = new java.util.TreeMap();//请求参数
                    try {
                        parameterMapNlrG.put("password", SystemHWUtil.getMD5(password.getBytes(SystemHWUtil.CHARSET_UTF)));
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    }
                    parameterMapNlrG.put("username", username);
                    requestInfoBeanOrderFVw.setRequestParameters(parameterMapNlrG);
                    requestInfoBeanOrderFVw.updateRequestBody();

                    com.common.bean.ResponseResult responseResultOrderNbd = requestInfoBeanOrderFVw.request(); //new RequestPanel.ResponseResult(requestInfoBeanLogin).invoke();
                    okButton.setEnabled(true);
                    String responseOrderuXf = responseResultOrderNbd.getResponseJsonResult();
                    System.out.println("responseText:" + responseOrderuXf);
                    String cookie = responseResultOrderNbd.getHeaderFieldVal(SystemHWUtil.KEY_HEADER_COOKIE);
                    System.out.println("cookie :" + cookie);

                    BaseResponseDto baseResponseDto = (BaseResponseDto) HWJacksonUtils.deSerialize(responseOrderuXf, BaseResponseDto.class);
                    if (baseResponseDto.result) {
                        if (null != loginParamInfo) {
                            loginParamInfo.setRequestCookie(cookie);
                            loginParamInfo.setPassword(password);
                            loginParamInfo.setUsername(username);
                        }
                        ToastMessage.toast("登录成功", 2000);
                        LoginDialog.this.dispose();
                    } else {
                        ToastMessage.toast(baseResponseDto.getMsgOfError(), 3000, Color.red);
                    }


                }
            });
            buttonPane.add(okButton);
            getRootPane().setDefaultButton(okButton);
        }
        {
            JButton cancelButton = new JButton("Cancel");
            cancelButton.setActionCommand("Cancel");
            cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    LoginDialog.this.dispose();
                }
            });
            buttonPane.add(cancelButton);
        }
        if (null != loginParamInfo) {
            usernameTxtField.setText(loginParamInfo.getUsername());
            passwordTextField.setText(loginParamInfo.getPassword());
        }
    }

}

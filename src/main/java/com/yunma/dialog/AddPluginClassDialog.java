package com.yunma.dialog;

import com.common.util.ClassFinder;
import com.string.widget.util.ValueWidget;
import com.swing.dialog.DialogUtil;
import com.swing.dialog.toast.ToastMessage;
import com.yunma.autotest.RequestPanel;
import com.yunma.util.TableUtil;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class AddPluginClassDialog extends JDialog {

    private static List<Class<?>> callbackClasses;
    private final JPanel contentPanel = new JPanel();
    private JLabel requestLabeInfo;
    private JComboBox puginClasscomboBox;
    /**
     * Launch the application.
     */
    /*public static void main(String[] args) {
		try {
			AddPluginClassDialog dialog = new AddPluginClassDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

    /**
     * Create the dialog.
     */
    public AddPluginClassDialog(final RequestPanel requestPanel) {
        setTitle("添加插件");
        setModal(true);
        setBounds(100, 100, 450, 120);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        GridBagLayout gbl_contentPanel = new GridBagLayout();
        gbl_contentPanel.columnWidths = new int[]{0, 0, 0};
        gbl_contentPanel.rowHeights = new int[]{0, 0, 0};
        gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
        contentPanel.setLayout(gbl_contentPanel);
        {
            JLabel label = new JLabel("插件类");
            GridBagConstraints gbc_label = new GridBagConstraints();
            gbc_label.insets = new Insets(0, 0, 5, 5);
            gbc_label.anchor = GridBagConstraints.WEST;
            gbc_label.gridx = 0;
            gbc_label.gridy = 0;
            contentPanel.add(label, gbc_label);
        }
        {
            puginClasscomboBox = new JComboBox();
            GridBagConstraints gbc_lpuginClasscomboBox = new GridBagConstraints();
            gbc_lpuginClasscomboBox.insets = new Insets(0, 0, 5, 0);
            gbc_lpuginClasscomboBox.fill = GridBagConstraints.HORIZONTAL;
            gbc_lpuginClasscomboBox.gridx = 1;
            gbc_lpuginClasscomboBox.gridy = 0;
            contentPanel.add(puginClasscomboBox, gbc_lpuginClasscomboBox);
        }
        {
            JLabel label = new JLabel("当前请求");
            GridBagConstraints gbc_label = new GridBagConstraints();
            gbc_label.anchor = GridBagConstraints.WEST;
            gbc_label.insets = new Insets(0, 0, 0, 5);
            gbc_label.gridx = 0;
            gbc_label.gridy = 1;
            contentPanel.add(label, gbc_label);
        }
        {
            requestLabeInfo = new JLabel("请求名称");
            GridBagConstraints gbc_requestLabeInfol = new GridBagConstraints();
            gbc_requestLabeInfol.anchor = GridBagConstraints.WEST;
            gbc_requestLabeInfol.gridx = 1;
            gbc_requestLabeInfol.gridy = 1;
            contentPanel.add(requestLabeInfo, gbc_requestLabeInfol);
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            JButton okButton = new JButton("安装");
            okButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    //设置插件类的名称,例如:"UpdateLicensePluginCallback"
                    requestPanel.getOriginalRequestInfoBean().setPluginClassSimpleName((String) puginClasscomboBox.getSelectedItem());
                    dispose();
                    ToastMessage.toast("安装插件成功", 2000);
                }
            });
            {
                String currentPluginClass = TableUtil.getPluginClassSimpleName(requestPanel);
                boolean noPlugin = ValueWidget.isNullOrEmpty(currentPluginClass);
                if (!noPlugin) {
                    JButton btnDelete = new JButton("卸载插件");
                    buttonPane.add(btnDelete);
                    btnDelete.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if (noPlugin) {
                                ToastMessage.toast("无插件可卸载", 3000, Color.RED);
                                return;
                            }
                            requestPanel.getOriginalRequestInfoBean().setPluginClassSimpleName(null);
                            dispose();
                            ToastMessage.toast("卸载插件成功", 2000);
                        }
                    });
                }
            }
            okButton.setActionCommand("OK");
            buttonPane.add(okButton);
            getRootPane().setDefaultButton(okButton);
            JButton cancelButton = new JButton("Cancel");
            cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    dispose();
                }
            });
            cancelButton.setActionCommand("Cancel");
            buttonPane.add(cancelButton);
            init(requestPanel);
        }
        DialogUtil.escape2CloseDialog(this);
    }

    private void init(RequestPanel requestPanel) {
        if (ValueWidget.isNullOrEmpty(callbackClasses)) {
            //获取 包 "com.yunma.dialog.plugin.impl" 下的所有class
            callbackClasses = ClassFinder.find("com.yunma.dialog.plugin.impl");
        }
        int size = callbackClasses.size();
        for (int i = 0; i < size; i++) {
            puginClasscomboBox.addItem(callbackClasses.get(i).getSimpleName());
        }
        String currentPluginClass = TableUtil.getPluginClassSimpleName(requestPanel);
        boolean noPlugin = ValueWidget.isNullOrEmpty(currentPluginClass);
        String message = null;
        if (noPlugin) {
            message = "暂无插件";
        } else {
            message = "当前插件:" + currentPluginClass;
        }
        requestLabeInfo.setText((ValueWidget.isNullOrEmpty(requestPanel.getRequestName()) ? requestPanel.getActionName() : requestPanel.getRequestName())
                + "(" + message + ")");
    }

}

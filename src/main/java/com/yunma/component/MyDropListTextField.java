package com.yunma.component;

import com.swing.component.DropListTextField;

import java.util.ArrayList;
import java.util.List;

/***
 * 继承DropListTextField
 * @author huangweii
 * 2015年10月28日
 */
public class MyDropListTextField extends DropListTextField {

    private static final long serialVersionUID = -6527320903816651789L;

    /***
     * 下拉菜单
     */
    @Override
    protected List<String> getDropListValue() {
        List<String> list = new ArrayList<String>();
        list.add("cia.chanapp.chanjet.com");
        list.add("house.yhskyc.com");
        list.add("blog.yhskyc.com");
        list.add("bsvc.chanapp.chanjet.com");
        list.add("i.chanjet.com");
        list.add("www.chanjet.com");
        list.add("store.chanjet.com");
        list.add("127.0.0.1");
        list.add("localhost");
        list.add("hbjltv.com");
        list.add("www.hbjltv.com");
        list.add("www.yunmasoft.com");
        list.add("www.yhskyc.com");
        list.add("cenc.chanapp.chanjet.com");
        list.add("convention.yunmasoft.com");
        return list;
    }

}

package com.yunma.component;

import com.swing.component.SimplePopupMenuTextArea;
import com.yunma.autotest.AutoTestPanel;
import com.yunma.listen.MyMenuBarActionListener;
import com.yunma.listen.PopupMenuListener;

import javax.swing.*;

public class MemoAssistPopupTextArea extends SimplePopupMenuTextArea {

    private static final long serialVersionUID = 36142324006714255L;
    private AutoTestPanel autoTestPanel;

    public MemoAssistPopupTextArea(AutoTestPanel autoTestPanel) {
        super();
        this.autoTestPanel = autoTestPanel;
        addPopupMenu2(textPopupMenu);
    }

    protected void addPopupMenu2(JPopupMenu textPopupMenu) {
        if (null == textPopupMenu) {
            return;
        }
        super.override4Extend(textPopupMenu);
        PopupMenuListener popupMenuListener = new PopupMenuListener(autoTestPanel, this);
        JMenuItem copyM = new JMenuItem("增加备忘");
        copyM.addActionListener(popupMenuListener);
        textPopupMenu.add(copyM);

        JMenuItem maximizeM = new JMenuItem("最大化");
        maximizeM.addActionListener(popupMenuListener);
        textPopupMenu.add(maximizeM);

        JMenuItem code2tokenM = new JMenuItem(MyMenuBarActionListener.Label_code2token);
        code2tokenM.addActionListener(popupMenuListener);
        textPopupMenu.add(code2tokenM);
    }

}

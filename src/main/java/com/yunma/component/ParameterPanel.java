package com.yunma.component;

import com.common.util.SystemHWUtil;
import com.io.hw.awt.color.CustomColor;
import com.string.widget.util.ValueWidget;
import com.swing.component.AssistPopupTextField;

import javax.swing.*;
import java.awt.*;

/**
 * Created by 黄威 on 03/11/2016.<br >
 */
public class ParameterPanel extends JPanel {
    private AssistPopupTextField parameterNameTextField;
    private AssistPopupTextField parameterValTextField;
    private String key;
    private String val;

    public ParameterPanel() {

    }

    public ParameterPanel(String key, String val) {
        this.key = key;
        this.val = val;
    }

    public void layout2() {
        layout2(null, null);
    }

    public void layout2(String key, String val) {
        if (null != key) {
            this.key = key;
        }
        if (null != val) {
            this.val = val;
        }
        JPanel panel_2 = this;
        setBackground(CustomColor.getMoreLightColor());
        GridBagLayout gbl_panel_2 = new GridBagLayout();
        gbl_panel_2.columnWidths = new int[]{0, 180, 55, 0, 0};
        gbl_panel_2.rowHeights = new int[]{0, 0};
        gbl_panel_2.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
        gbl_panel_2.rowWeights = new double[]{0.0, Double.MIN_VALUE};
        panel_2.setLayout(gbl_panel_2);

        JLabel label_2 = new JLabel("参数名:");
        GridBagConstraints gbc_label_2 = new GridBagConstraints();
        gbc_label_2.insets = new Insets(0, 0, 0, 5);
        gbc_label_2.anchor = GridBagConstraints.WEST;
        gbc_label_2.gridx = 0;
        gbc_label_2.gridy = 0;
        panel_2.add(label_2, gbc_label_2);

        parameterNameTextField = new AssistPopupTextField();
        Color backColor = CustomColor.getMoreLightColor();
        parameterNameTextField.setBackground(backColor);
        parameterNameTextField.placeHolder("参数名称");
        if (!ValueWidget.isNullOrEmpty(this.key)) {
            parameterNameTextField.setText(this.key);
        }
        GridBagConstraints gbc_parameterNameTextField = new GridBagConstraints();
        gbc_parameterNameTextField.insets = new Insets(0, 0, 0, 5);
        gbc_parameterNameTextField.fill = GridBagConstraints.HORIZONTAL;
        gbc_parameterNameTextField.gridx = 1;
        gbc_parameterNameTextField.gridy = 0;
        panel_2.add(parameterNameTextField, gbc_parameterNameTextField);
        parameterNameTextField.setColumns(10);

        parameterValTextField = new AssistPopupTextField();
        parameterValTextField.placeHolder("参数值");
        parameterValTextField.setBackground(backColor);
        if (!ValueWidget.isNullOrEmpty(this.val)) {
            parameterValTextField.setText(this.val);
        }
        GridBagConstraints gbc_parameterValTextField = new GridBagConstraints();
        gbc_parameterValTextField.fill = GridBagConstraints.HORIZONTAL;
        gbc_parameterValTextField.gridx = 3;
        gbc_parameterValTextField.gridy = 0;
        panel_2.add(parameterValTextField, gbc_parameterValTextField);
        parameterValTextField.setColumns(10);
    }

    public String getKey() {
        return parameterNameTextField.getText2();
    }

    public String getVal() {
        return parameterValTextField.getText2();
    }

    public void cleanUp() {
        this.parameterNameTextField.setText(SystemHWUtil.EMPTY);
        this.parameterValTextField.setText(SystemHWUtil.EMPTY);
    }
}

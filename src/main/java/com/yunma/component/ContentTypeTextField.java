package com.yunma.component;

import com.common.util.SystemHWUtil;
import com.swing.component.DropListTextField;

import java.util.ArrayList;
import java.util.List;

/***
 * 继承DropListTextField
 * @author huangweii
 * 2016年8月16日
 */
public class ContentTypeTextField extends DropListTextField {

    private static final long serialVersionUID = -6527320903816651789L;

    /***
     * 下拉菜单
     */
    @Override
    protected List<String> getDropListValue() {
        List<String> list = new ArrayList<String>();
        list.add(SystemHWUtil.CONTENTTYPE_X_WWW_FORM_URLENCODED);
        list.add(SystemHWUtil.RESPONSE_CONTENTTYPE_JAVASCRIPT);
        list.add(SystemHWUtil.RESPONSE_CONTENTTYPE_JSON_UTF);
        list.add(SystemHWUtil.RESPONSE_CONTENTTYPE_JSON_GBK);
        list.add(SystemHWUtil.RESPONSE_CONTENTTYPE_XML_UTF);
        list.add(SystemHWUtil.RESPONSE_CONTENTTYPE_JAVASCRIPT2);
        list.add(SystemHWUtil.RESPONSE_CONTENTTYPE_PLAIN);
        list.add(SystemHWUtil.CONTENTTYPE_OCTET_STREAM);
        list.add("multipart/form-data");
        return list;
    }

}

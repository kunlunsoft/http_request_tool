package com.yunma.component;

import com.common.bean.RequestInfoBean;

import javax.swing.*;

/**
 * Created by 黄威 on 16/10/2016.<br >
 * 保存时,可以指定要保存的请求panel
 */
public class RequestInfoCheckBox extends JCheckBox {
    private RequestInfoBean requestInfoBean;

    public RequestInfoCheckBox(String text) {
        super(text);
    }

    public RequestInfoCheckBox(String text, boolean selected) {
        super(text, selected);
    }

    public RequestInfoCheckBox(String text, RequestInfoBean requestInfoBean) {
        super(text);
        this.requestInfoBean = requestInfoBean;
    }

    public RequestInfoCheckBox(String text, boolean selected, RequestInfoBean requestInfoBean) {
        super(text, selected);
        this.requestInfoBean = requestInfoBean;
    }

    public RequestInfoBean getRequestInfoBean() {
        return requestInfoBean;
    }

    public void setRequestInfoBean(RequestInfoBean requestInfoBean) {
        this.requestInfoBean = requestInfoBean;
    }
}

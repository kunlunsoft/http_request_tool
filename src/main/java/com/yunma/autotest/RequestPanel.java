package com.yunma.autotest;

import com.common.bean.ParameterIncludeBean;
import com.common.bean.*;
import com.common.bean.exception.LogicBusinessException;
import com.common.dict.Constant2;
import com.common.util.RequestUtil;
import com.common.util.SystemHWUtil;
import com.common.util.WebServletUtil;
import com.common.util.WindowUtil;
import com.http.bean.HttpResponseResult;
import com.http.util.HttpSocketUtil;
import com.io.hw.awt.color.CustomColor;
import com.io.hw.file.util.FileUtils;
import com.io.hw.json.HWJacksonUtils;
import com.io.hw.json.JSONHWUtil;
import com.string.widget.util.RandomUtils;
import com.string.widget.util.RegexUtil;
import com.string.widget.util.ValueWidget;
import com.swing.callback.ActionCallback;
import com.swing.callback.Callback2;
import com.swing.component.*;
import com.swing.config.ConfigParam;
import com.swing.dialog.CustomDefaultDialog;
import com.swing.dialog.DialogUtil;
import com.swing.dialog.toast.ToastCallback;
import com.swing.dialog.toast.ToastMessage;
import com.swing.menu.MenuUtil2;
import com.swing.messagebox.GUIUtil23;
import com.swing.table.*;
import com.yunma.bean.PreRequestCallbackInfo;
import com.yunma.bean.RequestDefaultBean;
import com.yunma.bean.RequestParameterTabInfo;
import com.yunma.callback.RequestCallBack;
import com.yunma.component.ContentTypeTextField;
import com.yunma.component.MemoAssistPopupTextArea;
import com.yunma.component.MyDropListTextField;
import com.yunma.dialog.HttpHeadPanel;
import com.yunma.dialog.plugin.PluginInputDialog;
import com.yunma.dialog.plugin.callback.PluginCallback;
import com.yunma.listen.MyTableMenuListener;
import com.yunma.listen.OptionalParametersListener;
import com.yunma.listen.PopupMenuListener;
import com.yunma.util.TableUtil;
import net.sf.json.JSONException;
import org.apache.commons.collections.map.ListOrderedMap;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rtextarea.RTextArea;
import org.jb2011.lnf.beautyeye.ch3_button.BEButtonUI;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.DefaultCaret;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.*;
import java.util.List;

public class RequestPanel extends MyNamePanel {

    public static final String ACTION_COMMAND_RUN = "运行";
    public static final String LABEL_RUN = ACTION_COMMAND_RUN + "(Alt+Shift+X)";
    public static final String ACTION_COMMAND_COPY_REQUEST_PARAMETER = "复制请求要素";
    public static final String ACTION_COMMAND_COPY_RESPONSE = "复制应答结果";
    public static final String ACTION_COMMAND_COPY_REQUEST_INFO = "复制请求信息(整个请求)";
    public static final String ACTION_COMMAND_PASTE_REQUEST_INFO = "黏贴请求信息(整个请求)";
    public static final int tableScroll_height = 100;
    public static final int SINGLE_ROW_HEIGHT = 30;
    static final String[] columnNames = {"参数名", "参数值", "是否包含"};
    private static final long serialVersionUID = 4752550602394021590L;
    /***
     * url编码采用的编码方式
     */
    private static final String urlEncodeParameterCharset = SystemHWUtil.CHARSET_UTF;
    private static int maxTextLength = 3000;
    /***
     * 是否在进行界面设计
     */
    private static boolean debug = false;
    /***
     * 上一次Tab索引
     */
    private final RequestParameterTabInfo requestParameterTabInfo = new RequestParameterTabInfo();
    public AutoTestPanel autoTestPanel;
    /***
     * 自定义快捷键及响应事件
     */
    Map<String, ActionCallback> actionCallbackMap;
    /***
     * 请求方式,目前支持六种
     */
    private JComboBox<String> requestMethodComboBox_2;
    private AssistPopupTextField serverIpTextField_3;
    /***
     * 端口号
     */
    private AssistPopupTextField portTextField_6;
    /***
     * action 的访问路径
     */
    private AssistPopupTextField actionPathTextField_7;
    /***
     * 前置参数名
     */
    private AssistPopupTextField txtAuthcode;
    /***
     * 本请求参数名称
     */
    private AssistPopupTextField selfParameterTextField_3;
    private AssistPopupTextField requestIdTextField_5;
    /***
     * 前置请求的id
     */
    private JComboBox<String> preRequestIdComboBox_2;
    /***
     * 请求的cookie
     */
//    private MemoAssistPopupTextArea cookieTextArea;
    /***
     * 请求的编码
     */
    private JComboBox<String> encodingComboBox;
    private AssistPopupTextField requestNameTextField_4;
    /***
     * 是否执行前置请求
     *
     * @param autoTestPanel2
     */
    private JCheckBox preRequestCheckBox;
    private JCheckBox encodingCheckbox_8;
    /***
     * 是否应用HTTP默认值
     */
    private JCheckBox isApplyDefaultHTTPCheckBox;
    /***
     * 请求体是否是json字符串(不是标准的query string)<br />
     * {
     "priceInfo": [
     {
     "priceId":10487,
     "startTime": "2016-12-20 16:29:47",
     "endTime": "2018-12-20 16:29:47",
     "priceOptionAmounts":[
     {
     "priceOptionId":7,
     "qty":10
     },
     {
     "priceOptionId":23,
     "qty":10
     }
     ]
     }
     ]

     }
     */
    private JCheckBox chckbxjson;
    /***
     * 最外层的split<br />
     * 上面是请求信息(包括splitPane_inner)和http请求结果
     */
    private JSplitPane splitPane_out;
    /***
     * 内层的split<br />
     * 包括请求名称,服务器ip 和请求参数(请求头)
     */
    private JSplitPane splitPane_inner;
    private JTextArea urlDecodeTA;
    private AssistPopupTextField textField;
    private AssistPopupTextField textField_1;
    private AssistPopupTextField countTextField_2;
    /***
     * 请求发送后,返回的应答(未格式化).<br />
     * 在resultTextPane 的下面
     */
    private MemoAssistPopupTextArea respTextArea_9;
    /***
     * 请求体
     */
    private HttpRequestBodyTextArea requestBodyDataTA;
    /***
     * 请求的结果,比如返回的状态码
     */
    private MemoAssistPopupTextArea resultTextPane;
    /***
     * 前置请求信息
     */
    private PreRequestCallbackInfo preRequestCallbackInfo;
    /***
     * 是否已经被回调
     */
//    private boolean isCallbacked;
    private RequestPanel afterRequestPanel;
    /***
     * 当前请求的id
     */
    private String selfRequestId;
    /***
     * 前置请求的参数值(NOTE:不是参数名)
     */
    private Object preRequestParameterValue;
    /***
     * "请求参数"表格
     */
    private JTable parameterTable_1;
    /***
     * 请求发送成功之后的应答体(json格式)
     */
    private String responseJsonResult;
    /***
     * 当前请求panel在tab中的序号,用于设置tab的title
     */
    private int tabIndex2;
    /***
     * "格式化的json"
     */
    private RTextArea jsonFormattedTextArea_9;
    /**
     * 初步格式化, 保留\",即保留 json 字符串
     */
    private RTextArea jsonFormattedTextArea_10;
    /***
     * 是否对参数进行URL编码
     */
    private JCheckBox isEncodeParameterCheckbox;
    /***
     * 是否复制请求结果中指定字段的值
     */
    private JCheckBox copyFieldCheckbox;
    /***
     * "请求参数","请求体","已解码"
     */
    private JTabbedPane tabbedPane_1;
    /***
     * 格式化json的panel,在最下面的第二个标签页
     */
    private JPanel jsonFormattedPane;
    private RequestPanel beforeRequestPanel = null;
    /***
     * JCheckBox的默认颜色
     */
    private Color defaultForegroundColor;
    /***
     * 文本框的背景颜色
     */
    private Color defaultBackgroundColor;
    private AssistPopupTextField timingSecondTextField_2;
    private JScrollPane tableScroll;
    /***
     * 是否每次都请求前置请求
     */
    private JCheckBox everytimeCheckBox;
    private JButton timinButton_3;
    /***
     * 打开前置请求
     */
    private JButton openPreRequestButton_3;
    /***
     * 单个请求的备忘
     */
    private AssistPopupTextArea singleRequestNoteTA;
    /***
     * 请求的协议,是否是https
     */
    private JCheckBox chckbxSsl;
    private ContentTypeTextField contentTypeTextField_2;
    private AssistPopupTextField fullUrlTextField;
    //别名,用于搜索
    private AssistPopupTextField aliasTextField;
    private RequestInfoBean originalRequestInfoBean;
    private boolean rended = false;
    /***
     * "http请求结果","格式化的json","备忘" tab标签页
     */
    private JTabbedPane tabbedPane_22;
    /***
     * 请求参数表格所在的scroll
     */
    private JScrollPane panel_7JS2;
    /***
     * 请求参数模板
     */
    private List<RequestParameterTemplate> requestParameterTemplates;
    /***
     * "请求头"
     */
    private HttpHeadPanel httpHeadPanel;
    /**
     * "本请求参数"<br />
     * "本请求头参数"
     */
    private JComboBox<String> thisParameterCombobox;
    /***
     * "请求参数","请求头"标签页
     */
    private JTabbedPane tabbedPane_parameterHeader;
    /***
     * 要复制的属性名称<br />
     * 从 json字符串中解析
     */
    private AssistPopupTextField copyField;
    /***
     * "连续发送次数"
     */
    private JCheckBox continueSendCheckBox;
    /**
     * POST请求时,是否把请求体中的加号进行URL编码(只对加号进行URL编码)<br />
     * 正常情况下禁止选中
     */
    private JCheckBox plus2percent2BWhenPostCheckBox;
    private JButton runButton_55;

    public RequestPanel() {
//        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
//        contentPane.setLayout(new BorderLayout(0, 0));
//        setContentPane(contentPane);
        setSize(800, 600);
//        if (debug) {
        initialize3();
        setVisible(true);
//        }
    }

    /**
     * Create the frame. <br>
     * 非常耗时
     */
    public RequestPanel(AutoTestPanel autoTestPanel2, Map<String, ActionCallback> actionCallbackMap2
    ) {
        this.autoTestPanel = autoTestPanel2;
//        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
//        contentPane.setLayout(new BorderLayout(0, 0));
//        setContentPane(contentPane);
        if (debug) {
            setSize(800, 600);
        }
        this.actionCallbackMap = actionCallbackMap2;
        if (debug) {
            initialize3();
        }
    }

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        RequestPanel requestPan = new RequestPanel();
        requestPan.setVisible(true);
    }

    public static boolean isContains(List<ParameterIncludeBean> parameters, String key) {
        int index = TableUtil.isContains2(parameters, key);
        return (index != SystemHWUtil.NEGATIVE_ONE);
    }

    public static MouseInputListener getMouseInputListener(final JTable jTable, final RequestPanel currentRequestPanel
            , final AutoTestPanel autoTestPanel2) {

        return new MouseInputListener() {
            public int count;
            private java.util.Timer timer;

            public void mouseClicked(MouseEvent e) {
                /*if(e.getClickCount()==1){
                    final int rowCount = jTable.getSelectedRow();
                	final int columnCount=jTable.getSelectedColumn();
            		int modifiers = e.getModifiers();
            		modifiers -= MouseEvent.BUTTON3_MASK;
                	modifiers |= MouseEvent.FOCUS_EVENT_MASK;
                	MouseEvent ne = new MouseEvent(e.getComponent(), e.getID(),
    						e.getWhen(), modifiers, e.getX(), e.getY(),
    						2, false);
//                	processEvent(ne);
                	jTable.editCellAt(rowCount, columnCount,ne);
//                	CellEditor cellEditor=jTable.getCellEditor(rowCount, columnCount);
//                	cellEditor.shouldSelectCell(ne);
                	jTable.dispatchEvent(ne);
            	}*/
//                System.out.println("mouseClicked" + (count++));
                MenuUtil2.processEvent(e, jTable);

            }

            /***
             * in order to trigger Left-click the event
             */
            public void mousePressed(MouseEvent e) {
                MenuUtil2.processEvent(e, jTable);// is necessary!!!
//                System.out.println("mousePressed"+(count++));
            }

            public void mouseReleased(final MouseEvent e) {
                //
                final int rowCount = jTable.getSelectedRow();
                final int columnCount = jTable.getSelectedColumn();
                final RequestPanel currentRequestPanel2 = currentRequestPanel;
                if (e.getButton() == MouseEvent.BUTTON3) {// right click
//                	processEvent(e);

                    // System.out.println("row:" + rowCount);
                    //右键点击表格时,点击的单元格,正常情况返回的是String
//                	System.out.println("rowCount:"+rowCount);
//                	System.out.println("columnCount:"+columnCount);
                    if (rowCount == -1 || columnCount == -1) {
                        return;
                    }
                    if (e.getClickCount() > 1) {//右键双击
                        if (null != timer) {
                            timer.cancel();
                            timer = null;
                        } else {
                            return;//说明已经执行了方法rightClickAction
                        }
                        //增加行的高度
                        int rowAddedHeight = 40;
                        int rowHeight = jTable.getRowHeight(rowCount);//见类MyTableMenuListener 的232行
                        jTable.setRowHeight(rowCount, rowHeight + rowAddedHeight);
                        jTable.repaint();
                        currentRequestPanel.addTableHeight(rowAddedHeight);
                        autoTestPanel2.updateUI2();
                    } else {//启动定时器
                        if (null == timer) {
                            timer = new java.util.Timer();
                        }
                        timer.schedule(new java.util.TimerTask() {
                            @Override
                            public void run() {
                                timer = null;
                                rightClickAction(e, rowCount, columnCount, currentRequestPanel2, autoTestPanel2, jTable);
                            }
                        }, Constant2.RIGHT_CLICK_DELAY);
                    }
//                    processEvent(e);
                } else if (e.getButton() == MouseEvent.BUTTON2) {//鼠标中键
                    TableUtil3.tableCellMidClick(e, jTable, currentRequestPanel.getConfigParam());
                }

                /*else if (e.getButton() == MouseEvent.BUTTON1&& e.getClickCount()==1){
                    System.out.println("左键");
                	int modifiers = e.getModifiers();
                	modifiers |= MouseEvent.FOCUS_EVENT_MASK;
                	MouseEvent ne = new MouseEvent(e.getComponent(), e.getID(),
							e.getWhen(), modifiers, e.getX(), e.getY(),
							2, false);

//                	processEvent(ne);
//                	jTable.editCellAt(rowCount, columnCount,ne);
//                	CellEditor cellEditor=jTable.getCellEditor(rowCount, columnCount);
//                	cellEditor.shouldSelectCell(ne);
                	jTable.dispatchEvent(ne);
                }*/
//                System.out.println("mouseReleased"+(count++));
            }

            public void mouseEntered(MouseEvent e) {
                MenuUtil2.processEvent(e, jTable);
            }

            public void mouseExited(MouseEvent e) {
                MenuUtil2.processEvent(e, jTable);
            }

            public void mouseDragged(MouseEvent e) {
                MenuUtil2.processEvent(e, jTable);
            }

            public void mouseMoved(MouseEvent e) {
                MenuUtil2.processEvent(e, jTable);
            }

        };
    }

    public static void rightClickAction(MouseEvent e, int rowCount, int columnCount, RequestPanel currentRequestPanel2, AutoTestPanel autoTestPanel, JTable jTable) {
        JPopupMenu popupmenu = new JPopupMenu();
        JMenuItem runM = new JMenuItem(ACTION_COMMAND_RUN);
        JMenuItem cleanUp_runM = new JMenuItem("清空后再运行");
        JMenuItem copyParameterM = new JMenuItem(ACTION_COMMAND_COPY_REQUEST_PARAMETER);
        JMenuItem copyResponseM = new JMenuItem(ACTION_COMMAND_COPY_RESPONSE);
        JMenuItem copyParameter2JsonM = new JMenuItem("复制请求要素为json");
        MenuUtil2.strongMenuItem(copyParameter2JsonM);

        JMenuItem copyAccessTokenM = new JMenuItem("复制access_token");
        JMenuItem md5M = new JMenuItem(MenuUtil2.ACTION_CREATE_MD5);
        JMenuItem deMd5M = new JMenuItem(MenuUtil2.ACTION_MD5_DECODE);
        JMenuItem cleanUpCellM = new JMenuItem("清空单元格");
        cleanUpCellM.setActionCommand(MenuUtil2.ACTION_STR_CLEANUP_CELL);

        JMenuItem urlEncoderM = new JMenuItem(MenuUtil2.ACTION_URL_ENCODE);
        JMenuItem urlDecoderM = new JMenuItem(MenuUtil2.ACTION_URL_DECODE);
        JMenuItem copyCellM = new JMenuItem(MenuUtil2.ACTION_STR_COPY_CELL);
        JMenuItem pasteCellM = new JMenuItem(MenuUtil2.ACTION_STR_PASTE_CELL);
        JMenuItem replaceChinaQuotesM = new JMenuItem("替换中文双引号");
        JMenuItem sendPostM = new JMenuItem("打开网页发送POST请求");
        JMenuItem copyPanelM = new JMenuItem("复制请求panel");
        JMenuItem deleteCurrentPanelM = new JMenuItem("删除当前请求panel");
        JMenuItem copyKeyValM = new JMenuItem("复制key:val");
        JMenuItem rendTableM = new JMenuItem("rend table");
        JMenuItem cleanTableM = new JMenuItem("clean table");

        JMenuItem cleanResultM = new JMenuItem("清空结果");

        JMenuItem plusRowHeightM = new JMenuItem("增加row高度");
        JMenuItem reduceRowHeightM = new JMenuItem("减少row高度");
        JMenuItem maximizingTableM = new JMenuItem(MyTableMenuListener.ACTION_maximize_TABLE);
        JMenuItem addParameterM = new JMenuItem("添加参数");
        JMenuItem addParameterFromClipM = new JMenuItem("从剪切板添加参数");
        JMenuItem removeParameterM = new JMenuItem("删除参数");
        JMenuItem removeMoreParameterM = new JMenuItem("删除多个参数");
        JMenuItem removeAllParameterM = new JMenuItem("删除全部参数");
        MenuUtil2.strongMenuItem(removeMoreParameterM);
        removeMoreParameterM.setForeground(Color.red);

        JMenuItem getParameterTableHeightM = new JMenuItem("获取表格高度");
        JMenuItem getTableScrollPaneHeightM = new JMenuItem("获取tableScroll高度");
        JMenuItem adaptTableScrollPaneHeightM = new JMenuItem("自适应表格split高度");
        JMenuItem splitPaneHeightM = new JMenuItem("splitPane位置");
        JMenuItem splitPaneScrollBarHeightM = new JMenuItem("splitPane滚动条位置");
        JMenuItem textAreaInCellHeightM = new JMenuItem("获取textarea高度");
        // JMenuItem editM=new JMenuItem("edit");
        if (ValueWidget.isNullOrEmpty(currentRequestPanel2)) {
            currentRequestPanel2 = autoTestPanel.getCurrentRequestPanel();
        }
        MyTableMenuListener yMenuActionListener = new MyTableMenuListener(jTable, rowCount, columnCount, currentRequestPanel2);
        runM.addActionListener(yMenuActionListener);
        cleanUp_runM.addActionListener(yMenuActionListener);
        copyParameterM.addActionListener(yMenuActionListener);
        copyResponseM.addActionListener(yMenuActionListener);
        copyParameter2JsonM.addActionListener(yMenuActionListener);
        copyAccessTokenM.addActionListener(yMenuActionListener);
        cleanResultM.addActionListener(yMenuActionListener);
        plusRowHeightM.addActionListener(yMenuActionListener);
        reduceRowHeightM.addActionListener(yMenuActionListener);
        maximizingTableM.addActionListener(yMenuActionListener);
        addParameterM.addActionListener(yMenuActionListener);
        addParameterFromClipM.addActionListener(yMenuActionListener);
        removeParameterM.addActionListener(yMenuActionListener);
        removeMoreParameterM.addActionListener(yMenuActionListener);
        removeAllParameterM.addActionListener(yMenuActionListener);
        getParameterTableHeightM.addActionListener(yMenuActionListener);
        getTableScrollPaneHeightM.addActionListener(yMenuActionListener);
        adaptTableScrollPaneHeightM.addActionListener(yMenuActionListener);
        splitPaneHeightM.addActionListener(yMenuActionListener);
        splitPaneScrollBarHeightM.addActionListener(yMenuActionListener);
        textAreaInCellHeightM.addActionListener(yMenuActionListener);
        md5M.addActionListener(yMenuActionListener);
        deMd5M.addActionListener(yMenuActionListener);
        cleanUpCellM.addActionListener(yMenuActionListener);
        urlEncoderM.addActionListener(yMenuActionListener);
        urlDecoderM.addActionListener(yMenuActionListener);
        copyCellM.addActionListener(yMenuActionListener);
        pasteCellM.addActionListener(yMenuActionListener);
        replaceChinaQuotesM.addActionListener(yMenuActionListener);
        sendPostM.addActionListener(yMenuActionListener);
        copyPanelM.addActionListener(yMenuActionListener);
        deleteCurrentPanelM.addActionListener(yMenuActionListener);
        copyKeyValM.addActionListener(yMenuActionListener);
        rendTableM.addActionListener(yMenuActionListener);
        cleanTableM.addActionListener(yMenuActionListener);

        popupmenu.add(cleanUp_runM);
        popupmenu.add(runM);
        popupmenu.add(copyParameterM);
        popupmenu.add(copyResponseM);
        popupmenu.add(copyParameter2JsonM);
        popupmenu.add(copyAccessTokenM);
        popupmenu.add(md5M);
        popupmenu.add(deMd5M);
        popupmenu.add(cleanUpCellM);
        popupmenu.add(urlEncoderM);
        popupmenu.add(urlDecoderM);
        popupmenu.add(copyCellM);
        popupmenu.add(pasteCellM);
        popupmenu.add(replaceChinaQuotesM);
        popupmenu.add(sendPostM);
        popupmenu.add(copyPanelM);
        popupmenu.add(deleteCurrentPanelM);
        popupmenu.add(copyKeyValM);
        popupmenu.add(rendTableM);
        popupmenu.add(cleanTableM);
        popupmenu.add(cleanResultM);
        popupmenu.add(plusRowHeightM);
        popupmenu.add(reduceRowHeightM);
        popupmenu.add(maximizingTableM);
        popupmenu.add(addParameterM);
        popupmenu.add(addParameterFromClipM);
        popupmenu.add(removeParameterM);
        popupmenu.add(removeMoreParameterM);
        popupmenu.add(removeAllParameterM);
        popupmenu.add(getParameterTableHeightM);
        popupmenu.add(getTableScrollPaneHeightM);
        popupmenu.add(adaptTableScrollPaneHeightM);
        popupmenu.add(splitPaneHeightM);
        popupmenu.add(splitPaneScrollBarHeightM);
        popupmenu.add(textAreaInCellHeightM);
        //让右键菜单往右移动一点
        popupmenu.show(e.getComponent(), e.getX() + 15, e.getY());
    }

    /***
     * 如果参数中含有特殊字符&,空格或中文字符,则强制URL编码<br>
     * 为什么?因为http参数就是通过& 分隔的
     *
     * @param parameterIncludeBean
     */
    public static void urlencodeParameter(ParameterIncludeBean parameterIncludeBean) {
        String val = parameterIncludeBean.getValue();
        if (WebServletUtil.isShouldURLEncode(val)) {
            try {
                val = URLEncoder.encode(val, SystemHWUtil.CHARSET_UTF);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            parameterIncludeBean.setValue(val);
        }
    }

    /***
     * 增加参数后,使滚动条自动定位到底部
     * @param panel_7JS2
     */
    public static void scrollToBottom(JScrollPane panel_7JS2) {
        int maxHeight = panel_7JS2.getVerticalScrollBar().getMaximum();
//                System.out.println(vscrollHeight);
        panel_7JS2.getViewport().setViewPosition(new Point(0, maxHeight));
        panel_7JS2.updateUI();
    }

    private static int getTextAreaHeight(JTextComponent textComponent) {
        int height = textComponent.getPreferredSize().height;
        int height2 = textComponent.getHeight();
        if (height2 > height) {
            height = height2;
        }
        return height;
    }

    public static JTextComponent getCellTextComponent(Object cellVal) {
        JTextComponent textComponent = null;
        if (cellVal instanceof JScrollPane) {
            JScrollPane js = (JScrollPane) cellVal;
            textComponent = (JTextComponent) js.getViewport().getComponent(0);
        } else {
            textComponent = (JTextComponent) cellVal;
        }
        return textComponent;
    }

    private static File getDefaultSelectedFolder(String saveCodeDefaultFolderStr) {
        File defaultSelectFolder;
        if (ValueWidget.isNullOrEmpty(saveCodeDefaultFolderStr)) {
            defaultSelectFolder = null;
        } else {
            defaultSelectFolder = new File(saveCodeDefaultFolderStr);
            if (defaultSelectFolder.exists() && defaultSelectFolder.isDirectory()) {
                defaultSelectFolder = new File(saveCodeDefaultFolderStr + "请输入文件名");
            }
        }
        return defaultSelectFolder;
    }

    private static boolean onlineConfirm(RequestInfoBean requestInfoBean, String servletPath) {
        if (requestInfoBean.getServerIp().startsWith("store.chanjet.com")
                && (servletPath.contains("/logview/setConfig") || servletPath.contains("/logview/setCache"))) {
            //因为线上操作是危险操作,所以增加确认
            int result = JOptionPane.showConfirmDialog(null, "这是线上接口 ,确认要调用吗 ?", "确认",
                    JOptionPane.OK_CANCEL_OPTION);
            if (result != JOptionPane.OK_OPTION) {
                return true;
            }
        }
        return false;
    }

    private static void replacePreParamAlert(AssistPopupTextField selfParameterTextField_3) {
        final Color defaultColor = selfParameterTextField_3.getBackground();
        final String text = selfParameterTextField_3.getText2();
        new Thread(new Runnable() {
            @Override
            public void run() {

                selfParameterTextField_3.setBackground(Color.red);
                selfParameterTextField_3.setForeground(Color.white);
                selfParameterTextField_3.setText("替换 :" + text);
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                selfParameterTextField_3.setText(text);
                selfParameterTextField_3.setBackground(defaultColor);
                selfParameterTextField_3.setForeground(Color.black);
            }
        }).start();
    }

    public void initialize3() {
        if (rended) {
            System.out.println("请求panel已经渲染");
            return;
        }
//        JPanel contentPane =new JPanel();
//        this.getContentPane().add(contentPane,BorderLayout.CENTER);
        layout3(this);
        if (!debug) {
            layoutTable();
            rendTable();
            bindEvent();
            rended = true;
        }
    }

    public void layout3(Container contentPane) {
        setLayout(new BorderLayout(0, 0));
        splitPane_out = new JSplitPane();

        splitPane_out.setDividerSize(5);
        splitPane_out.setOrientation(JSplitPane.VERTICAL_SPLIT);
        splitPane_out.setOneTouchExpandable(true);

        splitPane_inner = new JSplitPane();//请求ID 和请求参数之间
        if (debug || autoTestPanel.isDebug()) {
            splitPane_inner.setDividerLocation(500);
        } else {
            splitPane_inner.setDividerLocation(getDividerHeight()[0]);
        }

        splitPane_inner.setDividerSize(5);
        splitPane_inner.setContinuousLayout(true);
        splitPane_inner.setOneTouchExpandable(true);
        splitPane_inner.setBackground(new Color(186, 238, 186));
        contentPane.add(splitPane_out);
        splitPane_inner.setOrientation(JSplitPane.VERTICAL_SPLIT);
        splitPane_out.setLeftComponent(splitPane_inner);

        JPanel panel_7 = new JPanel();
        JScrollPane panel_7JS = new JScrollPane(panel_7);
        splitPane_inner.setLeftComponent(panel_7JS);
        GridBagLayout gbl_panel_7 = new GridBagLayout();
        gbl_panel_7.columnWidths = new int[]{11, 54, 0, 0, 0, 0};
        gbl_panel_7.rowHeights = new int[]{0, 15, 0, 0, 0, 0, 0, 0};
        gbl_panel_7.columnWeights = new double[]{1.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
        gbl_panel_7.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        panel_7.setLayout(gbl_panel_7);

        JPanel panel_8 = new JPanel();
        GridBagConstraints gbc_panel_8 = new GridBagConstraints();
        gbc_panel_8.gridwidth = 5;
        gbc_panel_8.insets = new Insets(0, 0, 5, 0);
        gbc_panel_8.fill = GridBagConstraints.BOTH;
        gbc_panel_8.gridx = 0;
        gbc_panel_8.gridy = 0;
        panel_7.add(panel_8, gbc_panel_8);
        GridBagLayout gbl_panel_8 = new GridBagLayout();
        gbl_panel_8.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
        gbl_panel_8.rowHeights = new int[]{0, 0};
        gbl_panel_8.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
        gbl_panel_8.rowWeights = new double[]{0.0, Double.MIN_VALUE};
        panel_8.setLayout(gbl_panel_8);

        JLabel label_5 = new JLabel("请求名称");
        GridBagConstraints gbc_label_5 = new GridBagConstraints();
        gbc_label_5.anchor = GridBagConstraints.EAST;
        gbc_label_5.insets = new Insets(0, 0, 0, 5);
        gbc_label_5.gridx = 0;
        gbc_label_5.gridy = 0;
        panel_8.add(label_5, gbc_label_5);

        //请求名称
        requestNameTextField_4 = new AssistPopupTextField();
        requestNameTextField_4.setParentPanelOrFrame(this);
        requestNameTextField_4.setToolTipText("输入完成之后,回车生效");
        GridBagConstraints gbc_textField_4 = new GridBagConstraints();
        gbc_textField_4.insets = new Insets(0, 0, 0, 5);
        gbc_textField_4.fill = GridBagConstraints.HORIZONTAL;
        gbc_textField_4.gridx = 1;
        gbc_textField_4.gridy = 0;
        panel_8.add(requestNameTextField_4, gbc_textField_4);
        requestNameTextField_4.setColumns(10);
        requestNameTextField_4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!DialogUtil.verifyTFEmpty(requestNameTextField_4, "请求名称")) {
                    return;
                }
                updateTabTitle();
            }
        });
        //不能在文本失去焦点时触发,因为此时getCurrentSelectedIndex 已经不是title对应的tab了
        requestNameTextField_4.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e);
                if (!ValueWidget.isNullOrEmpty(requestNameTextField_4.getText2())) {
                    updateTabTitle(requestNameTextField_4, autoTestPanel.getCurrentSelectedIndex());
                }
            }
        });

        JSeparator separator_2 = new JSeparator();
        separator_2.setForeground(Color.WHITE);
        GridBagConstraints gbc_separator_2 = new GridBagConstraints();
        gbc_separator_2.insets = new Insets(0, 0, 0, 5);
        gbc_separator_2.gridx = 2;
        gbc_separator_2.gridy = 0;
        panel_8.add(separator_2, gbc_separator_2);

        JLabel label_1 = new JLabel("别名");
        GridBagConstraints gbc_label_1 = new GridBagConstraints();
        gbc_label_1.anchor = GridBagConstraints.EAST;
        gbc_label_1.insets = new Insets(0, 0, 0, 5);
        gbc_label_1.gridx = 3;
        gbc_label_1.gridy = 0;
        panel_8.add(label_1, gbc_label_1);

        aliasTextField = new AssistPopupTextField();
        aliasTextField.placeHolder("用于搜索");
        aliasTextField.setToolTipText("用于搜索");
        GridBagConstraints gbc_aliasTextField = new GridBagConstraints();
        gbc_aliasTextField.insets = new Insets(0, 0, 0, 5);
        gbc_aliasTextField.fill = GridBagConstraints.HORIZONTAL;
        gbc_aliasTextField.gridx = 4;
        gbc_aliasTextField.gridy = 0;
        panel_8.add(aliasTextField, gbc_aliasTextField);
        aliasTextField.setColumns(10);
        aliasTextField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!ValueWidget.isNullOrEmpty(aliasTextField.getText2())) {
                    aliasTextField.setEditable(false);
                }
            }
        });

        JLabel lblid = new JLabel("请求ID");
        GridBagConstraints gbc_lblid = new GridBagConstraints();
        gbc_lblid.anchor = GridBagConstraints.EAST;
        gbc_lblid.insets = new Insets(0, 0, 0, 5);
        gbc_lblid.gridx = 5;
        gbc_lblid.gridy = 0;
        panel_8.add(lblid, gbc_lblid);

        requestIdTextField_5 = new AssistPopupTextField();
        requestIdTextField_5.placeHolder("用于接口之间的依赖");
        requestIdTextField_5.setToolTipText("输入完成之后,回车生效");
        GridBagConstraints gbc_textField_5 = new GridBagConstraints();
        gbc_textField_5.fill = GridBagConstraints.HORIZONTAL;
        gbc_textField_5.gridx = 6;
        gbc_textField_5.gridy = 0;
        panel_8.add(requestIdTextField_5, gbc_textField_5);
        requestIdTextField_5.setColumns(10);
        requestIdTextField_5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                requestIDAction();
            }
        });

        JPanel panel_9 = new JPanel();
        GridBagConstraints gbc_panel_9 = new GridBagConstraints();
        gbc_panel_9.insets = new Insets(0, 0, 5, 0);
        gbc_panel_9.gridwidth = 5;
        gbc_panel_9.fill = GridBagConstraints.BOTH;
        gbc_panel_9.gridx = 0;
        gbc_panel_9.gridy = 1;
        panel_7.add(panel_9, gbc_panel_9);
        GridBagLayout gbl_panel_9 = new GridBagLayout();
        gbl_panel_9.columnWidths = new int[]{0, 187, 0, 0, 0, 0, 0, 0};
        gbl_panel_9.rowHeights = new int[]{0, 0};
        gbl_panel_9.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
        gbl_panel_9.rowWeights = new double[]{0.0, Double.MIN_VALUE};
        panel_9.setLayout(gbl_panel_9);

        JLabel lblNewLabel_1 = new JLabel("服务器ip");
        GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
        gbc_lblNewLabel_1.insets = new Insets(0, 0, 0, 5);
        gbc_lblNewLabel_1.gridx = 0;
        gbc_lblNewLabel_1.gridy = 0;
        panel_9.add(lblNewLabel_1, gbc_lblNewLabel_1);

        serverIpTextField_3 = new MyDropListTextField().setCallback2(new Callback2() {
            @Override
            public String callback(String serverIp, Object event) {
                if (serverIp.matches("^[a-zA-Z\\.]+$") && (!"localhost".equals(serverIp))) {
                    portTextField_6.setText("80");
                } else {
                    Integer localhostSwitchPort = autoTestPanel.getGlobalConfig().getLocalhostSwitchPort();
                    if (null != localhostSwitchPort) {
                        portTextField_6.setText(String.valueOf(localhostSwitchPort));
                    }
                }
                return null;
            }

            @Override
            public String getButtonLabel() {
                return null;
            }

            @Override
            public Color getBackGroundColor() {
                return null;
            }

            @Override
            public JPanel getUnicodePanel() {
                return null;
            }

            @Override
            public void setUnicodePanel(JPanel unicodePanel) {

            }
        });
        serverIpTextField_3.placeHolder("双击Shift 可以弹框选择");
        serverIpTextField_3.setToolTipText("双击Shift 可以弹框选择");
        GridBagConstraints gbc_txtAuthcode = new GridBagConstraints();
        gbc_txtAuthcode.fill = GridBagConstraints.HORIZONTAL;
        gbc_txtAuthcode.insets = new Insets(0, 0, 0, 5);
        gbc_txtAuthcode.gridx = 1;
        gbc_txtAuthcode.gridy = 0;
        panel_9.add(serverIpTextField_3, gbc_txtAuthcode);
        serverIpTextField_3.setColumns(18);
        //增加回车事件,回车即触发"发送请求"
        serverIpTextField_3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                requestAction(null);
            }
        });

        chckbxSsl = new JCheckBox("ssl(https)");
        GridBagConstraints gbc_chckbxSsl = new GridBagConstraints();
        gbc_chckbxSsl.insets = new Insets(0, 0, 0, 5);
        gbc_chckbxSsl.gridx = 2;
        gbc_chckbxSsl.gridy = 0;
        panel_9.add(chckbxSsl, gbc_chckbxSsl);

        JLabel label_6 = new JLabel("端口号");
        GridBagConstraints gbc_label_6 = new GridBagConstraints();
        gbc_label_6.anchor = GridBagConstraints.EAST;
        gbc_label_6.insets = new Insets(0, 0, 0, 5);
        gbc_label_6.gridx = 3;
        gbc_label_6.gridy = 0;
        panel_9.add(label_6, gbc_label_6);

        portTextField_6 = new AssistPopupTextField("80");
        GridBagConstraints gbc_textField_6 = new GridBagConstraints();
        gbc_textField_6.insets = new Insets(0, 0, 0, 5);
        gbc_textField_6.fill = GridBagConstraints.HORIZONTAL;
        gbc_textField_6.gridx = 4;
        gbc_textField_6.gridy = 0;
        panel_9.add(portTextField_6, gbc_textField_6);
        portTextField_6.setColumns(10);

        fullUrlTextField = new AssistPopupTextField();
        fullUrlTextField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (!fullUrlTextField.isEditable() && e.getClickCount() == 1) {
                    String fullUrlText = fullUrlTextField.getText2();
                    if (!ValueWidget.isNullOrEmpty(fullUrlText)) {
                        WindowUtil.setSysClipboardText(fullUrlText);
                        ToastMessage.toast("已复制到剪切板", 2000);
                    }
                }
            }
        });
        fullUrlTextField.placeHolder("完整url地址");//不可编辑
        fullUrlTextField.setToolTipText("完整url地址,发送请求后自动生成(单击复制)");
        fullUrlTextField.setEditable(false);
        GridBagConstraints gbc_fullUrlTextField = new GridBagConstraints();
        gbc_fullUrlTextField.fill = GridBagConstraints.HORIZONTAL;
        gbc_fullUrlTextField.gridx = 6;
        gbc_fullUrlTextField.gridy = 0;
        panel_9.add(fullUrlTextField, gbc_fullUrlTextField);
        fullUrlTextField.setColumns(10);

        JPanel panel_10 = new JPanel();
        GridBagConstraints gbc_panel_10 = new GridBagConstraints();
        gbc_panel_10.gridwidth = 5;
        gbc_panel_10.insets = new Insets(0, 0, 5, 0);
        gbc_panel_10.fill = GridBagConstraints.BOTH;
        gbc_panel_10.gridx = 0;
        gbc_panel_10.gridy = 2;
        panel_7.add(panel_10, gbc_panel_10);
        GridBagLayout gbl_panel_10 = new GridBagLayout();
        gbl_panel_10.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
        gbl_panel_10.rowHeights = new int[]{0, 0};
        gbl_panel_10.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, Double.MIN_VALUE};
        gbl_panel_10.rowWeights = new double[]{0.0, Double.MIN_VALUE};
        panel_10.setLayout(gbl_panel_10);


        JLabel label_2 = new JLabel("请求方式");
        GridBagConstraints gbc_label_2 = new GridBagConstraints();
        gbc_label_2.anchor = GridBagConstraints.EAST;
        gbc_label_2.insets = new Insets(0, 0, 0, 5);
        gbc_label_2.gridx = 0;
        gbc_label_2.gridy = 0;
        panel_10.add(label_2, gbc_label_2);

        requestMethodComboBox_2 = new JComboBox<String>();
        /*requestMethodComboBox_2.addItem("GET");
        requestMethodComboBox_2.addItem("POST");
        requestMethodComboBox_2.addItem("PUT");
        requestMethodComboBox_2.addItem("DELETE");
        requestMethodComboBox_2.addItem("OPTIONS");
        requestMethodComboBox_2.addItem("HEAD");*/
        for (Object requestMethod : Constant2.REQUEST_METHOD_MAP.keySet()) {
            requestMethodComboBox_2.addItem((String) requestMethod);
        }
        GridBagConstraints gbc_comboBox_2 = new GridBagConstraints();
        gbc_comboBox_2.insets = new Insets(0, 0, 0, 5);
        gbc_comboBox_2.fill = GridBagConstraints.HORIZONTAL;
        gbc_comboBox_2.gridx = 1;
        gbc_comboBox_2.gridy = 0;
        panel_10.add(requestMethodComboBox_2, gbc_comboBox_2);


        JLabel label_7 = new JLabel("接口路径");
        GridBagConstraints gbc_label_7 = new GridBagConstraints();
        gbc_label_7.anchor = GridBagConstraints.EAST;
        gbc_label_7.insets = new Insets(0, 0, 0, 5);
        gbc_label_7.gridx = 2;
        gbc_label_7.gridy = 0;
        panel_10.add(label_7, gbc_label_7);

        actionPathTextField_7 = new AssistPopupTextField();
        Font defaultFont = actionPathTextField_7.getFont();
        //改为大号粗体
        actionPathTextField_7.setFont(new Font(defaultFont.getName(), Font.BOLD, defaultFont.getSize() + 2));

        defaultBackgroundColor = actionPathTextField_7.getBackground();
        GridBagConstraints gbc_textField_7 = new GridBagConstraints();
        gbc_textField_7.insets = new Insets(0, 0, 0, 5);
        gbc_textField_7.fill = GridBagConstraints.HORIZONTAL;
        gbc_textField_7.gridx = 3;
        gbc_textField_7.gridy = 0;
        panel_10.add(actionPathTextField_7, gbc_textField_7);
        actionPathTextField_7.setColumns(20);
        actionPathTextField_7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                requestAction(null/*runButton */, true/*isThread */);
            }
        });
        if (!debug) {
            actionPathTextField_7.getDocument().addDocumentListener(new DocumentListener() {
                private boolean ignore = false;

                public synchronized boolean isIgnore() {
                    return ignore;
                }

                public synchronized void setIgnore(boolean ignore) {
                    this.ignore = ignore;
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                }

                @Override
                public void insertUpdate(DocumentEvent e) {
                    if (isIgnore()) {
                        System.out.println("直接忽略");
                        return;
                    }
//                System.out.println("insertUpdate");
                    String url = actionPathTextField_7.getText2();
                    if (ValueWidget.isNullOrEmpty(url)) {
                        return;
                    }
                    boolean needTrim = false;
                    final String newUrl = url.trim();
                    if (!newUrl.equals(url)) {
                        needTrim = true;
                    }
                    System.out.println("needTrim:" + needTrim);
                    url = newUrl;
                    if (RegexUtil.startsWith(url, "https?")) {//只会执行一次
                        //cia.chanapp.chanjet.com :8080 /internal_api/authorizeByJsonp
                        String ip = url.replaceAll("https?://([^:/]+)"/*"127.0.0.1" or "cia.chanapp.chanjet.com"*/ +
                                "(:[\\d]+)?"/* :8080 */ +
                                "(/[^?]*)\\??(.*)", "$1  $2  $3  $4");//$1  $2  $3 以两个空格分隔
                        //去掉\\.因 http://localhost:8080/error.html?errorMessage=%E8%AF%B7%E6%8C%87%E5%AE%9A%E8%B4%AD%E4%B9%B0%E7%9A%84%E4%BA%A7%E5%93%81
                        final String[] ip_port_servletPath = ip.split("[ ]{2,}");
                        if (ValueWidget.isNullOrEmpty(serverIpTextField_3.getText2())) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(200);
                                    } catch (InterruptedException e1) {
                                        e1.printStackTrace();
                                    }
                                    String requestBodyData = null;
                                    if (ip_port_servletPath.length == 2) {
                                        serverIpTextField_3.setText(ip_port_servletPath[0]);
                                        actionPathTextField_7.setText(ip_port_servletPath[1]);
                                    } else if (ip_port_servletPath.length == 3) {
                                        serverIpTextField_3.setText(ip_port_servletPath[0]);
                                        actionPathTextField_7.setText(ip_port_servletPath[1]);
                                        requestBodyData = ip_port_servletPath[2];
                                    } else if (ip_port_servletPath.length == 4) {
                                        serverIpTextField_3.setText(ip_port_servletPath[0]);
                                        portTextField_6.setText(ip_port_servletPath[1].replaceAll("^:", ""));//端口号前面有一个冒号
                                        actionPathTextField_7.setText(ip_port_servletPath[2]);
                                        requestBodyData = ip_port_servletPath[3];
                                    }
                                    if (!debug) {
                                        setParameterFromClip(requestBodyData, true);
                                        if (RegexUtil.startsWith(newUrl, "https")) {
                                            chckbxSsl.setSelected(true);
                                        }
                                    }
                                    setIgnore(true);
                                }
                            }).start();//为什么使用线程?否则报错:Attempt to mutate in notification
                        }
//                    System.out.println(ip);
                    } else if (needTrim) {
                        new Thread(new Runnable() {//只会执行一次
                            @Override
                            public void run() {
                                actionPathTextField_7.setText(newUrl);
                                setIgnore(true);
                            }
                        }).start();
                    }
                    actionPathTextField_7.setBackground(defaultBackgroundColor);
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                }
            });
        }
        encodingCheckbox_8 = new JCheckBox("编码");
        encodingCheckbox_8.setSelected(true);
        GridBagConstraints gbc_label_8 = new GridBagConstraints();
        gbc_label_8.anchor = GridBagConstraints.EAST;
        gbc_label_8.insets = new Insets(0, 0, 0, 5);
        gbc_label_8.gridx = 4;
        gbc_label_8.gridy = 0;
        panel_10.add(encodingCheckbox_8, gbc_label_8);

        encodingComboBox = new JComboBox<String>();
        encodingComboBox.addItem(SystemHWUtil.CHARSET_UTF);
        encodingComboBox.addItem(SystemHWUtil.CHARSET_GBK);
        encodingComboBox.addItem(SystemHWUtil.CHARSET_GB2312);
        encodingComboBox.addItem(SystemHWUtil.CHARSET_ISO88591);

        GridBagConstraints gbc_comboBox = new GridBagConstraints();
        gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
        gbc_comboBox.gridx = 5;
        gbc_comboBox.gridy = 0;
        panel_10.add(encodingComboBox, gbc_comboBox);

        JSeparator separator_1 = new JSeparator();
        GridBagConstraints gbc_separator_1 = new GridBagConstraints();
        gbc_separator_1.insets = new Insets(0, 0, 0, 5);
        gbc_separator_1.gridx = 6;
        gbc_separator_1.gridy = 0;
        panel_10.add(separator_1, gbc_separator_1);

        JPanel panel_25 = new JPanel();
        GridBagConstraints gbc_panel_25 = new GridBagConstraints();
        gbc_panel_25.anchor = GridBagConstraints.WEST;
        gbc_panel_25.fill = GridBagConstraints.HORIZONTAL;
        gbc_panel_25.gridx = 7;
        gbc_panel_25.gridy = 0;
        panel_10.add(panel_25, gbc_panel_25);

        JLabel lblcontentType = new JLabel("自定义content type");
        panel_25.add(lblcontentType);

        contentTypeTextField_2 = new ContentTypeTextField();
        contentTypeTextField_2.setEditable(false);//默认不允许编辑
        panel_25.add(contentTypeTextField_2);
        contentTypeTextField_2.setColumns(20);

        JPanel panel_2 = new JPanel();

        GridBagLayout gbl_panel_2 = new GridBagLayout();
        gbl_panel_2.columnWidths = new int[]{0, 120, 0, 0, 100, 0, 0, 0, 0};
        gbl_panel_2.rowHeights = new int[]{0, 0};
        gbl_panel_2.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
        gbl_panel_2.rowWeights = new double[]{0.0, Double.MIN_VALUE};
        panel_2.setLayout(gbl_panel_2);

        //POST请求时,是否把请求体中的加号进行URL编码(只对加号进行URL编码)
        plus2percent2BWhenPostCheckBox = new JCheckBox("对加号进行URL编码(POST)");
        plus2percent2BWhenPostCheckBox.setToolTipText("正常情况下禁止选中");
        GridBagConstraints gbc_plus2percent2BWhenPostCheckBox = new GridBagConstraints();
        gbc_plus2percent2BWhenPostCheckBox.insets = new Insets(0, 0, 0, 5);
        gbc_plus2percent2BWhenPostCheckBox.anchor = GridBagConstraints.EAST;
        gbc_plus2percent2BWhenPostCheckBox.gridx = 1;
        gbc_plus2percent2BWhenPostCheckBox.gridy = 0;
        panel_2.add(plus2percent2BWhenPostCheckBox, gbc_plus2percent2BWhenPostCheckBox);

//        JButton label_4 = new JButton();
        continueSendCheckBox = new JCheckBox("连续发送次数");
        GridBagConstraints gbc_label_4 = new GridBagConstraints();
        gbc_label_4.insets = new Insets(0, 0, 0, 5);
        gbc_label_4.anchor = GridBagConstraints.EAST;
        gbc_label_4.gridx = 2;
        gbc_label_4.gridy = 0;
        panel_2.add(continueSendCheckBox, gbc_label_4);
        /*continueSendCheckBox.addActionListener(new ActionListener() {
                                      @Override
                                      public void actionPerformed(ActionEvent e) {
                                          continueSendRequest();
                                      }
                                  }
        );*/

        countTextField_2 = new AssistPopupTextField();
        countTextField_2.placeHolder("连续发送请求的次数");
        GridBagConstraints gbc_timingSecondTextField_2 = new GridBagConstraints();
        gbc_timingSecondTextField_2.insets = new Insets(0, 0, 0, 5);
        gbc_timingSecondTextField_2.fill = GridBagConstraints.HORIZONTAL;
        gbc_timingSecondTextField_2.anchor = GridBagConstraints.NORTH;
        gbc_timingSecondTextField_2.gridx = 3;
        gbc_timingSecondTextField_2.gridy = 0;
        panel_2.add(countTextField_2, gbc_timingSecondTextField_2);
        countTextField_2.setColumns(10);

        JSeparator separator = new JSeparator();
        GridBagConstraints gbc_separator = new GridBagConstraints();
        gbc_separator.insets = new Insets(0, 0, 0, 5);
        gbc_separator.gridx = 4;
        gbc_separator.gridy = 0;
        panel_2.add(separator, gbc_separator);

        timingSecondTextField_2 = new AssistPopupTextField();
        GridBagConstraints gbc_timingSecondTextField_222 = new GridBagConstraints();
        gbc_timingSecondTextField_222.insets = new Insets(0, 0, 0, 5);
        gbc_timingSecondTextField_222.fill = GridBagConstraints.HORIZONTAL;
        gbc_timingSecondTextField_222.gridx = 5;
        gbc_timingSecondTextField_222.gridy = 0;
        panel_2.add(timingSecondTextField_2, gbc_timingSecondTextField_222);
        timingSecondTextField_2.setColumns(10);

        JLabel label = new JLabel("秒");
        GridBagConstraints gbc_label = new GridBagConstraints();
        gbc_label.insets = new Insets(0, 0, 0, 5);
        gbc_label.gridx = 6;
        gbc_label.gridy = 0;
        panel_2.add(label, gbc_label);

        timinButton_3 = new JButton("定时发送");
        GridBagConstraints gbc_timinButton_3 = new GridBagConstraints();
        gbc_timinButton_3.gridx = 7;
        gbc_timinButton_3.gridy = 0;
        panel_2.add(timinButton_3, gbc_timinButton_3);
        timinButton_3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String delayStr = timingSecondTextField_2.getText2();
                if (!DialogUtil.verifyTFEmpty(timingSecondTextField_2, "定时发送时间")) {
                    return;
                }
                if (!ValueWidget.isInteger(delayStr)) {
                    GUIUtil23.warningDialog("定时发送的时间必须是数字[秒]");
                    return;
                }
                final int secondDelay = Integer.parseInt(delayStr);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        /*if(!validate22()){
                            return;
                        }*/
                        try {
                            timinButton_3.setEnabled(false);
                            timingSecondTextField_2.setEditable(false);
                            Thread.sleep(secondDelay * 1000);//单位是毫秒
                            requestAction(null, false);
                            timinButton_3.setEnabled(true);
                            timingSecondTextField_2.setEditable(true);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        });

        JPanel panel_5 = new JPanel();

        GridBagConstraints gbc_panel_2 = new GridBagConstraints();
        gbc_panel_2.gridwidth = 5;
        gbc_panel_2.insets = new Insets(0, 0, 5, 0);
        gbc_panel_2.fill = GridBagConstraints.BOTH;
        gbc_panel_2.gridx = 0;
        gbc_panel_2.gridy = 3;
        panel_7.add(panel_5, gbc_panel_2);

        GridBagConstraints gbc_panel_5 = new GridBagConstraints();
        gbc_panel_5.insets = new Insets(0, 0, 5, 0);
        gbc_panel_5.gridwidth = 5;
        gbc_panel_5.fill = GridBagConstraints.BOTH;
        gbc_panel_5.gridx = 0;
        gbc_panel_5.gridy = 4;
        panel_7.add(panel_2, gbc_panel_5);
        GridBagLayout gbl_panel_5 = new GridBagLayout();
        gbl_panel_5.columnWidths = new int[]{0, 120, 0, 0, 120, 0, 0, 0, 0};
        gbl_panel_5.rowHeights = new int[]{0, 0};
        gbl_panel_5.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        gbl_panel_5.rowWeights = new double[]{0.0, Double.MIN_VALUE};
        panel_5.setLayout(gbl_panel_5);

        /*JLabel lblNewLabel = new JLabel("前置请求id");
        GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
        gbc_lblNewLabel.insets = new Insets(0, 0, 0, 5);
        gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
        gbc_lblNewLabel.gridx = 0;
        gbc_lblNewLabel.gridy = 0;
        panel_5.add(lblNewLabel, gbc_lblNewLabel);*/

        preRequestIdComboBox_2 = new JComboBox<String>();
        preRequestIdComboBox_2.setToolTipText("必须先选中左边的复选框");
        preRequestIdComboBox_2.addItem(SystemHWUtil.MIDDLE_LINE);

        preRequestCheckBox = new JCheckBox("前置请求id");
        defaultForegroundColor = preRequestCheckBox.getForeground();
        GridBagConstraints gbc_chckbxNewCheckBox = new GridBagConstraints();
        gbc_chckbxNewCheckBox.insets = new Insets(0, 0, 0, 5);
        gbc_chckbxNewCheckBox.gridx = 0;
        gbc_chckbxNewCheckBox.gridy = 0;
        panel_5.add(preRequestCheckBox, gbc_chckbxNewCheckBox);
        preRequestCheckBox.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                boolean isSelected = preRequestCheckBox.isSelected();
                setEnable4PreRequest(preRequestCheckBox, isSelected);
            }
        });

        GridBagConstraints gbc_requestIdcomboBox_2 = new GridBagConstraints();
        gbc_requestIdcomboBox_2.insets = new Insets(0, 0, 0, 5);
        gbc_requestIdcomboBox_2.fill = GridBagConstraints.HORIZONTAL;
        gbc_requestIdcomboBox_2.gridx = 1;
        gbc_requestIdcomboBox_2.gridy = 0;
        panel_5.add(preRequestIdComboBox_2, gbc_requestIdcomboBox_2);

        openPreRequestButton_3 = new JButton("打开前置请求");
        openPreRequestButton_3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                openPreRequestAction();
            }
        });
        GridBagConstraints gbc_button_3 = new GridBagConstraints();
        gbc_button_3.insets = new Insets(0, 0, 0, 5);
        gbc_button_3.gridx = 2;
        gbc_button_3.gridy = 0;
        panel_5.add(openPreRequestButton_3, gbc_button_3);

        JLabel label_11 = new JLabel("前置请求参数名");
        GridBagConstraints gbc_label_11 = new GridBagConstraints();
        gbc_label_11.anchor = GridBagConstraints.EAST;
        gbc_label_11.insets = new Insets(0, 0, 0, 5);
        gbc_label_11.gridx = 3;
        gbc_label_11.gridy = 0;
        panel_5.add(label_11, gbc_label_11);

        txtAuthcode = new AssistPopupTextField();
        GridBagConstraints gbc_txtAuthcode22 = new GridBagConstraints();
        gbc_txtAuthcode22.insets = new Insets(0, 0, 0, 5);
        gbc_txtAuthcode22.fill = GridBagConstraints.HORIZONTAL;
        gbc_txtAuthcode22.gridx = 4;
        gbc_txtAuthcode22.gridy = 0;
        panel_5.add(txtAuthcode, gbc_txtAuthcode22);
        txtAuthcode.setColumns(10);

        thisParameterCombobox = new JComboBox<>();
        thisParameterCombobox.addItem("本请求体参数");
        thisParameterCombobox.addItem("本请求头参数");
        GridBagConstraints gbc_label_12 = new GridBagConstraints();
        gbc_label_12.insets = new Insets(0, 0, 0, 5);
        gbc_label_12.gridx = 5;
        gbc_label_12.gridy = 0;
        panel_5.add(thisParameterCombobox, gbc_label_12);

        selfParameterTextField_3 = new AssistPopupTextField();
        GridBagConstraints gbc_textField_3 = new GridBagConstraints();
        gbc_textField_3.insets = new Insets(0, 0, 0, 5);
        gbc_textField_3.gridx = 6;
        gbc_textField_3.gridy = 0;
        panel_5.add(selfParameterTextField_3, gbc_textField_3);
        selfParameterTextField_3.setColumns(20);

        everytimeCheckBox = new JCheckBox("每次都请求");
        GridBagConstraints gbc_everytimeCheckBox = new GridBagConstraints();
        gbc_everytimeCheckBox.gridx = 7;
        gbc_everytimeCheckBox.gridy = 0;
        panel_5.add(everytimeCheckBox, gbc_everytimeCheckBox);

        JPanel panel_19 = new JPanel();
        GridBagConstraints gbc_panel_19 = new GridBagConstraints();
        gbc_panel_19.insets = new Insets(0, 0, 5, 0);
        gbc_panel_19.gridwidth = 5;
        gbc_panel_19.fill = GridBagConstraints.BOTH;
        gbc_panel_19.gridx = 0;
        gbc_panel_19.gridy = 5;
        panel_7.add(panel_19, gbc_panel_19);
        GridBagLayout gbl_panel_19 = new GridBagLayout();
        gbl_panel_19.columnWidths = new int[]{0, 0, 0, 0, 50};
        gbl_panel_19.rowHeights = new int[]{0, 0};
        gbl_panel_19.columnWeights = new double[]{0.0, 0.0, 0.0, 0, Double.MIN_VALUE};
        gbl_panel_19.rowWeights = new double[]{0.0, Double.MIN_VALUE};
        panel_19.setLayout(gbl_panel_19);

        isApplyDefaultHTTPCheckBox = new JCheckBox("是否应用http默认值");
        if (!debug && !ValueWidget.isNullOrEmpty(autoTestPanel.getGlobalConfig()) && autoTestPanel.getGlobalConfig().isAutoApplyDefaultHTTPWhenNewRequest()) {
            isApplyDefaultHTTPCheckBox.setSelected(true);
        }
        GridBagConstraints gbc_chckbxjson = new GridBagConstraints();
        gbc_chckbxjson.insets = new Insets(0, 0, 0, 5);
        gbc_chckbxjson.gridx = 0;
        gbc_chckbxjson.gridy = 0;
        panel_19.add(isApplyDefaultHTTPCheckBox, gbc_chckbxjson);

        //设置contentType的地方:com/http/util/HttpSocketUtil.java的getContentTypeString 方法
        chckbxjson = new JCheckBox("请求体不是标准表单");
        GridBagConstraints gbc_chckbxjson22 = new GridBagConstraints();
        gbc_chckbxjson22.gridx = 1;
        gbc_chckbxjson22.gridy = 0;
        panel_19.add(chckbxjson, gbc_chckbxjson22);
        /*chckbxjson.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {

            }
        });*/
        chckbxjson.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                System.out.println(" :" + e);
                if (chckbxjson.isSelected()) {
                    tabbedPane_1.setEnabledAt(0, false);//remove(0);
                    tabbedPane_1.setSelectedIndex(1);
                    isApplyDefaultHTTPCheckBox.setSelected(false);//此时请求体是整个json,所以不能使用常规的表单参数
                    requestMethodComboBox_2.setSelectedIndex(1);//选中"请求体是json" 时,请求方式自动改为POST
                    contentTypeTextField_2.setText("application/json;charset=UTF-8", true);
                    contentTypeTextField_2.setEditable(true);
                } else {
                    tabbedPane_1.setEnabledAt(0, true);//remove(0);
                    contentTypeTextField_2.setText("", true);
                }
            }
        });

        copyFieldCheckbox = new JCheckBox("是否复制");
//        copyFieldCheckbox.setEnabled(false);
        GridBagConstraints gbc_isEncodingCheckbox_233 = new GridBagConstraints();
//        gbc_isEncodingCheckbox_233.fill = GridBagConstraints.HORIZONTAL;
        gbc_isEncodingCheckbox_233.anchor = GridBagConstraints.NORTH;
        gbc_isEncodingCheckbox_233.gridx = 2;
        gbc_isEncodingCheckbox_233.gridy = 0;
        panel_19.add(copyFieldCheckbox, gbc_isEncodingCheckbox_233);
        copyFieldCheckbox.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                boolean isSelected = copyFieldCheckbox.isSelected();
                copyField.setEnabled(isSelected);
                copyField.requestFocus();
            }
        });

        copyField = new AssistPopupTextField();
        GridBagConstraints gbc_label_433 = new GridBagConstraints();
        gbc_label_433.insets = new Insets(0, 0, 0, 5);
        gbc_label_433.anchor = GridBagConstraints.WEST;
        gbc_label_433.fill = GridBagConstraints.HORIZONTAL;
        gbc_label_433.gridx = 3;
        gbc_label_433.gridy = 0;
        panel_19.add(copyField, gbc_label_433);
        copyField.setColumns(20);


        JPanel panel_20 = new JPanel();
        GridBagConstraints gbc_panel_20 = new GridBagConstraints();
        gbc_panel_20.gridwidth = 5;
        gbc_panel_20.insets = new Insets(0, 0, 0, 5);
        gbc_panel_20.fill = GridBagConstraints.BOTH;
        gbc_panel_20.gridx = 0;
        gbc_panel_20.gridy = 6;
        panel_7.add(panel_20, gbc_panel_20);

        JPanel panel_11 = new JPanel();
        panel_7JS2 = new JScrollPane(panel_11);
        panel_7JS2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        tabbedPane_parameterHeader = new JTabbedPane(JTabbedPane.TOP);
        Border border13233 = BorderFactory.createEtchedBorder(Color.blue,
                Color.blue);
        tabbedPane_parameterHeader.setBorder(border13233);

        tabbedPane_parameterHeader.addTab("请求参数", null, panel_7JS2, "标准的表单数据(点击回到参数顶部)");
        httpHeadPanel = new HttpHeadPanel(getOriginalRequestInfoBean() == null ? null : getOriginalRequestInfoBean().getHeaderMap());
        tabbedPane_parameterHeader.addTab("请求头", null, httpHeadPanel, "请求Header");
        splitPane_inner.setRightComponent(tabbedPane_parameterHeader);
        panel_11.setLayout(new BorderLayout(0, 0));

        tabbedPane_parameterHeader.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                scroll2TopParameter(e);
            }
        });
        tabbedPane_parameterHeader.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                scroll2TopParameter(e);
            }
        });

        tabbedPane_1 = new JTabbedPane(JTabbedPane.TOP);
        panel_11.add(tabbedPane_1, BorderLayout.CENTER);

        JPanel panel_12 = new JPanel();
        panel_12.setBackground(Color.WHITE);
        tabbedPane_1.addTab("请求参数", null, panel_12, "标准的表单数据");
        GridBagLayout gbl_panel_12 = new GridBagLayout();
        gbl_panel_12.columnWidths = new int[]{0, 0};
        gbl_panel_12.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
        gbl_panel_12.columnWeights = new double[]{1.0, Double.MIN_VALUE};
        gbl_panel_12.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        panel_12.setLayout(gbl_panel_12);

        JPanel panel_13 = new JPanel();
        GridBagConstraints gbc_panel_13 = new GridBagConstraints();
        gbc_panel_13.insets = new Insets(0, 0, 5, 0);
        gbc_panel_13.fill = GridBagConstraints.BOTH;
        gbc_panel_13.gridx = 0;
        gbc_panel_13.gridy = 0;
        panel_12.add(panel_13, gbc_panel_13);

        JLabel label_9 = new JLabel("请求参数");
        panel_13.add(label_9);
        JButton cleanUprunButton_55 = new JButton("清空后再运行");
        cleanUprunButton_55.setToolTipText("点击将发送http请求");
        cleanUpAction(cleanUprunButton_55);
        panel_13.add(cleanUprunButton_55);
        runButton_55 = new JButton(LABEL_RUN);
        runButton_55.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JButton button = (JButton) e.getSource();
                requestAction(button);
            }
        });
        panel_13.add(runButton_55);


        JButton runButton_66 = new JButton("打开网页发送POST请求");
        runButton_66.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                sendPostAction();
            }
        });
        panel_13.add(runButton_66);

        JButton cleanUpButton_66 = new JButton("清空结果");
//        cleanUpButton_66.setMnemonic('');
        cleanUpButton_66.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cleanUpTA();
            }
        });
        panel_13.add(cleanUpButton_66);


        final JPanel panel_14 = new JPanel();
        panel_14.setBorder(new LineBorder(Color.RED));
        panel_14.setBackground(Color.green);
        panel_14.setLayout(new BorderLayout());
        GridBagConstraints gbc_panel_14 = new GridBagConstraints();
        gbc_panel_14.insets = new Insets(0, 0, 5, 0);
        gbc_panel_14.fill = GridBagConstraints.HORIZONTAL;
        gbc_panel_14.gridx = 0;
        gbc_panel_14.gridy = 1;
        panel_12.add(panel_14, gbc_panel_14);

        JPanel panel_21 = new JPanel();
        panel_14.add(panel_21, BorderLayout.NORTH);
//        panel_21.setLayout(new GridLayout(0, 3, 40, 0));

        JButton cleanUpTable_66 = new JButton("清空表格");
        cleanUpTable_66.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                TableUtil3.cleanTableData(parameterTable_1, getConfigParam());
                repaintTable();
                //修改请求方式为GET
                requestMethodComboBox_2.setSelectedIndex(0);
            }
        });
        panel_21.add(cleanUpTable_66);
        JButton max_66 = new JButton(MyTableMenuListener.ACTION_maximize_TABLE + "(M)");
        max_66.setMnemonic('M');
        max_66.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DialogUtil.showMaximizeDialog(tableScroll, false);
                RequestPanel.this.repaint();//necessary
            }
        });
        panel_21.add(max_66);

        final JButton applyParametersTemplate = new JButton("应用参数模板");
        applyParametersTemplate.setMnemonic('M');
        applyParametersTemplate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<RequestParameterTemplate> requestParameterTemplates = getRequestParameterTemplates();
                if (ValueWidget.isNullOrEmpty(requestParameterTemplates) && null != getOriginalRequestInfoBean()) {
                    requestParameterTemplates = getOriginalRequestInfoBean().getRequestParameterTemplates();
                }
                if (ValueWidget.isNullOrEmpty(requestParameterTemplates)) {
                    ToastMessage.toast("暂未添加模板", 3000, Color.RED);
                    return;
                }
                int size = requestParameterTemplates.size();
                List<String> displaynames = new ArrayList<String>();
                for (int i = 0; i < size; i++) {
                    displaynames.add(requestParameterTemplates.get(i).getDisplayName());
                }
                /*if (ValueWidget.isNullOrEmpty(displaynames)) {
                    displaynames.add("线上账号(测试)");
                    displaynames.add("集测账号(测试)");
                }*/
                if (!ValueWidget.isNullOrEmpty(displaynames)) {
                    optionalParameters(autoTestPanel, displaynames, tabbedPane_22, 0);
                }

            }


        });
        panel_21.add(applyParametersTemplate);

        parameterTable_1 = new JTable();
        parameterTable_1.setRowSelectionAllowed(false);
//        JScrollPane jscrollPane22=new JScrollPane();
//        panel_14.add(parameterTable_1.getTableHeader(),BorderLayout.NORTH);
        tableScroll = new JScrollPane(parameterTable_1);
        Dimension di = tableScroll.getPreferredSize();
        di.height = tableScroll_height;
        tableScroll.setPreferredSize(di);
        parameterTable_1.setOpaque(false);
        tableScroll.getViewport().setOpaque(false);// important
        JPanel panel_15 = new JPanel();
        tableScroll.setRowHeaderView(panel_15);
        panel_14.add(tableScroll);
        //要防止下面的代码执行多次
        if (!debug) {
            final MouseInputListener mouseInputListener = getMouseInputListener(parameterTable_1, RequestPanel.this, RequestPanel.this.getAutoTestPanel());
            parameterTable_1.addMouseListener(mouseInputListener);
        }
//        panel_14.add(panel_15, BorderLayout.SOUTH);
        GridBagLayout gbl_panel_15 = new GridBagLayout();
        gbl_panel_15.columnWidths = new int[]{0, 0, 0};
        gbl_panel_15.rowHeights = new int[]{0, 0, 0, 0};
        gbl_panel_15.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
        gbl_panel_15.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
        panel_15.setLayout(gbl_panel_15);

        JButton addRowButton = new JButton("添加一行");
        addRowButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addParameter(null/*,getConfigParam()*/);
//                scrollToBottom(panel_7JS2);
            }
        });
        GridBagConstraints gbc_button = new GridBagConstraints();
        gbc_button.insets = new Insets(0, 0, 0, 5);
        gbc_button.gridx = 1;
        gbc_button.gridy = 0;
        panel_15.add(addRowButton, gbc_button);

        JButton btnAddFromClipboard = new JButton("从剪切板黏贴");
        btnAddFromClipboard.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addParameterClip();
//                scrollToBottom(panel_7JS2);
            }
        });
        GridBagConstraints gbc_btnAddFromClipboard = new GridBagConstraints();
        gbc_btnAddFromClipboard.gridx = 1;
        gbc_btnAddFromClipboard.gridy = 1;
        panel_15.add(btnAddFromClipboard, gbc_btnAddFromClipboard);

        JButton btnAddFromClipboardBatch = new JButton("从剪切板黏贴query string");
        if (!debug) {
            btnAddFromClipboardBatch.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    String requestBodyData = WindowUtil.getSysClipboardText();
                    setParameterFromClip(requestBodyData);
                }
            });
        }
        GridBagConstraints gbc_btnAddFromClipboardBatch = new GridBagConstraints();
        gbc_btnAddFromClipboardBatch.gridx = 1;
        gbc_btnAddFromClipboardBatch.gridy = 2;
        panel_15.add(btnAddFromClipboardBatch, gbc_btnAddFromClipboardBatch);
        JPanel panel_16 = new JPanel();
        GridBagConstraints gbc_panel_16 = new GridBagConstraints();
        gbc_panel_16.insets = new Insets(0, 0, 5, 0);
        gbc_panel_16.fill = GridBagConstraints.BOTH;
        gbc_panel_16.gridx = 0;
        gbc_panel_16.gridy = 3;
//        panel_12.add(panel_16, gbc_panel_16);
        GridBagLayout gbl_panel_16 = new GridBagLayout();
        gbl_panel_16.columnWidths = new int[]{0, 0, 0};
        gbl_panel_16.rowHeights = new int[]{0, 0};
        gbl_panel_16.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        gbl_panel_16.rowWeights = new double[]{0.0, Double.MIN_VALUE};
        panel_16.setLayout(gbl_panel_16);

        /*JLabel label_10 = new JLabel("预留");
        GridBagConstraints gbc_label_10 = new GridBagConstraints();
        gbc_label_10.insets = new Insets(0, 0, 0, 5);
        gbc_label_10.anchor = GridBagConstraints.EAST;
        gbc_label_10.gridx = 0;
        gbc_label_10.gridy = 0;
        panel_16.add(label_10, gbc_label_10);

        AssistPopupTextField textField_8 = new AssistPopupTextField();
        GridBagConstraints gbc_textField_8 = new GridBagConstraints();
        gbc_textField_8.fill = GridBagConstraints.HORIZONTAL;
        gbc_textField_8.gridx = 1;
        gbc_textField_8.gridy = 0;
        panel_16.add(textField_8, gbc_textField_8);
        textField_8.setColumns(10);

        JPanel panel = new JPanel();
        GridBagConstraints gbc_panel = new GridBagConstraints();
        gbc_panel.insets = new Insets(0, 0, 5, 0);
        gbc_panel.fill = GridBagConstraints.BOTH;
        gbc_panel.gridx = 0;
        gbc_panel.gridy = 4;
        panel_12.add(panel, gbc_panel);
        GridBagLayout gbl_panel = new GridBagLayout();
        gbl_panel.columnWidths = new int[]{0, 0, 0};
        gbl_panel.rowHeights = new int[]{0, 0};
        gbl_panel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        gbl_panel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
        panel.setLayout(gbl_panel);

        JLabel label = new JLabel("预留");
        GridBagConstraints gbc_label = new GridBagConstraints();
        gbc_label.insets = new Insets(0, 0, 0, 5);
        gbc_label.anchor = GridBagConstraints.EAST;
        gbc_label.gridx = 0;
        gbc_label.gridy = 0;
        panel.add(label, gbc_label);

        textField = new AssistPopupTextField();
        GridBagConstraints gbc_textField = new GridBagConstraints();
        gbc_textField.fill = GridBagConstraints.HORIZONTAL;
        gbc_textField.gridx = 1;
        gbc_textField.gridy = 0;
        panel.add(textField, gbc_textField);
        textField.setColumns(10);
*/

        /*JButton getTableData=new JButton("获取表格数据");
        getTableData.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				getTableParameters();
			}
		});
        GridBagConstraints gbc_btnAddFromClipboard22 = new GridBagConstraints();
        gbc_btnAddFromClipboard22.anchor=GridBagConstraints.WEST;
        gbc_btnAddFromClipboard22.gridx = 3;
        gbc_btnAddFromClipboard22.gridy = 0;
        panel_15.add(getTableData, gbc_btnAddFromClipboard22);*/
        /*JPanel panel_5 = new JPanel();
        GridBagConstraints gbc_panel_5 = new GridBagConstraints();
        gbc_panel_5.insets = new Insets(0, 0, 0, 5);
        gbc_panel_5.fill = GridBagConstraints.BOTH;
        gbc_panel_5.gridx = 1;
        gbc_panel_5.gridy = 5;
        panel_12.add(panel_5, gbc_panel_5);*/


        JPanel panel_17 = new JPanel();
        tabbedPane_1.addTab("请求体", null, panel_17, "请求体");

        JPanel panel_23 = new JPanel();
        tabbedPane_1.addTab("已解码", null, panel_23, null);
        panel_23.setLayout(new BorderLayout(0, 0));
        urlDecodeTA = new AssistPopupTextArea();
        urlDecodeTA.setLineWrap(true);
        urlDecodeTA.setWrapStyleWord(true);
        JScrollPane scrollPane_4 = new JScrollPane(urlDecodeTA);
        panel_23.add(scrollPane_4, BorderLayout.CENTER);

        JPanel panel_24 = new JPanel();
        panel_23.add(panel_24, BorderLayout.SOUTH);
        JButton copyDecodeBtn = ComponentUtil.getCopyBtn(urlDecodeTA);
        panel_24.add(copyDecodeBtn);
        tabbedPane_1.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JTabbedPane tabbedPane = (JTabbedPane) e.getSource();
                int selectedIndex = tabbedPane.getSelectedIndex();
                System.out.println(selectedIndex);
                if (selectedIndex == 1) {//请求体
                    requestBodyTabbed();
                }//if(selectedIndex==1)
                else if (selectedIndex == 0) {//请求参数

                    if (requestFormParameter()) return;
                } else {//if(selectedIndex==2)
                    String requestBodyData = requestBodyDataTA.getText2();
                    if (!ValueWidget.isNullOrEmpty(requestBodyData)) {
                        try {//url 解码
                            String result = URLDecoder.decode(requestBodyData, SystemHWUtil.CHARSET_UTF);
                            if (!ValueWidget.isNullOrEmpty(result)) {
                                urlDecodeTA.setText(result);
                            }
                        } catch (UnsupportedEncodingException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
                requestParameterTabInfo.setLastTabIndex(selectedIndex);
            }
        });
        GridBagLayout gbl_panel_17 = new GridBagLayout();
        gbl_panel_17.columnWidths = new int[]{0, 0, 0};
        gbl_panel_17.rowHeights = new int[]{0, 0, 0, 0, 0};
        gbl_panel_17.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        gbl_panel_17.rowWeights = new double[]{1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        panel_17.setLayout(gbl_panel_17);

        JScrollPane scrollPane_1 = new JScrollPane();
        Border border132 = BorderFactory.createEtchedBorder(Color.white,
                new Color(148, 145, 140));
        TitledBorder parameterTitle = new TitledBorder(border132, "请求体(表单参数)");
        scrollPane_1.setBorder(parameterTitle);
        GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
        gbc_scrollPane_1.insets = new Insets(0, 0, 5, 0);
        gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
        gbc_scrollPane_1.gridx = 1;
        gbc_scrollPane_1.gridy = 0;
        panel_17.add(scrollPane_1, gbc_scrollPane_1);

//        requestBodyDataTA = new MemoAssistPopupTextArea(autoTestPanel);//双击Shift有菜单
        requestBodyDataTA = new HttpRequestBodyTextArea();
        requestBodyDataTA.setLineWrap(true);
        requestBodyDataTA.setWrapStyleWord(true);
        scrollPane_1.setViewportView(requestBodyDataTA);

        JPanel panel_1 = new JPanel();
        GridBagConstraints gbc_panel_1 = new GridBagConstraints();
        gbc_panel_1.insets = new Insets(0, 0, 5, 0);
        gbc_panel_1.fill = GridBagConstraints.BOTH;
        gbc_panel_1.gridx = 1;
        gbc_panel_1.gridy = 1;
        panel_17.add(panel_1, gbc_panel_1);
        GridBagLayout gbl_panel_1 = new GridBagLayout();
        gbl_panel_1.columnWidths = new int[]{0, 0, 0};
        gbl_panel_1.rowHeights = new int[]{0, 0};
        gbl_panel_1.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        gbl_panel_1.rowWeights = new double[]{0.0, Double.MIN_VALUE};
        panel_1.setLayout(gbl_panel_1);

       /* JLabel label_1 = new JLabel("预留");
        GridBagConstraints gbc_label_1 = new GridBagConstraints();
        gbc_label_1.insets = new Insets(0, 0, 0, 5);
        gbc_label_1.anchor = GridBagConstraints.EAST;
        gbc_label_1.gridx = 0;
        gbc_label_1.gridy = 0;
        panel_1.add(label_1, gbc_label_1);

        textField_1 = new AssistPopupTextField();
        GridBagConstraints gbc_textField_1 = new GridBagConstraints();
        gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
        gbc_textField_1.gridx = 1;
        gbc_textField_1.gridy = 0;
        panel_1.add(textField_1, gbc_textField_1);
        textField_1.setColumns(10);*/

        JPanel panel_18 = new JPanel();
        GridBagConstraints gbc_panel_18 = new GridBagConstraints();
        gbc_panel_18.insets = new Insets(0, 0, 5, 0);
        gbc_panel_18.fill = GridBagConstraints.BOTH;
        gbc_panel_18.gridx = 1;
        gbc_panel_18.gridy = 2;
        panel_17.add(panel_18, gbc_panel_18);
        GridBagLayout gbl_panel_18 = new GridBagLayout();
        gbl_panel_18.columnWidths = new int[]{0, 0, 0, 0};
        gbl_panel_18.rowHeights = new int[]{0, 0};
        gbl_panel_18.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
        gbl_panel_18.rowWeights = new double[]{0.0, Double.MIN_VALUE};
        panel_18.setLayout(gbl_panel_18);

        JButton button_1 = new JButton("添加");

        GridBagConstraints gbc_button_1 = new GridBagConstraints();
        gbc_button_1.insets = new Insets(0, 0, 0, 5);
        gbc_button_1.gridx = 1;
        gbc_button_1.gridy = 0;
        panel_18.add(button_1, gbc_button_1);

        JButton button_2 = new JButton("浏览");
        GridBagConstraints gbc_button_2 = new GridBagConstraints();
        gbc_button_2.gridx = 2;
        gbc_button_2.gridy = 0;
        panel_18.add(button_2, gbc_button_2);


        int dividerLoc;
        if (debug || autoTestPanel.isDebug()) {
            dividerLoc = 900;
        } else {
            dividerLoc = getDividerHeight()[1];

        }
        splitPane_out.setDividerLocation(dividerLoc);


        JPanel panel = new JPanel();
        splitPane_out.setRightComponent(panel);
        panel.setLayout(new BorderLayout(0, 0));

        tabbedPane_22 = new JTabbedPane(JTabbedPane.TOP);
        tabbedPane_22.setTransferHandler(new TransferHandler("foreground"));
        if (!debug) {
            DragSource dragSource = DragSource.getDefaultDragSource();
            //将srcLabel转换成拖放源，它能接受复制、移动两种操作
            dragSource.createDefaultDragGestureRecognizer(tabbedPane_22,
                    DnDConstants.ACTION_COPY_OR_MOVE, new DragGestureListener() {
                        public void dragGestureRecognized(DragGestureEvent event) {
                            Transferable transferable = new Transferable() {

                                @Override
                                public boolean isDataFlavorSupported(DataFlavor flavor) {
                                    return true;
                                }

                                @Override
                                public DataFlavor[] getTransferDataFlavors() {
                                    return new DataFlavor[]{autoTestPanel.getDataFlavor()};
                                }

                                @Override
                                public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
                                    return RequestPanel.this;
                                }
                            };
                            //继续拖放操作,拖放过程中使用手状光标
                            event.startDrag(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR),
                                    transferable);
                        }
                    });

            cleanUprunButton_55.setDropTarget(new DropTarget(autoTestPanel.getMemoTextArea_1(), new DropTargetListener() {

                @Override
                public void dropActionChanged(DropTargetDragEvent dtde) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void drop(DropTargetDropEvent dtde) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void dragOver(DropTargetDragEvent dtde) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void dragExit(DropTargetEvent dte) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void dragEnter(DropTargetDragEvent dtde) {
                    // TODO Auto-generated method stub

                }
            }));
        }
        panel.add(tabbedPane_22);
        tabbedPane_22.addMouseListener(new MouseAdapter() {
            private int heightLevel = 0;

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }


            @Override
            public void mouseClicked(MouseEvent e) {
                JTabbedPane tabbedPane = (JTabbedPane) e.getSource();
                int selectedIndex = tabbedPane.getSelectedIndex();
                //当前选择的标签页
                Component selectedTab = tabbedPane.getComponentAt(selectedIndex);
                if (!ValueWidget.isNullOrEmpty(selectedTab) && jsonFormattedPane == selectedTab && e.getClickCount() == 2) {
                    switch (heightLevel % 3) {
                        case 0:
                            splitPane_out.setDividerLocation(getDividerHeight()[0]);
                            break;
                        case 1:
                            splitPane_out.setDividerLocation(15);
                            break;
                        case 2:
                            splitPane_inner.setDividerLocation(getDividerHeight()[0]);
                            splitPane_out.setDividerLocation(getDividerHeight()[1]);
                            RequestPanel.this.repaint();
                            break;
                        default:
                            break;
                    }
                    heightLevel++;
                }
            }
        });
        jsonFormattedPane = new JPanel();
        jsonFormattedPane.setLayout(new BorderLayout());


//        jsonFormattedTextArea_9 = new MemoAssistPopupTextArea(autoTestPanel);
        //格式化的json
        jsonFormattedTextArea_9 = new RSyntaxTextArea() {
            @Override
            protected JPopupMenu extendsJPopupMenu22(JPopupMenu menu) {
                JMenuItem analysisM = new JMenuItem("分析订单");
                PopupMenuListener popupMenuListener = new PopupMenuListener(autoTestPanel, this);
                analysisM.addActionListener(popupMenuListener);
                menu.add(analysisM);
                return super.extendsJPopupMenu22(menu);
            }

        };
        //双击变为可以编辑
        jsonFormattedTextArea_9.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    if (!jsonFormattedTextArea_9.isEditable()) {
                        jsonFormattedTextArea_9.setEditable(true);
                        DialogUtil.focusSelectAllTF(jsonFormattedTextArea_9);
                        jsonFormattedTextArea_9.repaint();
                    }
//                        System.out.println("Double Click!");
                }
                super.mouseClicked(e);
            }
        });
        //delete at 2018-02-02
//        DialogUtil.addKeyListener22(jsonFormattedTextArea_9);

        jsonFormattedTextArea_10 = new RSyntaxTextArea();
        jsonFormattedTextArea_10.setLineWrap(true);
        jsonFormattedTextArea_10.setWrapStyleWord(true);
        JScrollPane formatPanel = new JScrollPane(jsonFormattedTextArea_10);//初步格式化


        JScrollPane scrollPane322 = new JScrollPane(jsonFormattedTextArea_9);
        jsonFormattedTextArea_9.setLineWrap(true);
        jsonFormattedTextArea_9.setWrapStyleWord(true);
//        jsonFormattedTextArea_9.setEditable(false);//放开,可以编辑
        jsonFormattedPane.add(scrollPane322);

        JPanel responseResultPane = new JPanel();
        tabbedPane_22.addTab("http请求结果", null, responseResultPane, "http请求的debug信息");
        tabbedPane_22.addTab("格式化的json", null, jsonFormattedPane, "双击可以变大");
        singleRequestNoteTA = new AssistPopupTextArea();
        singleRequestNoteTA.setLineWrap(true);
        singleRequestNoteTA.setWrapStyleWord(true);
        tabbedPane_22.addTab("备忘", null, new JScrollPane(singleRequestNoteTA), null);
        tabbedPane_22.addTab("初步格式化", null, formatPanel, "初步格式化");

        GridBagLayout gbl_responseResultPane = new GridBagLayout();
        gbl_responseResultPane.columnWidths = new int[]{79, 0, 0};
        gbl_responseResultPane.rowHeights = new int[]{0, 0, 0};
        gbl_responseResultPane.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        gbl_responseResultPane.rowWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
        responseResultPane.setLayout(gbl_responseResultPane);

        JPanel panel_3 = new JPanel();
        GridBagConstraints gbc_panel_3 = new GridBagConstraints();
        gbc_panel_3.insets = new Insets(0, 0, 5, 5);
        gbc_panel_3.fill = GridBagConstraints.BOTH;
        gbc_panel_3.gridx = 0;
        gbc_panel_3.gridy = 0;
        responseResultPane.add(panel_3, gbc_panel_3);
        GridBagLayout gbl_panel_3 = new GridBagLayout();
        gbl_panel_3.columnWidths = new int[]{0, 0};
        gbl_panel_3.rowHeights = new int[]{0, 0, 0};
        gbl_panel_3.columnWeights = new double[]{1.0, Double.MIN_VALUE};
        gbl_panel_3.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
        panel_3.setLayout(gbl_panel_3);

       /* JLabel label_3 = new JLabel("返回类型");
        GridBagConstraints gbc_label_3 = new GridBagConstraints();
        gbc_label_3.insets = new Insets(0, 0, 5, 0);
        gbc_label_3.gridx = 0;
        gbc_label_3.gridy = 0;
        panel_3.add(label_3, gbc_label_3);

        JComboBox<String> comboBox_1 = new JComboBox<String>();
        comboBox_1.addItem("json");
        comboBox_1.addItem("html");
        GridBagConstraints gbc_comboBox_1 = new GridBagConstraints();
        gbc_comboBox_1.fill = GridBagConstraints.HORIZONTAL;
        gbc_comboBox_1.gridx = 0;
        gbc_comboBox_1.gridy = 1;
        panel_3.add(comboBox_1, gbc_comboBox_1);*/

        JPanel panel_4 = new JPanel();
        GridBagConstraints gbc_panel_4 = new GridBagConstraints();
        gbc_panel_4.insets = new Insets(0, 0, 5, 0);
        gbc_panel_4.fill = GridBagConstraints.BOTH;
        gbc_panel_4.gridx = 1;
        gbc_panel_4.gridy = 0;
        responseResultPane.add(panel_4, gbc_panel_4);
        panel_4.setLayout(new BorderLayout(0, 0));

        JScrollPane scrollPane = new JScrollPane();
        panel_4.add(scrollPane, BorderLayout.CENTER);

        respTextArea_9 = new MemoAssistPopupTextArea(autoTestPanel);
        respTextArea_9.setLineWrap(true);
        respTextArea_9.setWrapStyleWord(true);
        respTextArea_9.setEditable(false);
        DefaultCaret caretrespTextArea_9 = (DefaultCaret) respTextArea_9.getCaret();
        scrollPane.setViewportView(respTextArea_9);
        //        respTextArea_9.setColumns(10);

        JScrollPane scrollPane_2 = new JScrollPane();
        GridBagConstraints gbc_scrollPane_2 = new GridBagConstraints();
        gbc_scrollPane_2.gridwidth = 2;
        gbc_scrollPane_2.insets = new Insets(0, 0, 0, 5);
        gbc_scrollPane_2.fill = GridBagConstraints.BOTH;
        gbc_scrollPane_2.gridx = 0;
        gbc_scrollPane_2.gridy = 1;
        responseResultPane.add(scrollPane_2, gbc_scrollPane_2);

        resultTextPane = new MemoAssistPopupTextArea(autoTestPanel);
        resultTextPane.setLineWrap(true);
        resultTextPane.setWrapStyleWord(true);
        resultTextPane.setEditable(false);
        DefaultCaret caretresultTextPane = (DefaultCaret) resultTextPane.getCaret();
        scrollPane_2.setViewportView(resultTextPane);


        JPanel panel_6 = new JPanel();
        panel.add(panel_6, BorderLayout.NORTH);
        JButton cleanUprunButton_664 = new JButton("清空后再运行");
        cleanUprunButton_664.setUI(new BEButtonUI().setNormalColor(BEButtonUI.NormalColor.green));
        cleanUprunButton_664.setForeground(Color.white);
        cleanUprunButton_664.setToolTipText("点击将发送http请求");
        cleanUpAction(cleanUprunButton_664);
        panel_6.add(cleanUprunButton_664);
        JButton runButton_3 = new JButton(LABEL_RUN);
        runButton_3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JButton button = (JButton) e.getSource();
                requestAction(button);
            }
        });
        panel_6.add(runButton_3);


        JButton copyResponse = new JButton(ACTION_COMMAND_COPY_RESPONSE + "(不会重新请求)");
        copyResponse.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!ValueWidget.isNullOrEmpty(responseJsonResult)) {
                    WindowUtil.setSysClipboardText(responseJsonResult);
                }
            }
        });
        panel_6.add(copyResponse);

        JButton copyAccessToken = new JButton("复制access_token");
        copyAccessToken.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                final JButton button = (JButton) e.getSource();
                copyAccessToken(button);
            }
        });
        panel_6.add(copyAccessToken);

        JButton copyRequestInfo = new JButton(ACTION_COMMAND_COPY_REQUEST_INFO);
        copyRequestInfo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                copyRequestInfoAction();
            }
        });
        panel_6.add(copyRequestInfo);


        JButton pasteRequestInfo = new JButton(ACTION_COMMAND_PASTE_REQUEST_INFO);
        pasteRequestInfo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                final RequestInfoBean requestInfoBean = getRequestInfoBean(false);
                if (ValueWidget.isNullOrEmpty(requestInfoBean) || ValueWidget.isNullOrEmpty(requestInfoBean.getServerIp())) {
                    pasteRequestInfoAction();
                    return;
                }
                int result = JOptionPane.showConfirmDialog(null, "Are you sure to 黏贴请求信息 ?", "确认",
                        JOptionPane.OK_CANCEL_OPTION);
                if (result == JOptionPane.OK_OPTION) {
                    pasteRequestInfoAction();
                }
            }
        });
        panel_6.add(pasteRequestInfo);

        JButton paste2NewRequest = new JButton("黏贴为新请求");
        if (!debug) {
            paste2NewRequest.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    autoTestPanel.addOneRequest(true, true).pasteRequestInfoAction();
                }
            });
        }
        panel_6.add(paste2NewRequest);

        if (!debug) {
            caretrespTextArea_9.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
            caretresultTextPane.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
        }
    }

    private void scroll2TopParameter(AWTEvent e) {
        JTabbedPane tabbedPane = (JTabbedPane) e.getSource();
        int selectedIndex = tabbedPane.getSelectedIndex();
        if (selectedIndex == 0) {
            //请求参数 回到最上面
            scroll2Top();
        } else {
            //点击"请求头",cookie文本框聚焦
            httpHeadPanel.focus();
        }
    }

    private boolean requestFormParameter() {
        if (null == requestBodyDataTA) {
            return true;
        }
        String requestBodyData = requestBodyDataTA.getText2();
        if (ValueWidget.isNullOrEmpty(requestBodyData)) {
            return false;
        }
        Map requestMap = null;
        requestBodyData = requestBodyData.trim();
        if (requestBodyData.startsWith("{") && requestBodyData.endsWith("}")) {
            requestMap = (Map) HWJacksonUtils.deSerialize(requestBodyData, Map.class);
        } else {
            requestMap = new TreeMap();
            RequestUtil.setArgumentMap(requestMap, requestBodyData, true, null, null, false, false/*quoteEscape*/);
        }

        TableUtil3.setTableData3(parameterTable_1, requestMap, getConfigParam(), RequestPanel.this.actionCallbackMap);
        rendTable();//is Necessary
        return false;
    }

    private void requestBodyTabbed() {
        if (requestParameterTabInfo.getLastTabIndex() == 0) {
            if (!chckbxjson.isSelected()) {
//	                		String requestCharset = (String) encodingComboBox.getSelectedItem();
                requestBodyDataTA.setText(generateRequestBody(false/*isEncodeParameterCheckbox.isSelected()*/));
                requestBodyDataTA.requestFocus();
            }
            return;
        }
        if (requestParameterTabInfo.getLastTabIndex() == 2) {//"已解码"标签页
            String urlDecodeText = urlDecodeTA.getText();
            if (ValueWidget.isNullOrEmpty(urlDecodeText)) {
                return;
            }
            Map requestMap = new TreeMap();
            RequestUtil.setArgumentMap(requestMap, urlDecodeText, true, null, null, false, false/*quoteEscape*/);
            requestBodyDataTA.setText(ValueWidget.getRequestBodyFromMap(requestMap, true));
            requestBodyDataTA.requestFocus();
        }
    }

    /***
     * 是请求参数还是请求头参数 header
     * @return : 常规的请求体参数
     */
    public boolean isHttpRequestParameter() {
        return this.thisParameterCombobox.getSelectedIndex() == 0;
    }

    public void setParameterFromClip(String requestBodyData) {
        setParameterFromClip(requestBodyData, false);
    }

    public void setParameterFromClip(String requestBodyData, boolean urlDecode) {
        if (ValueWidget.isNullOrEmpty(requestBodyData)) {
            return;
        }
        Map requestMap = new TreeMap();
        RequestUtil.setArgumentMap(requestMap, requestBodyData, true, null, null, urlDecode, false);
        if (ValueWidget.isNullOrEmpty(requestMap)) {
            ToastMessage.toast("操作失败,因为query string为:\"" + requestBodyData + "\"", 2000, Color.red);
            return;
        }
        TableUtil3.cleanTableData(parameterTable_1, getConfigParam());
        /*ConfigParam configParam=new ConfigParam();
        configParam.setHasTextField(false);
        configParam.setTF_table_cell(true);
        configParam.setColumnNames(columnNames);*/
        TableUtil3.setTableData3(parameterTable_1, requestMap, getConfigParam(), RequestPanel.this.actionCallbackMap);
        rendTable();//is Necessary
        //解决黏贴query string时,请求参数表格没有展开的问题
        autoTestPanel.optimizParameterScroll(this, false);
    }

    /***
     * 使scroll的滚动条自动达到最底部
     */
    public void scroll2Bottom() {
        JScrollBar vertical = panel_7JS2.getVerticalScrollBar();
        int max = vertical.getMaximum();
        vertical.setValue(max);
        panel_7JS2.getViewport().setViewPosition(new Point(0, max));
        panel_7JS2.updateUI();
    }

    /***
     * 回到请求参数顶部
     */
    public void scroll2Top() {
        JScrollBar vertical = panel_7JS2.getVerticalScrollBar();
        int min = vertical.getMinimum();
        vertical.setValue(min);
        panel_7JS2.getViewport().setViewPosition(new Point(0, 0));
        panel_7JS2.updateUI();
    }

    public String getSanel_7JS2Height() {
        JScrollBar verticalScrollBar = panel_7JS2.getVerticalScrollBar();
        return "Height:" + verticalScrollBar.getHeight() + " , Max:" + verticalScrollBar.getMaximum();
    }

    /***
     * 连续发送请求
     */
    private void continueSendRequest() {
        String countStr = countTextField_2.getText2();
        if (!ValueWidget.isNullOrEmpty(countStr) && ValueWidget.isNumeric(countStr)) {
            final int count = Integer.parseInt(countStr);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < count; i++) {
                        requestAction(null, false, null);
                    }
                    ToastMessage.toast("完成发送请求 " + count + " 次", 2000);
                }
            }).start();
        } else {
            ToastMessage.toast("请输入连续发送请求的次数 ", 2000, Color.red);
            countTextField_2.requestFocus();
        }
    }

    private void rangeSendRequest() {
        RequestInRangeInfo requestInRangeInfo = this.getRequestInRangeInfo();
        if (null == requestInRangeInfo) {
            return;
        }

        RequestInfoBean requestInfoBean = getRequestInfoBean(false);
        List<ParameterIncludeBean> parameters = requestInfoBean.getParameters();
        String range = requestInRangeInfo.getRange();
        System.out.println("遍历指定范围发送请求 :" + range);
        if (range.contains("..")) {//模仿freeMark的语法
            String[] startEnd = range.split("\\.\\.");//[0..15]
            int start = Integer.parseInt(startEnd[0]);
            int end = Integer.parseInt(startEnd[1]);

            Map extraRequestParameters = new HashMap();
            for (int i = start; i < end + 1; i++) {
                extraRequestParameters.put(requestInRangeInfo.getParameterName(), String.valueOf(i));
                requestAction(null, false, extraRequestParameters);
            }
            return;
        }
        if (!range.startsWith("[")) {
            range = "[" + range + "]";
        }
//        if(range.contains("\"")){
        String[] ranges = (String[]) HWJacksonUtils.deSerialize(range, String[].class);
        int length = ranges.length;
        Map extraRequestParameters = new HashMap();
        for (int i = 0; i < length; i++) {
            extraRequestParameters.put(requestInRangeInfo.getParameterName(), ranges[i]);
            requestAction(null, false, extraRequestParameters);
        }
//        }
    }

    public RequestInRangeInfo getRequestInRangeInfo() {
        if (null == this.originalRequestInfoBean) {
            this.originalRequestInfoBean = new RequestInfoBean();
        }
        return this.originalRequestInfoBean.getRequestInRangeInfo();
    }

    private void bindEvent() {
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                System.out.println("RequestPanel event key code:" + e.getKeyCode());
            }
        });
    }

    /***
     * "清空后再运行"
     * @param cleanUprunButton_55
     */
    private void cleanUpAction(JButton cleanUprunButton_55) {
        cleanUprunButton_55.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                final JButton button = (JButton) e.getSource();
                cleanUpTA();
                runWithProgress(button);
            }
        });
    }

    public void runWithProgress(final JButton button) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (null != autoTestPanel) autoTestPanel.testIndeterminate();
                if (RequestPanel.this.getRequestInRangeInfo().isActivate()) {
                    rangeSendRequest();
                } else {
                    requestAction(button, false);
                }
                if (null != autoTestPanel) autoTestPanel.stopIndeterminate();
            }
        }).start();
    }

    public void copyRequestInfoAction() {
        RequestInfoBean requestInfoBean = getRequestInfoBean(true);
        //restful 复制的时候,不要复制备忘
        requestInfoBean.setSingleRequestNote(null);
        //复制整个请求时,不复制请求结果
        requestInfoBean.setJsonResult(null);
        String json = HWJacksonUtils.getJsonP(requestInfoBean);
        if (!ValueWidget.isNullOrEmpty(json)) {
            WindowUtil.setSysClipboardText(json);
        }
    }

    public void pasteRequestInfoAction() {
        String json = WindowUtil.getSysClipboardText();
        if (ValueWidget.isNullOrEmpty(json)) {
            return;
        }
        json = json.trim();
        RequestInfoBean requestInfoBean = null;
        try {
            requestInfoBean = (RequestInfoBean) HWJacksonUtils.deSerialize(json, RequestInfoBean.class);
            afterPasteRequestInfo(requestInfoBean);
        } catch (LogicBusinessException e) {
            e.printStackTrace();
            System.out.println(json);
            if (!json.startsWith("{")) {
                json = "{" + json;
                requestInfoBean = (RequestInfoBean) HWJacksonUtils.deSerialize(json, RequestInfoBean.class);
                afterPasteRequestInfo(requestInfoBean);
            } else {
                //解决json 解析报错,把换行替换为空格
                json = json.replace("\r\n", " ");
                json = json.replace("\n", " ");
                requestInfoBean = (RequestInfoBean) HWJacksonUtils.deSerialize(json, RequestInfoBean.class);
                afterPasteRequestInfo(requestInfoBean);
            }

        }
    }

    public void afterPasteRequestInfo(RequestInfoBean requestInfoBean) {
        resume(requestInfoBean, true);
        if (null == getOriginalRequestInfoBean()) {
            setOriginalRequestInfoBean(requestInfoBean);
        }
        this.initializePreRequestIdListbox();
//        adaptTableSize2(true/*isFrameRepaint*/, true/*isAdd*/, false/*isDel*/);
//        adaptTableSize2(true/*isFrameRepaint*/, true/*isAdd*/, false/*isDel*/);
        if (!debug) {
            autoTestPanel.optimizParameterScroll(this, true);
        }
    }

    /***
     *
     * @return
     */
    private int[] getDividerHeight() {
        int fullHeight = autoTestPanel.getFullHeight();
        if (autoTestPanel.isDebug()) {
            return new int[]{570, 60/*fullHeight*3/8*/};
        } else {
            return new int[]{150, fullHeight * 7 / 16};
        }

    }

    /***
     * 清空所有的文本框
     */
    public void cleanUpTA() {
        resultTextPane.setText(SystemHWUtil.EMPTY);
        respTextArea_9.setText(SystemHWUtil.EMPTY);
        try {//javax.swing.text.BadLocationException: Position not represented by view
            jsonFormattedTextArea_9.setText(SystemHWUtil.EMPTY);
        } catch (Exception e) {//TODO 去掉了异常打印
        }
        jsonFormattedTextArea_10.setText(SystemHWUtil.EMPTY);
    }

    /***
     * 修改请求ID
     */
    public void requestIDAction() {
        if (!DialogUtil.verifyTFEmpty(requestIdTextField_5, "请求ID")) {
            return;
        }
        requestIDDispache(true);
    }

    public void requestIDDispache() {
        requestIDDispache(false);
    }

    /***
     *
     * @param isManual : 是否是手动添加的
     */
    public void requestIDDispache(boolean isManual) {
        if (null == requestIdTextField_5 || debug) {
            return;
        }
        String requestId = requestIdTextField_5.getText2();
        //请求ID
        if (ValueWidget.isNullOrEmpty(requestId)) {
            if (isManual) {
                ToastMessage.toast("请求ID 为空", 2000, Color.red);
            }
            return;
        }

        if (autoTestPanel.getRequestIdList().contains(requestId)) {
            if (autoTestPanel.getRequestPanel4Map(requestId) != this) {//回车的是当前请求
                GUIUtil23.warningDialog("该请求ID 已经被占用,请重新填写:" + requestId);
//        	requestIdTextField_5.setText(SystemHWUtil.EMPTY);//已经发现请求ID重复了,所以直接清空
                return;
            }
        }
        if (requestIdTextField_5.isEditable()) {//如果"请求ID"输入框可编辑
            RequestPanel.this.autoTestPanel.putRequestId(requestIdTextField_5, RequestPanel.this);
            requestIdTextField_5.setEditable(false);
            selfRequestId = requestIdTextField_5.getText2();
            refreshAllPreId();
        }
    }

    /***
     * setCellEditor and setCellRenderer
     */
    public void rendTable() {
        if (debug) {
            return;
        }
        ConfigParam configParam = getConfigParam();
        parameterTable_1.getColumnModel().getColumn(2)
                .setCellEditor(new MyButtonEditor());
        parameterTable_1.getColumnModel().getColumn(2)
                .setCellRenderer(new MyButtonRender());
        if (AutoTestPanel.isTF_table_cell) {//若表格的单元格是JTextField
            parameterTable_1.getColumnModel().getColumn(0)
                    .setCellEditor(new MyTextFieldEditor(this.actionCallbackMap, configParam));
            parameterTable_1.getColumnModel().getColumn(0)
                    .setCellRenderer(new MyTextFieldRender(this.actionCallbackMap, configParam));
            parameterTable_1.getColumnModel().getColumn(1)
                    .setCellEditor(new MyTextFieldEditor(this.actionCallbackMap, configParam));
            parameterTable_1.getColumnModel().getColumn(1)
                    .setCellRenderer(new MyTextFieldRender(this.actionCallbackMap, configParam));
        }


        //设置row 高度
//        resetTableRowHeight();
        repaintTable();//单元格更新access_token 后,需要更新单元格
    }

    public void resetTableRowHeight() {
        int rowCount = parameterTable_1.getRowCount();
        for (int i = 0; i < rowCount; i++) {
            Object cellVal = parameterTable_1.getValueAt(i, 0);
            JTextComponent textComponent = getCellTextComponent(cellVal);
//            System.out.println(" :" +textComponent.getText());
            int height = getTextAreaHeight(textComponent);// textComponent.getPreferredSize().height;

            cellVal = parameterTable_1.getValueAt(i, 1);
            JTextComponent textComponent2 = getCellTextComponent(cellVal);
            int height2 = getTextAreaHeight(textComponent2);// textComponent2.getPreferredSize().height;
            System.out.println("height2 :" + height2);
            //以height为准
            if (height2 > height) {
                height = height2;
            }
            if (height > 25) {
                parameterTable_1.setRowHeight(i, height + height / 2/*parameterTable_1.getRowHeight(i)*/ + 5);
            }
        }
    }

    /***
     * 应用可选参数
     * @param frame
     * @param searchResult
     * @param tc
     * @param delta
     */
    public void optionalParameters(JFrame frame, List<String> searchResult, Component tc, int delta) {
        if (ValueWidget.isNullOrEmpty(searchResult)) {
            return;
        }
        JPopupMenu textPopupMenu = new JPopupMenu();
        textPopupMenu.setForeground(Color.red);
        textPopupMenu.setBackground(Color.blue);
        textPopupMenu.setOpaque(false);//透明度
        OptionalParametersListener searchPopupMenuListener = new OptionalParametersListener(this);

        for (int i = 0; i < searchResult.size(); i++) {
            String item = searchResult.get(i);
            JMenuItem optionalParameterM = new JMenuItem(item);
            optionalParameterM.setActionCommand(String.valueOf(i));
            optionalParameterM.addActionListener(searchPopupMenuListener);
            textPopupMenu.add(optionalParameterM);
        }
        JMenuItem cancelM = new JMenuItem("取消");
        Point point = tc.getLocation();
        if (null != cancelM) {
            cancelM.addActionListener(searchPopupMenuListener);
            textPopupMenu.add(cancelM);
        }

        int y = point.y + 320;// 下移一点
        if (delta > 0) {
            y = y - delta;
        }
        textPopupMenu.show(frame, point.x + 630, y);
    }

    /***
     * 初始化表格<br >只显示一行
     */
    public void layoutTable() {
        Object[][] datas = new Object[1][3];
        for (int i = 0; i < datas.length; i++) {
            Object[] objs = new Object[3];
            RadioButtonPanel panel = new RadioButtonPanel();
            panel.init();
            objs[2] = panel;
            if (AutoTestPanel.isTF_table_cell) {
                Color clor = CustomColor.getMoreLightColor();
                AssistPopupTextArea keyTA = new AssistPopupTextArea(this.actionCallbackMap);
                keyTA.setBackground(clor);
                objs[0] = new JScrollPane(keyTA);
                GenerateJsonTextArea valTA = new GenerateJsonTextArea(this.actionCallbackMap);
                valTA.setBackground(clor);
                objs[1] = new JScrollPane(valTA);
            }
            datas[i] = objs;
        }
        TableUtil3.appendTableData(parameterTable_1, datas, getConfigParam());
//    	repaintTable();

    }

    /***
     * 打开网页发送POST请求
     */
    public void sendPostAction() {
        RequestInfoBean requestInfoBean = getRequestInfoBean(true/*isSendHttpRequest*/, false/*isUrlEncoding*/);
        addHttpDefaultParameter(requestInfoBean/*,isEncodeParameterCheckbox.isSelected(),urlEncodeParameterCharset*/);
        //补充依赖接口参数
        String beforeId = requestInfoBean.getPreRequestId();
        if (isHasPreRequest(beforeId)) {
            beforeRequestPanel = getBeforeRequestPanel(beforeId);
            if (!ValueWidget.isNullOrEmpty(beforeRequestPanel)) {
                RequestInfoBean preRequestInfoBean = beforeRequestPanel.getRequestInfoBean(true);
                //前置请求执行
                ResponseResult preResponseResult = new ResponseResult(preRequestInfoBean).invoke();
                if (null == preResponseResult) {
                    ToastMessage.toast("preResponseResult is null", 3000, Color.RED);
                    return;
                }
                String preRequestParameterValue = TableUtil.getPreRequestVal(preResponseResult, requestInfoBean);
                if (!ValueWidget.isNullOrEmpty(preRequestParameterValue)) {
                    requestInfoBean.addParameter(requestInfoBean.getCurrRequestParameterName(), preRequestParameterValue);
                }
            }
        }
        Map requestParameters = requestInfoBean.getRequestParameters();
        String servletAction = requestInfoBean.getActionPath();
        String url = HttpSocketUtil.getFullUrl(servletAction, requestInfoBean);
//        String url = (requestInfoBean.isSsl() ? "https://" : "http://")
//                + requestInfoBean.getServerIp() + ":" + requestInfoBean.getPort() + requestInfoBean.getActionPath();
        String requestCharset = (String) encodingComboBox.getSelectedItem();
        String formData = HttpSocketUtil.getPostForm(requestParameters, url, requestCharset);
        String fileName = "index" + RandomUtils.getTimeRandom2() + ".html";
        String tempDir = System.getProperty("user.home");
        if (!tempDir.endsWith(File.separator)) {
            tempDir += File.separator;
        }
        String fileFullPath = tempDir + "AppData" + File.separator + "Local" + File.separator + "Temp" + File.separator + "http" + File.separator + fileName;
        System.out.println("fileFullPath:" + fileFullPath);
//		String tempDir=System.getProperty("java.io.tmpdir");

//		String fileFullPath=tempDir+"http"+File.separator+fileName;
        File file = new File(fileFullPath);
        String parentDirStr = SystemHWUtil.getParentDir(file);
        File parentDir = new File(parentDirStr);
        if (!parentDir.exists()) {
            parentDir.mkdirs();
        }
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e1) {
                e1.printStackTrace();
                GUIUtil23.errorDialog(e1);
            }
        }
        FileUtils.writeToFile(file, formData, requestCharset/*SystemHWUtil.CHARSET_UTF*/);
        //启用系统默认浏览器来打开网址。
        String url2 = "file:///" + fileFullPath.replaceAll("\\\\", "/");
        try {
            URI uri = new URI(url2);
            Desktop.getDesktop().browse(uri);
        } catch (URISyntaxException e2) {
            e2.printStackTrace();
//            GUIUtil23.errorDialog(e2);
            ToastMessage.toast(e2.getMessage(), 3000, Color.red);
        } catch (IOException e2) {
            e2.printStackTrace();
//            GUIUtil23.errorDialog(e2);
            WindowUtil.setSysClipboardText(url2);
            ToastMessage.toast(e2.getMessage(), 3000, Color.red);
        }
    }

    public void setTableData2(Object[][] datas) {
        DefaultTableModel model = new DefaultTableModel(datas, columnNames);
        parameterTable_1.setModel(model);
        this.parameterTable_1.setRowHeight(SINGLE_ROW_HEIGHT);
        rendTable();
    }

    public List<ParameterIncludeBean> getTableParameters() {
        Object[][] data2 = getParameter4Table();

        return TableUtil.getTableParameters(data2);
    }

    protected TreeMap getParameterMap(boolean isUrlEncoding, String requestCharset) {
        List<ParameterIncludeBean> parameters = getTableParameters();
        //如果请求参数中含有&特殊字符,则需要进行URL编码
        if (!ValueWidget.isNullOrEmpty(parameters)) {
            int size = parameters.size();
            for (int i = 0; i < size; i++) {
                ParameterIncludeBean parameterIncludeBean = parameters.get(i);
                if (isUrlEncoding) {
                    urlencodeParameter(parameterIncludeBean);
                }
            }
        }

        return TableUtil.getParameterMap(parameters, isUrlEncoding, requestCharset);
    }

    /***
     * 获取表格中的请求要素
     *
     * @return
     */
    protected Object[][] getParameter4Table() {
        return TableUtil.getParameter4Table(parameterTable_1/*,true*/).getOnlyStringValue();
    }

    /***
     * 刷新表格
     */
    public void repaintTable() {
        parameterTable_1.repaint();
    }

    private boolean isHasPreRequest(String beforeId) {
        if (null == preRequestCheckBox) {
            return getOriginalRequestInfoBean().isHasPreRequest();
        }
        return preRequestCheckBox.isSelected() && !ValueWidget.isNullOrEmpty(beforeId) && !SystemHWUtil.MIDDLE_LINE.equals(beforeId) /*&&!isCallbacked*/;
    }

    public void requestAction(final JButton runButton) {
//        String continueCount = countTextField_2.getText2();
//        if (ValueWidget.isNullOrEmpty(continueCount)) {
        requestAction(runButton, true);//仅发送一次
        /*} else {
            int result = JOptionPane.showConfirmDialog(null, "确定连续发送请求" + continueCount + " 次 ?", "确认",
                    JOptionPane.OK_CANCEL_OPTION);
            if (result == JOptionPane.OK_OPTION) {
                continueSendRequest();//连续发送请求
            } else {
                requestAction(runButton, true);
                System.out.println("仅发送一次");
            }
        }*/

    }

    public void openPreRequestAction() {
        String beforeId = (String) preRequestIdComboBox_2.getSelectedItem();
        System.out.println("beforeId:" + beforeId);
        if (!isHasPreRequest(beforeId)) {
            ToastMessage.toast("无前置请求", 2000, Color.RED);
            return;
        }
        /*RequestPanel beforeRequestPanel=getBeforeRequestPanel(beforeId);
        if(!ValueWidget.isNullOrEmpty(beforeRequestPanel)){
            tabbedPane_1.setSelectedComponent(beforeRequestPanel);
        }*/
        if (!debug) {
            selectRequestPanel(beforeId);
        }
    }

    public void selectRequestPanel(String beforeId) {
        int selectedTabIndex = autoTestPanel.getTabIndexByRequestID(beforeId);
        System.out.println("selectedTabIndex:" + selectedTabIndex);
        if (selectedTabIndex != SystemHWUtil.NEGATIVE_ONE) {
            autoTestPanel.getTabbedPane_2().setSelectedIndex(selectedTabIndex);
        }
    }

    public void autoGenerateSendHttpCode(boolean basic) {
        if (debug) {
            return;
        }
        RequestInfoBean requestInfoBean = this.getRequestInfoBean(false);
        String httpRequestCode = null;
        if (basic) {
            httpRequestCode = HttpSocketUtil.getBasicHttpRequestCodeUnitTest(requestInfoBean);
        } else {
            if (!ValueWidget.isNullOrEmpty(autoTestPanel.getGlobalConfig()) && autoTestPanel.getGlobalConfig().isGenerateRequestCodeWithUnitTest()) {
                httpRequestCode = HttpSocketUtil.getHttpRequestCodeUnitTest(requestInfoBean);
            } else {
                httpRequestCode = HttpSocketUtil.getHttpRequestCode(requestInfoBean);
            }
        }
        copyCode2Clipboard(httpRequestCode);
    }

    public void copyCode2Clipboard(String httpRequestCode) {
        if (debug) {
            return;
        }
        WindowUtil.setSysClipboardText(httpRequestCode);
        ToastMessage.toast("可直接黏贴", 2000);
        autoTestPanel.appendStr2LogFile(SystemHWUtil.DIVIDING_LINE + "自动生成代码 :" + SystemHWUtil.CRLF + httpRequestCode
                + SystemHWUtil.CRLF + SystemHWUtil.DIVIDING_LINE, true);
    }

    /***
     * 生成发送请求代码cURL(单行)
     * @param oneLine
     */
    public void autoGeneratecurlCode(boolean oneLine) {
        RequestInfoBean requestInfoBean = this.getRequestInfoBean(true);
        String httpRequestCode = HttpSocketUtil.getCurlCode(requestInfoBean);
        if (oneLine) {
            httpRequestCode = SystemHWUtil.CRLF2Blank(httpRequestCode);
        }
        copyCode2Clipboard(httpRequestCode);
    }

    /***
     * 生成python 代码
     */
    public void autoGenerateSendHttpPythonCode(boolean isExprt) {
        if (debug) {
            return;
        }
        RequestInfoBean requestInfoBean = this.getRequestInfoBean(false);
        String httpRequestCode = null;
        httpRequestCode = HttpSocketUtil.getPythonHttpRequestCode(requestInfoBean);

        if (isExprt) {//导出代码到本地文件,但是不会复制到剪切板
            if (saveCode(httpRequestCode)) return;
        } else {
            WindowUtil.setSysClipboardText(httpRequestCode);
            ToastMessage.toast("可直接黏贴", 2000);
        }
        autoTestPanel.appendStr2LogFile(SystemHWUtil.DIVIDING_LINE + "自动生成代码 python:" + SystemHWUtil.CRLF + httpRequestCode
                + SystemHWUtil.CRLF + SystemHWUtil.DIVIDING_LINE, true);
    }

    private boolean saveCode(String httpRequestCode) {
        if (debug) {
            return false;
        }
        String saveCodeDefaultFolderStr = null;
        saveCodeDefaultFolderStr = getSaveCodeDefaultFolder(saveCodeDefaultFolderStr);
        File defaultSelectFolder = null;
        defaultSelectFolder = getDefaultSelectedFolder(saveCodeDefaultFolderStr);
        File selectedFile = DialogUtil.chooseFileDialog(defaultSelectFolder, "保存Python 文件", autoTestPanel, "py");
        if (null == selectedFile) {
            return true;
        }
        //即时更新saveCodeDefaultFolder
        setSaveCodeDefaultFolder(selectedFile.getAbsolutePath());
        FileUtils.writeStrToFile(selectedFile, httpRequestCode, true);
        return false;
    }

    private String getSaveCodeDefaultFolder(String saveCodeDefaultFolderStr) {
        if (debug) {
            return SystemHWUtil.EMPTY;
        }
        if (!ValueWidget.isNullOrEmpty(autoTestPanel.getGlobalConfig())) {
            saveCodeDefaultFolderStr = autoTestPanel.getGlobalConfig().getSaveCodeDefaultFolder();
        }
        return saveCodeDefaultFolderStr;
    }

    private void setSaveCodeDefaultFolder(String saveCodeDefaultFolderStr) {
        if (!ValueWidget.isNullOrEmpty(autoTestPanel.getGlobalConfig())) {
            autoTestPanel.getGlobalConfig().setSaveCodeDefaultFolder(saveCodeDefaultFolderStr);
        }
    }

    public void requestAction(final JButton runButton, final boolean isThread) {
        if (continueSendCheckBox.isSelected()) {
            continueSendRequest();
            continueSendCheckBox.setSelected(false);//调用一次之后,自动取消选中,以免误操作
            return;
        }
        if (needSendRetryTimes()) {
            Integer sendRetryTimes = getAutoTestPanel().getGlobalConfig().getSendRetryTimes();
            if (isThread) {
                requestAction(runButton, isThread, null);
            } else {
                for (Integer i = 0; i < sendRetryTimes; i++) {
                    requestAction(runButton, isThread, null);
                }
            }
        } else {
            requestAction(runButton, isThread, null);
        }

    }

    private boolean needSendRetryTimes() {
        return getAutoTestPanel().getGlobalConfig().needSendRetryTimes(getActionName());
    }

    /***
     *
     * @param runButton
     * @param isThread : 是否要使用线程
     */
    public void requestAction(final JButton runButton, final boolean isThread, /*final boolean isToastStart*/final Map extraRequestParameters) {
        System.out.println("start send ################################################################# ");
        final RequestInfoBean requestInfoBean = getRequestInfoBean(true);
        if (ValueWidget.isNullOrEmpty(requestInfoBean)) {
            String errorMessage = "requestInfoBean is null";
            System.out.println(errorMessage);
            ToastMessage.toast(errorMessage, 2000, Color.RED);
            return;
        }
        if (ValueWidget.isNullOrEmpty(requestInfoBean.getCurrentRequestName())) {
            ToastMessage.toast("请输入请求名称", 2000, Color.RED);
            return;
        }
        if (!ValueWidget.isNullOrEmpty(runButton)) {
            runButton.setEnabled(false);
        }
        /*if (isToastStart) { 因为已经有进度条了
            new Thread(new Runnable() {
                @Override
                public void run() {
                    ToastMessage.toast("已发送请求,请稍后...", 500);
                }
            }).start();
        }*/
        String beforeId = requestInfoBean.getPreRequestId();
        boolean hasPreRequest = false;
        if (isHasPreRequest(beforeId)) {
            beforeRequestPanel = getBeforeRequestPanel(beforeId);
            if (!beforeRequestPanel.isAlreadyRended2()) {
                autoTestPanel.rendPanel(beforeRequestPanel);
            }
            if (!ValueWidget.isNullOrEmpty(beforeRequestPanel)) {
                hasPreRequest = true;//说明有前置请求
                beforeRequestPanel.setAfterRequestPanel(this);
                beforeRequestPanel.setRequestCallBack(new RequestCallBack() {
                    @Override
                    public void callback(String resultJson) throws JSONException, UnsupportedEncodingException, org.json.JSONException {
                        ComponentUtil.appendResult(respTextArea_9, "pre:" + resultJson, true);
                        Map<String, String> responseMap = JSONHWUtil.getMap(resultJson);
                        String preRequestKey = requestInfoBean.getPreRequestParameterName();
                        preRequestParameterValue = responseMap.get(preRequestKey);
//							isCallbacked=true;
                        if (preRequestKey.equals("auth_code") && ValueWidget.isNullOrEmpty(preRequestParameterValue)) {
                            String codeTmp = (String) responseMap.get("code");
//                            	System.out.println("code:"+codeTmp);
//                        	CodeExchangeTokenDialog codeExchangeTokenDialog=new CodeExchangeTokenDialog(codeTmp);
//                        	codeExchangeTokenDialog.exchangeAction();
//                			codeExchangeTokenDialog.setVisible(true);
                            resetPre();
                            return;
                        }
                        if (isThread) {//异步执行
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    if (needSendRetryTimes()) {
                                        Integer sendRetryTimes = getAutoTestPanel().getGlobalConfig().getSendRetryTimes();
                                        for (Integer i = 0; i < sendRetryTimes; i++) {
                                            doRequest(extraRequestParameters);
                                        }
                                    } else {
                                        doRequest(extraRequestParameters);
                                    }
                                    resetPre();
                                }
                            }).start();

                        } else {
                            doRequest(extraRequestParameters);
                            resetPre();
                        }
                    }
                });
                beforeRequestPanel.requestAction(null);
            }

        }
        if (!hasPreRequest) {//没有前置请求
            if (isThread) {//异步执行
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        doRequest(extraRequestParameters);
                    }
                }).start();
            } else {
                doRequest(extraRequestParameters);
            }
        }
        if (!ValueWidget.isNullOrEmpty(runButton)) {
            runButton.setEnabled(true);//按钮恢复可用
        }
    }

    /***
     * 清空前置请求
     */
    private void resetPre() {
        beforeRequestPanel.setRequestCallBack(null);
        beforeRequestPanel.preRequestCallbackInfo.setCallback(false);
        beforeRequestPanel = null;
    }

    public void copyAccessToken(JButton runButton) {
        if (ValueWidget.isNullOrEmpty(responseJsonResult)) {
            responseJsonResult = resultTextPane.getText2();
        }
        if (ValueWidget.isNullOrEmpty(responseJsonResult)) {
            requestAction(runButton);
        }
        if (ValueWidget.isNullOrEmpty(responseJsonResult)) {
            return;
        }
        try {
            Map responseMap = JSONHWUtil.getMap(responseJsonResult);
            if (null == responseMap) {
                ToastMessage.toast("应答结果不对", 2000, Color.red);
                return;
            }
            String access_token = (String) responseMap.get("access_token");
            if (!ValueWidget.isNullOrEmpty(access_token)) {
                WindowUtil.setSysClipboardText(access_token);
                ToastMessage.toast("已复制到剪切板", 2000);
            }
        } catch (JSONException e1) {
            e1.printStackTrace();
//                GUIUtil23.errorDialog(e1);
            ToastMessage.toast(e1.getMessage(), 3000, Color.red);
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
//                GUIUtil23.errorDialog(e1);
            ToastMessage.toast(e1.getMessage(), 3000, Color.red);
        }
    }

    /***
     * 把HTTP 请求默认值添加具体的单个请求中<br />
     * http默认值
     * @param requestInfoBean
     */
    private void addHttpDefaultParameter(RequestInfoBean requestInfoBean/*,boolean isUrlEncoding,String requestCharset*/) {
        RequestDefaultBean requestDefaultBean = autoTestPanel.getRequestDefault();
        TreeMap requestParameters = requestDefaultBean.getRequestParameters();
        if (!ValueWidget.isNullOrEmpty(requestParameters)) {
            //必须在 requestParameters.putAll(requestInfoBean.getRequestParameters());  之前执行,因为putAll 会修改其值
            List<ParameterIncludeBean> parameters = requestInfoBean.getParameters();
            for (Object obj : requestParameters.keySet()) {
                String key = (String) obj;
                int index = TableUtil.isContains2(parameters, key);
                if (index == SystemHWUtil.NEGATIVE_ONE) {
                    Object object = requestParameters.get(obj);
                    ParameterIncludeBean parameterIncludeBean = new ParameterIncludeBean();
                    parameterIncludeBean.setKey(key);
                    parameterIncludeBean.setValue((String) object);
                    parameters.add(parameterIncludeBean);
                }
            }

            requestParameters.putAll(requestInfoBean.getRequestParameters());
            requestInfoBean.setRequestParameters(requestParameters);
            requestInfoBean.setRequestBodyData(WebServletUtil.getRequestBodyFromMap(requestInfoBean.getRequestParameters()));
        }
        //编码
        if (ValueWidget.isNullOrEmpty(requestInfoBean.getCharset())) {
            requestInfoBean.setCharset(requestDefaultBean.getCharset());
        }
        //服务ip
        if (ValueWidget.isNullOrEmpty(requestInfoBean.getServerIp())) {
            requestInfoBean.setServerIp(requestDefaultBean.getServerIp());
        }
        //端口号
        if (ValueWidget.isNullOrEmpty(requestInfoBean.getPort())) {
            requestInfoBean.setPort(requestDefaultBean.getPort());
        }
        //默认cookie, 优先级比单个请求的cookie高,所以会覆盖掉单个请求中设置的cookie
        String defaultCookie2 = requestDefaultBean.getDefaultCookie();
        if (!ValueWidget.isNullOrEmpty(defaultCookie2) && !ValueWidget.isNullOrEmpty(defaultCookie2.trim())) {
            requestInfoBean.setRequestCookie(defaultCookie2.trim());
        }
    }

    /***
     * 把HTTP 请求默认值添加具体的单个请求中
     * @param parameters
     */
    private String addHttpDefaultParameter(RequestInfoBean requestInfoBean, List<ParameterIncludeBean> parameters
            , boolean isUrlEncoding, String requestCharset) {
        RequestDefaultBean requestDefaultBean = autoTestPanel.getRequestDefault();
        Map requestParameters = requestDefaultBean.getRequestParameters();
        String result = addExtra2RequestInfo(requestInfoBean, parameters, isUrlEncoding, requestCharset, requestParameters);
        //编码
        if (ValueWidget.isNullOrEmpty(requestInfoBean.getCharset())) {
            requestInfoBean.setCharset(requestDefaultBean.getCharset());
        }
        //服务ip
        if (ValueWidget.isNullOrEmpty(requestInfoBean.getServerIp())) {
            requestInfoBean.setServerIp(requestDefaultBean.getServerIp());
        }
        //端口号
        if (ValueWidget.isNullOrEmpty(requestInfoBean.getPort())) {
            requestInfoBean.setPort(requestDefaultBean.getPort());
        }
        return result;
    }

    private String addExtra2RequestInfo(RequestInfoBean requestInfoBean, List<ParameterIncludeBean> parameters, boolean isUrlEncoding, String requestCharset, Map requestParameters) {
        if (!ValueWidget.isNullOrEmpty(requestParameters)) {
            for (Object obj : requestParameters.keySet()) {
                if (!TableUtil.isContain(parameters, ((String) obj))) {
                    ParameterIncludeBean parameterIncludeBean = new ParameterIncludeBean();
                    parameterIncludeBean.setKey((String) obj);
                    String val = (String) requestParameters.get(obj);
                    if (isUrlEncoding && !ValueWidget.isNullOrEmpty(requestCharset)) {
                        try {
                            val = URLEncoder.encode(val, requestCharset);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    parameterIncludeBean.setValue(val);
                    parameters.add(parameterIncludeBean);
                }
            }
        }

        StringBuffer sbuffer = TableUtil3.getRequestBodyFromList(parameters, isUrlEncoding, urlEncodeParameterCharset, false/*isAutoUrlEncoding*/);
        String result = sbuffer.toString();
        if (sbuffer.length() != 0) {
            requestInfoBean.setRequestBodyData(result);
        }
        return result;
    }

    private boolean isOKResponse(int resCode) {
        return (resCode == 200 || resCode == 201);
    }

    private void doRequest(Map extraRequestParameters) {
        doRequest(true, extraRequestParameters);
    }

    /***
     * 发送请求,发送http请求
     * @param isSwing
     */
    private void doRequest(boolean isSwing, Map extraRequestParameters) {
        RequestInfoBean requestInfoBean = getRequestInfoBean(true);
        String servletPath = requestInfoBean.getActionPath();
        if (autoTestPanel.getGlobalConfig().isAlertConfirmWhenRequestOnline()) {
            if (onlineConfirm(requestInfoBean, servletPath)) return;
        }
        requestInfoBean.setGlobalConfig(autoTestPanel.getGlobalConfig());
        String requestCharset = requestInfoBean.getCharset();

        //以表格中的参数为准,不以Body Data标签页中的为标准
        String beforeId = requestInfoBean.getPreRequestId();

        String prekey = requestInfoBean.getCurrRequestParameterName();
        List<ParameterIncludeBean> parameters = requestInfoBean.getParameters();
        if (isHasPreRequest(beforeId) && ((!ValueWidget.isNullOrEmpty(preRequestParameterValue) && everytimeCheckBox.isSelected())
                || (!everytimeCheckBox.isSelected() && (
                ValueWidget.isNullOrEmpty(preRequestParameterValue) ||
                        ValueWidget.isNullOrEmpty(requestInfoBean.getRequestParameters().get(prekey)/*请求参数表格中对应pre 参数为空 */)
        )))) {
            replacePreParamAlert(selfParameterTextField_3);

            ComponentUtil.appendResult(respTextArea_9, "前置请求的参数值:" + this.preRequestParameterValue, false);
            //当前请求的参数名

            if (isHttpRequestParameter()) {//请求体参数
                int index = TableUtil.isContains2(parameters, prekey);
                if (index == SystemHWUtil.NEGATIVE_ONE) {
                    ParameterIncludeBean parameterIncludeBean = new ParameterIncludeBean();
                    parameterIncludeBean.setKey(prekey);
                    parameterIncludeBean.setValue((String) this.preRequestParameterValue);
                    parameters.add(parameterIncludeBean);
                } else {
                    if (preRequestParameterValue instanceof String) {
                        parameters.get(index).setValue((String) preRequestParameterValue);
                    } else {
                        parameters.get(index).setValue(String.valueOf(preRequestParameterValue));
                    }
                    requestInfoBean.getRequestParameters().put(prekey, preRequestParameterValue);
                }
            } else {//请求头参数header
                ListOrderedMap headerMap = requestInfoBean.getHeaderMap();
                headerMap.put(prekey, preRequestParameterValue);
            }

            updateParameter(requestInfoBean, parameters);
            if (requestInfoBean.isApplyDefaultHTTP()) {
                addHttpDefaultParameter(requestInfoBean, parameters, requestInfoBean.isUrlEncoding()/*isUrlEncoding*/, urlEncodeParameterCharset);//important
            }

        }//if
        if (!ValueWidget.isNullOrEmpty(extraRequestParameters)) {
            if (requestInfoBean.isRequestBodyIsJson()) {
                String jsonBody = requestInfoBean.getRequestBodyData();
                for (Object keyTmp : extraRequestParameters.keySet()) {
                    String key = (String) keyTmp;
                    Object val = extraRequestParameters.get(keyTmp);
                    jsonBody = jsonBody.replaceAll("(\"" + key + "\"[ ]*:)[ ]*[\\w]+", "$1" + val);
                }
                requestInfoBean.setRequestBodyData(jsonBody);
            } else {
                addExtra2RequestInfo(requestInfoBean, parameters, requestInfoBean.isUrlEncoding(), requestCharset, extraRequestParameters);
            }
        }
        requestInfoBean.setParameters(parameters);
        requestInfoBean.setAutoUrlEncoding(true);//TODO 应该从配置中读取
        if (isSwing) {
            ComponentUtil.appendResult(respTextArea_9, requestInfoBean.toString(), false);
        }

        ResponseResult responseResult = new ResponseResult(requestInfoBean/*, respTextArea_9, autoTestPanel, resultTextPane*/).invoke();
        fullUrlTextField.setText(requestInfoBean.getUrl());

        if (isSwing && null != respTextArea_9 && !ValueWidget.isNullOrEmpty(responseResult)) {
            ComponentUtil.appendResult(respTextArea_9, "request contentType:" + responseResult.getRequestContentType(), false);
            ComponentUtil.appendResult(respTextArea_9, "执行时间:" + responseResult.getDelta() + "\t毫秒", false);
        }
        String descUrl = "请求的地址:\n" + requestInfoBean.getUrl();
        if (isSwing) {
            ComponentUtil.appendResult(respTextArea_9, descUrl, false);
        }
        if (null != autoTestPanel) {
            //每次发送请求,都保存一次配置文件
            autoTestPanel.saveConfigCondition();
            autoTestPanel.appendStr2LogFile(descUrl, false);
        }

        String descParameter = "请求的参数:\n" + requestInfoBean.getRequestParameters();
        String descParameterPost = "Post请求的参数:\n" + requestInfoBean.getRequestBodyData();
        if (isSwing) {
            ComponentUtil.appendResult(respTextArea_9, descParameter, false);
            ComponentUtil.appendResult(respTextArea_9, descParameterPost, false);
        }
        if (isSwing) {
            autoTestPanel.appendStr2LogFile(descParameter);
            autoTestPanel.appendStr2LogFile(descParameterPost);
        }
        if (isSwing) {
            ComponentUtil.appendResult(respTextArea_9, "请求编码:" + requestCharset, false);
        }
        if (null == responseResult) {
            return;
        }
        if (isSwing && !ValueWidget.isNullOrEmpty(responseResult)) {
            ComponentUtil.appendResult(respTextArea_9, "response code :" + responseResult.getResCode(), false/*isAddDivide*/, false);
        }
        if (!ValueWidget.isNullOrEmpty(responseResult) && responseResult.is()) {
            ComponentUtil.appendResult(resultTextPane, "请检查请求的url 是否正确(" + requestInfoBean.getUrl() + ")", false);
            String errerMessage = "返回结果为空.";
            ComponentUtil.appendResult(resultTextPane, errerMessage, true);
            //                GUIUtil23.warningDialog(errerMessage);
            ToastMessage.toast("返回码:" + responseResult.getResCode(), 3000, Color.red);
            return;
        }
        HttpResponseResult resultArr = responseResult.getResultArr();
        int resCode = responseResult.getResCode();
        if (null == resultArr) {
            return;
        }
        String respContentType = resultArr.getContentType();
        System.out.println("respContentType:" + respContentType);//text/plain;charset=utf-8;text/html;charset=UTF-8
        if (isSwing) {
            ComponentUtil.appendResult(respTextArea_9, "response contentType :" + respContentType, true/*isAddDivide*/, false);
        }
        //只对/internal_api/v1/webLoginAppAndAuthorizeApp 接口做判断
        if (requestInfoBean.getActionPath().endsWith("/internal_api/v1/webLoginAppAndAuthorizeApp") && resCode == 404) {
            ToastMessage.toast("请确认auth_code 是否已经更新 或者/internal_api/authorizeByJsonp 接口的client_id是否正确", 4000, Color.RED);
        }
        boolean hasShowHtmlDialog = false;
        String responseResultJson = responseResult.getResponseJsonResult();
        responseJsonResult = responseResultJson;
//        getRequestInRangeInfo().setResponseSavePath("/Users/whuanghkl/tmp/response.txt");

        if (getRequestInRangeInfo().isActivate()) {
            if (getRequestInRangeInfo().isFormatJson()) {
                responseResultJson = JSONHWUtil.formatJson(responseResultJson);
            }
            FileUtils.writeStrToFile(new File(getRequestInRangeInfo().getResponseSavePath()), responseResultJson + SystemHWUtil.CRLF
                    + SystemHWUtil.ENGLISH_COMMA + SystemHWUtil.CRLF, true, true);
        }
        if (!ValueWidget.isNullOrEmpty(respContentType) && respContentType.contains("text/html") && (!ValueWidget.isNullOrEmpty(responseResultJson) && !responseResultJson.startsWith("{")) &&
                RegexUtil.contain2(responseResultJson, "html")) {
            showHtmlDialog();
            hasShowHtmlDialog = true;
        }


        if (isOKResponse(resCode) && !ValueWidget.isNullOrEmpty(preRequestCallbackInfo) && preRequestCallbackInfo.isCallback() && !ValueWidget.isNullOrEmpty(preRequestCallbackInfo.getRequestCallBack())) {//200 or 201 is right
            preRequestCallbackInfo.setCallback(false);
            try {
                preRequestCallbackInfo.getRequestCallBack().callback(responseResultJson);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (org.json.JSONException e) {
                e.printStackTrace();
            }
//                resetPre();
        }
        if (!ValueWidget.isNullOrEmpty(responseResultJson)) {
            ComponentUtil.appendResult(resultTextPane, responseResultJson, false, false);//不能根据resCode=200来判断,因为resCode为499也会返回json
        }
        if (isOKResponse(resCode)) {
            //如果是智能模式并且cookie输入框为空,则自动填充cookie
            if (null != autoTestPanel.getGlobalConfig() && autoTestPanel.getGlobalConfig().isIntelligenceMode()
                    && ValueWidget.isNullOrEmpty(httpHeadPanel.getRequestCookies())) {
                httpHeadPanel.setRequestCookies(responseResult.getCookies());
            }
//            ComponentUtil.appendResult(resultTextPane, responseResultJson, false, false);//不能根据resCode=200来判断,因为resCode为499也会返回json
            autoTestPanel.appendStr2LogFile(SystemHWUtil.DIVIDING_LINE + SystemHWUtil.CRLF + responseResultJson
                    + SystemHWUtil.CRLF + SystemHWUtil.DIVIDING_LINE, true);
            if (!ValueWidget.isNullOrEmpty(resultArr.getResponseCookie())) {
                String descCookie = "cookie:\n" + resultArr.getResponseCookie();
                ComponentUtil.appendResult(resultTextPane, descCookie, true, false);
                autoTestPanel.appendStr2LogFile(descCookie);
            }

            autoTestPanel.appendResponseSetCookie(resultArr.getResponseCookie());
            try {
                if (!ValueWidget.isNullOrEmpty(responseResultJson)) {
                    Map responseMap = null;
                    try {
                        responseMap = JSONHWUtil.getMap(responseResultJson);
                    } catch (Exception e) {
                        e.printStackTrace();//org.codehaus.jackson.JsonParseException
                    }
                    if (!ValueWidget.isNullOrEmpty(responseMap) &&
                            (requestInfoBean.getActionPath().equals("/internal_api/v1/webLoginAppAndAuthorizeApp")//特殊处理
                                    || requestInfoBean.getActionPath().endsWith("/login/webLogin"))) {
                        String errorCode = (String) responseMap.get("errorCode");
                        if (!ValueWidget.isNullOrEmpty(errorCode)) {
                            if (errorCode.equals("20129")) {
                                String errorMessage = "用户账号错误(可能是配置了CIA hosts)";
                                ComponentUtil.appendResult(resultTextPane, errorMessage, true);
                                ToastMessage.toast(errorMessage, 1000, Color.red);
                            } else if (errorCode.equals("20119")) {
                                ComponentUtil.appendResult(resultTextPane, "用户密码错误", true);
                                ToastMessage.toast("用户密码错误", 1000, Color.red);
                            } else if (errorCode.equals("20106")) {
                                ComponentUtil.appendResult(resultTextPane, "用户不存在", true);
                                ToastMessage.toast("用户不存在", 1000, Color.red);
                            } else if (errorCode.equals("20122")) {
                                ComponentUtil.appendResult(resultTextPane, "用户昵称重复", true);
                                ToastMessage.toast("用户昵称重复", 1000, Color.red);
                            }
                        }
                    }
//                        ComponentUtil.appendResult(respTextArea_9, responseResult.responseJsonResult, true);
                    String beforeFormatJson = JSONHWUtil.deleteCallback(responseResultJson, "callback");
                    try {
                        jsonFormattedTextArea_9.setText(JSONHWUtil.toJson(beforeFormatJson));//把callback去掉,便于解析json
                    } catch (Exception e) {
                        e.printStackTrace();
                        jsonFormattedTextArea_9.setText("json 格式化失败:" + e.getMessage());
                    }
                    jsonFormattedTextArea_10.setText(beforeFormatJson);
                    if (ValueWidget.isNullOrEmpty(singleRequestNoteTA.getText2())) {
                        singleRequestNoteTA.setText(responseResultJson);
                    }
                    if (!ValueWidget.isNullOrEmpty(responseMap) || responseResultJson.trim().startsWith("[")) {
                        //responseMap 为null,表示应答体不是json
                        //为什么不通过response contentType来判断,因为一些老接口,返回json,但是contentType却是html
                        JSONHWUtil.formatJson(jsonFormattedTextArea_9, true, null, true);
                        JSONHWUtil.formatJson(jsonFormattedTextArea_10, true, null, true);
                    }

                    //复制属性
                    if (copyFieldCheckbox.isSelected() && (!ValueWidget.isNullOrEmpty(copyField.getText2()))) {
                        String copyFieldName = copyField.getText2();
                        Map<String, String> responseMapLogin = responseResult.getResponseJsonMap();
                        String copyFieldVal = responseMapLogin.get(copyFieldName);
                        if (!ValueWidget.isNullOrEmpty(copyFieldVal)) {
                            WindowUtil.setSysClipboardText(copyFieldVal);
                            ToastMessage.toast("复制成功:" + SystemHWUtil.splitAndFilterString(copyFieldVal, 6), 2000);
                        }
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {//response code is not 200 or 201
            ComponentUtil.appendResult(resultTextPane, "返回的状态码:" + resCode, true);
            if (resCode == 400) {
                ToastMessage.toast("怀疑请求方式(" + requestMethodComboBox_2.getSelectedItem() + ")不对", 3000, Color.red);
            }
            if (!hasShowHtmlDialog && !ValueWidget.isNullOrEmpty(responseResultJson) && RegexUtil.isHTMLWebPage(responseResultJson)) {
                jsonFormattedTextArea_9.setText(responseResultJson);//把callback去掉,便于解析json
                //弹出框显示HTML
                showHtmlDialog();
            }
        }


    }

    public void updateParameter(RequestInfoBean requestInfoBean, List<ParameterIncludeBean> parameters) {
        String requestCharset = requestInfoBean.getCharset();
        setTableData3(parameters, requestInfoBean.isUrlEncoding(), requestCharset);//更新表格数据
        rendTable();
        requestInfoBean.setRequestBodyData(requestBodyDataTA.getText2());
    }

    private void showHtmlDialog() {
        //弹出框显示HTML
        System.out.println(responseJsonResult);
        CustomDefaultDialog customDefaultDialog = new CustomDefaultDialog(responseJsonResult, "显示HTML", true);
        customDefaultDialog.setVisible(true);
    }

    public RequestInfoBean getRequestInfoBean(boolean isSendHttpRequest) {
        return getRequestInfoBean(isSendHttpRequest, false);
    }

    /***
     *
     * @param isSendHttpRequest : 是否为了发送http请求
     * @return
     */
    public RequestInfoBean getRequestInfoBean(boolean isSendHttpRequest, boolean isUrlEncoding) {
        RequestInfoBean requestInfoBean = new RequestInfoBean();
//    	boolean isUrlEncoding=false;//isEncodeParameterCheckbox.isSelected();
        requestInfoBean.setUrlEncoding(isUrlEncoding);
        if (null == serverIpTextField_3) {
            return requestInfoBean;//实际上是空的
        }
        String serverIp = serverIpTextField_3.getText2().trim();
        if (isSendHttpRequest && ValueWidget.isNullOrEmpty(serverIp)) {
            ToastMessage.toast("请填写服务器ip", 2000, Color.red, new ToastCallback() {
                @Override
                public void callback() {
                    serverIpTextField_3.requestFocus();
                }
            }, null);
            return null;
        }

        requestInfoBean.setServerIp(serverIp);
        requestInfoBean.setPort(portTextField_6.getText2().trim());
        String requestMethodStr = (String) requestMethodComboBox_2.getSelectedItem();
        requestInfoBean.setRequestMethodDisplayName(requestMethodStr);
        requestInfoBean.setRequestMethod((Integer) Constant2.REQUEST_METHOD_MAP.get(requestMethodStr));
        /*if(!ValueWidget.isNullOrEmpty(requestMethodStr)&& requestMethodStr.equalsIgnoreCase("POST")){//POST请求
			requestInfoBean.setRequestMethod(Constant2.REQUEST_METHOD_POST);
		}else if("GET".equals(requestMethodStr)){//GET请求
			requestInfoBean.setRequestMethod(Constant2.REQUEST_METHOD_GET);
        }else if("PUT".equals(requestMethodStr)){//GET请求
			requestInfoBean.setRequestMethod(Constant2.REQUEST_METHOD_PUT);
        }else if("DELETE".equals(requestMethodStr)){//GET请求
			requestInfoBean.setRequestMethod(Constant2.REQUEST_METHOD_DELETE);
        }else if("OPTIONS".equals(requestMethodStr)){//GET请求
			requestInfoBean.setRequestMethod(Constant2.REQUEST_METHOD_OPTIONS);
        }else if("HEAD".equals(requestMethodStr)){//GET请求
			requestInfoBean.setRequestMethod(Constant2.REQUEST_METHOD_HEAD);
        }*/
        //以表格中的参数为准,不以Body Data标签页中的为标准
//        String requestBody = requestBodyDataTA.getText();
//        if (ValueWidget.isNullOrEmpty(requestBody)) {
        String requestCharset = (String) encodingComboBox.getSelectedItem();
        boolean toastWhenParametersNull = isSendHttpRequest;
        if (requestInfoBean.getRequestMethod() == Constant2.REQUEST_METHOD_GET) {
            toastWhenParametersNull = false;
        }
        String requestBody = generateRequestBody(isUrlEncoding/*,requestCharset*/, toastWhenParametersNull);
//        System.out.println("requestBody:"+requestBody);
//        }
        String requestBodyText = requestBodyDataTA.getText2();
        if (ValueWidget.isNullOrEmpty(requestBody) && chckbxjson.isSelected()) {
            requestBody = requestBodyText;
        }
        if (!ValueWidget.isNullOrEmpty(requestBody)) {
            //如果表格中没有参数,则不清空请求体
            requestBodyDataTA.setText(requestBody);
        }
        requestInfoBean.setPlus2percent2BWhenPost(plus2percent2BWhenPostCheckBox.isSelected());
        if (chckbxjson.isSelected() || isSendHttpRequest) {//只有选中"请求体是json"时才持久化请求体
            requestInfoBean.setRequestBodyData(requestBody);
        }
        requestInfoBean.setActionPath(actionPathTextField_7.getText2().trim());
        requestInfoBean.setPreRequestParameterName(txtAuthcode.getText2());
        requestInfoBean.setCurrRequestParameterName(selfParameterTextField_3.getText2());
        String beforeId = (String) preRequestIdComboBox_2.getSelectedItem();
        /***
         * 前置请求的id
         */
        requestInfoBean.setPreRequestId(beforeId);

        if (ValueWidget.isNullOrEmpty(requestInfoBean.getPreRequestIds())) {
            //解决复制请求panel,新复制的panel中"前置请求id"下拉框为空的问题
            int preIdsCount = preRequestIdComboBox_2.getItemCount();
            Set<String> preRequestIds = new HashSet<>();
            for (int i = 0; i < preIdsCount; i++) {
                Object itemVal = preRequestIdComboBox_2.getItemAt(i);
                if (!ValueWidget.isNullOrEmpty(itemVal) && !SystemHWUtil.MIDDLE_LINE.equals(itemVal)) {
                    preRequestIds.add((String) itemVal);
                }
            }
            requestInfoBean.setPreRequestIds(preRequestIds);
        }

        requestInfoBean.setRequestID(requestIdTextField_5.getText2());

        requestInfoBean.setCharset(requestCharset);
        /***
         * Map 形式的请求要素
         */
        requestInfoBean.setRequestParameters(getParameterMap(isUrlEncoding, urlEncodeParameterCharset));
        //请求名称
        requestInfoBean.setCurrentRequestName(requestNameTextField_4.getText2());
        if (null != httpHeadPanel) {
            String requestCookie = httpHeadPanel.getRequestCookies();
            if (!ValueWidget.isNullOrEmpty(requestCookie)) {
                requestInfoBean.setRequestCookie(requestCookie);
            }
        }
        requestInfoBean.setSendCount(countTextField_2.getText2());
        requestInfoBean.setApplyDefaultHTTP(isApplyDefaultHTTPCheckBox.isSelected());
        requestInfoBean.setRequestBodyIsJson(chckbxjson.isSelected());
        requestInfoBean.setHasPreRequest(preRequestCheckBox.isSelected());
        String responseJson = jsonFormattedTextArea_9.getText();
        if (!ValueWidget.isNullOrEmpty(responseJson) && responseJson.length() < maxTextLength) {
            requestInfoBean.setJsonResult(responseJson);
        }

        requestInfoBean.setSendPreRequestPer(isSendPreRequestPer());
        String timingSecondStr = timingSecondTextField_2.getText2();
        if (!ValueWidget.isNullOrEmpty(timingSecondStr) && ValueWidget.isInteger(timingSecondStr)) {
            requestInfoBean.setTimingSecond(Integer.parseInt(timingSecondStr));
        }
//        System.out.println("每次发送前置请求:" + requestInfoBean.isSendPreRequestPer());
        if (null != singleRequestNoteTA) {
            requestInfoBean.setSingleRequestNote(singleRequestNoteTA.getText2());
        }
        //单个请求的应答信息,包括应答体(response body)和response cookie
        if (resultTextPane.getText2().length() < maxTextLength) {
            requestInfoBean.setResponseOfSingleRequest(resultTextPane.getText2());
        }
        requestInfoBean.setEncodingCheckbox(encodingCheckbox_8.isSelected());
        if (null != chckbxSsl) {
            requestInfoBean.setSsl(chckbxSsl.isSelected());
        }
        if (null != contentTypeTextField_2) {
            requestInfoBean.setCustomRequestContentType(contentTypeTextField_2.getText2());
        }
        String fullUrl = fullUrlTextField.getText2();
        if (!ValueWidget.isNullOrEmpty(fullUrl)) {
            requestInfoBean.setUrl(fullUrl);
        }
        //别名
        String alias = aliasTextField.getText2();
        if (!ValueWidget.isNullOrEmpty(alias)) {
            requestInfoBean.setAlias(alias);
        }
        //请求头
        if (null != httpHeadPanel) {
            requestInfoBean.setHeaderMap(httpHeadPanel.getHeaderMap());
        }
        requestInfoBean.setRequestParameterTemplates(getRequestParameterTemplates());

        List<ParameterIncludeBean> parameters = getTableParameters();
        requestInfoBean.setParameters(parameters);
        //HTTP默认请求参数
        if (requestInfoBean.isApplyDefaultHTTP()) {
            addHttpDefaultParameter(requestInfoBean/*,requestInfoBean.isUrlEncoding(),urlEncodeParameterCharset*/);//important
        }

        requestInfoBean.setHttpRequestParameter(isHttpRequestParameter());
        requestInfoBean.setResponseResultTabIndex(this.tabbedPane_22.getSelectedIndex());
        requestInfoBean.setCopyFieldInResponse(copyFieldCheckbox.isSelected());
        if (!ValueWidget.isNullOrEmpty(copyField.getText2())) {
            requestInfoBean.setCopyFieldName(copyField.getText2());
        }

        return requestInfoBean;
    }

    private boolean isSendPreRequestPer() {
        return everytimeCheckBox.isSelected();
    }

    public void resume(RequestInfoBean requestInfoBean) {
        resume(requestInfoBean, false);
    }

    public void resume(RequestInfoBean requestInfoBean, boolean isCopy) {
        if (null == requestInfoBean) {
            requestInfoBean = getOriginalRequestInfoBean();
        } else if (ValueWidget.isNullOrEmpty(getOriginalRequestInfoBean())) {
            setOriginalRequestInfoBean(requestInfoBean);//解决复制请求panel,新复制的panel中"前置请求id"下拉框为空的问题
        }
        if (null == requestInfoBean) {
            return;
        }
        if (null == requestBodyDataTA) {
            return;
        }
        requestBodyDataTA.setText(requestInfoBean.getRequestBodyData());
        if (!ValueWidget.isNullOrEmpty(requestBodyDataTA.getText2())) {//只有有内容时,才设置只读
            requestBodyDataTA.setEditable(false); //added at 2018-02-04
        }

        actionPathTextField_7.setText(requestInfoBean.getActionPath());
        txtAuthcode.setText(requestInfoBean.getPreRequestParameterName());
        selfParameterTextField_3.setText(requestInfoBean.getCurrRequestParameterName());
        serverIpTextField_3.setText(requestInfoBean.getServerIp());
        portTextField_6.setText(requestInfoBean.getPort());
        int requestMethod = requestInfoBean.getRequestMethod();
        String requestMethodStr;
        /*if (requestMethod == Constant2.REQUEST_METHOD_GET) {
            requestMethodStr = "GET";
        } else {
            requestMethodStr = "POST";
        }*/
        requestMethodStr = (String) SystemHWUtil.reverseMap(Constant2.REQUEST_METHOD_MAP).get(requestMethod);
        requestMethodComboBox_2.setSelectedItem(requestMethodStr);
        Map<String, Object> requestParameters = requestInfoBean.getRequestParameters();
        if (requestInfoBean.isRequestBodyIsJson()) {//"请求参数"表格不可用
            requestBodyDataTA.setText(requestInfoBean.getRequestBodyData());
        } else {
            /*ConfigParam configParam=new ConfigParam();
            configParam.setHasTextField(false);
            configParam.setTF_table_cell(true);
            configParam.setColumnNames(columnNames);*/
            TableUtil3.setTableData3(parameterTable_1, requestParameters, getConfigParam(), RequestPanel.this.actionCallbackMap);
            rendTable();
        }
        //如果请求参数为空,且请求头不为空,则自动选择"请求头"标签页,避免输错参数,
        //比如本来要输入请求头的,结果误输入到请求参数中
        if (ValueWidget.isNullOrEmpty(requestParameters) && ValueWidget.isNullOrEmpty(requestInfoBean.getRequestBodyData())
                && (!ValueWidget.isNullOrEmpty(requestInfoBean.getHeaderMap()))) {
            tabbedPane_parameterHeader.setSelectedIndex(1);
        }

        requestIdTextField_5.setText(requestInfoBean.getRequestID());
        String currentRequestName = requestInfoBean.getCurrentRequestName();
        if (isCopy) {
            currentRequestName = currentRequestName + "(2)";
        }
        requestNameTextField_4.setText(currentRequestName);
        String requestCookie = requestInfoBean.getRequestCookie();
        if (!ValueWidget.isNullOrEmpty(requestCookie)) {
//            cookieTextArea.setText(requestCookie);
            httpHeadPanel.setRequestCookies(requestCookie);
        }
        countTextField_2.setText(requestInfoBean.getSendCount());
        if (requestInfoBean.isUrlEncoding() && !ValueWidget.isNullOrEmpty(isEncodeParameterCheckbox)) {
            isEncodeParameterCheckbox.setSelected(requestInfoBean.isUrlEncoding());
        }
        /*if (requestInfoBean.isApplyDefaultHTTP()) {
            isApplyDefaultHTTPCheckBox.setSelected(requestInfoBean.isApplyDefaultHTTP());
        }*/
        if (requestInfoBean.isRequestBodyIsJson()) {
            chckbxjson.setSelected(requestInfoBean.isRequestBodyIsJson());
            tabbedPane_1.setEnabledAt(0, false);//remove(0);
            tabbedPane_1.setSelectedIndex(1);
        }
        if (requestInfoBean.isHasPreRequest()) {
            preRequestCheckBox.setSelected(requestInfoBean.isHasPreRequest());
        }
        setEnable4PreRequest(preRequestCheckBox, requestInfoBean.isHasPreRequest());
        //设置编码
        String charset = requestInfoBean.getCharset();
        if (!ValueWidget.isNullOrEmpty(charset)) {
            encodingComboBox.setSelectedItem(charset);
        }
        if (!ValueWidget.isNullOrEmpty(requestInfoBean.getJsonResult()) &&
                requestInfoBean.getJsonResult().length() < maxTextLength) {
            jsonFormattedTextArea_9.setText(requestInfoBean.getJsonResult());
        }
        if (0 != requestInfoBean.getTimingSecond()) {
            timingSecondTextField_2.setText(String.valueOf(requestInfoBean.getTimingSecond()));
        }
        if (!ValueWidget.isNullOrEmpty(requestInfoBean.getResponseOfSingleRequest()) &&
                requestInfoBean.getResponseOfSingleRequest().length() < maxTextLength) {
            resultTextPane.setText(requestInfoBean.getResponseOfSingleRequest());
        }
        everytimeCheckBox.setSelected(requestInfoBean.isSendPreRequestPer());

        //如果备忘过长,则进行截断
        String singleRequestNote = requestInfoBean.getSingleRequestNote();
        if (!ValueWidget.isNullOrEmpty(singleRequestNote)
                && singleRequestNote.length() > maxTextLength) {
            requestInfoBean.setSingleRequestNote(singleRequestNote.substring(0, maxTextLength - 1));
        }
        singleRequestNoteTA.setText(requestInfoBean.getSingleRequestNote());
        encodingCheckbox_8.setSelected(requestInfoBean.isEncodingCheckbox());
        chckbxSsl.setSelected(requestInfoBean.isSsl());

        aliasTextField.setText(requestInfoBean.getAlias());
        if (isCopy) {
            updateTabTitle();//更新Tab的标题
            httpHeadPanel.initHeader(requestInfoBean.getHeaderMap());
        }
        if (requestInfoBean.isHttpRequestParameter()) {
            this.thisParameterCombobox.setSelectedIndex(0);
        } else {
            this.thisParameterCombobox.setSelectedIndex(1);
        }
        //设置参数模板
        setRequestParameterTemplates(requestInfoBean.getRequestParameterTemplates());

        //增加错误提示
        if (ValueWidget.isNullOrEmpty(actionPathTextField_7.getText2())) {
            actionPathTextField_7.setBackground(Color.red);
        } else {
            fullUrlTextField.setText(requestInfoBean.getUrl());
        }
        //"http请求结果","格式化的json","备忘"标签页的selectedIndex
        int responseResultTabIndex = requestInfoBean.getResponseResultTabIndex();
        if (responseResultTabIndex >= this.tabbedPane_22.getTabCount()) {
            responseResultTabIndex--;
        }
        if (responseResultTabIndex >= this.tabbedPane_22.getTabCount()) {
            responseResultTabIndex = this.tabbedPane_22.getTabCount() - 1;
        }
        this.tabbedPane_22.setSelectedIndex(responseResultTabIndex);
        //要复制的response中的字段名称
        copyField.setText(requestInfoBean.getCopyFieldName());
        if (requestInfoBean.isCopyFieldInResponse()) {
            copyFieldCheckbox.setSelected(true);
        } else {
            copyField.setEnabled(false);
        }
    }

    public void resume(String requestInfoBeanStr) throws JsonParseException, JsonMappingException {
        RequestInfoBean requestInfoBean = (RequestInfoBean) HWJacksonUtils.deSerialize(requestInfoBeanStr, RequestInfoBean.class);
        resume(requestInfoBean);
    }

    public void addParameterClip() {
        String text = WindowUtil.getSysClipboardText();
        if (!ValueWidget.isNullOrEmpty(text)) {
            addParameter(text/*,getConfigParam()*/);
        }

    }

    public void adaptTableSize2() {
        adaptTableSize2(true, true/*isAdd*/, false/*isDel*/);
    }

    /***
     * 获取"请求参数"表格的高度<br />
     * parameterTable_1 <tableScroll <panel_7JS2
     * @return
     */
    public int getTableHeight() {
        int tableHeight = parameterTable_1.getHeight();//0个行:0;<br>1个行:30;<br>2行:60;<br>3行:90
        return tableHeight;
    }

    /***
     * 获取表格外层JScrollPane的高度
     * @return
     */
    public int getTableScrollHeight() {
        Dimension di = tableScroll.getPreferredSize();
        return di.height;
    }

    /***
     * 使tableScroll 的高度自适应表格的高度
     */
    public void adaptTableSize2(boolean isFrameRepaint, boolean isAdd, boolean isDel) {
        if (null == parameterTable_1) {
            return;
        }
//        parameterTable_1.repaint();
//        parameterTable_1.mode validate();
        int tableHeight = parameterTable_1.getHeight();//0个行:0;<br>1个行:30;<br>2行:60;<br>3行:90
        int tableHeight2 = parameterTable_1.getPreferredSize().height;
        if (tableHeight2 > tableHeight) {
            tableHeight = tableHeight2;
        }
        int rowCount = parameterTable_1.getModel().getRowCount();
        if (0 == tableHeight && rowCount > 0) {//初始启动时tableHeight确实为0
            tableHeight = rowCount * SINGLE_ROW_HEIGHT;
        }
        Dimension di = tableScroll.getPreferredSize();
        int newScrollHeight;
        /*if (isAdd) {//增加参数
            newScrollHeight = tableHeight + 70;
            int tableScrollHeightPlus30 = di.height + 30;
            if (newScrollHeight < tableScrollHeightPlus30) {
                newScrollHeight = tableScrollHeightPlus30;
            }
        } else if (!isDel) {//不是删除参数,用在什么地方呢?打开程序时
            newScrollHeight = tableHeight + 40;
        } else {//删除参数时
            if (tableHeight < 120) {//解决删除"请求参数"表格的行时,高度不够的问题
                tableHeight = 120;
            }


        }*/
        newScrollHeight = tableHeight + 20;//20不能缺少,20是table header的高度
        //如果是删除参数,就没有400px的限制
        if ((!isDel) && (newScrollHeight <= di.height || di.height >= 400)) {
            return;
        }
        di.height = newScrollHeight;
        tableScroll.setPreferredSize(di);
        tableScroll.repaint();
        tableScroll.updateUI();
        if (isFrameRepaint) {
            autoTestPanel.updateUI2();
        }
    }

    /***
     * 跳转表格外面tableScroll 的高度
     * @param delta
     */
    public void addTableHeight(int delta) {
        Dimension di = tableScroll.getPreferredSize();
        di.height = di.height + delta;
        tableScroll.setPreferredSize(di);
        tableScroll.repaint();
        tableScroll.updateUI();
    }

    /***
     * 表格增加一行
     */
    public void addParameter(String parameterText/*,ConfigParam configParam*/) {
        boolean hasEmptyRow = false;
        int index = SystemHWUtil.NEGATIVE_ONE;
        Object[][] onlyData = null;
        if (!ValueWidget.isNullOrEmpty(parameterText)) {
            TableDataBean tableDataBean = TableUtil3.getParameter4Table(this.parameterTable_1);
            onlyData = tableDataBean.getOnlyStringValue();
            for (int i = 0; i < onlyData.length; i++) {
                Object[] rowData = onlyData[i];
                if (ValueWidget.isNullOrEmpty(rowData[0]) && ValueWidget.isNullOrEmpty(rowData[1])) {
                    //key 和val都为空
                    hasEmptyRow = true;
                    index = i;
                    break;
                }
            }
        }
        List<ParameterIncludeBean> parameterIncludeBeans = TableUtil3.getParameterIncludeBeans(parameterText);
        if (hasEmptyRow) {
            hasEmptyRow = false;

            int size = parameterIncludeBeans.size();
            String valTmp = parameterIncludeBeans.get(0).getValue();
            if (null == valTmp) {
                valTmp = SystemHWUtil.EMPTY;
            }
            TableUtil3.setTableValueAt(parameterTable_1, index, 0, parameterIncludeBeans.get(0).getKey(), getConfigParam());
            TableUtil3.setTableValueAt(parameterTable_1, index, 1, valTmp, getConfigParam());
            if (size > 1) {
                parameterIncludeBeans.remove(0);
                TableUtil3.addParameter(parameterTable_1, false, AutoTestPanel.isTF_table_cell, parameterIncludeBeans, actionCallbackMap);
            }

        } else {
            TableUtil.addParameter(this.parameterTable_1, false/*hasTextField*/, AutoTestPanel.isTF_table_cell, parameterIncludeBeans, actionCallbackMap);
        }
        /*parameterTable_1.repaint();
        parameterTable_1.updateUI();
        parameterTable_1.validate();
        panel_7JS2.updateUI();*/
//        adaptTableSize2();
        autoTestPanel.optimizParameterScroll(this, false);
    }

    public String generateRequestBody(boolean isUrlEncoding/*,String requestCharset*/) {
        return generateRequestBody(isUrlEncoding, true/*isSendHttpRequest*/);
    }

    public String generateRequestBody(boolean isUrlEncoding/*,String requestCharset*/, boolean toastWhenParametersNull) {
        if (chckbxjson.isSelected()) {
            return requestBodyDataTA.getText2().replaceAll("^\"?([^\"]*)\"?$", "$1");
//            return SystemHWUtil.delDoubleQuotation(requestBodyDataTA.getText2());//8月24日下午11点 ,5935
            //removed at 2018年2月4日 ,因为 会把{"ak":"F4AA34B89513F0D087CA0EF11A3277469DC74905","cid":" 中的双引号全部去掉
        }
        List<ParameterIncludeBean> parameters = getTableParameters();
        if (ValueWidget.isNullOrEmpty(parameters)) {
            if (toastWhenParametersNull) {
                ToastMessage.toast("请求要素为空", 2000, new Color(108, 121, 0));
            }
            return SystemHWUtil.EMPTY;
        }
        StringBuffer sbuffer = TableUtil3.getRequestBodyFromList(parameters, isUrlEncoding, urlEncodeParameterCharset, false/*isAutoUrlEncoding*/);
        if (sbuffer != null && sbuffer.length() != 0) {
            return sbuffer.toString().replaceAll("&$", SystemHWUtil.EMPTY);
        }

        return SystemHWUtil.EMPTY;
    }

    protected RequestPanel getBeforeRequestPanel(String id) {
        return this.autoTestPanel.getRequestPanel4Map(id);
    }

    public void setRequestCallBack(RequestCallBack requestCallBack) {
        if (ValueWidget.isNullOrEmpty(preRequestCallbackInfo)) {
            preRequestCallbackInfo = new PreRequestCallbackInfo();
        }
        preRequestCallbackInfo.setRequestCallBack(requestCallBack);
    }

    @Override
    public String toString() {
        String requestName = requestNameTextField_4.getText2();
        if (ValueWidget.isNullOrEmpty(requestName)) {
            requestName = this.actionPathTextField_7.getText2();
        }
        return this.selfRequestId + ":" + requestName;
    }

    public String getRequestId() {
        if (null == requestIdTextField_5) {
            return getOriginalRequestInfoBean().getRequestID();
        }
        return requestIdTextField_5.getText2();
    }

    public void setRequestId(String requestId) {
        requestIdTextField_5.setText(requestId);
    }

    //    @Override
    public String getRequestName() {
        if (null != requestNameTextField_4) {
            return requestNameTextField_4.getText2();
        }
        if (null == getOriginalRequestInfoBean()) {
            return null;
        } else {
            return getOriginalRequestInfoBean().getCurrentRequestName();
        }
    }

    private void refreshAllPreId() {
        java.util.List<RequestPanel> requestPanelList = this.autoTestPanel.getAllRequestPanelList();
        for (RequestPanel pan : requestPanelList) {
            pan.refreshPreId();
        }

    }

    /***
     * 获取接口名称
     *
     * @return
     */
    public String getActionName() {
        if (null == this.actionPathTextField_7) {
            return this.originalRequestInfoBean.getActionPath();
        }
        return this.actionPathTextField_7.getText2();
    }

    //    @Override
    public String getServerIp() {
        if (null == serverIpTextField_3) {
            return this.originalRequestInfoBean.getServerIp();
        }
        return serverIpTextField_3.getText2();
    }

    public void setServerIp(String serverIp) {
        if (this.serverIpTextField_3 == null) {
            return;
        }
        this.serverIpTextField_3.setText(serverIp);
    }

    public String getAlias() {
        if (null == this.aliasTextField) {
            return this.originalRequestInfoBean.getAlias();
        }
        return this.aliasTextField.getText2();
    }

    public void refreshPreId() {
        if (null == preRequestIdComboBox_2) {
            return;
        }
        String currentPreRequestId = (String) preRequestIdComboBox_2.getSelectedItem();
        refreshPreId(currentPreRequestId);
    }

    public String getPreRequestId() {
        if (null == preRequestIdComboBox_2) {
            return getOriginalRequestInfoBean().getRequestID();
        } else {
            return (String) preRequestIdComboBox_2.getSelectedItem();
        }
    }

    public void setPreRequestId(String preRquestId) {
        if (!SystemHWUtil.MIDDLE_LINE.equals(preRquestId)) {
            preRequestIdComboBox_2.setSelectedItem(preRquestId);
        }
    }

    /***
     * 初始化"前置请求id"下拉框
     */
    public void initializePreRequestIdListbox() {
        RequestInfoBean requestInfoBean = getOriginalRequestInfoBean();
        if (null == requestInfoBean) {
            return;
        }
        Set<String> preRequestIds = requestInfoBean.getPreRequestIds();
        if (ValueWidget.isNullOrEmpty(preRequestIds) && !ValueWidget.isNullOrEmpty(requestInfoBean.getPreRequestId())) {
            //兼容复制请求信息为panel时,没有设置依赖的前置请求id的情况
            if (null == preRequestIds) {
                preRequestIds = new HashSet<String>();
            }
            preRequestIds.add(requestInfoBean.getPreRequestId());
        }
        if (ValueWidget.isNullOrEmpty(preRequestIds)) {
            return;
        }
        for (String item : preRequestIds) {
            if (!ValueWidget.isNullOrEmpty(item)) {//防止下拉框中有空字符串的项
                this.preRequestIdComboBox_2.addItem(item);
            }
        }
        String currentPreRequestId = this.getOriginalRequestInfoBean().getPreRequestId();
        if (!ValueWidget.isNullOrEmpty(currentPreRequestId) && !currentPreRequestId.equals(SystemHWUtil.MIDDLE_LINE)) {
            this.preRequestIdComboBox_2.setSelectedItem(currentPreRequestId);
        }
    }

    /***
     * 仅仅更新"前置请求id"下拉框
     */
    public void refreshPreIdCombobox() {
        java.util.List<String> requestIdList2 = this.autoTestPanel.getRequestIdList();
        int length = requestIdList2.size();
        preRequestIdComboBox_2.removeAllItems();//先删除原下拉框的数据
        preRequestIdComboBox_2.addItem(SystemHWUtil.MIDDLE_LINE);
        for (int i = 0; i < length; i++) {
            String requestId22 = requestIdList2.get(i);
            if (null != requestId22) {
                requestId22 = requestId22.trim();
            }
            if (ValueWidget.isNullOrEmpty(requestId22)) {
                continue;
            }
            if (!requestId22.equals(getRequestId())) {//排查自己的请求ID
                preRequestIdComboBox_2.addItem(requestId22);
            }
        }
    }

    public void updatePreId(String newRequestId, String oldRequestId) {
        String preRequestId = getPreRequestId();
        refreshPreIdCombobox();
        if (preRequestId.equals(oldRequestId)) {
            preRequestIdComboBox_2.setSelectedItem(newRequestId);
        }

    }

    public void rendPreRequestId() {

    }

    /***
     * 更新本tab的"前置请求id"下拉框
     */
    public void refreshPreId(String newPreRequestId) {
        //获取所有的请求ID
        java.util.List<String> requestIdList2 = this.autoTestPanel.getRequestIdList();
        int length = requestIdList2.size();
        preRequestIdComboBox_2.removeAllItems();//先删除原下拉框的数据
        preRequestIdComboBox_2.addItem(SystemHWUtil.MIDDLE_LINE);
        int selectedIndex = SystemHWUtil.NEGATIVE_ONE;
        for (int i = 0; i < length; i++) {
            String requestId22 = requestIdList2.get(i);
            if (!requestId22.equals(getRequestId())) {//排查自己的请求ID
                preRequestIdComboBox_2.addItem(requestId22);
                selectedIndex++;
                /*if(!ValueWidget.isNullOrEmpty(newPreRequestId)&& requestId22.equals(newPreRequestId)){
                	selectedIndex=realSelectedIndex;
                }*/
            }
        }
        String currentPreRequestId = (String) preRequestIdComboBox_2.getSelectedItem();
        if (!ValueWidget.isNullOrEmpty(currentPreRequestId) && !newPreRequestId.equals(SystemHWUtil.MIDDLE_LINE)) {
            //若当前tab已经选择了"前置请求id"
            preRequestIdComboBox_2.setSelectedIndex(selectedIndex + 1);
        }
    }

    /*public String getSaveContent() {
        RequestInfoBean requestInfoBean = getRequestInfoBean(false);
        String content = HWJacksonUtils.getJsonP(requestInfoBean);
        return content;
    }*/

    /***
     * 不会智能地对请求参数进行url编码
     * @param parameters
     * @param isUrlEncoding
     * @param requestCharset
     */
    public void setTableData3(List<ParameterIncludeBean> parameters, boolean isUrlEncoding, String requestCharset) {
        int size = parameters.size();
        if (size <= 0) {
            return;
        }//if
        Object[][] datas = new Object[size][];
        for (int i = 0; i < size; i++) {
            ParameterIncludeBean parameterIncludeBean = parameters.get(i);
            String key = parameterIncludeBean.getKey();
            String val = parameterIncludeBean.getValue();

            Object[] objs = new Object[3];
            RadioButtonPanel panel = new RadioButtonPanel();
            panel.init();
            objs[0] = key;
            objs[2] = panel;

            if (ValueWidget.isNullOrEmpty(val)/*||val.equals("null")*/) {//配置文件中保存的是"null",而不是null
                val = SystemHWUtil.EMPTY;
            }
            objs[1] = val;
            datas[i] = objs;
        }//for
//            setTableData2(datas);
        TableUtil3.appendTableData(parameterTable_1, datas, getConfigParam());
        //因为是要填入请求面板"请求体"中的,所以没有url编码
        StringBuffer sbuffer = TableUtil3.getRequestBodyFromList(parameters, isUrlEncoding, requestCharset, false/*isAutoURLEncode*/);
        requestBodyDataTA.setText(sbuffer.toString());//触发request body的更新
    }

    public int getTabIndex2() {
        return tabIndex2;
    }

    public void setTabIndex2(int tabIndex) {
        this.tabIndex2 = tabIndex;
    }

    /***
     * 上面的文本框
     * @return
     */
    public String getRequestInfoAndResult22() {
        if (null == respTextArea_9) {
            if (null == originalRequestInfoBean) {
                return null;
            }
            return originalRequestInfoBean.getJsonResult();
        }
        return respTextArea_9.getText2();
    }

	/*public void toExecutePreQequest(){
    	preRequestCheckBox.setSelected(true);
    }*/

    public void setRequestInfoAndResult22(String input) {
        if (null == respTextArea_9) {
            return;
        }
        respTextArea_9.setText(input);
    }

    /***
     * 下面的文本框
     * @return
     */
    public String getResponseTextPaneText22() {
        if (null == resultTextPane) {
            return null;
        }
        return resultTextPane.getText2();
    }

    public void setResponseTextPaneText22(String input) {
        if (null == resultTextPane) {
            return;
        }
        resultTextPane.setText(input);
    }

    public AssistPopupTextArea getRespTextArea_9() {
        return respTextArea_9;
    }

    public void setRespTextArea_9(MemoAssistPopupTextArea respTextArea_9) {
        this.respTextArea_9 = respTextArea_9;
    }

    public AssistPopupTextArea getResultTextPane() {
        return resultTextPane;
    }

    public void setResultTextPane(MemoAssistPopupTextArea resultTextPane) {
        this.resultTextPane = resultTextPane;
    }

    public String getResponseJsonResult() {
        return responseJsonResult;
    }

    public void setResponseJsonResult(String responseJsonResult) {
        this.responseJsonResult = responseJsonResult;
    }

    public AutoTestPanel getAutoTestPanel() {
        return autoTestPanel;
    }

    public void setAutoTestPanel(AutoTestPanel autoTestPanel) {
        this.autoTestPanel = autoTestPanel;
    }

    public RequestPanel getAfterRequestPanel() {
        return afterRequestPanel;
    }

    public void setAfterRequestPanel(RequestPanel afterRequestPanel) {
        if (ValueWidget.isNullOrEmpty(preRequestCallbackInfo)) {
            preRequestCallbackInfo = new PreRequestCallbackInfo();
        }
        this.afterRequestPanel = afterRequestPanel;
        preRequestCallbackInfo.setCallback(true);
    }

    public AssistPopupTextArea getRequestBodyDataTA() {
        return requestBodyDataTA;
    }

    public void setRequestBodyDataTA(HttpRequestBodyTextArea requestBodyDataTA) {
        this.requestBodyDataTA = requestBodyDataTA;
    }

    public JTable getParameterTable_1() {
        return parameterTable_1;
    }

    public void setParameterTable_1(JTable parameterTable_1) {
        this.parameterTable_1 = parameterTable_1;
    }

    public JTextArea getJsonFormattedTextArea_9() {
        return jsonFormattedTextArea_9;
    }

    public void setJsonFormattedTextArea_9(
            RTextArea jsonFormattedTextArea_9) {
        this.jsonFormattedTextArea_9 = jsonFormattedTextArea_9;
    }

    public void setEnable4PreRequest(JCheckBox preRequestCheckBox2, boolean isEditable) {
//    	preRequestIdComboBox_2.setEditable(isEditable);
        preRequestIdComboBox_2.setEnabled(isEditable);
        thisParameterCombobox.setEnabled(isEditable);
        everytimeCheckBox.setEnabled(isEditable);
        txtAuthcode.setEditable(isEditable);
        selfParameterTextField_3.setEditable(isEditable);
        openPreRequestButton_3.setEnabled(isEditable);
        if (isEditable) {
            preRequestCheckBox2.setForeground(defaultForegroundColor);//复选框恢复黑色
        } else {
            preRequestCheckBox2.setForeground(Color.gray);//复选框置灰
        }

    }

    private void updateTabTitle() {
        updateTabTitle(null, -1);
    }

    /***
     * 更新Tab的标题
     * <br>不能在文本失去焦点时触发,因为此时getCurrentSelectedIndex 已经不是title对应的tab了
     */
    private void updateTabTitle(AssistPopupTextField textField, int index) {//TODO 删除tab之后tabIndex2没有更新
//        System.out.println(autoTestPanel.getCurrentSelectedIndex());

        RequestPanel requestPanel = null;
        if (null == textField) {
            textField = requestNameTextField_4;
        } else {
            requestPanel = (RequestPanel) textField.getParentPanelOrFrame();
            //注意:切换tab的时候current panel并不是文本框失去焦点的那个panel
            if (requestPanel != autoTestPanel.getCurrentRequestPanel()) {
                index = autoTestPanel.getLastIndex();
            }
        }
        if (index == -1) {
            index = autoTestPanel.getCurrentSelectedIndex();
        }
        String title = textField.getText2();

        autoTestPanel.setTabTitle2(index/*tabIndex2*/, title);

        autoTestPanel.setTitle(title);
    }

    public AssistPopupTextField getActionPathTextField_7() {
        return actionPathTextField_7;
    }

    /***
     * "http请求结果","格式化的json","备忘" tab标签页
     * @return
     */
    public JTabbedPane getTabbedPane_22() {
        return tabbedPane_22;
    }

    public RequestInfoBean getOriginalRequestInfoBean() {
        if (null == originalRequestInfoBean) {
            originalRequestInfoBean = new RequestInfoBean();
        }
        return originalRequestInfoBean;
    }

    public void setOriginalRequestInfoBean(RequestInfoBean originalRequestInfoBean) {
        this.originalRequestInfoBean = originalRequestInfoBean;
    }

    public boolean isAlreadyRended2() {
        return (serverIpTextField_3 != null);
    }

    public List<RequestParameterTemplate> getRequestParameterTemplates() {
        return requestParameterTemplates;
    }

    public void setRequestParameterTemplates(List<RequestParameterTemplate> requestParameterTemplates) {
        this.requestParameterTemplates = requestParameterTemplates;
    }

    public RequestParameterTemplate getRequestParameterTemplate(String displayName) {
        if (ValueWidget.isNullOrEmpty(this.requestParameterTemplates) || ValueWidget.isNullOrEmpty(displayName)) {
            return null;
        }
        int size = this.requestParameterTemplates.size();
        for (int i = 0; i < size; i++) {
            RequestParameterTemplate requestParameterTemplate = this.requestParameterTemplates.get(i);
            if (requestParameterTemplate.getDisplayName().equals(displayName)) {
                return requestParameterTemplate;
            }
        }
        return null;
    }

    public String getRequetId() {
        if (null != requestIdTextField_5) {
            return requestIdTextField_5.getText2();
        }
        RequestInfoBean originalRequestInfoBean = getOriginalRequestInfoBean();
        if (null == originalRequestInfoBean) {
            return null;
        } else {
            return originalRequestInfoBean.getRequestID();
        }
    }

    /***
     * 是否立即发送请求<br />
     * 存储到什么地方呢?<br />
     * RequestPanel的成员变量OriginalRequestInfoBean 中
     */
    public boolean isSendRightNow() {
        RequestInfoBean requestInfoBean = getOriginalRequestInfoBean();
        if (null != requestInfoBean) {
            return requestInfoBean.isSendRightNow();
        } else {
            return false;
        }
    }

    public void setSendRightNow(boolean sendRightNow) {
        getOriginalRequestInfoBean().setSendRightNow(sendRightNow);
    }

    public int getsplitPaneInnerLocation() {
        if (null == splitPane_inner) {
            return 0;
        }
        return splitPane_inner.getDividerLocation();
    }

    public void minussplitPaneInnerLocation() {
        int dividerLocation = splitPane_inner.getDividerLocation();
        JScrollBar verticalScrollBar = panel_7JS2.getVerticalScrollBar();
        splitPane_inner.setDividerLocation(dividerLocation - (verticalScrollBar.getMaximum()/**进度条的总高度*/ - verticalScrollBar.getHeight()));
        updateUI();
        autoTestPanel.repaint();
    }

    /***
     * 参数表格所在的scroll
     * @return
     */
    public JScrollPane getTableScroll() {
        return tableScroll;
    }

    /***
     * 获取插件类
     * @return
     */
    public PluginCallback getPluginCallback() {
        String pluginClassSimpleName = getOriginalRequestInfoBean().getPluginClassSimpleName();
        if (ValueWidget.isNullOrEmpty(pluginClassSimpleName)) {
            return null;
        }
        try {
            return (PluginCallback) Class.forName("com.yunma.dialog.plugin.impl." + pluginClassSimpleName).newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
//        return new UpdateLicensePluginCallback();
    }

    /***
     * 启动插件
     */
    public void startPlugin() {
//        System.out.println(" 启动插件:" );
        String pluginClassSimpleName = getOriginalRequestInfoBean().getPluginClassSimpleName();
        if (ValueWidget.isNullOrEmpty(pluginClassSimpleName)) {
            ToastMessage.toast("暂未安装插件", 2000, Color.red);
            return;
        }
        String selectText = jsonFormattedTextArea_9.getSelectedText();
        PluginInputDialog pluginInputDialog = new PluginInputDialog(selectText, this);
        pluginInputDialog.setVisible(true);
    }

    public ConfigParam getConfigParam() {
        if (debug) {
            return new ConfigParam();
        }
        ConfigParam configParam = autoTestPanel.getConfigParam();
        if (null == configParam) {
            autoTestPanel.setConfigParam(new ConfigParam());
            configParam = autoTestPanel.getConfigParam();
        }
        configParam.setHasTextField(false);
        configParam.setTF_table_cell(true);
        configParam.setColumnNames(columnNames);
//只能模式时:表格单元格文本域只读
        configParam.setTableCellReadonly(autoTestPanel.getGlobalConfig().isIntelligenceMode());
        return configParam;
    }

    public JButton getRunButton_55() {
        return runButton_55;
    }
}

package com.yunma.autotest;

import com.cmd.dos.hw.util.CMDUtil;
import com.common.bean.*;
import com.common.bean.ModifyResponseBodyBean;
import com.common.dict.Constant2;
import com.common.util.*;
import com.http.util.HttpSocketUtil;
import com.io.hw.file.util.FileUtils;
import com.io.hw.json.HWJacksonUtils;
import com.string.widget.util.ValueWidget;
import com.swing.callback.ActionCallback;
import com.swing.component.*;
import com.swing.config.ConfigParam;
import com.swing.config.TextFieldConfigInfo;
import com.swing.dialog.DialogUtil;
import com.swing.dialog.GenericDialog;
import com.swing.dialog.GenericFrame;
import com.swing.dialog.toast.ToastMessage;
import com.swing.event.EventHWUtil;
import com.swing.menu.MenuUtil2;
import com.swing.messagebox.GUIUtil23;
import com.time.util.TimeHWUtil;
import com.yunma.bean.*;
import com.yunma.bean.diary.LoginParamInfo;
import com.yunma.dialog.*;
import com.yunma.listen.MyMenuBarActionListener;
import com.yunma.listen.MyTableMenuListener;
import com.yunma.util.Debug;
import com.yunma.util.TableUtil;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.MouseInputListener;
import javax.swing.plaf.ProgressBarUI;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetAdapter;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import java.util.List;
import java.util.Timer;


public class AutoTestPanel extends GenericFrame {
    public static final String configFileName = ".share_http.properties";
    /***
     * 配置文件路径
     */
    public static final String configFilePath = getHomeDir() + configFileName;
    /***
     * 日志文件路径(不是log4j)
     */
    public static final String logFilePath = getHomeDir() + ".log_http.log";
    public static final String encodingLabelValue = "当前的编码是:%s,\t当前的操作系统类型是%s";
    public static final String title = "restful 接口调试工具";
    public static final String HTTP_request = "HTTP请求";
    protected static final Logger logger = Logger.getLogger(AutoTestPanel.class);
    private static final long serialVersionUID = 8557174256037018817L;
    /***
     * 表格的cell(单元格)是否是AssistPopupTextArea文本域
     */
    static boolean isTF_table_cell = true;
    /**
     * 参考:https://www.charlesproxy.com/documentation/configuration/browser-and-system-configuration/
     */
    static boolean isproxy = false;
    static String proxyPort;
    private static CheckDnsDialog dialog;
    private static int count = 0;
    /***
     * 这是我自定义的
     */
    private static DataFlavor dataFlavor = new DataFlavor("java/swing", "swing组件");
    /***
     * 接口对应的服务器ip,可以是域名
     */
    private final AssistPopupTextField serverIpTextField;
    /***
     * 接口对应的服务器端口
     */
    private final AssistPopupTextField portTextField_1;
    private final AssistPopupTextField contextPathTF;
    private final JTable deFaultParameterTable;
    /***
     * <请求ID,请求Panel>
     */
    private final Map<String, RequestPanel> requestPanelMap = new HashMap<String, RequestPanel>();
    private final Map<RequestPanel, Integer> requestPanelIndexMap = new HashMap<RequestPanel, Integer>();
    private final List<String> requestIdList = new ArrayList<String>();
    private final List<RequestPanel> requestPanelList = new ArrayList<RequestPanel>();
    /***
     * 所有的RequestPanel
     */
    private final List<RequestPanel> allRequestPanelList = new ArrayList<RequestPanel>();
    private final JTabbedPane tabbedPane_2;//package 访问权限
    /***
     * 应答的Set-Cookie
     */
    private final AssistPopupTextArea responseSetCookieTA;
    private final AssistPopupTextArea defaultTextArea;
    /***
     * tab 的序号,用于使用快捷键Shift+Tab 切换Tab
     */
    private final List<Integer> indexList = new ArrayList<Integer>();
    /***
     * 备忘/笔记
     */
    private final AssistPopupTextArea memoTextArea_1;
    /***
     * 备忘/笔记
     */
    private final AssistPopupTextArea memoTextArea2;
    /***
     * 搜索输入框
     */
    private final AssistPopupTextField searchTextField;
    /***
     * 请求的编码
     */
    private JComboBox<String> requestCharsetComboBox;
    private JTable table_1;
    private AssistPopupTextField textField_8;
    /***
     * 配置文件
     */
    private File configFile;
    /***
     * 当前的RequestPanel,通过点击标签页切换
     */
    private RequestPanel currentRequestPanel;
    private int maxTabIndex2 = 0;
    /***
     * 刚启动时获取的WholeRequestBean
     */
    private WholeRequestBean oldWholeRequestBean;
    private GlobalInfo globalInfo;
    private boolean isDebug = false;
    /***
     * "http 默认值"和请求面板之间
     */
    private JSplitPane splitPane;
    /***
     * 程序启动时读取的配置文件内容
     */
    private String resumeInput;
    /***
     * 二维码对话框
     */
    private QRCodeDialog qrCodeDialog;
    /***
     * 进度条的默认颜色
     */
    private Color progressDefaultColor;
    /***
     * 文本框的默认颜色
     */
    private Color tfDefaultColor;
    private ProgressBarUI progressBarUI;
    private JProgressBar progressBar;
    /***
     * key:"Command_enter","Ctrl_enter","alt_enter"<br />
     * 快捷键映射事件响应程序
     */
    private Map<String, ActionCallback> actionCallbackMap;
    /***
     * 默认cookie,<br >优先级比单个请求的cookie高,所以会覆盖掉单个请求中设置的cookie
     */
    private JTextArea commonCookie;
    /***
     * 是否已经初始化,什么叫初始化呢?<br>
     *     所有的面板已经加载完成.
     */
    private boolean isInitialized = false;
    /***
     * 所有请求的id
     */
    private List<String> requestIdList2;
    /***
     * 解绑对话框的配置
     */
    private UnbindMobileBean unbindMobileBean = new UnbindMobileBean();
    /***
     * 修改应答题(response)
     */
    private ModifyResponseBodyBean modifyResponseBodyBean = new ModifyResponseBodyBean();
    /**
     * "maven安装本地jar"的配置
     */
    private MvnInstallConfig mvnInstallConfig = new MvnInstallConfig();
    /***
     * 配置:生成二维码
     */
    private QRCodeInfoBean qrCodeInfoBean = new QRCodeInfoBean();
    /***
     * 配置:通过json字符串生成java bean
     */
    private Json2JavaBean json2JavaBean = new Json2JavaBean();
    /***
     * 配置:登录的用户名和密码
     */
    private LoginParamInfo loginParamInfo = new LoginParamInfo();
    private AssembleDynamicUrlInfo assembleDynamicUrlInfo = new AssembleDynamicUrlInfo();
    /***
     * 参数表格(Jtable)渲染的配置参数
     */
    private ConfigParam configParam;

    /**
     * Create the frame.
     */
    public AutoTestPanel(boolean isproxy2, String proxyPort2) {
        if (isproxy2) {
            if (ValueWidget.isNullOrEmpty(proxyPort2)) {
                proxyPort2 = "8888";
            }
            System.setProperty("http.proxyHost", "127.0.0.1");
            System.setProperty("http.proxyPort", proxyPort2);
            System.out.println("设置了代理");
            ToastMessage.toast("设置了代理,port:" + proxyPort2, 2000);
        }

        //给请求参数文本框增加绑定事件响应程序
        addListenAction();
//        DialogUtil.lookAndFeel2();
//		setBounds(100, 100, 1262, 770);
//        setSize(700,500);
        setTitle(title);
        try {
            setIcon("com/yunma/img/logo.jpg", AutoTestPanel.class);
        } catch (IOException e2) {
            e2.printStackTrace();
            GUIUtil23.errorDialog(e2);
        }
        fullScreen2(this);
        setExtendedState(this.getExtendedState() | JFrame.MAXIMIZED_BOTH);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        splitPane = new JSplitPane();
        splitPane.setOneTouchExpandable(true);
        splitPane.setDividerLocation(0);//最外层的split的左边(http默认值)的宽度
        splitPane.setContinuousLayout(true);
        getContentPane().add(splitPane, BorderLayout.CENTER);

        JPanel topPanel = new JPanel();
        topPanel.setBackground(Color.WHITE);

        final JTabbedPane tabbedPane_1 = new JTabbedPane(JTabbedPane.TOP);
        tabbedPane_1.addTab("http 默认值", null, topPanel, "http 默认值");
        splitPane.setLeftComponent(tabbedPane_1);
        setDropTarget2(tabbedPane_1, tabbedPane_1, this);

        GridBagLayout gbl_panel = new GridBagLayout();
        gbl_panel.columnWidths = new int[]{0, 0, 0};
        gbl_panel.rowHeights = new int[]{0, 0, 60/**cookie文本框的高度 */, 0, 0, 0};
        gbl_panel.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
        gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, 1.0, Double.MIN_VALUE};
        topPanel.setLayout(gbl_panel);

        JPanel panel = new JPanel();
        GridBagConstraints gbc_panel = new GridBagConstraints();
        gbc_panel.insets = new Insets(0, 0, 5, 0);
        gbc_panel.fill = GridBagConstraints.BOTH;
        gbc_panel.gridx = 0;
        gbc_panel.gridy = 0;
        topPanel.add(panel, gbc_panel);
        GridBagLayout gbl_panel22 = new GridBagLayout();
        gbl_panel22.columnWidths = new int[]{0, 110/*服务器ip文本框的宽度*/, 0, 100/*"端口号"文本框宽度*/, 0};
        gbl_panel22.rowHeights = new int[]{0, 0};
        gbl_panel22.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        gbl_panel22.rowWeights = new double[]{0.0, Double.MIN_VALUE};
        panel.setLayout(gbl_panel22);

        JLabel lblip = new JLabel("服务器ip");
        GridBagConstraints gbc_lblip = new GridBagConstraints();
        gbc_lblip.insets = new Insets(0, 0, 0, 5);
        gbc_lblip.gridx = 0;
        gbc_lblip.gridy = 0;
        panel.add(lblip, gbc_lblip);

        serverIpTextField = new AssistPopupTextField();
        GridBagConstraints gbc_textField = new GridBagConstraints();
        gbc_textField.fill = GridBagConstraints.HORIZONTAL;
        gbc_textField.insets = new Insets(0, 0, 0, 5);
        gbc_textField.gridx = 1;
        gbc_textField.gridy = 0;
        panel.add(serverIpTextField, gbc_textField);
        serverIpTextField.setText("127.0.0.1");
        serverIpTextField.setColumns(20);

        JLabel label = new JLabel("端口号");
        GridBagConstraints gbc_label = new GridBagConstraints();
        gbc_label.insets = new Insets(0, 0, 0, 5);
        gbc_label.gridx = 2;
        gbc_label.gridy = 0;
        panel.add(label, gbc_label);

        portTextField_1 = new AssistPopupTextField();
        GridBagConstraints gbc_portTextField_1 = new GridBagConstraints();
        gbc_portTextField_1.fill = GridBagConstraints.HORIZONTAL;
        gbc_portTextField_1.gridx = 3;
        gbc_portTextField_1.gridy = 0;
        panel.add(portTextField_1, gbc_portTextField_1);
        portTextField_1.setText("8080");
        portTextField_1.setColumns(10);

        JPanel panel_1 = new JPanel();
        GridBagConstraints gbc_panel_1 = new GridBagConstraints();
        gbc_panel_1.insets = new Insets(0, 0, 5, 0);
        gbc_panel_1.fill = GridBagConstraints.BOTH;
        gbc_panel_1.gridx = 0;
        gbc_panel_1.gridy = 1;
        topPanel.add(panel_1, gbc_panel_1);
        GridBagLayout gbl_panel_1 = new GridBagLayout();
        gbl_panel_1.columnWidths = new int[]{0, 0, 0, 0, 0};
        gbl_panel_1.rowHeights = new int[]{0, 0};
        gbl_panel_1.columnWeights = new double[]{0.0, 1.0, 0.0, 1.0, Double.MIN_VALUE};
        gbl_panel_1.rowWeights = new double[]{0.0, Double.MIN_VALUE};
        panel_1.setLayout(gbl_panel_1);

        JLabel lblNewLabel = new JLabel("路径");
        GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
        gbc_lblNewLabel.insets = new Insets(0, 0, 0, 5);
        gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
        gbc_lblNewLabel.gridx = 0;
        gbc_lblNewLabel.gridy = 0;
        panel_1.add(lblNewLabel, gbc_lblNewLabel);

        contextPathTF = new AssistPopupTextField();
        contextPathTF.setText("/tv_mobile");
        GridBagConstraints gbc_contextPathTF = new GridBagConstraints();
        gbc_contextPathTF.insets = new Insets(0, 0, 0, 5);
        gbc_contextPathTF.fill = GridBagConstraints.HORIZONTAL;
        gbc_contextPathTF.gridx = 1;
        gbc_contextPathTF.gridy = 0;
        panel_1.add(contextPathTF, gbc_contextPathTF);
        contextPathTF.setColumns(20);

		/*JLabel label_1 = new JLabel("编码");
        GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.anchor = GridBagConstraints.EAST;
		gbc_label_1.insets = new Insets(0, 0, 0, 5);
		gbc_label_1.gridx = 2;
		gbc_label_1.gridy = 0;
		panel_1.add(label_1, gbc_label_1);

		requestCharsetComboBox = new JComboBox<String>();
		requestCharsetComboBox.addItem(SystemHWUtil.CHARSET_GBK);
		requestCharsetComboBox.addItem(SystemHWUtil.CHARSET_UTF);
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 3;
		gbc_comboBox.gridy = 0;
		panel_1.add(requestCharsetComboBox, gbc_comboBox);*/

        JPanel panel_8 = new JPanel();
        GridBagConstraints gbc_panel_8 = new GridBagConstraints();
        gbc_panel_8.insets = new Insets(0, 0, 5, 0);
        gbc_panel_8.fill = GridBagConstraints.BOTH;
        gbc_panel_8.gridx = 0;
        gbc_panel_8.gridy = 2;
        topPanel.add(panel_8, gbc_panel_8);
        GridBagLayout gbl_panel_8 = new GridBagLayout();
        gbl_panel_8.columnWidths = new int[]{0, 0, 0};
        gbl_panel_8.rowHeights = new int[]{0, 0};
        gbl_panel_8.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
        gbl_panel_8.rowWeights = new double[]{1.0, Double.MIN_VALUE};
        panel_8.setLayout(gbl_panel_8);

        JScrollPane scrollPane_2 = new JScrollPane();
        GridBagConstraints gbc_scrollPane_2 = new GridBagConstraints();
        gbc_scrollPane_2.gridwidth = 2;
        gbc_scrollPane_2.fill = GridBagConstraints.BOTH;
        gbc_scrollPane_2.gridx = 0;
        gbc_scrollPane_2.gridy = 0;
        panel_8.add(scrollPane_2, gbc_scrollPane_2);

        //默认cookie,<br >优先级比单个请求的cookie高,所以会覆盖掉单个请求中设置的cookie
        commonCookie = new JTextArea();
        scrollPane_2.setViewportView(commonCookie);
        Border border13 = BorderFactory.createEtchedBorder(Color.white,
                new Color(148, 145, 140));
        TitledBorder openFileTitle = new TitledBorder(border13, "默认cookie(优先级最高)");
        scrollPane_2.setBorder(openFileTitle);

		/*JLabel label_5 = new JLabel("请求方式");
		GridBagConstraints gbc_label_5 = new GridBagConstraints();
		gbc_label_5.insets = new Insets(0, 0, 0, 5);
		gbc_label_5.anchor = GridBagConstraints.EAST;
		gbc_label_5.gridx = 0;
		gbc_label_5.gridy = 0;
		panel_8.add(label_5, gbc_label_5);*/

        /**
         请求的方式
         JComboBox<String> requestMethodComboBox = new JComboBox<String>();
         requestMethodComboBox.addItem("GET");
         requestMethodComboBox.addItem("POST");
         GridBagConstraints gbc_requestMethodcomboBox = new GridBagConstraints();
         gbc_requestMethodcomboBox.fill = GridBagConstraints.HORIZONTAL;
         gbc_requestMethodcomboBox.gridx = 1;
         gbc_requestMethodcomboBox.gridy = 0;
         panel_8.add(requestMethodComboBox, gbc_requestMethodcomboBox);
         */
        JPanel panel_2 = new JPanel();
        GridBagConstraints gbc_panel_2 = new GridBagConstraints();
        gbc_panel_2.insets = new Insets(0, 0, 5, 0);
        gbc_panel_2.fill = GridBagConstraints.BOTH;
        gbc_panel_2.gridx = 0;
        gbc_panel_2.gridy = 3;
        topPanel.add(panel_2, gbc_panel_2);
        panel_2.setLayout(new BorderLayout(0, 0));

        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        panel_2.add(tabbedPane);


        JPanel panel_3 = new JPanel();
        tabbedPane.addTab("Parameters", null, panel_3, null);
        panel_3.setLayout(new BorderLayout(0, 0));

        deFaultParameterTable = new JTable();
        panel_3.add(new JScrollPane(deFaultParameterTable), BorderLayout.CENTER);
        deFaultParameterTable.setRowSelectionAllowed(false);
        final MouseInputListener mouseInputListener = RequestPanel.getMouseInputListener(deFaultParameterTable, null, this);//
        deFaultParameterTable.addMouseListener(mouseInputListener);
        JPanel panel_4 = new JPanel();
        panel_3.add(panel_4, BorderLayout.SOUTH);

        JButton addArguButton = new JButton("添加");
        addArguButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addParameter(SystemHWUtil.EMPTY);
            }
        });
        panel_4.add(addArguButton);

        JButton btnAddFrom = new JButton("add from clipboard");
        btnAddFrom.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addParameterClip();
            }
        });
        panel_4.add(btnAddFrom);

        JPanel panel_6 = new JPanel();
        setDropTarget2(panel_6, tabbedPane_1, this);
        panel_3.add(panel_6, BorderLayout.NORTH);

        JLabel label_4 = new JLabel("同请求一起发送的参数");
        panel_6.add(label_4);

        JPanel panel_5 = new JPanel();
        tabbedPane.addTab("Body Data", null, panel_5, null);
        GridBagLayout gbl_panel_5 = new GridBagLayout();
        gbl_panel_5.columnWidths = new int[]{0, 0, 0, 0};
        gbl_panel_5.rowHeights = new int[]{0, 0, 0, 0};
        gbl_panel_5.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
        gbl_panel_5.rowWeights = new double[]{1.0, 0.0, 0.0, Double.MIN_VALUE};
        panel_5.setLayout(gbl_panel_5);

        JScrollPane scrollPane = new JScrollPane();
        GridBagConstraints gbc_scrollPane = new GridBagConstraints();
        gbc_scrollPane.gridwidth = 3;
        gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
        gbc_scrollPane.fill = GridBagConstraints.BOTH;
        gbc_scrollPane.gridx = 0;
        gbc_scrollPane.gridy = 0;
        panel_5.add(scrollPane, gbc_scrollPane);

        defaultTextArea = new AssistPopupTextArea();
        defaultTextArea.setLineWrap(true);
        defaultTextArea.setWrapStyleWord(true);
        scrollPane.setViewportView(defaultTextArea);

        tabbedPane.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JTabbedPane tabbedPane = (JTabbedPane) e.getSource();
                int selectedIndex = tabbedPane.getSelectedIndex();
                System.out.println(selectedIndex);
                if (selectedIndex == 1) {
                    String bodyData = generateRequestBody();
                    if (!ValueWidget.isNullOrEmpty(bodyData)) {
                        defaultTextArea.setText(bodyData);
                        defaultTextArea.requestFocus();
                    }
                    return;
                }//if(selectedIndex==1)
                String requestBodyData = defaultTextArea.getText();
                if (!ValueWidget.isNullOrEmpty(requestBodyData)) {
                    Map requestMap = new HashMap();
                    RequestUtil.setArgumentMap(requestMap, requestBodyData, true, null, null, false, false/*quoteEscape*/);
                    setTableData3(requestMap);
                }
            }
        });

        JLabel label_2 = new JLabel("预留");
        GridBagConstraints gbc_label_2 = new GridBagConstraints();
        gbc_label_2.anchor = GridBagConstraints.EAST;
        gbc_label_2.insets = new Insets(0, 0, 5, 5);
        gbc_label_2.gridx = 0;
        gbc_label_2.gridy = 1;
        panel_5.add(label_2, gbc_label_2);

        AssistPopupTextField textField_1 = new AssistPopupTextField();
        GridBagConstraints gbc_textField_1 = new GridBagConstraints();
        gbc_textField_1.insets = new Insets(0, 0, 5, 5);
        gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
        gbc_textField_1.gridx = 1;
        gbc_textField_1.gridy = 1;
        panel_5.add(textField_1, gbc_textField_1);
        textField_1.setColumns(10);

        JLabel label_3 = new JLabel("预留");
        GridBagConstraints gbc_label_3 = new GridBagConstraints();
        gbc_label_3.anchor = GridBagConstraints.EAST;
        gbc_label_3.insets = new Insets(0, 0, 0, 5);
        gbc_label_3.gridx = 0;
        gbc_label_3.gridy = 2;
        panel_5.add(label_3, gbc_label_3);

        AssistPopupTextField textField_2 = new AssistPopupTextField();
        GridBagConstraints gbc_textField_2 = new GridBagConstraints();
        gbc_textField_2.insets = new Insets(0, 0, 0, 5);
        gbc_textField_2.fill = GridBagConstraints.HORIZONTAL;
        gbc_textField_2.gridx = 1;
        gbc_textField_2.gridy = 2;
        panel_5.add(textField_2, gbc_textField_2);
        textField_2.setColumns(10);

        JPanel panel_10 = new JPanel();
        setDropTarget2(panel_10, tabbedPane_1, this);
        GridBagConstraints gbc_panel_10 = new GridBagConstraints();
        gbc_panel_10.fill = GridBagConstraints.BOTH;
        gbc_panel_10.gridx = 0;
        gbc_panel_10.gridy = 4;
        topPanel.add(panel_10, gbc_panel_10);
        panel_10.setLayout(new BorderLayout(0, 0));

        JScrollPane scrollPane_1 = new JScrollPane();
        panel_10.add(scrollPane_1);

        responseSetCookieTA = new AssistPopupTextArea();
        responseSetCookieTA.setLineWrap(true);
        responseSetCookieTA.setWrapStyleWord(true);
        responseSetCookieTA.setEditable(false);
        scrollPane_1.setViewportView(responseSetCookieTA);
        Border border22 = BorderFactory.createEtchedBorder(Color.white,
                new Color(148, 145, 140));
        TitledBorder openFileTitle22 = new TitledBorder(border22, "应答Set-Cookie(所有请求)");
        scrollPane_1.setBorder(openFileTitle22);

        JPanel panel_11 = new JPanel();
        tabbedPane_1.addTab("备忘", null, panel_11, null);
        panel_11.setLayout(new BorderLayout(0, 0));

        JSplitPane splitPane_1 = new JSplitPane();
        panel_11.add(splitPane_1, BorderLayout.CENTER);
        splitPane_1.setOrientation(JSplitPane.VERTICAL_SPLIT);
        splitPane_1.setDividerLocation((getFullHeight() - 80) / 2);
        splitPane_1.setDividerSize(5);
        JPanel topPanel1 = new JPanel();
        JPanel bottomPanel2 = new JPanel();
        splitPane_1.setRightComponent(bottomPanel2);
        bottomPanel2.setLayout(new BorderLayout(0, 0));

        memoTextArea_1 = new AssistPopupTextArea();
        memoTextArea_1.setLineWrap(true);
        memoTextArea_1.setWrapStyleWord(true);
        bottomPanel2.add(new JScrollPane(memoTextArea_1), BorderLayout.CENTER);

        splitPane_1.setLeftComponent(topPanel1);
        topPanel1.setLayout(new BorderLayout(0, 0));

        memoTextArea2 = new AssistPopupTextArea();
        memoTextArea2.setLineWrap(true);
        memoTextArea2.setWrapStyleWord(true);
        topPanel1.add(new JScrollPane(memoTextArea2), BorderLayout.CENTER);

        tabbedPane_2 = new JTabbedPane(JTabbedPane.TOP);
        tabbedPane_2.setBackground(Color.WHITE);
        tabbedPane_2.addMouseListener(getMouseInputListener(tabbedPane_2, this));
        tabbedPane_2.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);//不换行
        splitPane.setRightComponent(tabbedPane_2);
        addOneTab(new RequestPanel(this, actionCallbackMap));
        tabbedPane_2.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JTabbedPane tabbedPane = (JTabbedPane) e.getSource();
                setTitleWhenTabbed(tabbedPane);
                int selectedIndex = tabbedPane.getSelectedIndex();
//				tabbedPane.setBackgroundAt(selectedIndex, Color.red);
                indexList.add(selectedIndex);
                final RequestPanel requestPanel = getCurrentRequestPanel();
                if (isInitialized() && !requestPanel.isAlreadyRended2()) {
//                    ToastMessage.toast("正在渲染...", 1000);
                    rendPanel(requestPanel);
//                    updateUI2();
                    optimizParameterScroll(requestPanel, false);
                    tabbedPane_2.setToolTipTextAt(selectedIndex, requestPanel.getServerIp() + requestPanel.getActionName());
                }
            }
        });
        tabbedPane_2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                JTabbedPane tabbedPane = (JTabbedPane) e.getSource();
                setTitleWhenTabbed(tabbedPane);
                int selectedIndex = tabbedPane.getSelectedIndex();
                System.out.println("selectedIndex:" + selectedIndex);
                //当前选择的标签页
                Component selectedTab = tabbedPane.getComponentAt(selectedIndex);
                if (ValueWidget.isNullOrEmpty(selectedTab)) {
                    return;
                }
                currentRequestPanel = (RequestPanel) selectedTab;
//                	tabbedPane.setBackgroundAt(selectedIndex, Color.red);
                if (e.getClickCount() == 2) {
                    System.out.println("双击");
//    					currentRequestPanel.requestAction();
                    ToastMessage.toast("id:" + currentRequestPanel.getTabIndex2(), 1000);
                }
            }
        });

        JPanel panel_7 = new JPanel();
        getContentPane().add(panel_7, BorderLayout.NORTH);
        GridBagLayout gbl_panel_7 = new GridBagLayout();
        gbl_panel_7.columnWidths = new int[]{56, 0, 0, 0, 0, 153, 296, 0};
        gbl_panel_7.rowHeights = new int[]{0, 0};
        gbl_panel_7.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        gbl_panel_7.rowWeights = new double[]{0.0, Double.MIN_VALUE};
        panel_7.setLayout(gbl_panel_7);

        JButton runButton = new JButton("运行");
        runButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                RequestPanel requestPanel = (RequestPanel) tabbedPane_2.getSelectedComponent();
                JButton button = (JButton) e.getSource();
                requestPanel.requestAction(button);
            }
        });
        /*GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(0, 0, 0, 5);
		gbc_btnNewButton.gridx = 0;
		gbc_btnNewButton.gridy = 0;
		panel_7.add(runButton, gbc_btnNewButton);*/

        JButton addRequestButton_1 = new JButton("添加请求");
        addRequestButton_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (getGlobalConfig().isNoConfirmOnAddRequestPane()) {
                    addOneRequest(true, true);
                    return;
                }
                int result = JOptionPane.showConfirmDialog(null, "Are you sure to ADD ?", "确认",
                        JOptionPane.OK_CANCEL_OPTION);
                if (result == JOptionPane.OK_OPTION) {
                    addOneRequest(true, true);
                }
            }
        });

        JSeparator separator = new JSeparator();
        GridBagConstraints gbc_separator = new GridBagConstraints();
        gbc_separator.insets = new Insets(0, 0, 0, 5);
        gbc_separator.gridx = 0;
        gbc_separator.gridy = 0;
        panel_7.add(separator, gbc_separator);
        GridBagConstraints gbc_addRequestButton_1 = new GridBagConstraints();
        gbc_addRequestButton_1.insets = new Insets(0, 0, 0, 5);
        gbc_addRequestButton_1.gridx = 1;
        gbc_addRequestButton_1.gridy = 0;
        panel_7.add(addRequestButton_1, gbc_addRequestButton_1);

        JButton unBindBtn = new JButton("解绑手机号");
        unBindBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UnbindMobileDialog unbindMobileDialog = new UnbindMobileDialog();
                unbindMobileDialog.setUnbindMobileBean(AutoTestPanel.this.getUnbindMobileBean());
                unbindMobileDialog.launchFrame();
            }
        });
        /*JButton btnNewButton = new JButton("检查DNS(D)");
        btnNewButton.setMnemonic('D');
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (dialog == null) {
					dialog = new CheckDnsDialog(false);
				}
				dialog.setVisible(true);
				dialog.execute("i.chanjet.com", "i.static.chanjet.com", "store.chanjet.com", "cloud.static.chanjet.com", true);
			}
		});*/
        GridBagConstraints gbc_btnNewButton2 = new GridBagConstraints();
        gbc_btnNewButton2.insets = new Insets(0, 0, 0, 5);
        gbc_btnNewButton2.gridx = 2;
        gbc_btnNewButton2.gridy = 0;
        panel_7.add(unBindBtn, gbc_btnNewButton2);

        JSeparator separator_1 = new JSeparator();
        GridBagConstraints gbc_separator_1 = new GridBagConstraints();
        gbc_separator_1.insets = new Insets(0, 0, 0, 5);
        gbc_separator_1.gridx = 3;
        gbc_separator_1.gridy = 0;
        panel_7.add(separator_1, gbc_separator_1);

        //通过请求的"请求名称"进行搜索,若搜到,则自动选中
        searchTextField = new AssistPopupTextField();
        tfDefaultColor = searchTextField.getBackground();
        String placeholder4 = "输入关键字回车搜索(control+K聚焦,以两个空格分割)";
        searchTextField.placeHolder(placeholder4);
        searchTextField.setToolTipText(placeholder4);
        searchTextField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String keyWord = searchTextField.getText();
                if (ValueWidget.isNullOrEmpty(keyWord)) {
                    return;
                }
                Set<Integer> searchResult = MenuUtil2.searchAction(keyWord, tabbedPane_2, true/*includeActionName*/);
                if (searchResult == null) return;
                //如果只有一个就直接选中
                int resultSize = searchResult.size();
                if (resultSize == 0) {//没有搜索到
                    ToastMessage.toast("无结果", 1000, Color.red);
                } else if (resultSize == 1) {//仅搜索到一条记录
                    MenuUtil2.setBackgroundWhenSearch(searchTextField/*, tfDefaultColor*/);
                    searchSuccess(searchResult.iterator().next());
                } else {
                    JPopupMenu textPopupMenu = MenuUtil2.searchResultList(AutoTestPanel.this, tabbedPane_2, searchResult, searchTextField);
                    MenuUtil2.setBackgroundWhenSearch(searchTextField/*, tfDefaultColor*/);
                    Component[] components = textPopupMenu.getComponents();
                    for (int i = 0; i < components.length; i++) {
                        final JMenuItem menuItem = (JMenuItem) components[i];
                        menuItem.addMouseListener(new MouseAdapter() {

                            @Override
                            public void mouseEntered(MouseEvent e) {
//                                System.out.println(" :" + menuItem.getText()/*.substring(20)*/);
                                String result = SystemHWUtil.splitAndFilterString(menuItem.getText(), -1);
                                //获取tabbed的index
                                // "(524)获取用户信息(2)线上 | cia.c.. /api/v1/user"
                                Integer index = Integer.parseInt(result.replaceAll("^[\\s]*\\(([\\d]+)\\).*", "$1"));
                                RequestPanel requestPanel = getRequestPanel(index - 1);
                                RequestInfoBean requestInfoBean = requestPanel.getOriginalRequestInfoBean();
                                Map<String, Object> requestParameters = requestInfoBean.getRequestParameters();
                                if (requestParameters.size() > 0) {
                                    StringBuffer stringBuffer = new StringBuffer();
                                    for (Map.Entry entry : requestParameters.entrySet()) {
                                        stringBuffer.append(entry.getKey()).append(":").append("<span style=\"color: #ff4646;font-weight: bolder;\">" + entry.getValue() + "</span>");
                                        stringBuffer.append("<br>");
                                    }
                                    System.out.println("stringBuffer :" + stringBuffer.toString());
                                    menuItem.setToolTipText("<html>" + stringBuffer.toString() + "</html>");
                                } else {
                                    menuItem.setToolTipText("无参数");
                                }
                            }

                        });
                    }
                }
            }
        });

		/*JButton btndns = new JButton("关闭\"检查DNS\"对话框");
        btndns.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (dialog == null) {
					dialog = new CheckDnsDialog(false);
				}
				dialog.setVisible(false);//隐藏对话框
				ToastMessage.toast("对话框已关闭", 1000);
			}
		});*/

        JButton button = new JButton("生成二维码");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showQRCodeDialog();
            }
        });
        GridBagConstraints gbc_button = new GridBagConstraints();
        gbc_button.insets = new Insets(0, 0, 0, 5);
        gbc_button.gridx = 4;
        gbc_button.gridy = 0;
        panel_7.add(button, gbc_button);

        final JButton json2BeanBtn = new JButton("json转bean");
        json2BeanBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Json2BeanDialog json2BeanDialog = new Json2BeanDialog(getJson2JavaBean(), AutoTestPanel.this);
                json2BeanDialog.setVisible(true);
            }
        });

        GridBagConstraints gbc_btndns = new GridBagConstraints();
        gbc_btndns.insets = new Insets(0, 0, 0, 5);
        gbc_btndns.gridx = 5;
        gbc_btndns.gridy = 0;
        panel_7.add(json2BeanBtn, gbc_btndns);
        GridBagConstraints gbc_textField22 = new GridBagConstraints();
        gbc_textField22.fill = GridBagConstraints.HORIZONTAL;
        gbc_textField22.gridx = 6;
        gbc_textField22.gridy = 0;
        panel_7.add(searchTextField, gbc_textField22);
        searchTextField.setColumns(10);

        final JButton assembleUrlBtn = new JButton("组装url");
        assembleUrlBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AssembleUrlDialog assembleUrlDialog = new AssembleUrlDialog(AutoTestPanel.this);
                assembleUrlDialog.setVisible(true);
            }
        });
        GridBagConstraints gbc_AssembleUrl = new GridBagConstraints();
//        gbc_AssembleUrl.fill = GridBagConstraints.HORIZONTAL;
        gbc_AssembleUrl.gridx = 7;
        gbc_AssembleUrl.gridy = 0;
        panel_7.add(assembleUrlBtn, gbc_AssembleUrl);

        //插件机制,后续改为快捷键
        JButton openPluginBtn = new JButton("启动插件");
        openPluginBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getCurrentRequestPanel().startPlugin();
            }
        });
        GridBagConstraints gbc_openPluginBtn = new GridBagConstraints();
//        gbc_AssembleUrl.fill = GridBagConstraints.HORIZONTAL;
        gbc_openPluginBtn.gridx = 8;
        gbc_openPluginBtn.gridy = 0;
        panel_7.add(openPluginBtn, gbc_openPluginBtn);


        JPanel panel_9 = new JPanel();
        panel_9.setBackground(Color.WHITE);
        panel_9.setLayout(new GridLayout(2, 1, 0, 0));
        getContentPane().add(panel_9, BorderLayout.SOUTH);

        progressBar = new JProgressBar();
        progressDefaultColor = progressBar.getForeground();
        this.progressBarUI = progressBar.getUI();
        progressBar.setBorderPainted(true);

        panel_9.add(progressBar);

        JLabel encodingLabel_1 = DialogUtil.getEncodingLabel();
        encodingLabel_1.setBackground(Color.WHITE);
        panel_9.add(encodingLabel_1);

        try {
            readConfig();
        } catch (IOException e1) {
            e1.printStackTrace();
            GUIUtil23.errorDialog(e1);
        }
        setGlobalShortCuts();
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                saveConfigCondition();
                super.windowClosing(e);
            }
        });
        setMenuBar2();
        addGlobalKey();
        ending();
        timingSave();
    }

    public static String getHomeDir() {
        return System.getProperty("user.home") + File.separator;
    }

    /***
     * 优化"请求参数"所在scroll的显示
     * @param requestPanel
     * @param isAdd
     */
    public static void optimizParameterScroll(final RequestPanel requestPanel, final boolean isAdd) {
        new Thread(new Runnable() {
            //解决选择新标签页,请求参数表格没有撑开的问题
            @Override
            public void run() {
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                if (null != requestPanel) {
                    requestPanel.resetTableRowHeight();
                    requestPanel.adaptTableSize2(true/* 很有必要,不能去掉 isFrameRepaint*/, isAdd/*isAdd*/, false/*isDel*/);

                    scroll2bottom(requestPanel);//使滚动条处于底部
                }
            }
        }).start();//new Thread
    }

    /***
     * 使滚动条处于底部(贴底)
     * @param requestPanel
     */
    public static void scroll2bottom(final RequestPanel requestPanel) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                requestPanel.scroll2Bottom();

            }
        }).start();
    }

    public static void setDropTarget2(JComponent component, final JTabbedPane tabbedPane_1, final AutoTestPanel autoTestPanel) {
        new DropTarget(component, DnDConstants.ACTION_COPY_OR_MOVE,
                new DropTargetAdapter() {
                    @Override
                    public void drop(DropTargetDropEvent dtde)// 重写适配器的drop方法
                    {
                        try {
                            Transferable t = dtde.getTransferable();
                            Object object = t.getTransferData(dataFlavor);
                            System.out.println(t);

                            if (object instanceof RequestPanel) {
                                RequestPanel requestPanel = (RequestPanel) object;
                                JTabbedPane tabbedPane = requestPanel.getTabbedPane_22();
                                System.out.println(tabbedPane);
                                tabbedPane_1.addTab("应答结果-" + requestPanel.getRequestName(), null, tabbedPane, "应答结果");
                                int size = tabbedPane_1.getTabCount();
                                tabbedPane_1.setSelectedIndex(size - 1);
                                autoTestPanel.getCurrentRequestPanel().updateUI();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        boolean oldUI = true;

        if (!ValueWidget.isNullOrEmpty(args)) {
            if (args[0].equals("tf")) {
                isTF_table_cell = true;
            } else if (args[0].equals("proxy")) {
                isproxy = true;
                if (args.length > 1) {
                    proxyPort = args[1];
                }
            } else if (args[0].equals("ui")) {
                oldUI = false;
            }

        }
        //TODO 禁用文本域的自动补全路径 后续写到配置文件
        UndoTextField.setTextFieldConfigInfo(new TextFieldConfigInfo());
        if (oldUI) {
            DialogUtil.lookAndFeel2();
        } else {
            try {
                org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper.launchBeautyEyeLNF();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    AutoTestPanel frame = new AutoTestPanel(isproxy, proxyPort);
                    frame.setVisible(true);
//					frame.updateUITable2();

                } catch (Exception e) {
                    e.printStackTrace();
                    GUIUtil23.errorDialog(e);
                }
            }
        });
    }

    public static MouseInputListener getMouseInputListener(final JTabbedPane tabbedPane/*,final RequestPanel currentRequestPanel*/
            , final AutoTestPanel autoTestPanel) {

        return new MouseInputListener() {
            public int count;

            public void mouseClicked(MouseEvent e) {
                /*if(e.getClickCount()==1){
					final int rowCount = jTable.getSelectedRow();
                	final int columnCount=jTable.getSelectedColumn();
            		int modifiers = e.getModifiers();
            		modifiers -= MouseEvent.BUTTON3_MASK;
                	modifiers |= MouseEvent.FOCUS_EVENT_MASK;
                	MouseEvent ne = new MouseEvent(e.getComponent(), e.getID(),
    						e.getWhen(), modifiers, e.getX(), e.getY(),
    						2, false);
//                	processEvent(ne);
                	jTable.editCellAt(rowCount, columnCount,ne);
//                	CellEditor cellEditor=jTable.getCellEditor(rowCount, columnCount);
//                	cellEditor.shouldSelectCell(ne);
                	jTable.dispatchEvent(ne);
            	}*/
//				System.out.println("mouseClicked" + (count++));
                processEvent(e);

            }

            /***
             * in order to trigger Left-click the event
             */
            public void mousePressed(MouseEvent e) {
                processEvent(e);// is necessary!!!
//                System.out.println("mousePressed"+(count++));
            }

            public void mouseReleased(MouseEvent e) {

                RequestPanel currentRequestPanel2 = (RequestPanel) tabbedPane.getSelectedComponent();
                if (ValueWidget.isNullOrEmpty(currentRequestPanel2)) {
                    currentRequestPanel2 = autoTestPanel.getCurrentRequestPanel();
                }
                /*if(null== autoTestPanel.globalInfo){
					autoTestPanel.globalInfo=new GlobalInfo();
				}
				int selectedIndex=autoTestPanel.getCurrentSelectedIndex();

				autoTestPanel.globalInfo.setCurrentTabbedIndex(selectedIndex);*/
                if (e.getButton() != MouseEvent.BUTTON3) {// right click
                    return;
                }/*else if (e.getButton() == MouseEvent.BUTTON1&& e.getClickCount()==1){
                    System.out.println("左键");
                	int modifiers = e.getModifiers();
                	modifiers |= MouseEvent.FOCUS_EVENT_MASK;
                	MouseEvent ne = new MouseEvent(e.getComponent(), e.getID(),
							e.getWhen(), modifiers, e.getX(), e.getY(),
							2, false);

//                	processEvent(ne);
//                	jTable.editCellAt(rowCount, columnCount,ne);
//                	CellEditor cellEditor=jTable.getCellEditor(rowCount, columnCount);
//                	cellEditor.shouldSelectCell(ne);
                	jTable.dispatchEvent(ne);
                }*/
                // right click
                processEvent(e);

                // System.out.println("row:" + rowCount);
                //右键点击表格时,点击的单元格,正常情况返回的是String
//                	System.out.println("rowCount:"+rowCount);
//                	System.out.println("columnCount:"+columnCount);


                JPopupMenu popupmenu = new JPopupMenu();
                JMenuItem runM = new JMenuItem(RequestPanel.ACTION_COMMAND_RUN);
                JMenuItem cleanUp_runM = new JMenuItem("清空后再运行");
                JMenuItem copyParameterM = new JMenuItem(RequestPanel.ACTION_COMMAND_COPY_REQUEST_PARAMETER);
                JMenuItem copyResponseM = new JMenuItem(RequestPanel.ACTION_COMMAND_COPY_RESPONSE);
                JMenuItem copyAccessTokenM = new JMenuItem("复制access_token");
                JMenuItem cleanUpCellM = new JMenuItem("清空单元格");
                cleanUpCellM.setActionCommand(MenuUtil2.ACTION_STR_CLEANUP_CELL);

                JMenuItem sendPostM = new JMenuItem("打开网页发送POST请求");
                JMenuItem copyPanelM = new JMenuItem("复制请求panel");

                MenuUtil2.strongMenuItem(copyPanelM);

                JMenuItem deleteCurrentPanelM = new JMenuItem("删除当前请求panel");
                MenuUtil2.strongMenuItem(deleteCurrentPanelM);
                deleteCurrentPanelM.setForeground(Color.red);

                JMenuItem cleanResultM = new JMenuItem("清空结果");
                JMenuItem copyRequestInfoM = new JMenuItem(RequestPanel.ACTION_COMMAND_COPY_REQUEST_INFO);
                JMenuItem pasteRequestInfoM = new JMenuItem(RequestPanel.ACTION_COMMAND_PASTE_REQUEST_INFO);
                JMenuItem addNewRequestM = new JMenuItem("添加新请求");
                JMenuItem hideOtherRequestM = new JMenuItem(MyTableMenuListener.ACTION_HIDE_OTHER_REQUEST);
                JMenuItem maxRequestPanelM = new JMenuItem("最大化请求面板");
                JMenuItem openPreRequestPanelM = new JMenuItem("打开前置请求");
                JMenuItem generateSendHttpRequestCodeM = new JMenuItem("生成发送请求代码");
                JMenuItem generateSendHttpRequestCodePythonM = new JMenuItem("生成发送请求代码python");
                JMenuItem generateSendHttpRequestCodePythonAndExportM = new JMenuItem("生成发送请求代码python并保存");
                JMenuItem generateSendHttpRequestCodeCurlM = new JMenuItem("生成发送请求代码cURL");
                JMenuItem generateSendHttpRequestCodeCurlOneLineM = new JMenuItem("生成发送请求代码cURL(单行)");
                JMenuItem generateBasicSendHttpRequestCodeM = new JMenuItem("生成发送请求代码(更基础)");
                JMenuItem modifyResponseBodyM = new JMenuItem("修改代理应答体");
                JMenuItem updateConfigFileM = new JMenuItem("上传配置文件");

                MyTableMenuListener yMenuActionListener = new MyTableMenuListener(null, SystemHWUtil.NEGATIVE_ONE
                        , SystemHWUtil.NEGATIVE_ONE, currentRequestPanel2);
                runM.addActionListener(yMenuActionListener);
                cleanUp_runM.addActionListener(yMenuActionListener);
                copyParameterM.addActionListener(yMenuActionListener);
                copyResponseM.addActionListener(yMenuActionListener);
                copyAccessTokenM.addActionListener(yMenuActionListener);
                cleanResultM.addActionListener(yMenuActionListener);
                copyRequestInfoM.addActionListener(yMenuActionListener);
                pasteRequestInfoM.addActionListener(yMenuActionListener);
                addNewRequestM.addActionListener(yMenuActionListener);
                hideOtherRequestM.addActionListener(yMenuActionListener);
                maxRequestPanelM.addActionListener(yMenuActionListener);
                openPreRequestPanelM.addActionListener(yMenuActionListener);
                generateSendHttpRequestCodeM.addActionListener(yMenuActionListener);
                generateSendHttpRequestCodePythonM.addActionListener(yMenuActionListener);
                generateSendHttpRequestCodePythonAndExportM.addActionListener(yMenuActionListener);
                generateSendHttpRequestCodeCurlM.addActionListener(yMenuActionListener);
                generateSendHttpRequestCodeCurlOneLineM.addActionListener(yMenuActionListener);
                if (autoTestPanel.isMoreBasicHttpSendCode()) {
                    generateBasicSendHttpRequestCodeM.addActionListener(yMenuActionListener);
                }
                modifyResponseBodyM.addActionListener(yMenuActionListener);
                updateConfigFileM.addActionListener(yMenuActionListener);
                cleanUpCellM.addActionListener(yMenuActionListener);
                sendPostM.addActionListener(yMenuActionListener);
                copyPanelM.addActionListener(yMenuActionListener);
                deleteCurrentPanelM.addActionListener(yMenuActionListener);

                popupmenu.add(cleanUp_runM);
                popupmenu.add(runM);
                popupmenu.add(copyParameterM);
                popupmenu.add(copyResponseM);
                popupmenu.add(copyAccessTokenM);
                popupmenu.add(cleanUpCellM);
                popupmenu.add(sendPostM);
                popupmenu.add(copyPanelM);
                popupmenu.add(deleteCurrentPanelM);
                popupmenu.add(cleanResultM);
                popupmenu.add(copyRequestInfoM);
                popupmenu.add(pasteRequestInfoM);
                popupmenu.add(addNewRequestM);
                popupmenu.add(hideOtherRequestM);
                popupmenu.add(maxRequestPanelM);
                popupmenu.add(openPreRequestPanelM);
                popupmenu.add(generateSendHttpRequestCodeM);
                popupmenu.add(generateSendHttpRequestCodePythonM);
                popupmenu.add(generateSendHttpRequestCodePythonAndExportM);
                popupmenu.add(generateSendHttpRequestCodeCurlM);
                popupmenu.add(generateSendHttpRequestCodeCurlOneLineM);
                if (autoTestPanel.isMoreBasicHttpSendCode()) {
                    popupmenu.add(generateBasicSendHttpRequestCodeM);
                }
                popupmenu.add(modifyResponseBodyM);
//                    popupmenu.add(updateConfigFileM);
                popupmenu.show(e.getComponent(), e.getX(), e.getY());
//                    processEvent(e);
//                System.out.println("mouseReleased"+(count++));
            }

            public void mouseEntered(MouseEvent e) {
                processEvent(e);
            }

            public void mouseExited(MouseEvent e) {
                processEvent(e);
            }

            public void mouseDragged(MouseEvent e) {
                processEvent(e);
            }

            public void mouseMoved(MouseEvent e) {
                processEvent(e);
            }

            private void processEvent(MouseEvent e) {
                // Right-click on
                MenuUtil2.processEvent(e, tabbedPane);
            }
        };
    }

    /***
     * 生成备份文件的名称
     *
     * @param baseName
     * @return
     */
    private static String getTodayBakName(String baseName) {
        String dateFormat = TimeHWUtil.formatDate(new Date(), TimeHWUtil.YYYYMMDD_NO_LINE);
        return baseName + dateFormat;
    }

    public static DataFlavor getDataFlavor() {
        return dataFlavor;
    }

    private static void adaptTableAction(RequestPanel requestPanel) {
        requestPanel.resetTableRowHeight();
        requestPanel.adaptTableSize2(true/* 很有必要,不能去掉 isFrameRepaint*/, false/*isAdd*/, false/*isDel*/);
    }

    public static void rendPanel(RequestPanel requestPanel) {
        requestPanel.initialize3();
        requestPanel.resume((RequestInfoBean) null);
        requestPanel.initializePreRequestIdListbox();
        requestPanel.updateUI();
    }

    @Override
    protected void globalShortCuts(KeyEvent kE) {
        if (kE.getID() == KeyEvent.KEY_PRESSED) {
//            System.out.println(" :" +kE.getKeyCode()+" , "+kE.getKeyChar());
            if (kE.getKeyCode() == KeyEvent.VK_R//caps lock+R
                    && EventHWUtil.isControlDown(kE)
                    && kE.isAltDown()
                    && kE.isShiftDown()
                    && kE.isControlDown()
                    ) {
                System.out.println("run ");
                RequestPanel requestPanel = (RequestPanel) tabbedPane_2.getSelectedComponent();
                requestPanel.requestAction(null);
            } else if (kE.getKeyCode() == KeyEvent.VK_HOME/*目前是右边的Alt/option*/) {
                //和上面快捷键触发的动作一样
                RequestPanel requestPanel = (RequestPanel) tabbedPane_2.getSelectedComponent();
                requestPanel.requestAction(null);
            } else if (kE.getKeyCode() == KeyEvent.VK_P//caps lock+R
                    && EventHWUtil.isControlDown(kE)/*&& kE.isControlDown()*/) {
                getCurrentRequestPanel().startPlugin();
            }
        }

    }

    /***
     * 定时器保存配置文件
     */
    private void timingSave() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
//                System.out.println("time :" );
                saveConfigCondition();
            }
        }, 1000 * 60 * 5/*5分钟 */, 1000 * 60 * 1/*1分钟 */);
    }

    public boolean isMoreBasicHttpSendCode() {
        return null != getGlobalConfig() && getGlobalConfig().isMoreBasicHttpSendCode();
    }

    public void addListenAction() {
        actionCallbackMap = new HashMap<String, ActionCallback>();
        actionCallbackMap.put("Command_enter", new ActionCallback() {
            @Override
            public void actionPerformed(ActionEvent actionEvent, JComponent component) {
                System.out.println("success Command_enter");
                AutoTestPanel.this.getCurrentRequestPanel().runWithProgress(null);
            }
        });
        actionCallbackMap.put("Ctrl_enter", new ActionCallback() {
            @Override
            public void actionPerformed(ActionEvent actionEvent, JComponent component) {
                System.out.println("success Ctrl_enter!!!!");
                AutoTestPanel.this.getCurrentRequestPanel().runWithProgress(null);
            }
        });
    }

    /***
     * 左右移动<br>
     * 将进度条设置为不确定模式
     */
    public void testIndeterminate() {
        if (SystemHWUtil.isMacOSX) {//如果是mac book
            progressBar.setUI(new StripedProgressBarUI(true, true));
        }
        progressBar.setStringPainted(true);
        progressBar.setString(".....");
        progressBar.setIndeterminate(true);
        progressBar.setForeground(Color.blue);
        progressBar.setBackground(Color.yellow);
    }

    /***
     * 停止不确定模式
     */
    public void stopIndeterminate() {
        System.out.println(progressBar.getForeground());
        System.out.println(progressBar.getBackground());
        progressBar.setUI(this.progressBarUI);
        progressBar.setIndeterminate(false);
        progressBar.setStringPainted(true);
        progressBar.setString(Constant2.PROGRESSINITSTR);
        progressBar.setValue(100);
//        progressBar.repaint();
//        updateUI2();
//		copyProgressBar.setBackground(Color.blue);
    }

    public void saveConfigCondition() {
        if (!isDebug && (null == getGlobalInfo() || getGlobalInfo().isWillSave())) {
            saveConfig();
        }
    }

    /***
     * command+e<br />
     * ctrl+e,Ctrl +E
     */
    @Override
    public void showRecentList() {
        super.showRecentList();
        System.out.println("showRecentList");
        List<Integer> integers = new ArrayList<Integer>();
//        System.out.println("indexList:"+indexList);
        List<Integer> uniqueList = SystemHWUtil.uniqueInt(indexList);
        int length = uniqueList.size();
//        System.out.println("uniqueList:"+uniqueList);
        for (int i = length - 1; (i >= 0) && (i > length - 10); i--) {
            int index = uniqueList.get(i);
            if (index != getCurrentSelectedIndex()) {
                if (!integers.contains(index)) {
                    integers.add(index);
                }

            }
        }
//        System.out.println(integers);
        MenuUtil2.searchResultList(AutoTestPanel.this, tabbedPane_2, integers, searchTextField);
    }

    /***
     * 收尾
     */
    public void ending() {
//		System.out.println(globalInfo.isHasShowWish());
        if (globalInfo == null || !globalInfo.isHasShowWish()) {
            Calendar calendar = TimeHWUtil.getCalendar();
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int mouth = calendar.get(Calendar.MONTH);
            if (mouth == 0) {
                if (day == 30 || day == 31 || day >= 1 && day <= 4) {
                    showWish();
                }
            }

            System.out.println(mouth + "-" + day);
        }

        if (getGlobalConfig().isSelectAllOnFocusSearchTF()) {
            searchTextField.addFocusListener(new FocusAdapter() {
                @Override
                public void focusGained(FocusEvent e) {
                    super.focusGained(e);
                    searchTextField.selectAll();
                }
            });
        }
    }

    private void showWish() {
        if (globalInfo == null) {
            globalInfo = new GlobalInfo();
        }
        globalInfo.setHasShowWish(true);
        GenericDialog dialog = new GenericDialog() {
            static final String resourcePath1 = "com/yunma/img/15198975_141743445107_2.jpg";
            static final String resourcePath2 = "com/yunma/img/40.jpg";
            static final int delta = 25;
            private int count = 0;

            @Override
            public void layout3(Container contentPane) {
                Color backgroundColor = Color.WHITE;
                super.layout3(contentPane);
                contentPane.setBackground(backgroundColor);
                setBackground(backgroundColor);
                setTitle("元旦快乐,亲 ^_^");
                setModal(true);
                final JLabel imgLabel = new JLabel();
                imgLabel.setBackground(backgroundColor);

                setImage1(imgLabel);
                JScrollPane jScrollPane = new JScrollPane(imgLabel);
                jScrollPane.setBackground(backgroundColor);
                contentPane.add(jScrollPane);
                addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosing(WindowEvent e) {
                        super.windowClosing(e);
                    }
                });

                java.util.Timer timer = new java.util.Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        setImage2(imgLabel);
                        addClick(imgLabel, resourcePath1, resourcePath2);
                    }
                }, 5000);

            }

            private void setImage2(final JLabel imgLabel) {
                try {
                    setLabelImg1(imgLabel, DialogUtil.getImageIcon(resourcePath2, AutoTestPanel.class));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                setLoc(730 + delta, 701 + delta + 25);
            }

            private void setImage1(final JLabel imgLabel) {
                try {
                    setLabelImg1(imgLabel, DialogUtil.getImageIcon(resourcePath1, AutoTestPanel.class));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                setLoc(1024 + delta, 738 + delta + 25);
            }

            private void addClick(final JLabel imgLabel, final String resourcePath1, final String resourcePath2) {
                imgLabel.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        System.out.println();
                        if (e.getButton() != MouseEvent.BUTTON1) {
                            return;
                        }
                        super.mouseClicked(e);
                        if (count % 2 == 0) {
                            setImage1(imgLabel);
                        } else {
                            setImage2(imgLabel);
                        }
                        count++;
                    }
                });
            }

            private void setLabelImg1(JLabel imgLabel, ImageIcon imageIcon) throws IOException {
                imgLabel.setIcon(imageIcon);
            }
        };
        dialog.launchFrame();
    }

    public void searchSuccess(int i) {
        tabbedPane_2.setSelectedIndex(i);
        getCurrentRequestPanel().requestFocus();
//		ToastMessage.toast("搜索成功", 1000);
    }

    /***
     * 全局菜单
     */
    private void setMenuBar2() {
        logFile2 = new File(logFilePath);//日志文件,不是log4j
        JMenuBar menuBar = new JMenuBar();
        JMenu fileM = new JMenu("File");
        JMenuItem createMd5 = new JMenuItem("生成MD5值");
        MyMenuBarActionListener myMenuBarActionListener = new MyMenuBarActionListener(this);
        createMd5.addActionListener(myMenuBarActionListener);
        createMd5.setActionCommand(MenuUtil2.ACTION_CREATE_MD5);
        fileM.add(createMd5);

        JMenuItem openLoggerM = new JMenuItem("打开日志文件");
        openLoggerM.addActionListener(myMenuBarActionListener);
        fileM.add(openLoggerM);

        JMenuItem copyConfigM = new JMenuItem("复制配置内容");
        copyConfigM.addActionListener(myMenuBarActionListener);
        fileM.add(copyConfigM);

        ///Users/whuanghkl/.share_http.properties
        JMenuItem backupConfigM = new JMenuItem("备份配置文件");
        backupConfigM.addActionListener(myMenuBarActionListener);
        fileM.add(backupConfigM);

        JMenuItem deltaLoadConfigM = new JMenuItem("增量加载配置文件");
        deltaLoadConfigM.addActionListener(myMenuBarActionListener);
        fileM.add(deltaLoadConfigM);

        JMenuItem copyConfigPathM = new JMenuItem("复制配置文件路径");
        copyConfigPathM.addActionListener(myMenuBarActionListener);
        fileM.add(copyConfigPathM);

        JMenuItem modifyRequestIdM = new JMenuItem("修改请求ID");
        modifyRequestIdM.addActionListener(myMenuBarActionListener);
        fileM.add(modifyRequestIdM);

        JMenuItem modifyFileM = new JMenuItem("替换文件内容");
        modifyFileM.addActionListener(myMenuBarActionListener);
        fileM.add(modifyFileM);

        JMenuItem installJarFileM = new JMenuItem("安装jar到本地仓库");
        installJarFileM.addActionListener(myMenuBarActionListener);
        fileM.add(installJarFileM);

        JMenuItem exchangeCodeM = new JMenuItem("code换token");
        exchangeCodeM.setAccelerator(KeyStroke.getKeyStroke('Q', InputEvent.CTRL_MASK));//这个快捷键是ctrl+Q
        exchangeCodeM.addActionListener(myMenuBarActionListener);
        fileM.add(exchangeCodeM);

        JMenuItem qrCodeM = new JMenuItem("生成二维码");
        qrCodeM.addActionListener(myMenuBarActionListener);
        fileM.add(qrCodeM);

        JMenuItem tomcatConfM = new JMenuItem("tomcat配置文件");
        tomcatConfM.setAccelerator(KeyStroke.getKeyStroke('T', InputEvent.CTRL_MASK));//这个快捷键是ctrl+T
        tomcatConfM.addActionListener(myMenuBarActionListener);
        fileM.add(tomcatConfM);

        JMenuItem orderInfoM = new JMenuItem("订单信息");
        orderInfoM.setAccelerator(KeyStroke.getKeyStroke('O', InputEvent.CTRL_MASK));//这个快捷键是ctrl+O
        orderInfoM.addActionListener(myMenuBarActionListener);
        fileM.add(orderInfoM);

        JMenuItem deleteRequestBatchM = new JMenuItem("批量删除请求");
        deleteRequestBatchM.addActionListener(myMenuBarActionListener);
        fileM.add(deleteRequestBatchM);

        JMenuItem dnsRequestBatchM = new JMenuItem("检查DNS");
        dnsRequestBatchM.addActionListener(myMenuBarActionListener);
        fileM.add(dnsRequestBatchM);

        JMenuItem saveRequestBatchM = new JMenuItem("保存");
        saveRequestBatchM.addActionListener(myMenuBarActionListener);
        fileM.add(saveRequestBatchM);

        JMenuItem uploadFileM = new JMenuItem("上传文件");
        uploadFileM.addActionListener(myMenuBarActionListener);
        fileM.add(uploadFileM);

        JMenuItem addRequestParameterM = new JMenuItem("添加参数模板");
        addRequestParameterM.addActionListener(myMenuBarActionListener);
        fileM.add(addRequestParameterM);

        JMenuItem addRequestPluginM = new JMenuItem("安装插件");
        addRequestPluginM.addActionListener(myMenuBarActionListener);
        fileM.add(addRequestPluginM);

        JMenuItem rangeRequestM = new JMenuItem("遍历取值范围");//[0..15]
        rangeRequestM.addActionListener(myMenuBarActionListener);
        fileM.add(rangeRequestM);

        JMenuItem configM = new JMenuItem("配置面板");
        MenuUtil2.strongMenuItem(configM);
        configM.addActionListener(myMenuBarActionListener);
        fileM.add(configM);

        JMenuItem generateSendHttpCodeM = new JMenuItem("生成发送请求代码");
        generateSendHttpCodeM.addActionListener(myMenuBarActionListener);
        fileM.add(generateSendHttpCodeM);


        JMenuItem modifyResponseBodyM = new JMenuItem("修改代理应答体");
        modifyResponseBodyM.addActionListener(myMenuBarActionListener);
        fileM.add(modifyResponseBodyM);

        JMenuItem updateConfigFileM = new JMenuItem("上传配置文件");
        updateConfigFileM.addActionListener(myMenuBarActionListener);
        fileM.add(updateConfigFileM);

        JMenuItem writeDiaryM = new JMenuItem("记录日志");
        writeDiaryM.addActionListener(myMenuBarActionListener);
        fileM.add(writeDiaryM);


        JMenuItem rsaM = new JMenuItem("RSA加解密");
        rsaM.addActionListener(myMenuBarActionListener);
        fileM.add(rsaM);

        JMenuItem exitM = new JMenuItem("exit");
        exitM.addActionListener(myMenuBarActionListener);
        fileM.add(exitM);

        menuBar.add(fileM);
		/*JMenuItem checkDNSM = new JMenuItem("检查DNS");
		checkDNSM.addActionListener(myMenuBarActionListener);
		menuBar.add(checkDNSM);*/

//        JMenu helpM = new JMenu("Help");
        JMenuItem childHelpM = new JMenuItem("help");
        childHelpM.setActionCommand("help");
//        helpM.add(childHelpM);

        menuBar.add(childHelpM);
        childHelpM.addActionListener(myMenuBarActionListener);

        JMenuItem childLoginINfoM = new JMenuItem("login info");
        menuBar.add(childLoginINfoM);
        childLoginINfoM.addActionListener(myMenuBarActionListener);

        setJMenuBar(menuBar);
    }

    private void setTitleWhenTabbed(JTabbedPane tabbedPane) {
        if (tabbedPane_2.getTabCount() == 0) {
            return;
        }
        int selectedIndex = tabbedPane.getSelectedIndex();
        if (selectedIndex == SystemHWUtil.NEGATIVE_ONE) {
            return;
        }
//        System.out.println("selectedIndex22:"+selectedIndex);
        //当前选择的标签页
        Component selectedTab = tabbedPane.getComponentAt(selectedIndex);
        if (ValueWidget.isNullOrEmpty(selectedTab) || !(selectedTab instanceof RequestPanel)) {
            return;
        }
        currentRequestPanel = (RequestPanel) selectedTab;
//        	System.out.println("currentRequestPanel:"+currentRequestPanel.getRequestName());
        String tabTitle = currentRequestPanel.getRequestName();
        if (ValueWidget.isNullOrEmpty(tabTitle)) {
            tabTitle = AutoTestPanel.title;
        }
        AutoTestPanel.this.setTitle(tabTitle);
        if (!tabTitle.equals(AutoTestPanel.this.getTitle())) {
            AutoTestPanel.this.setTitle(tabTitle);
        }
        updateUI2();
    }

    public void updateUI2() {
        AutoTestPanel.this.repaint();
    }

    private void addOneRequest(boolean isRealAdd) {
        addOneRequest(null, isRealAdd);
    }

    /***
     * 被resume 调用<br />
     * 仅仅是增加panel,还无内容
     */
    private void addOneRequest(RequestPanel requestPanel, boolean isRealAdd) {
        addOneRequest(requestPanel, false, isRealAdd);
    }

    public RequestPanel addOneRequest(boolean isManual, boolean isRealAdd) {
        return addOneRequest(null, isManual, isRealAdd);
    }

    /***
     * 添加一个新的tab<br >增加新的请求
     * @param isManual : 是否手动添加
     */
    public RequestPanel addOneRequest(RequestPanel requestPanel, boolean isManual, boolean isRealAdd) {
        if (null == requestPanel) {
            requestPanel = new RequestPanel(this, actionCallbackMap);
        }

        if (isManual) {//如果是在界面点击添加tab的情况
            requestPanel.refreshPreId();
            addMaxTabIndex2();
            requestPanel.setTabIndex2(getMaxTabIndex2());
        }
        if (isRealAdd) {
            addOneTab(requestPanel);
        }
        if (isManual) {//如果是在界面点击添加tab的情况
            String clipText = WindowUtil.getSysClipboardText();
            if (!ValueWidget.isNullOrEmpty(clipText) && clipText.length() < 60) {
                requestPanel.getActionPathTextField_7().setText(clipText.replace("http://localhost:", SystemHWUtil.EMPTY), true);
            }
            requestPanel.getActionPathTextField_7().requestFocus();
        }
        return requestPanel;
    }

    /***
     * 获取上一次Tab的序号
     * @return
     */
    public int getLastIndex() {
        int length = indexList.size();
        if (length < 2) {
            return SystemHWUtil.NEGATIVE_ONE;
        }
        return indexList.get(length - 2);
    }

    protected RequestDefaultBean getRequestDefault() {
        RequestDefaultBean requestDefaultBean = new RequestDefaultBean();
        requestDefaultBean.setServerIp(serverIpTextField.getText());
        requestDefaultBean.setPort(portTextField_1.getText());
        requestDefaultBean.setContextPath(contextPathTF.getText());

        requestDefaultBean.setRequestParameters(getTableParameters());
        if (null != requestCharsetComboBox) {
            String requestMethodStr = (String) requestCharsetComboBox.getSelectedItem();
            if (requestMethodStr.equalsIgnoreCase(Constant2.HTTP_REQUESTMETHOD_POST)) {//POST请求
                requestDefaultBean.setRequestMethod(Constant2.REQUEST_METHOD_POST);
            } else {//GET请求
                requestDefaultBean.setRequestMethod(Constant2.REQUEST_METHOD_GET);
            }
        }
        requestDefaultBean.setDefaultCookie(commonCookie.getText().trim());
        return requestDefaultBean;
    }

    public void putRequestId(String id, RequestPanel requestPanel) {
        requestIdList.add(id);
        requestPanelList.add(requestPanel);
        this.requestPanelMap.put(id, requestPanel);
    }

    /***
     * 把请求ID 和tab 映射起来
     * @param idTF
     * @param requestPanel
     */
    public void putRequestId(AssistPopupTextField idTF, RequestPanel requestPanel) {
        String id = idTF.getText();
        putRequestId(id, requestPanel);
    }

    private void addOneTab(RequestPanel requestPanel) {
        tabbedPane_2.addTab("http请求-" + (++count), null, requestPanel, null);
        tabbedPane_2.setSelectedIndex(tabbedPane_2.getTabCount() - 1);
        if (null != requestPanel) {
            allRequestPanelList.add(requestPanel);
        }

    }

    private void addOneTabSimple(JComponent jComponent) {
        tabbedPane_2.addTab("http请求-" + (++count), null, jComponent, null);
    }

    public RequestPanel getRequestPanel4Map(String id) {
        return this.requestPanelMap.get(id);
    }

    public List<String> getRequestIdList() {
        return requestIdList;
    }

    public List<RequestPanel> getRequestPanelList() {
        return requestPanelList;
    }

    public List<RequestPanel> getAllRequestPanelList() {
        return allRequestPanelList;
    }

    /***
     * 保存到配置文件中<br />
     * 保存配置文件
     */
    @Override
    public void saveConfig() {
        List<RequestInfoBean> requestInfoBeans = getRequestInfoBeens();
        saveConfig(requestInfoBeans);
    }

    public List<RequestInfoBean> getRequestInfoBeens() {
        int size = this.allRequestPanelList.size();
        List<RequestInfoBean> requestInfoBeans = new ArrayList<RequestInfoBean>();
           /*StringBuffer sbuffer=new StringBuffer();
	       sbuffer.append("[");
	       for(int i=0;i<size;i++){
	    	   RequestPanel requestPanel=this.allRequestPanelList.get(i);
	    	   String content=requestPanel.getSaveContent();
	    	   sbuffer.append(content);
	    	   if(i<size-1){
	    		   sbuffer.append(SystemHWUtil.ENGLISH_COMMA);
	    	   }
	       }
	       sbuffer.append("]");*/
        for (int i = 0; i < size; i++) {
            RequestPanel requestPanel = this.allRequestPanelList.get(i);
            RequestInfoBean requestInfoBean = null;
            if (requestPanel.isAlreadyRended2()) {
                requestInfoBean = requestPanel.getRequestInfoBean(false);
                if (null != requestPanel.getOriginalRequestInfoBean()) {
                    requestInfoBean.setParametersHistory(requestPanel.getOriginalRequestInfoBean().getParametersHistory());
                    requestInfoBean.setSendRightNow(requestPanel.getOriginalRequestInfoBean().isSendRightNow());
                    requestInfoBean.setRequestInRangeInfo(requestPanel.getOriginalRequestInfoBean().getRequestInRangeInfo());
                    requestInfoBean.setPluginClassSimpleName(requestPanel.getOriginalRequestInfoBean().getPluginClassSimpleName());
                }
                //isSendRightNow只能从getOriginalRequestInfoBean中获取
            } else {
                requestInfoBean = requestPanel.getOriginalRequestInfoBean();
            }
            if (null != requestInfoBean) {
                requestInfoBean.setParameters(null);//冗余字段,不序列化,只是为了传递到com/common/bean/ResponseResult.java
                requestInfoBeans.add(requestInfoBean);
            }
        }
        return requestInfoBeans;
    }

    public void saveConfig(List<RequestInfoBean> requestInfoBeans) {
        WholeRequestBean wholeRequestBean = new WholeRequestBean();
        wholeRequestBean.setRequestInfoBeans(requestInfoBeans);
        wholeRequestBean.setRequestDefaultBean(getRequestDefault());
        wholeRequestBean.setUnbindMobileBean(getUnbindMobileBean());
        wholeRequestBean.setModifyResponseBodyBean(getModifyResponseBodyBean());
        wholeRequestBean.setMvnInstallConfig(getMvnInstallConfig());
        wholeRequestBean.setQrCodeInfoBean(getQrCodeInfoBean());
        wholeRequestBean.setJson2JavaBean(getJson2JavaBean());
        wholeRequestBean.setLoginParamInfo(getLoginParamInfo());
        wholeRequestBean.setAssembleDynamicUrlInfo(getAssembleDynamicUrlInfo());
        //全局的现场
        RequestPanel currentPanel = (RequestPanel) tabbedPane_2.getSelectedComponent();
        int currentIndex = tabbedPane_2.getSelectedIndex();
//	       GlobalInfo globalInfo=new GlobalInfo();
        if (null == globalInfo) {
            globalInfo = new GlobalInfo();
        }
        globalInfo.setCurrentTabbedIndex(currentIndex);
        if (null == currentPanel) {
            return;
        }
        globalInfo.setResponseText(currentPanel.getRequestInfoAndResult22());
        globalInfo.setStatusText(currentPanel.getResponseTextPaneText22());
        globalInfo.setMemorandum1(memoTextArea_1.getText());
        globalInfo.setMemorandum2(memoTextArea2.getText());
        wholeRequestBean.setGlobalInfo(globalInfo);

        try {
            /*List<String> exclusiveProperties=new ArrayList<String>();
            exclusiveProperties.add("requestBodyData");*/
            //需要把requestBodyData持久化
            boolean isSame = ReflectHWUtils.isSamePropertyValue(oldWholeRequestBean, wholeRequestBean, (List<String>) null);
            System.out.println("试图保存配置文件,same:" + isSame);
            if (isSame) {
                System.out.println("没有修改,不用保存配置文件");
                return;
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
//		   if(configFile==null){
        configFile = new File(configFilePath);
//		   }
        deleteBak();
        try {
            backUp(configFile);
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        if (!configFile.exists()) {
            try {
                SystemHWUtil.createEmptyFile(configFile);
            } catch (IOException e) {
                e.printStackTrace();
                GUIUtil23.errorDialog(e);
            }
        }
        CMDUtil.show(configFilePath);//因为隐藏文件是只读的

        //校验文件大小
        long oldSize = configFile.length();
        String newContent = HWJacksonUtils.getJsonP(wholeRequestBean);
        try {
            long nowSize = newContent.getBytes(SystemHWUtil.CHARSET_UTF).length;
            int delta = (int) ((oldSize - nowSize) / 1024 / 1024);//单位:M
            System.out.println("delta :" + delta + "M");
            if (delta > 2 && nowSize < 1024 * 1024) {
                System.out.println("因bug导致内容不正确,所以不保存");
                CMDUtil.hide(configFilePath);
                return;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        //处理
        System.out.println("保存文件:" + configFilePath);
        FileUtils.writeToFile(configFile, newContent, SystemHWUtil.CHARSET_UTF);
        oldWholeRequestBean = wholeRequestBean;
        CMDUtil.hide(configFilePath);
    }

    /***
     * 删除备份文件,例如<br>
     * .share_http.properties_bak20160302<br>
     * .share_http.properties_bak20160206
     */
    private void deleteBak() {
        String parentDir = SystemHWUtil.getParentDir(configFilePath);//C:\Users\Administrator
        String baseName = getBakBaseName();//.share_http.properties
        File[] files = FileUtils.getFilesByPathPrefix(new File(parentDir), baseName);
        Arrays.sort(files);
        int limit = 10;//备份的个数
        int size = files.length;
        if (size > limit) {
            for (int i = 0; i < size - limit; i++) {
                File file = files[i];
                System.out.println("delete bak:" + file.getAbsolutePath());
                file.delete();
            }
        }
    }

    /***
     * 备份.share_http.properties
     *
     * @param configFile
     * @throws IOException
     */
    private void backUp(File configFile) throws IOException {
        if (!configFile.exists()) {
            System.out.println("不存在:" + configFile.getAbsolutePath());
            return;
        }
        String parentDir = SystemHWUtil.getParentDir(configFilePath);//C:\Users\Administrator
        String baseName = getBakBaseName();//.share_http.properties
        String todayBakFileName = getTodayBakName(baseName);
        File newFile = new File(parentDir, todayBakFileName);
        if (newFile.exists()) {
            System.out.println("已经存在,无需备份:" + newFile.getAbsolutePath());
        } else {
            SystemHWUtil.copyFile(configFile, newFile);
            System.out.println("备份:" + newFile.getAbsolutePath());
        }
    }

    private String getBakBaseName() {
        return SystemHWUtil.getFileSimpleName(configFilePath) + "_bak";
    }

    /***
     * 读取配置文件
     * @throws IOException
     */
    private void readConfig() throws IOException {
        boolean isThread = true;//是否以多线程初始化请求
        if (isDebug) {
            String input = Debug.input;// FileUtils.getFullContent4(this.getClass().getResourceAsStream("/share_http.debug.txt"),SystemHWUtil.CHARSET_UTF);
            resume(input);
            return;
        }

        configFile = new File(configFilePath);
        if (!configFile.exists()) {
            configFile = new File(configFileName);
        }
        if (!configFile.exists()) {
            return;
        }
        testIndeterminate();//启动进度条
        final long start = System.currentTimeMillis();
        System.out.println("start\t" + start);
        resumeInput = readConfigFile2str(configFile);
        resumeAction(isThread, start);
    }

    private String readConfigFile2str(File configFile) throws IOException {
        if (!configFile.exists()) {
            return null;
        }
        InputStream inStream = new FileInputStream(configFile);
        String resumeInput = FileUtils.getFullContent4(inStream, SystemHWUtil.CHARSET_UTF);
        inStream.close();//及时关闭资源
        return resumeInput;
    }

    private void resumeAction(boolean isThread, final long start) throws JsonParseException {
        if (isThread) {
            resume(resumeInput);
            stopIndeterminate();
            return;
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    resume(resumeInput);
                } catch (JsonParseException e) {
                    e.printStackTrace();
                }
                long end = System.currentTimeMillis();
                System.out.println("end\t" + end);
                System.out.println("耗时:" + (end - start) / 1000);
                stopIndeterminate();
            }
        }).start();
    }

    private void fillRequestPanelIntegerMap(int index, RequestPanel requestPanel) {
        this.requestPanelIndexMap.put(requestPanel, index);
    }

    public int getTabIndex(RequestPanel requestPanel) {
        Integer index = (Integer) SystemHWUtil.reverseMap(this.requestPanelIndexMap).get(requestPanel);
        return index;
    }

    /***
     *
     * @param input : 配置文件的内容
     * @throws JsonParseException
     */
    private void resume(String input) throws JsonParseException {
        if (ValueWidget.isNullOrEmpty(input)) {
            return;
        }
        WholeRequestBean wholeRequestBean = null;
        wholeRequestBean = (WholeRequestBean) HWJacksonUtils.deSerialize(input, WholeRequestBean.class);
        List<RequestInfoBean> list = null;
        if (null == wholeRequestBean) {
            list = HWJacksonUtils.deSerializeList(input, RequestInfoBean.class);
            wholeRequestBean = new WholeRequestBean();
            wholeRequestBean.setRequestInfoBeans(list);
        } else {
            list = wholeRequestBean.getRequestInfoBeans(); //(List<RequestInfoBean>) HWJacksonUtils.deSerializeList(input, RequestInfoBean.class);
        }
        if (null != wholeRequestBean.getUnbindMobileBean()) {
            setUnbindMobileBean(wholeRequestBean.getUnbindMobileBean());
        }
        if (null != wholeRequestBean.getModifyResponseBodyBean()) {
            setModifyResponseBodyBean(wholeRequestBean.getModifyResponseBodyBean());
        }
        if (null != wholeRequestBean.getMvnInstallConfig()) {
            setMvnInstallConfig(wholeRequestBean.getMvnInstallConfig());
        }
        if (null != wholeRequestBean.getQrCodeInfoBean()) {
            setQrCodeInfoBean(wholeRequestBean.getQrCodeInfoBean());
        }
        if (null != wholeRequestBean.getJson2JavaBean()) {
            setJson2JavaBean(wholeRequestBean.getJson2JavaBean());
        }
        if (null != wholeRequestBean.getLoginParamInfo()) {
            setLoginParamInfo(wholeRequestBean.getLoginParamInfo());
        }
        if (null != wholeRequestBean.getAssembleDynamicUrlInfo()) {
            setAssembleDynamicUrlInfo(wholeRequestBean.getAssembleDynamicUrlInfo());
        }
        globalInfo = wholeRequestBean.getGlobalInfo();
        if (globalInfo == null) {
            return;
        }
        oldWholeRequestBean = SystemHWUtil.clone2(wholeRequestBean);
        //反序列化

        int size = list.size();
        maxTabIndex2 = size - 1;
        if (size > 0) {//增加tab
            for (int i = 0; i < size - 1; i++) {//TODO ?为什么要-1
                addOneRequest(null, true);
            }
        }

        resumePanel(wholeRequestBean, list, size);
        afterInitialize(list);
        //设置全局的配置,比如左边的备忘
        parseGlobalInfo(wholeRequestBean);
        if (null == getGlobalConfig()) {
            setGlobalConfig(new GlobalConfig());
        }
        if (getGlobalConfig().isIntelligenceMode()) {//如果是智能模式,判断如果没有请求体,则修改请求方式为GET
            for (int j = 0; j < size; j++) {
                RequestInfoBean requestInfoBean = list.get(j);
                if (ValueWidget.isNullOrEmpty(requestInfoBean.getRequestBodyData()) &&
                        ValueWidget.isNullOrEmpty(requestInfoBean.getRequestParameters())) {
                    if (requestInfoBean.getRequestMethod() == Constant2.REQUEST_METHOD_POST) {
                        requestInfoBean.setRequestMethod(Constant2.REQUEST_METHOD_GET);
                    }
                }
            }
        }
        //渲染当前的请求panel
        renderCurrentPanel();

        updateUITable2();
//		ToastMessage.toast("请求面板已全部加载", 2000);
        setInitialized(true);

    }

    /***
     * 初始化之后<br>
     *     设置"前置请求id"下拉框
     */
    private void afterInitialize(List<RequestInfoBean> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            RequestPanel requestPanel = this.allRequestPanelList.get(i);
            RequestInfoBean requestInfoBean = requestPanel.getOriginalRequestInfoBean();
            if (null == requestInfoBean) {
                continue;
            }
            String requestId = requestInfoBean.getRequestID();
            Set<String> preRequestIds = new HashSet<String>(requestIdList2);
            if (!ValueWidget.isNullOrEmpty(requestId)) {
                preRequestIds.remove(requestId);
            }
            requestInfoBean.setPreRequestIds(preRequestIds);
        }
        /*int length=requestIdList2.size();
        for(int i=0;i<length;i++) {
            String requestId22 = requestIdList2.get(i);
        }*/
    }

    private void resumePanel(WholeRequestBean wholeRequestBean, List<RequestInfoBean> list, int size) {
        //各个tab分别恢复现场
        //获取所有的请求ID
        requestIdList2 = new ArrayList<String>();
        for (int i = 0; i < size; i++) {
            RequestInfoBean requestInfoBean = list.get(i);
            if (null == requestInfoBean) {
                continue;
            }
            RequestPanel requestPanel = this.allRequestPanelList.get(i);
            String requestId = requestInfoBean.getRequestID();
            if (!ValueWidget.isNullOrEmpty(requestId)) {
                requestIdList2.add(requestId);
            }
            putRequestId(requestId, requestPanel);
            fillRequestPanelIntegerMap(i, requestPanel);
            requestPanel.setTabIndex2(i);
//            requestPanel.resume(requestInfoBean);//在RequestPanel.initialize3()执行完成之后才能调用
            //原始请求信息
            requestPanel.setOriginalRequestInfoBean(requestInfoBean);
            String title = requestInfoBean.getCurrentRequestName();
            if (ValueWidget.isNullOrEmpty(title)) {
                title = HTTP_request;
            }
            tabbedPane_2.setTitleAt(i/*requestPanel.setTabIndex2(i);*/, "(" + (i + 1) + ")" + title);
        }

        /*for (int i = 0; i < size; i++) {

            RequestPanel requestPanel = this.allRequestPanelList.get(i);
            //设置前置请求下拉框
            requestPanel.requestIDDispache();
        }
        for (int i = 0; i < size; i++) {
            RequestInfoBean requestInfoBean = list.get(i);
            if (null == requestInfoBean) {
                continue;
            }
            RequestPanel requestPanel = this.allRequestPanelList.get(i);
            String beforeId = requestInfoBean.getPreRequestId();
            if (!ValueWidget.isNullOrEmpty(beforeId)) {
                requestPanel.setPreRequestId(beforeId);
                if(!SystemHWUtil.MIDDLE_LINE.equals(beforeId)){
                    requestPanel.toExecutePreQequest();
                }
            }
        }*/

        RequestDefaultBean requestDefaultBean = wholeRequestBean.getRequestDefaultBean();
        if (null != requestDefaultBean) {
            resume(requestDefaultBean);
        }
    }

    private void parseGlobalInfo(WholeRequestBean wholeRequestBean) {
        memoTextArea_1.setText(globalInfo.getMemorandum1());
        memoTextArea2.setText(globalInfo.getMemorandum2());
        globalInfo.setWillSave(true);//不应该持久化
    }

    private void renderCurrentPanel() {
        int selectIndex = globalInfo.getCurrentTabbedIndex();
        if (selectIndex > tabbedPane_2.getTabCount() - 1) {
            selectIndex = tabbedPane_2.getTabCount() - 1;
        }
        if (selectIndex < 0) {
            selectIndex = 0;
        }
        tabbedPane_2.setSelectedIndex(selectIndex);
        indexList.add(selectIndex);
        RequestPanel requestPanel = this.allRequestPanelList.get(selectIndex);
        if (null != requestPanel) {
            rendPanel(requestPanel);
            requestPanel.setRequestInfoAndResult22(globalInfo.getResponseText());
//            requestPanel.setResponseTextPaneText22(globalInfo.getStatusText());
            adaptTableAction(requestPanel);
            scroll2bottom(requestPanel);
        }
    }

    public void updateUITable2() {
        updateUITable2(allRequestPanelList);
    }

    /***
     * 使每个panel中的表格高度自适应
     *
     * @param allRequestPanelList
     */
    public void updateUITable2(List<RequestPanel> allRequestPanelList) {
        int size = allRequestPanelList.size();
        for (int i = 0; i < size; i++) {
            RequestPanel requestPanel = this.allRequestPanelList.get(i);
            if (null != requestPanel) {
                requestPanel.adaptTableSize2(false/*isFrameRepaint*/, false/*isAdd*/, false/*isDel*/);
            }

        }
        updateUI2();
    }

    /***
     * HTTP 请求默认值
     * @param requestDefaultBean
     */
    private void resume(RequestDefaultBean requestDefaultBean) {
        serverIpTextField.setText(requestDefaultBean.getServerIp());
        portTextField_1.setText(requestDefaultBean.getPort());
        contextPathTF.setText(requestDefaultBean.getContextPath());
        //默认cookie,<br >优先级比单个请求的cookie高,所以会覆盖掉单个请求中设置的cookie
        if (!ValueWidget.isNullOrEmpty(requestDefaultBean.getDefaultCookie())) {
            commonCookie.setText(requestDefaultBean.getDefaultCookie().trim());
        }
        setTableData3(requestDefaultBean.getRequestParameters());
    }

    /***
     * 设置子tab的标题
     * @param index
     * @param title
     */
    public void setTabTitle2(int index, String title) {
        tabbedPane_2.setTitleAt(index, title);
    }

	/*@Override
    public void beforeDispose() {
		super.beforeDispose();
		System.out.println("窗口关闭");
		if (!isDebug && getGlobalInfo().isWillSave()) {
			saveConfig();
		}
	}*/

    public void addMaxTabIndex2() {
        maxTabIndex2++;
    }

    public int getMaxTabIndex2() {
        return maxTabIndex2;
    }

    /***
     * 用于各个请求添加应答的cookie
     *
     * @param responseSetCookie
     */
    public void appendResponseSetCookie(Object responseSetCookie) {
        if (!ValueWidget.isNullOrEmpty(responseSetCookie)) {
            ComponentUtil.appendResult(responseSetCookieTA, responseSetCookie, true, false);
        }
    }

    public void openLoggerFile2() {
        File logFile = new File(logFilePath);
        if (logFile.exists()) {
            FileUtils.open_file(logFile);
        } else {
            ToastMessage.toast("日志文件:" + logFilePath + "不存在", 3000, Color.red);
        }
    }

    public void copyConfigFilePath() {
        WindowUtil.setSysClipboardText(configFilePath);
    }

    /***
     * 增量加载配置文件
     */
    public void deltaLoadConfig() {
        File configFile2 = new File(configFileName);
        if (!configFile2.exists()) {
            DialogBean dialogBean = DialogUtil.browser4(JFileChooser.FILES_ONLY, this, null);
            if (dialogBean.isSuccess()) {
                configFile2 = dialogBean.getSelectedFile();
            }
        }
        if (!configFile2.exists()) {
            return;
        }
        try {
            String resumeInput = readConfigFile2str(configFile2);
            WholeRequestBean wholeRequestBean = (WholeRequestBean) HWJacksonUtils.deSerialize(resumeInput, WholeRequestBean.class);
            List<RequestInfoBean> requestInfoBeans = getRequestInfoBeens();
            requestInfoBeans.addAll(wholeRequestBean.getRequestInfoBeans());
            saveConfig(requestInfoBeans);
//            ToastMessage.toast("加载完成,请重新启动.", 2000);
            GUIUtil23.infoDialog("加载完成,请重新启动.");
            /*try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            this.dispose();
            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /***
     * 备份配置文件:/Users/whuanghkl/.share_http.properties
     */
    public void backupConfigFile() {
//        DialogUtil.showSaveDialog(null,this,null);
        java.io.File picSaveFile = DialogUtil.chooseFileDialog(null, "备份配置文件", this, "properties");

        if (null == picSaveFile) {
            ToastMessage.toast("取消操作", 2000, Color.RED);
            return;
        }
        try {
            SystemHWUtil.copyFile(configFile, picSaveFile);
        } catch (IOException e) {
            e.printStackTrace();
            GUIUtil23.errorDialog("复制失败");
        }
    }

    /***
     * 复制配置文件
     */
    public void copyConfigFileContent() {
        File configFile = new File(configFilePath);
        String content;
        try {
            content = FileUtils.getFullContent2(configFile, SystemHWUtil.CHARSET_UTF);
            if (!ValueWidget.isNullOrEmpty(content)) {
                WindowUtil.setSysClipboardText(content);
                ToastMessage.toast("已复制到剪切板", 500);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /***
     * 获取表格中的请求要素
     *
     * @return
     */
    protected Object[][] getParameter4Table() {
        TableModel model = deFaultParameterTable.getModel();
        int rowCount = model.getRowCount();//参数的个数
        System.out.println("rowCount:" + rowCount);
        if (rowCount == 0) {
            System.out.println("row is null:" + rowCount);
            return null;
        }
        int columnCount = model.getColumnCount();
        Object[][] data2 = new Object[rowCount][];
        for (int rowIndex = 0; rowIndex < rowCount; rowIndex++) {
            if (!ValueWidget.isNullOrEmpty(model.getValueAt(rowIndex, 0))) {
                Object[] objs = new Object[columnCount];
                for (int j = 0; j < columnCount; j++) {
                    Object val = model.getValueAt(rowIndex, j);
                    if (!ValueWidget.isNullOrEmpty(val)) {
                        objs[j] = val;
                    }
                }
                data2[rowIndex] = objs;
            }


        }
//    	System.out.println(data2.length);
        return data2;
    }

    protected TreeMap<String, Object> getTableParameters() {
        Object[][] data2 = getParameter4Table();
        if (ValueWidget.isNullOrEmpty(data2)) {
            return null;
        }
        if (data2.length <= 0) {
            return null;
        }
        TreeMap<String, Object> parameterMap = new TreeMap();
        for (int rowIndex = 0; rowIndex < data2.length; rowIndex++) {
            if (ValueWidget.isNullOrEmpty(data2[rowIndex])) {
                continue;
            }
            String key = (String) data2[rowIndex][0];
            //过滤掉相同的key
            if (!ValueWidget.isNullOrEmpty(key) && !parameterMap.containsKey(key)) {
                Object valueObj = data2[rowIndex][1];
                String val;
                if (valueObj == null) {
                    val = SystemHWUtil.EMPTY;
                } else {
                    val = (String) valueObj;
                }
                parameterMap.put(key, val);
            }
        }
        return parameterMap;
    }

    public String generateRequestBody() {
        Map parameterMap = getTableParameters();
        return WebServletUtil.getRequestBodyFromMap(parameterMap);
    }

    public void setTableData3(Map requestMap) {
        if (ValueWidget.isNullOrEmpty(requestMap)) {
            return;
        }
        int length = requestMap.size();
        if (length <= 0) {
            return;
        }//if
        Object[][] datas = new Object[length][];
        int count = 0;
        for (Object obj : requestMap.keySet()) {
            Object val = requestMap.get(obj);
            Object[] objs = new Object[3];
            RadioButtonPanel panel = new RadioButtonPanel();
            panel.init();
            objs[2] = panel;

//		    		objs[2]="c"+i;
            objs[0] = obj;
            if (ValueWidget.isNullOrEmpty(val) || val.equals("null")) {//配置文件中保存的是"null",而不是null
                val = SystemHWUtil.EMPTY;
            }
            objs[1] = val;
            datas[count] = objs;
            count++;
        }//for
        setTableData2(datas);
    }

    private void setTableData2(Object[][] datas) {
        DefaultTableModel model = new DefaultTableModel(datas, new String[]{"参数名", "参数值"});
        deFaultParameterTable.setModel(model);
        deFaultParameterTable.setRowHeight(30);
//        rendTable();
    }

    private void addParameterClip() {
        String text = WindowUtil.getSysClipboardText();
        addParameter(text);
    }

    /***
     * 表格增加一行
     */
    private void addParameter(String key) {
        System.out.println("增加一行");
        DefaultTableModel tableModel = (DefaultTableModel) this.deFaultParameterTable.getModel();
        Object[] rowData = new Object[]{key, null};
        tableModel.addRow(rowData);
    }

    public void deleteCurrentPanel(RequestPanel requestPanel) {
        int selectedIndex = tabbedPane_2.getSelectedIndex();
        requestPanel = getRequestPanel(selectedIndex);
        tabbedPane_2.removeTabAt(selectedIndex);
        if (requestPanel != null) {
            requestPanelList.remove(requestPanel);
            allRequestPanelList.remove(requestPanel);
        }
        ToastMessage.toast("删除成功!", 1000);
    }

    public RequestPanel getRequestPanel(int selectedIndex) {
        RequestPanel requestPanel = null;
        Component comp = tabbedPane_2.getComponentAt(selectedIndex);
        if (comp instanceof RequestPanel) {
            requestPanel = (RequestPanel) comp;
        }
        return requestPanel;
    }

    /***
     * 获取当前选中的RequestPanel
     * @return
     */
    public RequestPanel getCurrentRequestPanel() {
        Component comp = this.tabbedPane_2.getSelectedComponent();
        if (comp instanceof RequestPanel) {
            return (RequestPanel) comp;
        } else {
            return null;
        }
    }

    /***
     * @param requestID : 请求ID
     * @return : Tab index
     */
    public int getTabIndexByRequestID(String requestID) {
        int size = tabbedPane_2.getTabCount();
        if (size == 0) {
            return SystemHWUtil.NEGATIVE_ONE;
        }
        for (int i = 0; i < size; i++) {
            Component comp = this.tabbedPane_2.getComponentAt(i);//TODO 不能使用getTabComponentAt
            if (comp == null || !(comp instanceof RequestPanel)) {
                continue;
            }
            RequestPanel requestPanel = (RequestPanel) comp;
            requestPanel.setTabIndex2(i);
//				System.out.println("requestPanel.getRequestId():"+requestPanel.getRequestId());
            if (requestID.equals(requestPanel.getRequestId())) {
                return i;
            }
        }
        return SystemHWUtil.NEGATIVE_ONE;
    }

    /***
     * 当前tab的序号
     * @return
     */
    public int getCurrentSelectedIndex() {
        return this.tabbedPane_2.getSelectedIndex();
    }

    /***
     * 修改请求ID
     */
    public void modifyRequestIdAction(String newRequestId) {
        RequestPanel currentRequestPanel = getCurrentRequestPanel();
        String oldRequestId = currentRequestPanel.getRequestId();
        if (ValueWidget.isNullOrEmpty(oldRequestId)) {
            ToastMessage.toast("原请求ID 为空,不做处理", 3000, Color.red);
            return;
        }
        //老请求ID 对应的RequestPanel
        RequestPanel oldRequestPanel = this.requestPanelMap.get(oldRequestId);
        this.requestPanelMap.remove(oldRequestId);
        this.requestPanelMap.put(newRequestId, oldRequestPanel);
        int index = requestIdList.indexOf(oldRequestId);
        requestIdList.remove(index);
        requestIdList.add(index, newRequestId);

        oldRequestPanel.setRequestId(newRequestId);
        java.util.List<RequestPanel> requestPanelList = this.getAllRequestPanelList();
        for (RequestPanel requestPanelTmp : requestPanelList) {
            String preRequestId = requestPanelTmp.getPreRequestId();
            if (preRequestId.equals(newRequestId)) {
                continue;
            }
            if (preRequestId.equals(oldRequestId)) {//该请求的前置请求id是要修改的请求id
                requestPanelTmp.updatePreId(newRequestId, oldRequestId);
            } else {
                requestPanelTmp.refreshPreId();
            }
        }
        /*for(String key2:this.requestPanelMap.keySet()){
    		System.out.println("key2:"+key2);
    		if(!key2.equals(newRequestId)){
    			RequestPanel requestPanelTmp=this.requestPanelMap.get(key2);
    			String preRequestId= requestPanelTmp.getPreRequestId();
    			if(preRequestId.equals(oldRequestId)){//该请求的前置请求id是要修改的请求id
    				requestPanelTmp.updatePreId(newRequestId, oldRequestId);
    			}else{
    				requestPanelTmp.refreshPreId();
    			}
    		}
    	}*/

    }

    /***
     * 增加全局快捷键Shift+Tab<br>
     * Ctrl+Shift+X
     */
    private void addGlobalKey() {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        // 注册应用程序全局键盘事件, 所有的键盘事件都会被此事件监听器处理.
        toolkit.addAWTEventListener(
                new java.awt.event.AWTEventListener() {
                    public void eventDispatched(AWTEvent event) {
                        if (event.getClass() != KeyEvent.class) {
                            return;
                        }
                        KeyEvent kE = ((KeyEvent) event);
                        // 处理按键事件 Shift+Tab
                        if ((kE.getKeyCode() == KeyEvent.VK_TAB)//TODO 注意:Ctrl+Tab 是系统的快捷键,容易和程序混淆
                                && (((InputEvent) event)
                                .isShiftDown()) && kE.getID() == KeyEvent.KEY_PRESSED) {
                            int lastIndex = getLastIndex();
                            if (lastIndex < 0) {
                                return;
                            }
//								indexList.add(tabbedPane.getSelectedIndex());
                            if (lastIndex >= tabbedPane_2.getTabCount()) {//解决删除请求panel之后,tabbed索引越界的问题
                                --lastIndex;
                            }
                            tabbedPane_2.setSelectedIndex(lastIndex);
                        } else if ((kE.getKeyCode() == KeyEvent.VK_X) && (((InputEvent) event)//发送请求
                                .isShiftDown()) && (((InputEvent) event)
                                .isAltDown()) && kE.getID() == KeyEvent.KEY_PRESSED) {//Alt+Shift+X
                            AutoTestPanel.this.currentRequestPanel.requestAction(null, true);
                        } else if ((kE.getKeyCode() == KeyEvent.VK_M) && (!((InputEvent) event)//最大化窗口
                                .isShiftDown()) && (EventHWUtil.isControlDown(((InputEvent) event))) && kE.getID() == KeyEvent.KEY_PRESSED) {//Ctrl+M
                            DialogUtil.showMaximizeDialog(getCurrentRequestPanel().getRespTextArea_9());
                        } else if ((kE.getKeyCode() == KeyEvent.VK_H) && (!((InputEvent) event)//显示帮助,因为mac command+H 是隐藏,所以这里把command键换成ctrl
                                .isShiftDown()) && ((InputEvent) event).isControlDown() && kE.getID() == KeyEvent.KEY_PRESSED) {//Ctrl+H
                            TableUtil.showHelpDialog();
                        } else if (kE.getKeyCode() == KeyEvent.VK_K/*Ctrl +K*/
                                && (kE.isControlDown() || kE.isMetaDown())
                                && kE.getID() == KeyEvent.KEY_PRESSED) {
                            searchTextField.requestFocus();
                            searchTextField.selectAll();
                        } else if ((!((InputEvent) event)//显示配置面板,mac :command;window:ctrl
                                .isShiftDown()) && (EventHWUtil.isControlDown((InputEvent) event) || ((InputEvent) event).isControlDown()) && kE.getID() == KeyEvent.KEY_PRESSED) {//Ctrl+H
                            if ((kE.getKeyCode() == KeyEvent.VK_COMMA)) {
                                showConfigDialog();
                            } else if ((kE.getKeyCode() == KeyEvent.VK_SEMICOLON)) {//分号,显示登录信息对话框
                                LoginInfoDialog dialog = new LoginInfoDialog();
                                dialog.setModal(true);
                                dialog.launch();
                            } else if (kE.getKeyCode() == KeyEvent.VK_PERIOD) {// command+.(句号)  单独按下command_left 相当于command+`
                                RequestPanel requestPanel = AutoTestPanel.this.currentRequestPanel;
                                requestPanel.requestAction(requestPanel.getRunButton_55(), true);
                            }
                        }
                    }
                }, java.awt.AWTEvent.KEY_EVENT_MASK);
    }

    public void showConfigDialog() {
        ConfigDialog dialog = new ConfigDialog(AutoTestPanel.this);
        dialog.launchFrame("配置面板");
    }

    public JTextArea getMemoTextArea_1() {
        return memoTextArea_1;
    }

    public JTextArea getMemoTextArea2() {
        return memoTextArea2;
    }

    public void showQRCodeDialog() {
        if (null == qrCodeDialog) {//关闭"生成二维码"对话框时,下次打开时会保持上次的输入
            qrCodeDialog = new QRCodeDialog(this, getQrCodeInfoBean());
        }
        qrCodeDialog.setVisible(true);
    }

    public JTabbedPane getTabbedPane_2() {
        return tabbedPane_2;
    }

    public GlobalInfo getGlobalInfo() {
        return globalInfo;

    }

    public void setGlobalInfo(GlobalInfo globalInfo) {
        this.globalInfo = globalInfo;
    }

    public JSplitPane getSplitPane() {
        return splitPane;
    }

    public boolean isDebug() {
        return isDebug;
    }

    public boolean isInitialized() {
        return isInitialized;
    }

    public void setInitialized(boolean initialized) {
        isInitialized = initialized;
    }

    public UnbindMobileBean getUnbindMobileBean() {
        return unbindMobileBean;
    }

    public void setUnbindMobileBean(UnbindMobileBean unbindMobileBean) {
        this.unbindMobileBean = unbindMobileBean;
    }

    public ModifyResponseBodyBean getModifyResponseBodyBean() {
        return modifyResponseBodyBean;
    }

    public void setModifyResponseBodyBean(ModifyResponseBodyBean modifyResponseBodyBean) {
        this.modifyResponseBodyBean = modifyResponseBodyBean;
    }

    public QRCodeInfoBean getQrCodeInfoBean() {
        return qrCodeInfoBean;
    }

    public void setQrCodeInfoBean(QRCodeInfoBean qrCodeInfoBean) {
        this.qrCodeInfoBean = qrCodeInfoBean;
    }

    public Json2JavaBean getJson2JavaBean() {
        return json2JavaBean;
    }

    public void setJson2JavaBean(Json2JavaBean json2JavaBean) {
        this.json2JavaBean = json2JavaBean;
    }

    public GlobalConfig getGlobalConfig() {
        GlobalInfo globalInfo = getGlobalInfo();
        if (null == globalInfo) {
            return null;
        } else {
            return globalInfo.getGlobalConfig();
        }
    }

    public void setGlobalConfig(GlobalConfig globalConfig) {
        GlobalInfo globalInfo = getGlobalInfo();
        if (null == globalInfo) {
            globalInfo = new GlobalInfo();
            globalInfo.setGlobalConfig(globalConfig);
            setGlobalInfo(globalInfo);
        } else {
            globalInfo.setGlobalConfig(globalConfig);
        }
    }

    /**
     * @return the mvnInstallConfig
     */
    public MvnInstallConfig getMvnInstallConfig() {
        return mvnInstallConfig;
    }

    /**
     * @param mvnInstallConfig the mvnInstallConfig to set
     */
    public void setMvnInstallConfig(MvnInstallConfig mvnInstallConfig) {
        this.mvnInstallConfig = mvnInstallConfig;
    }

    /***
     * "fullUrl":"http://blog.yhskyc.com/convention2/upload/image/upload_share_http.properties"
     * @return
     */
    public Map uploadConfigFile2() {
        File file = configFile;
        String fileName = configFile.getName().replaceAll("^\\.", "");
        Map<String, String> parameters = new HashMap<>();
//        parameters.put(Constant2.PARAMETER_SAME_FILE_NAME, Constant2.PARAMETER_VALUE_ON);
        try {
            FileInputStream ins = new FileInputStream(file);
            String result = HttpSocketUtil.uploadFile("http://blog.yhskyc.com/convention2/config/upload",
                    ins, parameters, fileName);
            System.out.println("result :" + result);
            Map map = (Map) HWJacksonUtils.deSerialize(result, Map.class);

            String fullUrl = (String) map.get("fullUrl");
//            ComponentUtil.appendResult(resultTextArea, fullUrl, false, false);
            appendStr2LogFile(fullUrl, false);
            return map;
//            return ValueWidget.unescapeHTML((String) map.get("imgTag"));
        } catch (JsonParseException e1) {
            e1.printStackTrace();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return null;
    }

    public LoginParamInfo getLoginParamInfo() {
        return loginParamInfo;
    }

    private void setLoginParamInfo(LoginParamInfo loginParamInfo) {
        this.loginParamInfo = loginParamInfo;
    }

    public AssembleDynamicUrlInfo getAssembleDynamicUrlInfo() {
        return assembleDynamicUrlInfo;
    }

    public void setAssembleDynamicUrlInfo(AssembleDynamicUrlInfo assembleDynamicUrlInfo) {
        this.assembleDynamicUrlInfo = assembleDynamicUrlInfo;
    }

    public ConfigParam getConfigParam() {
        return configParam;
    }

    public void setConfigParam(ConfigParam configParam) {
        this.configParam = configParam;
    }
}


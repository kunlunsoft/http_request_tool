package com.yunma.listen;

import com.yunma.dialog.DiarySearchFrame;
import com.yunma.dialog.LoginDialog;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by whuanghkl on 17/5/11.
 */
public class DiaryMenuListener implements ActionListener {
    private DiarySearchFrame diarySearchFrame;

    public DiaryMenuListener(DiarySearchFrame diarySearchFrame) {
        this.diarySearchFrame = diarySearchFrame;
    }

    public DiaryMenuListener() {
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        if (command.equals("login")) {
            System.out.println(" :" + "login");
            LoginDialog dialog = new LoginDialog(this.diarySearchFrame.getCookieSet(), this.diarySearchFrame.getLoginParamInfo());
            dialog.setVisible(true);
        }
    }
}

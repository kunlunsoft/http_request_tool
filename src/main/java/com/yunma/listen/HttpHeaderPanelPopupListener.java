package com.yunma.listen;

import com.swing.menu.MenuUtil2;
import com.yunma.component.ParameterPanel;
import com.yunma.dialog.HttpHeadPanel;
import com.yunma.util.TableUtil;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by 黄威 on 15/12/2016.<br >
 */
public class HttpHeaderPanelPopupListener implements ActionListener {
    private HttpHeadPanel httpHeadPanel;
    private java.awt.event.MouseEvent mouseEvent;

    public HttpHeaderPanelPopupListener(HttpHeadPanel httpHeadPanel, java.awt.event.MouseEvent mouseEvent) {
        this.httpHeadPanel = httpHeadPanel;
        this.mouseEvent = mouseEvent;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        if (command.equals(MenuUtil2.ACTION_STR_CLEANUP)) {
            System.out.println("clean");
            TableUtil.cleanUpParameterpanel(httpHeadPanel.getParameterPanels());
        } else if (command.equals("黏贴header")) {
            httpHeadPanel.pasteHeader();
        } else if (command.equals("复制header")) {
            httpHeadPanel.copyHeader();
        } else if (command.equals("删除这行")) {
            Object object = this.mouseEvent.getSource();
            if (object instanceof ParameterPanel) {
                ParameterPanel parameterPanel = (ParameterPanel) object;
                httpHeadPanel.deleteOneRow(parameterPanel);
            }
        }
    }
}

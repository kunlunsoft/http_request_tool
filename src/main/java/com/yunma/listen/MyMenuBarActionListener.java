package com.yunma.listen;

import com.common.util.WindowUtil;
import com.string.widget.util.ValueWidget;
import com.swing.dialog.*;
import com.swing.dialog.toast.ToastMessage;
import com.swing.menu.MenuUtil2;
import com.yunma.autotest.AutoTestPanel;
import com.yunma.dialog.*;
import com.yunma.dialog.rsa.RSASwingApp;
import com.yunma.util.TableUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

/***
 * listen to menu.
 *
 * @author huangwei
 *
 */
public class MyMenuBarActionListener implements ActionListener {
    public static final String Label_code2token = "code换token";
    private AutoTestPanel swingApp = null;

    public MyMenuBarActionListener(AutoTestPanel swingApp) {
        super();
        this.swingApp = swingApp;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String command = event.getActionCommand();

        // System.out.println(command);
        if (command.equals(MenuUtil2.ACTION_CREATE_MD5)) {
            GenerateMD5Dialog generateMD5Dialog = new GenerateMD5Dialog(true/*是否是模态*/);
            generateMD5Dialog.setVisible(true);
        } else if (command.equals("打开日志文件")) {// 打开日志文件
            this.swingApp.openLoggerFile2();
        } else if (command.equals("复制配置内容")) {// 复制配置内容
            this.swingApp.copyConfigFileContent();
        } else if (command.equals("复制配置文件路径")) {// 复制配置文件路径
            this.swingApp.copyConfigFilePath();
        } else if (command.equals("备份配置文件")) {// 备份配置文件
            this.swingApp.backupConfigFile();
        } else if (command.equals("增量加载配置文件")) {// 增量加载配置文件
            this.swingApp.deltaLoadConfig();
        } else if (command.equals("修改请求ID")) {// clear pass data
            ModifyRequestIdDialog modifyRequestIdDialog = new ModifyRequestIdDialog(swingApp);
            modifyRequestIdDialog.getNewRequestIdTF().requestFocus();
            modifyRequestIdDialog.setVisible(true);
            modifyRequestIdDialog.getNewRequestIdTF().requestFocus();
//			this.swingApp.modifyRequestIdAction("1111");
        } else if (command.equals("检查DNS")) {
            CheckDnsDialog dialog = new CheckDnsDialog(false);
            dialog.setVisible(true);
            dialog.execute("i.chanjet.com", "i.static.chanjet.com", "store.chanjet.com", "cloud.static.chanjet.com", true);


        } else if (command.equals("替换文件内容")) {
            SedDialog sedDialog = new SedDialog();
            sedDialog.setVisible(true);
        } else if (command.equals("安装jar到本地仓库")) {
            MavenTookitDialog sedDialog = new MavenTookitDialog(swingApp.getMvnInstallConfig());
            sedDialog.setVisible(true);
        } else if (command.equals("exit")) {//退出,一定要确认提示
            int result = JOptionPane.showConfirmDialog(null, "Are you sure to Exit ?", "确认",
                    JOptionPane.OK_CANCEL_OPTION);
            if (result == JOptionPane.OK_OPTION) {
                swingApp.dispose();
                System.exit(0);
            }
        } else if (command.equals(Label_code2token)) {
            String clipboardText = WindowUtil.getSysClipboardText();
            if (!ValueWidget.isNullOrEmpty(clipboardText) && clipboardText.length() > 32) {
                clipboardText = null;
            }
//            CodeExchangeTokenDialog codeExchangeTokenDialog = new CodeExchangeTokenDialog(clipboardText);
//            codeExchangeTokenDialog.setVisible(true);
        } else if (command.equals("生成二维码")) {
            swingApp.showQRCodeDialog();
        } else if (command.equals("tomcat配置文件")) {
            TomcatConfDialog dialog = new TomcatConfDialog();
            dialog.setVisible(true);
        } else if (command.equals("help")) {
            TableUtil.showHelpDialog();
        } else if (command.equals("订单信息")) {
            AppStoreLink2 dialog = new AppStoreLink2(swingApp);
            dialog.setVisible(true);
        } else if (command.equals("批量删除请求")) {
            // TODO
        } else if (command.equals("保存")) {
            SaveSelectDialog dialog = new SaveSelectDialog(swingApp);
            dialog.launchFrame("请选择要保存的请求");
        } else if (command.equals("添加参数模板")) {
            AddOptionalParametersDialog addOptionalParametersDialog = new AddOptionalParametersDialog(swingApp.getTabbedPane_2());
//            dialog.launchFrame("请选择要保存的panel");
            addOptionalParametersDialog.setVisible(true);//java.awt.IllegalComponentStateException: component must be showing on the screen to determine its location
        } else if (command.equals("上传文件")) {
            UploadPicDialog dialog = new UploadPicDialog(swingApp);
            dialog.launchFrame("上传文件");
        } else if (command.equals("配置面板")) {
            swingApp.showConfigDialog();
        } else if (command.equals("生成发送请求代码")) {
            swingApp.getCurrentRequestPanel().autoGenerateSendHttpCode(false);
        } else if (command.equals("修改代理应答体")) {
            ProxyModifyResponseDialog dialog = new ProxyModifyResponseDialog(swingApp.getModifyResponseBodyBean());
//            dialog.setModifyResnseBodyBean(swingApp.getModifyResponseBodyBean());
            dialog.launch();
        } else if (command.equals("上传配置文件")) {
            Map map = swingApp.uploadConfigFile2();
            if (null == map) {
                ToastMessage.toast("上传失败", 3000, Color.red);
            } else {
                String fullUrl = (String) map.get("fullUrl");
                ToastMessage.toast("上传成功:" + fullUrl, 4000);
                swingApp.appendStr2LogFile("上传配置文件:" + fullUrl);
            }
        } else if (command.equals("记录日志")) {
            new DiarySearchFrame(swingApp.getLoginParamInfo()).setVisible(true);
        } else if (command.equals("遍历取值范围")) {
            RequestInRangeDialog addOptionalParametersDialog = new RequestInRangeDialog(swingApp.getTabbedPane_2());
            addOptionalParametersDialog.setVisible(true);//java.awt.IllegalComponentStateException: component must be showing on the screen to determine its location
        } else if (command.equals("login info")) {
            LoginInfoDialog dialog = new LoginInfoDialog();
            dialog.setModal(true);
            dialog.launch();
        } else if (command.equals("安装插件")) {
            AddPluginClassDialog addPluginClassDialog = new AddPluginClassDialog(swingApp.getCurrentRequestPanel());
            addPluginClassDialog.setVisible(true);
        } else if (command.equals("RSA加解密")) {
            RSASwingApp rsaSwingApp = new RSASwingApp();
            rsaSwingApp.setParentComponent(new GenericDialog() {
                @Override
                public void layout3(Container contentPane) {
                    this.setContentPane(rsaSwingApp.getRootPanel());
                    rsaSwingApp.setMenuBar(this);
                    this.setModal(true);
                    this.setTitle("RSA 加解密工具");
                    DialogUtil.escape2CloseDialog(this);
                    this.setLoc(1000, 700);
                }
            }.launchFrame());
        }
    }

}

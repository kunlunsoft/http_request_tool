package com.yunma.listen;

import com.string.widget.util.ValueWidget;
import com.swing.component.ComponentUtil;
import com.swing.dialog.DialogUtil;
import com.yunma.autotest.AutoTestPanel;

import javax.swing.text.JTextComponent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PopupMenuListener implements ActionListener {
    private AutoTestPanel autoTestPanel;
    private JTextComponent area2;

    public PopupMenuListener(AutoTestPanel autoTestPanel, JTextComponent area2) {
        super();
        this.autoTestPanel = autoTestPanel;
        this.area2 = area2;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        if (command.equals("增加备忘")) {
            String selectContent = area2.getSelectedText();
            if (!ValueWidget.isNullOrEmpty(selectContent) && (null != autoTestPanel)) {
                ComponentUtil.appendResult(autoTestPanel.getMemoTextArea_1(), selectContent, true, true, false);
            }
        } else if (command.equals("最大化")) {
            DialogUtil.showMaximizeDialog(area2);
        } else if (command.equals(MyMenuBarActionListener.Label_code2token)) {
            String clipboardText = area2.getSelectedText();
//			CodeExchangeTokenDialog codeExchangeTokenDialog = new CodeExchangeTokenDialog(clipboardText);
//			codeExchangeTokenDialog.setVisible(true);
        } else if (command.equals("分析订单")) {
            System.out.println("分析订单");
        }
    }


    public AutoTestPanel getAutoTestPanel() {
        return autoTestPanel;
    }

    public void setAutoTestPanel(AutoTestPanel autoTestPanel) {
        this.autoTestPanel = autoTestPanel;
    }

}

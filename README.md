# http_request_tool
这是一个发送http 请求的java swing工具,
用于测试restful 接口

## 界面
![这里写链接内容](2017-06-07_08-21-55.jpg)
各面板介绍
1. 面板(1):
请求参数,包括请求的URL,请求方法(get or post),请求参数(form表单参数)
2. 面板(2):
请求结果
3. 面板(3):http请求默认值(共享内容).比如这里设置了端口号之后,
如果具体接口中没有设置端口,则使用面板(3) 中设置的端口.

# 功能介绍
## 请求的结果如果是json,则进行格式化

## 变更
1. 强制要求填写请求名称,便于搜索
2. 程序启动的时候不记录"是否应用HTTP默认值",
因为记录一次之后, http 默认请求参数已经添加到当前请求中了

## 异常
### IllegalBlockSizeException: Data must not be longer than 128 bytes
分段解密
![这里写链接内容](2017-12-01_10-13-59.jpg)

## 约定
请求参数输入框中的内容必须是URL编码之前的,
不能是URL编码之后的,因为工具会自动进行URL编码

## 测试
1. mac Dock 是否有图标;
2. "RSA加解密"对话框是否能打开
![这里写链接内容](2018-02-02_16-19-20.jpg)


![这里写链接内容](2018-02-02_16-21-01.jpg)